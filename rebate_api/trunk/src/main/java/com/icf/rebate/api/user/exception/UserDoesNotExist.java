package com.icf.rebate.api.user.exception;

import com.icf.rebate.api.util.UtilManager;
/**
 * User Does Not exists exception represents an use case where the
 * User, the user wants But its not exist in our catalog.
 * 
 */
public class UserDoesNotExist extends GenericUserException
{
   private String errorCode = "ESUB1002";
   private String errorMessage = UtilManager.getErrorMessage(errorCode);

   /**
    * Constructs an instance of <code>InvalidConfiguration</code>.
    */
   public UserDoesNotExist ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>SubscriberDoesNotExist</code> with the
    * specified detail message.
    */
   public UserDoesNotExist ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String mportalIdentifier = "05131966";

   /**
    * Constructs an instance of <code>SubscriberDoesNotExist</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public UserDoesNotExist ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>SubscriberDoesNotExist</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public UserDoesNotExist ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }

   public void setErrorMessage( String errorMessage )
   {
      this.errorMessage = errorMessage;
   }
}