package com.icf.rebate.api.logger;

import java.util.HashMap;
import java.util.Iterator;

import com.icf.rebate.api.event.EventManager;
import com.icf.rebate.api.reload.IRefreshListener;
/**
 * MPLoggerManager is the IMPLogger implementation class.
 * 
 */
public class MPLoggerManager implements IRefreshListener
{
   HashMap<String, IMPLogger> loggerMap = new HashMap<String, IMPLogger>();
   private EventManager eventManager;
   private LogConfigLocator logConfigLocator;
   private static Object _lock = new Object();

   // private static MPLoggerManager _instance = null;
   public MPLoggerManager ()
   {
   }

   /**
    * Initialise whatever required before calling any method from this class
    */
   public void initialize()
   {
      /**
       * The below code is used for adding current class instance in Event
       * manager list, for refreshing properties and xml files.
       */
      eventManager.addRefreshListner(this);
   }

   /**
    * This method is used for reloading logger property files.
    */
   public void refreshNotify()
   {
      try
      {
         Iterator<String> loggerItr = loggerMap.keySet().iterator();
         while (loggerItr.hasNext())
         {
            loggerMap.get(loggerItr.next()).reloadLogger();
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Returns an instance of MPLoggerManager
    * 
    * @param loggerName
    * @return
    * @throws Exception
    */
   /*
    * public static IMPLogger getLoggerInstance( String loggerName ) { if
    * (_instance == null) { synchronized (_lock) { _instance = new
    * MPLoggerManager(); } } return _instance.getLogger(loggerName); }
    */
   /**
    * Returns the MPLogger instance which has be used for logging.
    * 
    * @param loggerName - Name of the logger instance that is to be returned.
    * @return IMPLogger - Logger instance which can be used for logging.
    */
   public IMPLogger getLogger( String loggerName )
   {
      if (! loggerMap.containsKey(loggerName))
      {
         synchronized (_lock)
         {
            IMPLogger mpLogger = new MPLoggerImpl(loggerName, logConfigLocator);
            loggerMap.put(loggerName, mpLogger);
         }
      }
      return loggerMap.get(loggerName);
   }

   /**
    * @param eventManager the eventManager to set
    */
   public void setEventManager( EventManager eventManager )
   {
      this.eventManager = eventManager;
   }

   public LogConfigLocator getLogConfigLocator()
   {
      return logConfigLocator;
   }

   public void setLogConfigLocator( LogConfigLocator logConfigLocator )
   {
      this.logConfigLocator = logConfigLocator;
   }
}
