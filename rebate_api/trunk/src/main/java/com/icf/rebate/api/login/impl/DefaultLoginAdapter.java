package com.icf.rebate.api.login.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.login.exception.InActiveUserException;
import com.icf.rebate.api.login.exception.UserAuthenticationException;
import com.icf.rebate.api.login.exception.UserBlockedException;
import com.icf.rebate.api.login.exception.UserNameNotFoundException;
import com.icf.rebate.api.login.exception.UserNotAllowedException;
import com.icf.rebate.api.login.intrface.ILoginAdapter;
import com.icf.rebate.api.login.intrface.ILoginDAO;
import com.icf.rebate.api.user.exception.PwdResetNotAllowedException;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.user.intrface.IUserDao;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.CharacterSets;
import com.icf.rebate.api.util.PasswordGenerator;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.api.util.UtilManager;
import com.icf.rebate.api.util.PasswordGenerator.PasswordCharacterSet;
/**
 * This class DefaultLoginAdapter will handle all login operaion.This class
 * implements {@link ILoginAdapter}
 * 
 */
public class DefaultLoginAdapter implements ILoginAdapter
{
   private final String THIS_CLASS = "DefaultLoginAdapter";
   private IMPLogger logger;
   private ConfigurationManager configManager;
   /**
    * represents the User DAO.
    */
   private IUserDao userDao = null;
   /**
    * represents the Login DAO.
    */
   private ILoginDAO loginDao = null;
   private EmailUser emailUser;
   private String INVALID_USERNAME_OR_PWD = "The username or password you entered is incorrect.";

   /**
    * Authenticates a user with the given user name and password.
    * 
    * @param userName - User to be authenticated with this user name.
    * @param password - User to be authenticated with this password.
    * @return user - User Profile for the given user.
    * @throws UserNameNotFoundException - If user doesn not exist for the given
    *            user name.
    * @throws UserAuthenticationException - If user enters Invalid password.
    * @throws InternalServerException - If any runtime exception occurs.
    * @throws UserBlockedException - throws if user is blocked.
    * 
    */
   public User authenticateAndGetUser( String userName, String password ) throws UserNameNotFoundException,
         UserAuthenticationException, InternalServerException, UserBlockedException, InActiveUserException,
         UserDoesNotExist
   {
      final String THIS_METHOD = "authenticateAndGetUser";
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Authenticating userName ");
      userName = userName.trim();
      password = password.trim();
      User user = userDao.getUserDetails(userName);
      if (user == null || user.getPassword() == null)
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User does not exist ");
         throw new UserNameNotFoundException(INVALID_USERNAME_OR_PWD);
      }
      if (user != null && ( user.getUserStatus() == 9002 ))
      {
         throw new InActiveUserException("Your account is inactive.Please contact administrator.");
      }
      int attempt = userDao.getUnsuccesssAttempt(user.getUserId());
      if (isUserBlocked(attempt))
      {
         changeUserStatus(user.getUserId());
         throw new UserBlockedException(" User is blocked ");
      }
      // String encryptedPassword = UtilManager.encrypt(password);
      if (! password.equals(user.getPassword()))
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "user name:" + userName);
         // insertOrUpdateUnsuccessAttempt(user.getUserId());
         attempt = attempt + 1;
         if (isUserBlocked(attempt))
         {
            changeUserStatus(user.getUserId());
            throw new UserBlockedException(" User is blocked ");
         }
         throw new UserAuthenticationException(INVALID_USERNAME_OR_PWD);
      }
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
            "Authentication Successfull, Geting Subscriber profile from Subscriber Manager.");
      User userProfile;
      try
      {
         userProfile = userDao.getUserById(user.getUserId());
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User RoleId:" + userProfile.getUserRole());
      }
      catch (Exception e)
      {
         throw new InternalServerException(INVALID_USERNAME_OR_PWD, e);
      }
      UserPasswordTrackingBean userBean = isUserLoggedFirstTime(userProfile.getUserId());
      if (! userBean.isUserLoggedFirstTime())
      {
         updateLoggedInUserDetail(userBean, false, false);
      }
      else
      {
         userBean.setUserId(userProfile.getUserId());
         userBean.setNewPassword(userProfile.getPassword());
         addLoggedInUserDetail(userBean);
      }
      user.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
      user.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
            "MSG_LOGIN_SUCCESS"));
      return user;
   }

   /**
    * Authenticates a user with the given user name and password if request is
    * coming from console.
    * 
    * @param userName - User to be authenticated with this user name.
    * @param password - User to be authenticated with this password.
    * @return user - User Profile for the given user.
    * @throws UserNameNotFoundException - If user doesn not exist for the given
    *            user name.
    * @throws UserAuthenticationException - If user enters Invalid password.
    * @throws InternalServerException - If any runtime exception occurs.
    * @throws UserBlockedException - throws if user is blocked.
    * 
    */
   public User authenticateAndGetUser( String userName, String password, boolean forWebConsole )
         throws UserNameNotFoundException, UserAuthenticationException, InternalServerException, UserBlockedException,
         InActiveUserException, UserNotAllowedException, UserDoesNotExist
   {
      final String THIS_METHOD = "authenticateAndGetUser";
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Authenticating userName ");
      userName = userName.trim();
      password = password.trim();
      User user = userDao.getUserDetails(userName);
      if (user == null || user.getPassword() == null)
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User does not exist ");
         throw new UserNameNotFoundException(INVALID_USERNAME_OR_PWD);
      }
      if (user != null && ( user.getUserStatus() == 9002 ))
      {
         throw new InActiveUserException("Your account is inactive.Please contact administrator.");
      }
      int attempt = userDao.getUnsuccesssAttempt(user.getUserId());
      if (isUserBlocked(attempt))
      {
         changeUserStatus(user.getUserId());
         throw new UserBlockedException(" User is blocked ");
      }
      String encryptedPassword = UtilManager.encrypt(password);
      if (! encryptedPassword.equals(user.getPassword()))
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "user name:" + userName);
         insertOrUpdateUnsuccessAttempt(user.getUserId());
         attempt = attempt + 1;
         if (isUserBlocked(attempt))
         {
            changeUserStatus(user.getUserId());
            throw new UserBlockedException(" User is blocked ");
         }
         throw new UserAuthenticationException(INVALID_USERNAME_OR_PWD);
      }
      if (user.getUserRole() != 1 && user.getUserRole() != 3)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Only Admin and Managers are allowed to login through Web Console, user role= " + user.getUserRole());
         throw new UserNotAllowedException("User not allowed to login. ");
      }
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
            "Authentication Successfull, Geting Subscriber profile from Subscriber Manager.");
      User userProfile;
      try
      {
         userProfile = userDao.getUserById(user.getUserId());
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User RoleId:" + userProfile.getUserRole());
      }
      catch (Exception e)
      {
         throw new InternalServerException(INVALID_USERNAME_OR_PWD, e);
      }
      UserPasswordTrackingBean userBean = isUserLoggedFirstTime(userProfile.getUserId());
      if (! userBean.isUserLoggedFirstTime())
      {
         updateLoggedInUserDetail(userBean, false);
      }
      else
      {
         userBean.setUserId(userProfile.getUserId());
         userBean.setNewPassword(userProfile.getPassword());
         addLoggedInUserDetail(userBean);
      }
      return user;
   }

   /**
    * Check whether user is blocked or not
    * 
    * @param attempt - int - no of unsuccessful attempt made by user
    * @return boolean - true if user blocked ,otherwise false
    */
   public boolean isUserBlocked( int attempt )
   {
      final String THIS_METHOD = "isUserBlocked";
      boolean isUserBlocked = false;
      int attemptAllowed = 0;
      // change this hardcoding
      attemptAllowed = 5;
      // check whether this rule apply or not
      if (attemptAllowed != 0)
      {
         if (attempt >= attemptAllowed)
         {
            isUserBlocked = true;
            logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User is blocked.");
         }
      }
      return isUserBlocked;
   }

   /**
    * insert or update the unsuccessful attempt for given user
    * 
    * @param userId - unique id of user
    */
   public void insertOrUpdateUnsuccessAttempt( long userId )
   {
      int attempt = 0;
      if (userId != 0)
      {
         attempt = userDao.getUnsuccesssAttempt(userId);
         if (attempt == 0)
         {
            attempt = userDao.insertUnsuccessAttempt(userId);
         }
         else
         {
            attempt = attempt + 1;
            userDao.updateUnsuccesssAttempt(userId, attempt);
         }
      }
   }

   /**
    * Make User InActive for given user Id
    * 
    * @param userId - for this change status to inactive
    */
   private void changeUserStatus( long userId )
   {
      userDao.UpdateUserStatusToInActive(userId);
   }

   public IUserDao getUserDao()
   {
      return userDao;
   }

   public void setUserDao( IUserDao userDao )
   {
      this.userDao = userDao;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   /**
    * Send new password to registered email id
    * 
    * @param email - validate email id
    * @return User - containing user details
    * @throws PwdResetNotAllowedException
    * @throws UserNameNotFoundException
    * @throws InActiveUserException
    * @throws UserBlockedException
    * @throw InternalServerException
    */
   public User forgetPassword( String userName ) throws PwdResetNotAllowedException, UserNameNotFoundException, InActiveUserException, UserBlockedException, InternalServerException
   {
	   final String THIS_METHOD = "forgotPassword";
	   //User profile = new User();
       User profile = null;
	   try {
	   profile = userDao.getUserDetails(userName);
       //       int minLength = configManager.getPropertyAsInt(WEB_PASSWORD,
       //       SPORTApiConstant.PASSWORD_MIN_LEN, 0);
       //       String newPassword = UtilManager.generateRandomString(minLength);
    
	   
       // Error cases  
       if (profile == null || profile.getPassword() == null)
       {
          logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User does not exist ");
          throw new UserNameNotFoundException(INVALID_USERNAME_OR_PWD);
       }
       if (profile != null && profile.getUserRole() == 1) {
    	   throw new PwdResetNotAllowedException("Accounts of this type are not allowed to reset password.");
       }
       if (profile != null && ( profile.getUserStatus() == 9002 ))
       {
          throw new InActiveUserException("Your account is inactive.Please contact administrator.");
       }
       int attempt = userDao.getUnsuccesssAttempt(profile.getUserId());
       if (isUserBlocked(attempt))
       {
          changeUserStatus(profile.getUserId());
          throw new UserBlockedException(" User is blocked ");
       }
       
       Set<PasswordCharacterSet> charSet = new HashSet<PasswordCharacterSet>(EnumSet.allOf(CharacterSets.SummerCharacterSets.class));
	   final String generatedPassword = new String(new PasswordGenerator(charSet, 8, 10).generatePassword());
       UserPasswordTrackingBean pwdTrackingBean = loginDao.getLoggedInUserDetail(profile.getUserId());
       pwdTrackingBean.setNewPassword(generatedPassword);
      
       if (pwdTrackingBean.getUserId() <= 0)
       {
    	   pwdTrackingBean.setUserId(profile.getUserId());
       }
      
       resetPassword(pwdTrackingBean, false);
      
       if (new AbstractValidator().validateEmail(profile.getUserName())) {
    	   emailUser.sendEmailForForgotPwd(profile.getUserName(), generatedPassword, profile.getUserName());
       }
       else {
    	   emailUser.sendUsernameNotValidEmail(profile.getUserName());
       }
	   } catch (Exception e) {
		   e.printStackTrace();
		   throw e; // These will be handled by the caller
	   }
       return profile;
   }

   /**
    * send password to register email id by validating UserName
    * 
    * @param userName - who forget the password
    * @return User - User details
    * @throws UserDoesNotExist - if user does not exist in db
    * @throws InvalidDataException - if input data is invalid
    * @throws InternalServerException - If any runtime exception occurs.
    */
   public User forgetPasswordWithUserName( String userName ) throws UserDoesNotExist, InvalidDataException,
         InternalServerException
   {
      User user = userDao.getUserDetails(userName);
      if (user == null || user.getUserId() <= 0)
      {
         throw new UserDoesNotExist("Username dosen't exist.");
      }
      int minLength = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD,
            RebateApiConstant.PASSWORD_MIN_LEN, 0);
      String newPassword = UtilManager.generateRandomString(minLength);
      UserPasswordTrackingBean pwdTrackingBean = loginDao.getLoggedInUserDetail(user.getUserId());
      pwdTrackingBean.setNewPassword(newPassword);
      if (pwdTrackingBean.getUserId() <= 0)
      {
         pwdTrackingBean.setUserId(user.getUserId());
      }
      resetPassword(pwdTrackingBean);
      if (new AbstractValidator().validateEmail(user.getUserName())) {
    	  emailUser.sendEmailForForgotPwd(user.getUserName(), newPassword, user.getUserName());
      }
      else {
    	  emailUser.sendUsernameNotValidEmail(user.getUserName());
      }
    	  return user;
   }

   /**
    * Check is user logged in first time for given user id
    * 
    * @param userId - check is user logged in first time for this userid
    * @return UserPasswordTrackingBean - User details
    */
   public UserPasswordTrackingBean isUserLoggedFirstTime( long userId )
   {
      return loginDao.getLoggedInUserDetail(userId);
   }

   /**
    * Update logged in used details using UserPasswordTrackingBean
    * 
    * @param UserPasswordTrackingBean
    * @param isPwdUpdate
    */
   public void updateLoggedInUserDetail( UserPasswordTrackingBean pwdTrackingBean, boolean isPwdUpdate )
         throws InternalServerException
   {
      final String THIS_METHOD = "updateLoggedInUserDetail";
      UserPasswordTrackingBean userBean = loginDao.getLoggedInUserDetail(pwdTrackingBean.getUserId());
      if (userBean.getUserId() > 0) // user details already available
      {
         pwdTrackingBean.setPreviousLoginTime(userBean.getCurrentLoginTime());
         String currentDate = UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(),
               RebateApiConstant.DATE_FORMAT);
         pwdTrackingBean.setCurrentLoginTime(currentDate);
         if (isPwdUpdate)
         {
            pwdTrackingBean.setPasswordUpdatedOn(currentDate);
            pwdTrackingBean.setNewPassword(UtilManager.encrypt(pwdTrackingBean.getNewPassword()));
            pwdTrackingBean.setResetFlag(ILoginDAO.RESET_FLAG_NO);
         }
         loginDao.updateLoggedInUserDetail(pwdTrackingBean, isPwdUpdate);
         // TO make unsuccessful counter 0
         loginDao.deleteUnsuccesssAttempt(pwdTrackingBean.getUserId());
         if (isPwdUpdate)
         {
            loginDao.insertUserPasswordHistory(pwdTrackingBean);
            loginDao.updatePasswordInUserProfile(pwdTrackingBean.getUserId(), pwdTrackingBean.getNewPassword());
         }
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
               "Updating User's detail in to Database finished successfully.");
      }
      else
      {
         // insert the user details first time
         insertUserPwdTrackDetails(pwdTrackingBean);
      }
   }

   /**
    * Insert logged in user details using UserPasswordTrackingBean object
    * 
    * @param pwdTrackingBean
    * @throws InternalServerException
    */
   public void addLoggedInUserDetail( UserPasswordTrackingBean pwdTrackingBean ) throws InternalServerException
   {
      // insert the user details first time
      insertUserPwdTrackDetails(pwdTrackingBean);
   }

   /**
    * Insert User Password details using UserPasswordTrackingBean object
    * 
    * @param pwdTrackingBean
    * @throws InternalServerException
    */
   public void insertUserPwdTrackDetails( UserPasswordTrackingBean pwdTrackingBean ) throws InternalServerException
   {
      // set the encrypted password
      // pwdTrackingBean.setNewPassword(UtilManager.encrypt(pwdTrackingBean.getNewPassword()));
      // pwdTrackingBean.setAnswer(UtilManager.encrypt(pwdTrackingBean.getAnswer()));
      String currentDate = UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(),
            RebateApiConstant.DATE_FORMAT);
      pwdTrackingBean.setCurrentLoginTime(currentDate);
      pwdTrackingBean.setPreviousLoginTime(currentDate);
      pwdTrackingBean.setPasswordUpdatedOn(currentDate);
      pwdTrackingBean.setResetFlag(ILoginDAO.RESET_FLAG_NO);
      loginDao.insertUserPwdTrackDetail(pwdTrackingBean);
      loginDao.insertUserPasswordHistory(pwdTrackingBean);
      loginDao.updatePasswordInUserProfile(pwdTrackingBean.getUserId(), pwdTrackingBean.getNewPassword());
   }

   public ConfigurationManager getConfigManager()
   {
      return configManager;
   }

   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }

   /**
    * Reset oldpassword to new password
    * 
    * @param pwdTrackingBean - contains User details
    * @throws InternalServerException - throws if any server erro occurs
    */
   private void resetPassword( UserPasswordTrackingBean pwdTrackingBean ) throws InternalServerException
   {
      String THIS_METHOD = "resetPassword";
      pwdTrackingBean.setPreviousLoginTime(pwdTrackingBean.getCurrentLoginTime());
      String currentDate = UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(),
            RebateApiConstant.DATE_FORMAT);
      pwdTrackingBean.setCurrentLoginTime(currentDate);
      pwdTrackingBean.setPasswordUpdatedOn(currentDate);
      pwdTrackingBean.setNewPassword(UtilManager.encrypt(pwdTrackingBean.getNewPassword()));
      pwdTrackingBean.setResetFlag(ILoginDAO.RESET_FLAG_YES);
      loginDao.updateLoggedInUserDetail(pwdTrackingBean, true);
      // TO make unsuccessful counter 0
      loginDao.deleteUnsuccesssAttempt(pwdTrackingBean.getUserId());
      loginDao.insertUserPasswordHistory(pwdTrackingBean);
      loginDao.updatePasswordInUserProfile(pwdTrackingBean.getUserId(), pwdTrackingBean.getNewPassword());
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
            "Updating User's detail in to Database finished successfully.");
   }

   /**
    * Reset oldpassword to new password
    * 
    * @param pwdTrackingBean - contains User details
    * @throws InternalServerException - throws if any server error occurs
    */
   private void resetPassword( UserPasswordTrackingBean pwdTrackingBean, boolean resetFlag ) throws InternalServerException
   {
      String THIS_METHOD = "resetPassword";
      pwdTrackingBean.setPreviousLoginTime(pwdTrackingBean.getCurrentLoginTime());
      String currentDate = UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(),
            RebateApiConstant.DATE_FORMAT);
      pwdTrackingBean.setCurrentLoginTime(currentDate);
      pwdTrackingBean.setPasswordUpdatedOn(currentDate);
      pwdTrackingBean.setNewPassword(UtilManager.encrypt(pwdTrackingBean.getNewPassword()));
      pwdTrackingBean.setResetFlag(resetFlag ? ILoginDAO.RESET_FLAG_YES : ILoginDAO.RESET_FLAG_NO);
      loginDao.updateLoggedInUserDetail(pwdTrackingBean, true);
      // TO make unsuccessful counter 0
      loginDao.deleteUnsuccesssAttempt(pwdTrackingBean.getUserId());
      loginDao.insertUserPasswordHistory(pwdTrackingBean);
      loginDao.updatePasswordInUserProfile(pwdTrackingBean.getUserId(), pwdTrackingBean.getNewPassword());
      loginDao.resetPasswordFlag(pwdTrackingBean.getUserId(), resetFlag);
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
            "Updating User's detail in to Database finished successfully.");
   }
   
   public ILoginDAO getLoginDao()
   {
      return loginDao;
   }

   public void setLoginDao( ILoginDAO loginDao )
   {
      this.loginDao = loginDao;
   }

   public EmailUser getEmailUser()
   {
      return emailUser;
   }

   public void setEmailUser( EmailUser emailUser )
   {
      this.emailUser = emailUser;
   }

   public void resetPwdInternally( User user ) throws InternalServerException
   {
      UserPasswordTrackingBean pwdTrackingBean = loginDao.getLoggedInUserDetail(user.getUserId());
      pwdTrackingBean.setNewPassword(user.getPassword());
      if (pwdTrackingBean.getUserId() <= 0)
      {
         pwdTrackingBean.setUserId(user.getUserId());
      }
      resetPassword(pwdTrackingBean);
   }

   public void deleteUserLoginData( long userId )
   {
      loginDao.deleteUnsuccesssAttempt(userId);
      loginDao.deleteUserPwdHistory(userId);
      loginDao.deleteUserPwdTrack(userId);
   }

   /**
    * Check that password is correct or not
    * 
    * @param userBean - UserPasswordTrackingBean bean contain password info
    * @return PasswordResponse bean proper flag and error massage
    * @throws InternalServerException - Throws if any exception occurred
    */
   public PasswordResponse isPasswordCorrect( UserPasswordTrackingBean userBean ) throws InternalServerException
   {
      final String THIS_METHOD = "isPasswordCorrect";
      PasswordResponse response = new PasswordResponse();
      // check the old Password correct or not
      /*
       * if (!(isOldPasswordCorrect(userBean))) { String message =
       * configManager.getPropertyAsString(WEB_PASSWORD, "MSG_OLD_PWD_WRONG");
       * 
       * response.addError(message);
       * 
       * logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
       * "Old Password is not correct with DB.");
       * 
       * return response; }
       */
      // check the new Password and Confirm password are same
      if (! ( userBean.getNewPassword().compareTo(userBean.getConfirmNewPassword()) == 0 ))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_NEW_PWD_AND_CONFIRM_NEW_PWD_EQUAL");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "New password and confirm new password are not equal.");
         return response;
      }
      if (userBean.getNewPassword().contains(" "))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PWD_SHOULD_NOT_CONTAIN_WHITE_SPACE");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password contains space(s).");
         return response;
      }
      int minLength = 0;
      int maxLength = 0;
      minLength = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD, "PASSWORD_MIN_LEN", 0);
      maxLength = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD, "PASSWORD_MAX_LEN", 0);
      // Check the password have min length
      if (minLength != 0)
      {
         if (userBean.getNewPassword().length() < minLength)
         {
            List<String> args = new ArrayList<String>();
            args.add(String.valueOf(minLength));
            String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD, "MSG_PASSWORD_MIN_LEN");
            response.addError(getErrorMessage(message, args));
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password has length less than " + minLength);
            return response;
         }
      }
      // check the password have max length
      if (maxLength != 0)
      {
         if (userBean.getNewPassword().length() > maxLength)
         {
            List<String> args = new ArrayList<String>();
            args.add(String.valueOf(maxLength));
            String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD, "MSG_PASSWORD_MAX_LEN");
            response.addError(getErrorMessage(message, args));
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password has length greater than " + maxLength);
            return response;
         }
      }
      // Check degree of diffrence
      /*
       * int minCharDiff = 0;
       * 
       * minCharDiff = configManager.getPropertyAsInt(WEB_PASSWORD,
       * "PASSWORD_MIN_DIFF_FROM_PREVIOUS_PASSWORD", 0);
       * 
       * if (minCharDiff > 0) { int diff =
       * getLevenshteinDistance(userBean.getOldPassword(), userBean
       * .getNewPassword());
       * 
       * if (diff < minCharDiff) { List<String> args = new ArrayList<String>();
       * args.add(String.valueOf(minCharDiff));
       * 
       * String message = configManager.getPropertyAsString(WEB_PASSWORD,
       * "MSG_DEGREE_DIFF_FOR_NEW_PASSWORD");
       * 
       * response.addError(getErrorMessage(message, args));
       * 
       * return response; } }
       */
      // check the password disallowed UserName
      if (isPasswordDisAllowedUserName())
      {
         if (( isPasswordContainUsername(userBean.getUserName(), userBean.getNewPassword()) ))
         {
            String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
                  "MSG_PASSWORD_NOT_CONTAIN_USERNAME");
            response.addError(message);
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password contains username");
            return response;
         }
      }
      // Check with password history
      if (! ( validatePasswordWithUserPasswordHistory(userBean.getUserName(), userBean.getNewPassword()) ))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PASSWORD_USED_PREVIOUSLY");
         response.addError(message);
         return response;
      }
      boolean specialCharNotExist = false;
      // check the password have special character
      if (isPasswordAllowSpecialChars())
      {
         if (! ( isPasswordContainsSpecialChar(userBean.getNewPassword()) ))
         {
            specialCharNotExist = true;
         }
      }
      boolean numericNotExist = false;
      // check the password have Number
      if (isPasswordAllowNumerics())
      {
         if (! ( isPasswordContainsNumeric(userBean.getNewPassword()) ))
         {
            numericNotExist = true;
         }
      }
      // if both (Numeric and Special Char)
      if (specialCharNotExist && numericNotExist)
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PASSWORD_CONTAIN_NUMERIC_CHAR");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Password does not contain at least one non alphabetic character");
         return response;
      }
      // check password for alphabets
      if (! isPwdContainsAlphabets(userBean.getNewPassword()))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PWD_SHOULD_CONTAIN_ALPHABET");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Password does not contain at least one  alphabetic character");
         return response;
      }
      return response;
   }

   /**
    * Add the argument values in message and Generate the dynamic message,
    * 
    * @param message - String static message.
    * @param args - list of argument values, which need to add in message
    * @return Dynamic message with argument value
    */
   public String getErrorMessage( String message, List<String> args )
   {
      if (message != null)
      {
         if (args.size() > 0)
         {
            for (int i = 0; i < args.size(); i++)
            {
               String strVariable = "<" + ( i + 1 ) + ">";
               message = message.replaceAll(strVariable, args.get(i));
            }
         }
      }
      return message;
   }

   /**
    * Check whether "Username is allow in Password" rule apply or not.
    * 
    * @return isPasswordDisAllowedUserName
    */
   public boolean isPasswordDisAllowedUserName()
   {
      boolean isPasswordDisAllowedUserName = false;
      String propertyValue = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
            "PASSWORD_DISALLOWED_USERNAME_BLN");
      if (Boolean.valueOf(propertyValue).booleanValue())
      {
         isPasswordDisAllowedUserName = true;
      }
      return isPasswordDisAllowedUserName;
   }

   /**
    * Check whether the password contains the username or not.
    * 
    * @param userName
    * @param password
    * @return isPasswordContainUsername
    */
   public boolean isPasswordContainUsername( String userName, String password )
   {
      final String THIS_METHOD = "isPasswordContainUsername";
      boolean isPasswordContainUsername = false;
      isPasswordContainUsername = password.matches("(?i).*" + userName + ".*");
      if (isPasswordContainUsername)
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Password contains username.");
      }
      return isPasswordContainUsername;
   }

   /**
    * validate that the new password has not been a repeat of the last
    * passwords, with being configurable both to number of passwords maintained
    * as history
    * 
    * @param userName
    * @param newPassword
    * @return firstTimeLoggedUser
    * @throws InternalServerException
    */
   private boolean validatePasswordWithUserPasswordHistory( String userName, String newPassword )
         throws InternalServerException
   {
      boolean validPassword = true;
      long userId = userDao.getUserById(userName);
      List<UserPasswordTrackingBean> passwordHistory = loginDao.retrievePasswordsUsedBytheUser(userId);
      String encryptedPWD = UtilManager.encrypt(newPassword);
      int previousPasswordCounts = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD,
            "PREVIOUS_PASSWORD_SHOULD_NOT_REUSE_COUNT", 0);
      int passwordDayCounts = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD,
            "PREVIOUS_PASSWORD_SHOULD_NOT_REUSE_DAY_INTERVAL", 0);
      if (previousPasswordCounts <= 0 && passwordDayCounts <= 0)
      {
         return validPassword;
      }
      int size = passwordHistory.size();
      for (int i = 0; i < size; i++)
      {
         UserPasswordTrackingBean userPwdHistoryBean = passwordHistory.get(i);
         String previousPassword = userPwdHistoryBean.getOldPassword();
         Date dbDate = UtilManager.formatDateFromString(userPwdHistoryBean.getPasswordUpdatedOn(),
               RebateApiConstant.DATE_FORMAT);
         Date currDate = new Date(System.currentTimeMillis());
         long DayDiff = getDateDifference(dbDate, currDate, true);
         if (i < previousPasswordCounts || DayDiff < passwordDayCounts)
         {
            if (previousPassword.equalsIgnoreCase(encryptedPWD))
            {
               validPassword = false;
               break;
            }
         }
         else
         {
            break;
         }
      }
      return validPassword;
   }

   /**
    * return the difference of two dates in days or hour.
    * 
    * @param dbDate - Data from Data base
    * @param cureentDate - current date
    * @param isDiffInDays - boolean , true - calculate difference in Days, false
    *           - calculate difference in Hour
    * @return diff - long difference between database date and current date
    */
   private long getDateDifference( Date dbDate, Date cureentDate, boolean isDiffInDays )
   {
      long diff = 0L;
      Calendar today = new GregorianCalendar();
      Calendar datbaseDate = new GregorianCalendar();
      datbaseDate.setTime(dbDate);
      long diffInMillis = today.getTimeInMillis() - datbaseDate.getTimeInMillis();
      if (isDiffInDays)
      {
         diff = diffInMillis / ( 24 * 60 * 60 * 1000 );
      }
      else
      {
         diff = diffInMillis / ( 60 * 60 * 1000 );
      }
      return diff;
   }

   /**
    * Check whether "special chars is allow in Password" rule apply or not.
    * 
    * @return isPasswordAllowSpecialChars
    */
   public boolean isPasswordAllowSpecialChars()
   {
      boolean isPasswordAllowSpecialChars = false;
      String propertyValue = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
            "PASSWORD_ALLOWED_SPECIAL_CHARS_BLN");
      if (Boolean.valueOf(propertyValue).booleanValue())
      {
         isPasswordAllowSpecialChars = true;
      }
      return isPasswordAllowSpecialChars;
   }

   /**
    * Check whether the password contains special char or not
    * 
    * @param password
    * @return isPasswordContainsSpecialChar
    */
   public boolean isPasswordContainsSpecialChar( String password )
   {
      final String THIS_METHOD = "isPasswordContainsSpecialChar";
      boolean isPasswordContainsSpecialChar = false;
      Pattern patternSpecialChar;
      Matcher matcher;
      String ptrSpecial = configManager.getPropertyWithComma(RebateApiConstant.WEB_PASSWORD,
            "PASSWORD_ALLOWED_SPECIAL_CHARS");
      patternSpecialChar = Pattern.compile(ptrSpecial);
      matcher = patternSpecialChar.matcher(password);
      if (matcher.find())
      {
         isPasswordContainsSpecialChar = true;
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password contains atleast one Special character.");
      }
      return isPasswordContainsSpecialChar;
   }

   /**
    * Check whether "Nemeric value is allow in Password" rule apply or not.
    * 
    * @return isPasswordAllowNumerics
    */
   public boolean isPasswordAllowNumerics()
   {
      boolean isPasswordAllowNumerics = false;
      String propertyValue = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
            "PASSWORD_ALLOWED_NUMERIC_BLN");
      if (Boolean.valueOf(propertyValue).booleanValue())
      {
         isPasswordAllowNumerics = true;
      }
      return isPasswordAllowNumerics;
   }

   /**
    * Check whether the password contains the numeric value or not.
    * 
    * @param password
    * @return isPasswordContainsNumber
    */
   public boolean isPasswordContainsNumeric( String password )
   {
      final String THIS_METHOD = "isPasswordContainsNumeric";
      boolean isPasswordContainsNumber = false;
      Pattern patternNumber;
      Matcher matcher;
      String ptrNumber = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD, "PASSWORD_ALLOWED_NUMERIC");
      patternNumber = Pattern.compile(ptrNumber);
      matcher = patternNumber.matcher(password);
      matcher.groupCount();
      if (matcher.find())
      {
         isPasswordContainsNumber = true;
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password contains atleast one Numeric character.");
      }
      return isPasswordContainsNumber;
   }

   /**
    * Checks password for alphabets. returns true if the password contains at
    * least one alphabet. else returns false.
    * 
    * @param newPassword
    * @return
    */
   private boolean isPwdContainsAlphabets( String newPassword )
   {
      String pattern = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD, "PASSWORD_ALLOWED_ALPHABETS");
      return newPassword.matches(pattern);
   }

   public void resetPasswordFlag( final long userId, final boolean flag )
   {
      userDao.modifyPwdResetFlag(flag, userId);
   }

   public void updateLoggedInUserDetail( UserPasswordTrackingBean pwdTrackingBean, boolean isPwdUpdate, boolean runtime )
         throws InternalServerException
   {
      final String THIS_METHOD = "updateLoggedInUserDetail";
      UserPasswordTrackingBean userBean = loginDao.getLoggedInUserDetail(pwdTrackingBean.getUserId());
      if (userBean.getUserId() > 0) // user details already available
      {
         pwdTrackingBean.setPreviousLoginTime(userBean.getCurrentLoginTime());
         String currentDate = UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(),
               RebateApiConstant.DATE_FORMAT);
         pwdTrackingBean.setCurrentLoginTime(currentDate);
         if (isPwdUpdate)
         {
            pwdTrackingBean.setPasswordUpdatedOn(currentDate);
            pwdTrackingBean.setNewPassword(pwdTrackingBean.getNewPassword());
            pwdTrackingBean.setResetFlag(ILoginDAO.RESET_FLAG_NO);
         }
         loginDao.updateLoggedInUserDetail(pwdTrackingBean, isPwdUpdate);
         // TO make unsuccessful counter 0
         loginDao.deleteUnsuccesssAttempt(pwdTrackingBean.getUserId());
         if (isPwdUpdate)
         {
            loginDao.insertUserPasswordHistory(pwdTrackingBean);
            loginDao.updatePasswordInUserProfile(pwdTrackingBean.getUserId(), pwdTrackingBean.getNewPassword());
         }
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
               "Updating User's detail in to Database finished successfully.");
      }
      else
      {
         // insert the user details first time
         insertUserPwdTrackDetails(pwdTrackingBean);
      }
   }

   /**
    * Check that password is correct or not
    * 
    * @param userBean - UserPasswordTrackingBean bean contain password info
    * @return PasswordResponse bean proper flag and error massage
    * @throws InternalServerException - Throws if any exception occurred
    */
   public PasswordResponse isPasswordCorrect( UserPasswordTrackingBean userBean, boolean runtime )
         throws InternalServerException
   {
      final String THIS_METHOD = "isPasswordCorrect";
      PasswordResponse response = new PasswordResponse();
      // check the old Password correct or not
      /*
       * if (!(isOldPasswordCorrect(userBean))) { String message =
       * configManager.getPropertyAsString(WEB_PASSWORD, "MSG_OLD_PWD_WRONG");
       * 
       * response.addError(message);
       * 
       * logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
       * "Old Password is not correct with DB.");
       * 
       * return response; }
       */
      // check the new Password and Confirm password are same
      if (! ( userBean.getNewPassword().compareTo(userBean.getConfirmNewPassword()) == 0 ))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_NEW_PWD_AND_CONFIRM_NEW_PWD_EQUAL");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "New password and confirm new password are not equal.");
         return response;
      }
      if (userBean.getNewPassword().contains(" "))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PWD_SHOULD_NOT_CONTAIN_WHITE_SPACE");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password contains space(s).");
         return response;
      }
      int minLength = 0;
      int maxLength = 0;
      minLength = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD, "PASSWORD_MIN_LEN", 0);
      maxLength = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD, "PASSWORD_MAX_LEN", 0);
      // Check the password have min length
      if (minLength != 0)
      {
         if (userBean.getNewPassword().length() < minLength)
         {
            List<String> args = new ArrayList<String>();
            args.add(String.valueOf(minLength));
            String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD, "MSG_PASSWORD_MIN_LEN");
            response.addError(getErrorMessage(message, args));
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password has length less than " + minLength);
            return response;
         }
      }
      // check the password have max length
      if (maxLength != 0)
      {
         if (userBean.getNewPassword().length() > maxLength)
         {
            List<String> args = new ArrayList<String>();
            args.add(String.valueOf(maxLength));
            String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD, "MSG_PASSWORD_MAX_LEN");
            response.addError(getErrorMessage(message, args));
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password has length greater than " + maxLength);
            return response;
         }
      }
      // Check degree of diffrence
      /*
       * int minCharDiff = 0;
       * 
       * minCharDiff = configManager.getPropertyAsInt(WEB_PASSWORD,
       * "PASSWORD_MIN_DIFF_FROM_PREVIOUS_PASSWORD", 0);
       * 
       * if (minCharDiff > 0) { int diff =
       * getLevenshteinDistance(userBean.getOldPassword(), userBean
       * .getNewPassword());
       * 
       * if (diff < minCharDiff) { List<String> args = new ArrayList<String>();
       * args.add(String.valueOf(minCharDiff));
       * 
       * String message = configManager.getPropertyAsString(WEB_PASSWORD,
       * "MSG_DEGREE_DIFF_FOR_NEW_PASSWORD");
       * 
       * response.addError(getErrorMessage(message, args));
       * 
       * return response; } }
       */
      // check the password disallowed UserName
      if (isPasswordDisAllowedUserName())
      {
         if (( isPasswordContainUsername(userBean.getUserName(), userBean.getNewPassword()) ))
         {
            String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
                  "MSG_PASSWORD_NOT_CONTAIN_USERNAME");
            response.addError(message);
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Password contains username");
            return response;
         }
      }
      // Check with password history
      if (! ( validatePasswordWithUserPasswordHistory(userBean.getUserName(), userBean.getNewPassword(), false) ))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PASSWORD_USED_PREVIOUSLY");
         response.addError(message);
         return response;
      }
      boolean specialCharNotExist = false;
      // check the password have special character
      if (isPasswordAllowSpecialChars())
      {
         if (! ( isPasswordContainsSpecialChar(userBean.getNewPassword()) ))
         {
            specialCharNotExist = true;
         }
      }
      boolean numericNotExist = false;
      // check the password have Number
      if (isPasswordAllowNumerics())
      {
         if (! ( isPasswordContainsNumeric(userBean.getNewPassword()) ))
         {
            numericNotExist = true;
         }
      }
      // if both (Numeric and Special Char)
      if (specialCharNotExist && numericNotExist)
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PASSWORD_CONTAIN_NUMERIC_CHAR");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Password does not contain at least one non alphabetic character");
         return response;
      }
      // check password for alphabets
      if (! isPwdContainsAlphabets(userBean.getNewPassword()))
      {
         String message = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
               "MSG_PWD_SHOULD_CONTAIN_ALPHABET");
         response.addError(message);
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Password does not contain at least one  alphabetic character");
         return response;
      }
      return response;
   }

   /**
    * validate that the new password has not been a repeat of the last
    * passwords, with being configurable both to number of passwords maintained
    * as history
    * 
    * @param userName
    * @param newPassword
    * @return firstTimeLoggedUser
    * @throws InternalServerException
    */
   private boolean validatePasswordWithUserPasswordHistory( String userName, String newPassword, boolean flag )
         throws InternalServerException
   {
      boolean validPassword = true;
      long userId = userDao.getUserById(userName);
      List<UserPasswordTrackingBean> passwordHistory = loginDao.retrievePasswordsUsedBytheUser(userId);
      // String encryptedPWD = UtilManager.encrypt(newPassword);
      int previousPasswordCounts = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD,
            "PREVIOUS_PASSWORD_SHOULD_NOT_REUSE_COUNT", 0);
      int passwordDayCounts = configManager.getPropertyAsInt(RebateApiConstant.WEB_PASSWORD,
            "PREVIOUS_PASSWORD_SHOULD_NOT_REUSE_DAY_INTERVAL", 0);
      if (previousPasswordCounts <= 0 && passwordDayCounts <= 0)
      {
         return validPassword;
      }
      int size = passwordHistory.size();
      for (int i = 0; i < size; i++)
      {
         UserPasswordTrackingBean userPwdHistoryBean = passwordHistory.get(i);
         String previousPassword = userPwdHistoryBean.getOldPassword();
         Date dbDate = UtilManager.formatDateFromString(userPwdHistoryBean.getPasswordUpdatedOn(),
               RebateApiConstant.DATE_FORMAT);
         Date currDate = new Date(System.currentTimeMillis());
         long DayDiff = getDateDifference(dbDate, currDate, true);
         if (i < previousPasswordCounts || DayDiff < passwordDayCounts)
         {
            if (previousPassword.equalsIgnoreCase(newPassword))
            {
               validPassword = false;
               break;
            }
         }
         else
         {
            break;
         }
      }
      return validPassword;
   }
}
