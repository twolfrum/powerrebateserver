package com.icf.rebate.api.login.exception;

import com.icf.rebate.api.exception.GenericAPIException;
import com.icf.rebate.api.util.UtilManager;
/**
 * This class InActiveUserException represents use case user is inactive
 *
 */
public class InActiveUserException extends GenericAPIException
{
   /**
    * Represents error code for {@link UserAuthenticationException}
    */
   private String errorCode = "ELOGIN1006";
   /**
    * Represents error msg for {@link UserAuthenticationException}
    */
   private String errorMessage = UtilManager.getErrorMessage(errorCode);
   /**
    * Represents Serial Version UID.
    */
   private static final long serialVersionUID = 1L;

   /**
    * Constructs an instance of <code>InActiveUserException</code>.
    */
   public InActiveUserException ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>InActiveUserException</code> with the
    * specified detail message.
    */
   public InActiveUserException ( String msg )
   {
      super(msg);
   }

   /**
    * Constructs an instance of <code>InActiveUserException</code> with the
    * specified detail message.
    * 
    * @param e - is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public InActiveUserException ( String msg, Throwable e )
   {
      super(msg, e);
   }

   @Override
   public String getErrorCode()
   {
      // TODO Auto-generated method stub
      return errorCode;
   }

   @Override
   public String getErrorMessage()
   {
      // TODO Auto-generated method stub
      return errorMessage;
   }

   /**
    * Sets the error message.
    * 
    * @param errorMessage - String Value
    */
   public void setErrorMessage( String errorMessage )
   {
      this.errorMessage = errorMessage;
   }
}
