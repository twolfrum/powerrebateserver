package com.icf.rebate.api.company.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.util.BStatus;
@XmlRootElement
@XmlType(propOrder = { "companyId", "companyName", "icon", "fields" })
public class Company extends BStatus
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private long companyId;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String companyName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String icon;
   @JsonIgnore
   private String state;
   @JsonIgnore
   private String address;
   @JsonIgnore
   private String programNumber;
   @JsonIgnore
   private String companyDisplayName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private Map<String, String> fields;
   @JsonIgnore
   private List<User> usersList;
   @JsonIgnore
   private String abbrvCompanyName;

   private boolean sendToPrimary;
   private boolean sendToPremise;
   private boolean sendToContractor;

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }

   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<DashboardItems> dashboardItems;

   public List<DashboardItems> getDashboardItems()
   {
      return dashboardItems;
   }

   public void setDashboardItems( List<DashboardItems> dashboardItems )
   {
      this.dashboardItems = dashboardItems;
   }

   public String getIcon()
   {
      return icon;
   }

   public void setIcon( String icon )
   {
      this.icon = icon;
   }

   public String getCompanyName()
   {
      return companyName;
   }

   public void setCompanyName( String companyName )
   {
      this.companyName = companyName;
   }

   public String getState()
   {
      return state;
   }

   public void setState( String state )
   {
      this.state = state;
   }

   public String getAddress()
   {
      return address;
   }

   public void setAddress( String address )
   {
      this.address = address;
   }

   public long getCompanyId()
   {
      return companyId;
   }

   public void setCompanyId( long companyId )
   {
      this.companyId = companyId;
   }

   public List<User> getUsersList()
   {
      return usersList;
   }

   public void setUsersList( List<User> usersList )
   {
      this.usersList = usersList;
   }

   public String getCompanyDisplayName()
   {
      return companyDisplayName;
   }

   public void setCompanyDisplayName( String companyDisplayName )
   {
      this.companyDisplayName = companyDisplayName;
   }

   public Map<String, String> getFields()
   {
      return fields;
   }

   public void setFields( Map<String, String> fields )
   {
      this.fields = fields;
   }

public String getProgramNumber() {
	return programNumber;
}

public void setProgramNumber(String programNumber) {
	this.programNumber = programNumber;
}

public boolean getSendToPrimary() {
	return sendToPrimary;
}

public void setSendToPrimary(boolean sendToPrimary) {
	this.sendToPrimary = sendToPrimary;
}

public boolean getSendToPremise() {
	return sendToPremise;
}

public void setSendToPremise(boolean sendToPremise) {
	this.sendToPremise = sendToPremise;
}

public boolean getSendToContractor() {
	return sendToContractor;
}

public void setSendToContractor(boolean sendToContractor) {
	this.sendToContractor = sendToContractor;
}

public String getAbbrvCompanyName() {
	return abbrvCompanyName;
}

public void setAbbrvCompanyName(String abbrvCompanyName) {
	this.abbrvCompanyName = abbrvCompanyName;
}

@Override
public String toString() {
	return "Company [companyId=" + companyId + ", companyName=" + companyName + ", address=" + address
			+ ", programNumber=" + programNumber + ", companyDisplayName=" + companyDisplayName + ", fields=" + fields
			+ ", usersList=" + usersList + ", dashboardItems=" + dashboardItems + "]";
}


}
