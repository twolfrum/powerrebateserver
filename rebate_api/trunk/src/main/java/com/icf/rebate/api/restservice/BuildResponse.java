package com.icf.rebate.api.restservice;

import java.util.Map;

import com.icf.rebate.api.user.impl.User;
public class BuildResponse
{
   User userDetails;
   Map<String, String> responseHeaderMap;
   String contentType;
   byte[] responseObject;
   String responseCode;
   String responseMsg;

   public BuildResponse ()
   {
   }

   public void putHeader( String key, String value )
   {
      responseHeaderMap.put(key, value);
   }

   public User getUserDetails()
   {
      return userDetails;
   }

   public void setUserDetails( User userDetails )
   {
      this.userDetails = userDetails;
   }

   public Map<String, String> getResponseHeaderMap()
   {
      return responseHeaderMap;
   }

   public void setResponseHeaderMap( Map<String, String> responseHeaderMap )
   {
      this.responseHeaderMap = responseHeaderMap;
   }

   public String getContentType()
   {
      return contentType;
   }

   public void setContentType( String contentType )
   {
      this.contentType = contentType;
   }

   public byte[] getResponseObject()
   {
      return responseObject;
   }

   public void setResponseObject( byte[] responseObject )
   {
      this.responseObject = responseObject;
   }

   public String getResponseCode()
   {
      return responseCode;
   }

   public void setResponseCode( String responseCode )
   {
      this.responseCode = responseCode;
   }

   public String getResponseMsg()
   {
      return responseMsg;
   }

   public void setResponseMsg( String responseMsg )
   {
      this.responseMsg = responseMsg;
   }
}
