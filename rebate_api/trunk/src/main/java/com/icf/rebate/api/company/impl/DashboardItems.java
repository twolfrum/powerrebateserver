package com.icf.rebate.api.company.impl;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
public class DashboardItems
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String id;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String name;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String icon;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<DashboardItems> options;

   public String getId()
   {
      return id;
   }

   public void setId( String id )
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName( String name )
   {
      this.name = name;
   }

   public String getIcon()
   {
      return icon;
   }

   public void setIcon( String icon )
   {
      this.icon = icon;
   }

   public List<DashboardItems> getOptions()
   {
      return options;
   }

   public void setOptions( List<DashboardItems> options )
   {
      this.options = options;
   }

   public void setOptions( DashboardItems item )
   {
      if (options == null)
         options = new ArrayList<DashboardItems>();
      options.add(item);
   }
}
