package com.icf.rebate.api.login.intrface;

import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.login.exception.InActiveUserException;
import com.icf.rebate.api.login.exception.UserAuthenticationException;
import com.icf.rebate.api.login.exception.UserBlockedException;
import com.icf.rebate.api.login.exception.UserNameNotFoundException;
import com.icf.rebate.api.login.exception.UserNotAllowedException;
import com.icf.rebate.api.login.impl.PasswordResponse;
import com.icf.rebate.api.login.impl.UserPasswordTrackingBean;
import com.icf.rebate.api.user.exception.PwdResetNotAllowedException;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
/**
 * This interface ILoginAdapter will expose all the login operation needs to be
 * done.
 * 
 */
public interface ILoginAdapter
{
   /**
    * Authenticates a user with the given user name and password for Runtime
    * API.
    * 
    * @param userName - User to be authenticated with this user name.
    * @param password - User to be authenticated with this password.
    * @return user - User Profile for the given user.
    * @throws UserNameNotFoundException - If user doesn not exist for the given
    *            user name.
    * @throws UserAuthenticationException - If user enters Invalid password.
    * @throws InternalServerException - If any runtime exception occurs.
    * @throws UserBlockedException
    */
   public User authenticateAndGetUser( String userName, String password ) throws UserNameNotFoundException,
         UserAuthenticationException, InternalServerException, InActiveUserException, UserBlockedException,
         UserDoesNotExist;

   /**
    * Authenticates a user with the given user name and password for console.
    * 
    * @param userName - User to be authenticated with this user name.
    * @param password - User to be authenticated with this password.
    * @param forWebConsole - indicates request coming from console.
    * @return user - User Profile for the given user.
    * @throws UserNameNotFoundException - If user doesn not exist for the given
    *            user name.
    * @throws UserAuthenticationException - If user enters Invalid password.
    * @throws InternalServerException - If any runtime exception occurs.
    * @throws UserBlockedException
    */
   public User authenticateAndGetUser( String userName, String password, boolean forWebConsole )
         throws UserNameNotFoundException, UserAuthenticationException, InternalServerException, InActiveUserException,
         UserBlockedException, UserNotAllowedException, UserDoesNotExist;

   /**
    * Send new password to registered email id
    * 
    * @param email - validate email id
    * @return User - containing user details
    * @throws UserDoesNotExist
    * @throws InvalidDataException
    * @throws InternalServerException
    */
   public User forgetPassword( String userName ) throws PwdResetNotAllowedException, UserNameNotFoundException, InActiveUserException, UserBlockedException, InternalServerException;

   /**
    * send password to register email id by validating UserName
    * 
    * @param userName - who forget the password
    * @return User - User details
    * @throws UserDoesNotExist - if user does not exist in db
    * @throws InvalidDataException - if input data is invalid
    * @throws InternalServerException - If any runtime exception occurs.
    */
   public User forgetPasswordWithUserName( String userName ) throws UserDoesNotExist, InvalidDataException,
         InternalServerException;

   /**
    * 
    * @param user
    * @throws InternalServerException
    */
   public void resetPwdInternally( User user ) throws InternalServerException;

   /**
    * 
    * @param userId
    */
   public void deleteUserLoginData( long userId );

   /**
    * Update the logged in user details in to data source
    * 
    * @param pwdTrackingBean - UserPasswordTrackingBean info of user password
    *           tracking
    * @param isPwdUpdate - If true then update the password , If false then
    *           don't update password. just update other information
    * @param isAnswerUpdate - if true then encrypt the answer and update else
    *           don't encrypt
    * @throws InternalServerException - If any exception occurred while updating
    *            logged in user details
    */
   public void updateLoggedInUserDetail( UserPasswordTrackingBean pwdTrackingBean, boolean isPwdUpdate )
         throws InternalServerException;

   /**
    * Check that password is correct or not
    * 
    * @param userBean - UserPasswordTrackingBean bean contain password info
    * @return PasswordResponse bean proper flag and error massage
    * @throws InternalServerException - Throws if any exception occurred
    */
   public PasswordResponse isPasswordCorrect( UserPasswordTrackingBean userBean ) throws InternalServerException;

   /**
    * Reset password flag based on Input flag for given user Id
    * 
    * @param userId - reset flag for this user Id
    * @param flag - if true set Y else set N
    */
   public void resetPasswordFlag( final long userId, final boolean flag );
}
