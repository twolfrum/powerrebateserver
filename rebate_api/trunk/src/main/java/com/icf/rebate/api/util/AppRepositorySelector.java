package com.icf.rebate.api.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Hierarchy;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.spi.LoggerRepository;
import org.apache.log4j.spi.RepositorySelector;
import org.apache.log4j.spi.RootLogger;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.Document;
public class AppRepositorySelector implements RepositorySelector
{
   private static boolean initialized = false;
   private static Object guard = LogManager.getRootLogger();
   private static Map<ClassLoader, Hierarchy> repositories = new HashMap<ClassLoader, Hierarchy>();
   private static LoggerRepository defaultRepository;

   public static synchronized void init( String log4jXMLPath ) // throws
                                                               // ServletException
   {
      try
      {
         if (! initialized) // set the global RepositorySelector
         {
            defaultRepository = LogManager.getLoggerRepository();
            RepositorySelector theSelector = new AppRepositorySelector();
            LogManager.setRepositorySelector(theSelector, guard);
            initialized = true;
         }
         Hierarchy hierarchy = new Hierarchy(new RootLogger(Level.DEBUG));
         // loadLog4JConfig(servletContext, hierarchy);
         loadLog4JConfig(hierarchy, log4jXMLPath);
         ClassLoader loader = Thread.currentThread().getContextClassLoader();
         repositories.put(loader, hierarchy);
      }
      catch (Exception e)
      {
         System.out.println("Exception while initializing AppRepositorySelector.");
         e.printStackTrace();
      }
   }

   public static synchronized void removeFromRepository()
   {
      repositories.remove(Thread.currentThread().getContextClassLoader());
   }

   // load log4j.xml from WEB-INF
   private static void loadLog4JConfig( Hierarchy hierarchy, String log4jXMLPath ) throws ServletException
   // (ServletContext servletContext, Hierarchy hierarchy) throws
   // ServletException
   {
      try
      {
         // String log4jFile = "/WEB-INF/log4j.xml";
         // ResourceBundle resource =
         // ResourceBundle.getBundle(UtilManager.PROPERTY_FILE_NAME);
         // String log4jXMLPath =
         // resource.getString(UtilManager.LOG4J_XML_PATH);
         // InputStream log4JConfig = new
         // FileInputStream(log4jXMLPath);//servletContext.getResourceAsStream(log4jFile);
         InputStream log4JConfig = AppRepositorySelector.class.getResourceAsStream(log4jXMLPath);
         Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(log4JConfig);
         DOMConfigurator conf = new DOMConfigurator();
         conf.doConfigure(doc.getDocumentElement(), hierarchy);
      }
      catch (Exception e)
      {
         throw new ServletException(e);
      }
   }

   public static boolean isAppRepositoryInitialized()
   {
      return initialized;
   }

   private AppRepositorySelector ()
   {
   }

   public LoggerRepository getLoggerRepository()
   {
      ClassLoader loader = Thread.currentThread().getContextClassLoader();
      LoggerRepository repository = (LoggerRepository) repositories.get(loader);
      if (repository == null)
      {
         return defaultRepository;
      }
      else
      {
         return repository;
      }
   }
}
