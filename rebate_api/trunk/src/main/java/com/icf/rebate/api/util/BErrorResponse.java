package com.icf.rebate.api.util;

import java.io.IOException;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "statusCode", "title", "message", "details" })
public class BErrorResponse
{
   String statusCode;
   // String title;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String statusMsg;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String details;

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }

   public String getStatusCode()
   {
      return statusCode;
   }

   public void setStatusCode( String statusCode )
   {
      this.statusCode = statusCode;
   }

   // public String getTitle() {
   // return title;
   // }
   //
   // public void setTitle(String title) {
   // this.title = title;
   // }
   public String getStatusMsg()
   {
      return statusMsg;
   }

   public void setStatusMsg( String statusMsg )
   {
      this.statusMsg = statusMsg;
   }

   public String getDetails()
   {
      return details;
   }

   public void setDetails( String details )
   {
      this.details = details;
   }
}
