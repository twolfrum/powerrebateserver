package com.icf.rebate.api.module.impl;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "accountNumber", "firstName", "lastName", "address", "city", "state", "zip",
      "contactType", "phone", "cell", "email", "company","gpsAddress","geoLong","geoLat"})
public class FormUserBean implements Serializable
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String accountNumber;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String accountNumber2;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String contractorAccountNumber;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String firstName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String lastName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String address;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String contractorCompany;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String address1;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String address2;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String city;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String state;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String zip;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String contactType;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String phone;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String cell;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String email;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String company;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   FormUserBean customerAddress;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String gpsAddress;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String geoLong;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String geoLat;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String technicianName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String technicianId;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String technicianPhone;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String customerIncentive;
   
   public FormUserBean getCustomerAddress()
   {
      return customerAddress;
   }

   public void setCustomerAddress( FormUserBean customerAddress )
   {
      this.customerAddress = customerAddress;
   }

   public String getAccountNumber()
   {
      return accountNumber;
   }

   public void setAccountNumber( String accountNumber )
   {
      this.accountNumber = accountNumber;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public void setFirstName( String firstName )
   {
      this.firstName = firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName( String lastName )
   {
      this.lastName = lastName;
   }

   public String getAddress()
   {
      return address;
   }

   public void setAddress( String address )
   {
      this.address = address;
   }

   public String getCity()
   {
      return city;
   }

   public void setCity( String city )
   {
      this.city = city;
   }

   public String getState()
   {
      return state;
   }

   public void setState( String state )
   {
      this.state = state;
   }

   public String getZip()
   {
      return zip;
   }

   public void setZip( String zip )
   {
      this.zip = zip;
   }

   public String getPhone()
   {
      return phone;
   }

   public void setPhone( String phone )
   {
      this.phone = phone;
   }

   public String getCell()
   {
      return cell;
   }

   public void setCell( String cell )
   {
      this.cell = cell;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail( String email )
   {
      this.email = email;
   }

   public String getContactType()
   {
      return contactType;
   }

   public void setContactType( String contactType )
   {
      this.contactType = contactType;
   }

   public String getCompany()
   {
      return company;
   }

   public void setCompany( String company )
   {
      this.company = company;
   }

   public String getGpsAddress()
   {
      return gpsAddress;
   }

   public void setGpsAddress( String gpsAddress )
   {
      this.gpsAddress = gpsAddress;
   }

   public String getGeoLong()
   {
      return geoLong;
   }

   public void setGeoLong( String geoLong )
   {
      this.geoLong = geoLong;
   }

   public String getGeoLat()
   {
      return geoLat;
   }

   public void setGeoLat( String geoLat )
   {
      this.geoLat = geoLat;
   }

	public String getAddress1() {
		return address1;
	}
	
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getAddress2() {
		return address2;
	}
	
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getContractorCompany() {
		return contractorCompany;
	}
	
	public void setContractorCompany(String contractorCompany) {
		this.contractorCompany = contractorCompany;
	}
	
	public String getAccountNumber2() {
		return accountNumber2;
	}
	
	public void setAccountNumber2(String accountNumber2) {
		this.accountNumber2 = accountNumber2;
	}
	
	public String getContractorAccountNumber() {
		return contractorAccountNumber;
	}
	
	public void setContractorAccountNumber(String contractorAccountNumber) {
		this.contractorAccountNumber = contractorAccountNumber;
	}

	public String getTechnicianName() {
		return technicianName;
	}

	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}

	public String getTechnicianId() {
		return technicianId;
	}

	public void setTechnicianId(String technicianId) {
		this.technicianId = technicianId;
	}

	public String getTechnicianPhone() {
		return technicianPhone;
	}

	public void setTechnicianPhone(String technicianPhone) {
		this.technicianPhone = technicianPhone;
	}

	public String getCustomerIncentive() {
		return customerIncentive;
	}

	public void setCustomerIncentive(String customerIncentive) {
		this.customerIncentive = customerIncentive;
	}
}
