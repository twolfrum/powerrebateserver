package com.icf.rebate.api.module.impl;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.company.impl.CompanyManager;
import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.login.exception.InActiveUserException;
import com.icf.rebate.api.login.exception.UserAuthenticationException;
import com.icf.rebate.api.login.exception.UserBlockedException;
import com.icf.rebate.api.login.exception.UserNameNotFoundException;
import com.icf.rebate.api.login.impl.EmailUser;
import com.icf.rebate.api.login.intrface.ILoginAdapter;
import com.icf.rebate.api.login.intrface.ILoginValidator;
import com.icf.rebate.api.module.exception.JsonNotPresentException;
import com.icf.rebate.api.module.impl.Submission.SubmissionItem;
import com.icf.rebate.api.restservice.AbstractManager;
import com.icf.rebate.api.restservice.BuildResponse;
import com.icf.rebate.api.stub.Attribute;
import com.icf.rebate.api.stub.AttributeScrubber;
import com.icf.rebate.api.stub.BaseAttributes;
import com.icf.rebate.api.stub.EquipmentAttributes;
import com.icf.rebate.api.stub.ImportApplication;
import com.icf.rebate.api.stub.ImportApplicationItem;
import com.icf.rebate.api.stub.ImportContact;
import com.icf.rebate.api.stub.ImportContacts;
import com.icf.rebate.api.stub.ImportEquipment;
import com.icf.rebate.api.stub.ImportEquipmentItem;
import com.icf.rebate.api.stub.ImportRecord;
import com.icf.rebate.api.stub.ImportSurvey;
import com.icf.rebate.api.stub.SurveyAttributes;
import com.icf.rebate.api.stub.SystemRecord;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.BStatus;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.api.util.UtilManager;

/**
 * This class contains upload zip file and get model fields requests.
 * 
 * 
 */
public class ModuleRestManager extends AbstractManager {
	private final String THIS_CLASS = "ModuleRestManager";
	private IMPLogger logger;
	private ConfigurationManager configManager;
	private CompanyManager companyManager;
	private static final int BUFFER_SIZE = 4096;
	private ILoginValidator loginValidator;
	private ILoginAdapter loginAdapter;
	List<String> filesListInDir;
	StringWriter stringWriter;
	private EmailUser emailUser;
	
	private static final String TAX_ID_KEY = "TAX_ID";
	private static final String USERNAME_KEY = "USERNAME";
	private static final int DUCT_SEALING = 1;
	private static final int AIR_SEALING = 2;

	
	/**
	 * Extract input stream , process and store in specified destination
	 * location
	 * 
	 * @param request
	 *            - HttpServletRequest
	 * @param userDetails
	 *            - user name and password for validation and compnayname -
	 *            store file for specific company
	 * @param uploadedInputStream
	 *            - input zip stream
	 * @return response bean containing success and failure status code and
	 *         message based on input stream process.
	 * @throws UserNameNotFoundException
	 *             - throws if user name not found
	 * @throws InActiveUserException
	 *             - throws if user is blocked
	 * @throws UserAuthenticationException
	 *             - throws if validation fails
	 * @throws Exception
	 */
	/*
	 * public Response uploadZipFile( @Context HttpServletRequest request,
	 * Map<String, String> userDetails, InputStream uploadedInputStream ) throws
	 * Exception { final String THIS_METHOD = "uploadZipFile"; BuildResponse
	 * responseBuilder = new BuildResponse(); User user = new User(); Response
	 * response = null; byte[] responseData = null; String GUUID =
	 * UUID.randomUUID().toString(); GUUID =
	 * GUUID.substring(GUUID.lastIndexOf("-") + 1, GUUID.length()); try {
	 * logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD,"GUUID: " + GUUID);
	 * logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD,
	 * "File uploading begins for user " + user.getUserName());
	 * logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD,
	 * "request content-length: " + request.getHeader("content-length")); String
	 * companyName = userDetails.get("companyName").toUpperCase();
	 * UtilManager.userNameValidation(userDetails.get("username"));
	 * this.loginValidator.validate(userDetails.get("username"),
	 * userDetails.get("password")); user =
	 * this.loginAdapter.authenticateAndGetUser(userDetails.get("username"),
	 * userDetails.get("password")); Map<String, Object> zipContent =
	 * processZipFile(uploadedInputStream, GUUID, companyName);
	 * zipContent.put("ICF_BASE_DIRECTORY",
	 * configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
	 * RebateApiConstant.ICF_BASE_DIRECTORY));
	 * zipContent.put("MPORTAL_BASE_DIRECTORY",
	 * configManager.getPropertyAsString( RebateApiConstant.REBATE_API_PROPERTY,
	 * RebateApiConstant.MPORTAL_BASE_DIRECTORY)); zipContent.put("companyName",
	 * companyName); // File creation begins in separate thread CreateFileThread
	 * createFileThread = new CreateFileThread(zipContent); Thread thread = new
	 * Thread(createFileThread); thread.start(); BStatus bStatus = new
	 * BStatus();
	 * bStatus.setStatusCode(configManager.getPropertyAsString(RebateApiConstant
	 * .REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
	 * bStatus.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.
	 * REBATE_MESSAGES_PROPERTY, "MSG_ZIP_SUCCESS")); responseData =
	 * bStatus.formatJSONString().getBytes(Charset.forName("UTF-8"));
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_ZIP_SUCCESS"));
	 * logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD,
	 * "File Uploaded successfully for user " + user.getUserName()); } catch
	 * (InvalidDataException e) { logger.log(IMPLogger.ERROR, THIS_CLASS,
	 * THIS_METHOD, "InvalidDataException  occured while validating userName " ,
	 * e); responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	 * "CODE_FAILURE_INVALID_USER_PASSWORD"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE")); }
	 * catch (UserNameNotFoundException e) { logger.log(IMPLogger.ERROR,
	 * THIS_CLASS, THIS_METHOD,
	 * "UserNameNotFoundException  occured while authenticating userName " , e);
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	 * "CODE_FAILURE_INVALID_USER_PASSWORD"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE")); }
	 * catch (InActiveUserException e) { logger.log(IMPLogger.ERROR, THIS_CLASS,
	 * THIS_METHOD,
	 * "InActiveUserException  occured while authenticating userName " , e);
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	 * "CODE_FAILURE_INACTIVE_USER"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	 * "MSG_FAILURE_INACTIVE_USER")); } catch (UserAuthenticationException e) {
	 * logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	 * "UserAuthenticationException  occured while authenticating userName " ,
	 * e); responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	 * "CODE_FAILURE_INVALID_USER_PASSWORD"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE")); }
	 * catch (UserBlockedException e) { logger.log(IMPLogger.ERROR, THIS_CLASS,
	 * THIS_METHOD,
	 * "UserBlockedException  occured while authenticating userName " , e);
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	 * "CODE_FAILURE_INVALID_USER_PASSWORD"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE")); }
	 * catch (ZipException e) { logger.log(IMPLogger.ERROR, THIS_CLASS,
	 * THIS_METHOD, "ZipException  occured while authenticating userName " , e);
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_ZIP_FAILURE")); } catch
	 * (JsonNotPresentException e) { logger.log(IMPLogger.ERROR, THIS_CLASS,
	 * THIS_METHOD, "ZipException  occured while authenticating userName " , e);
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	 * "MSG_JSON_UNAVAILABLE_FAILURE")); } catch (InternalServerException e) {
	 * logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	 * "InternalServerException  occured while authenticating userName " , e);
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_FAILURE")); } catch
	 * (Exception ex) { logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	 * "Exception occured while uploading zip file:" , ex);
	 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
	 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
	 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_FAILURE")); }
	 * responseBuilder.setResponseObject(responseData); response =
	 * buildRESTResponse(responseBuilder, 0); return response; }
	 */

	/**
	 * upload zip file into ICF_SERVER_LOCATION
	 * 
	 * @param request
	 *            - HttpServletRequest
	 * @param userDetails
	 *            - user name and password for validation and compnayname -
	 *            store file for specific company
	 * @param uploadedInputStream
	 *            - input zip stream
	 * @return response bean containing success and failure status code and
	 *         message based on input stream process.
	 * @throws UserNameNotFoundException
	 *             - throws if user name not found
	 * @throws InActiveUserException
	 *             - throws if user is blocked
	 * @throws UserAuthenticationException
	 *             - throws if validation fails
	 * @throws Exception
	 */
	public Response uploadZipFile(HttpHeaders httpHeaders, HttpServletRequest request) {
		final String THIS_METHOD = "uploadZipFile";
		BuildResponse responseBuilder = new BuildResponse();
		User user = new User();
		Response response = null;
		byte[] responseData = null;
		byte[] zipData = null;
		String GUUID = UUID.randomUUID().toString();
		GUUID = GUUID.substring(GUUID.lastIndexOf("-") + 1, GUUID.length());
		BStatus status = new BStatus();
		try {
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "File uploading begins for user " + user.getUserName());
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
					"request content-length: " + request.getHeader("content-length"));
			
			Company company = companyManager.getCompanyByName(httpHeaders.getRequestHeader("companyName").get(0));
			String companyName = company.getAbbrvCompanyName();
			
			UtilManager.userNameValidation(httpHeaders.getRequestHeader("username").get(0));
			this.loginValidator.validate(httpHeaders.getRequestHeader("username").get(0),
					httpHeaders.getRequestHeader("password").get(0));
			user = this.loginAdapter.authenticateAndGetUser(httpHeaders.getRequestHeader("username").get(0),
					httpHeaders.getRequestHeader("password").get(0));
			Map<String, Object> zipContent = null;
			try {
				Map<String, String> userProperties = new HashMap<>();
				// Add user properties to map
				userProperties.put(TAX_ID_KEY, user.getTaxId());
				userProperties.put(USERNAME_KEY, user.getUserName());
				
				//cache the content of the uploaded zip file into a byte array so it can be processed and saved
				//as two separate files: the converted-to-xml version and the original version (for troubleshooting)
				ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			    int nRead;
			    byte[] data = new byte[1024];
			    while ((nRead = request.getInputStream().read(data, 0, data.length)) != -1) {
			        buffer.write(data, 0, nRead);
			    }
			    buffer.flush();
			    zipData = buffer.toByteArray();
				
				zipContent = processZipFile(new ByteArrayInputStream(zipData), GUUID, companyName, userProperties);
			} catch (IOException ex) {
				logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
						"ZipException  occured while authenticating userName ", ex);
				status.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
						"CODE_FAILURE_INTERNAL"));
				status.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
						"MSG_ZIP_FAILURE"));
				responseData = status.formatJSONString().getBytes(Charset.forName("UTF-8"));
				responseBuilder.setResponseCode(configManager
						.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
				responseBuilder.setResponseMsg(configManager
						.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_ZIP_FAILURE"));
				responseBuilder.setResponseObject(responseData);
				response = buildRESTResponse(responseBuilder, 0);
				return response;
			}
			// configManager takes a param1->filename, param2 ->property within
			// file, returns the property value
			zipContent.put("ICF_BASE_DIRECTORY", configManager
					.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY, RebateApiConstant.ICF_BASE_DIRECTORY));
			zipContent.put("MPORTAL_BASE_DIRECTORY", configManager.getPropertyAsString(
					RebateApiConstant.REBATE_API_PROPERTY, RebateApiConstant.MPORTAL_BASE_DIRECTORY));
			zipContent.put("companyName", companyName);
			// File creation begins in separate threads
			// First start thread to create "processed" zip file
			CreateFileThread createFileThread = new CreateFileThread(zipContent);
			Thread thread1 = new Thread(createFileThread);
			thread1.start();

//The following thread process is commented out in case it's needed for future trouble-shooting purposes.
//The presence of PII in the .zip->.json file precludes archiving the uploaded zip file in a prod environment.
			// Now start thread to create the original "un-processed" zip file
//			ArchiveUploadedFileThread archiveFileThread = 
//					new ArchiveUploadedFileThread(zipData, 
//							(String)zipContent.get("ICF_BASE_DIRECTORY"),
//							(String) zipContent.get("companyName"),
//							(String) zipContent.get("zipName"));
//			Thread thread2 = new Thread(archiveFileThread);
//			thread2.start();
			
			BStatus bStatus = new BStatus();
			bStatus.setStatusCode(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
			bStatus.setStatusMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_ZIP_SUCCESS"));
			responseData = bStatus.formatJSONString().getBytes(Charset.forName("UTF-8"));
			responseBuilder.setResponseCode(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_ZIP_SUCCESS"));
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
					"File Uploaded successfully for user " + user.getUserName());
		} catch (InvalidDataException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"InvalidDataException  occured while validating userName ", e);
			responseBuilder.setResponseCode(configManager.getPropertyAsString(
					RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INVALID_USER_PASSWORD"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE"));
		} catch (UserNameNotFoundException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"UserNameNotFoundException  occured while authenticating userName ", e);
			responseBuilder.setResponseCode(configManager.getPropertyAsString(
					RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INVALID_USER_PASSWORD"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE"));
		} catch (InActiveUserException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"InActiveUserException  occured while authenticating userName ", e);
			responseBuilder.setResponseCode(configManager
					.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INACTIVE_USER"));
			responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
					"MSG_FAILURE_INACTIVE_USER"));
		} catch (UserAuthenticationException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"UserAuthenticationException  occured while authenticating userName ", e);
			responseBuilder.setResponseCode(configManager.getPropertyAsString(
					RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INVALID_USER_PASSWORD"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE"));
		} catch (UserBlockedException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"UserBlockedException  occured while authenticating userName ", e);
			responseBuilder.setResponseCode(configManager.getPropertyAsString(
					RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INVALID_USER_PASSWORD"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE"));
		}
		/*
		 * catch (ZipException e) { logger.log(IMPLogger.ERROR, THIS_CLASS,
		 * THIS_METHOD, "ZipException  occured while authenticating userName " ,
		 * e);
		 * responseBuilder.setResponseCode(configManager.getPropertyAsString(
		 * RebateApiConstant.REBATE_MESSAGES_PROPERTY,
		 * "CODE_FAILURE_INTERNAL"));
		 * responseBuilder.setResponseMsg(configManager.getPropertyAsString(
		 * RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_ZIP_FAILURE")); }
		 */
		catch (JsonNotPresentException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "ZipException  occured while authenticating userName ",
					e);
			responseBuilder.setResponseCode(configManager
					.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
			responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
					"MSG_JSON_UNAVAILABLE_FAILURE"));
		} catch (InternalServerException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"InternalServerException  occured while authenticating userName ", e);
			responseBuilder.setResponseCode(configManager
					.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_FAILURE"));
		} catch (Exception ex) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while uploading zip file:", ex);
			responseBuilder.setResponseCode(configManager
					.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_FAILURE"));
		} catch (OutOfMemoryError e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"InternalServerException  occured while authenticating userName ", e);
			responseBuilder.setResponseCode(configManager
					.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
			responseBuilder.setResponseMsg(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_FAILURE"));
		}
		responseBuilder.setResponseObject(responseData);
		response = buildRESTResponse(responseBuilder, 0);
		return response;
	}

	/**
	 * Get module fields
	 * 
	 * @param request
	 *            - HttpServletRequest
	 * @param companyName
	 *            - Get module fields for this company
	 * @return response contains success or failure code and messages
	 * @throws Exception
	 */
	public Response getModuleFields(@Context HttpServletRequest request, String companyName) throws Exception {
		Response response = null;
		byte[] responseData = null;
		BuildResponse responseBuilder = new BuildResponse();
		final String THIS_METHOD = "getModuleFields";
		try {
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Getting module fields begins");
			String filePath = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
					RebateApiConstant.MODULE_PATH);
			if (UtilManager.checkFileExisits(filePath, companyName)) {
				filePath = filePath.replace("${companyname}", companyName);
			} else {
				filePath = filePath.replace("${companyname}", "default");
			}
			String moduleFields = UtilManager.getModuleData(filePath);
			responseData = moduleFields.getBytes(Charset.forName("UTF-8"));
			responseBuilder.setResponseCode(
					configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
			responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
					"MSG_CONFIG_SUCCESS"));
		} catch (Exception ex) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while uploading zip file:", ex);
		}
		responseBuilder.setResponseObject(responseData);
		response = buildRESTResponse(responseBuilder, 0);
		return response;
	}

	public IMPLogger getLogger() {
		return logger;
	}

	public void setLogger(IMPLogger logger) {
		this.logger = logger;
	}

	public ConfigurationManager getConfigManager() {
		return configManager;
	}

	public void setConfigManager(ConfigurationManager configManager) {
		this.configManager = configManager;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	
	public void sendTACs(FormData formObject, byte[] tacImage, String imageType, String userName){
		final String THIS_METHOD = "sendTACs";
		
		try {
			Company utility = companyManager.getCompanyByName(formObject.getContractorDetails().getCompany());
			if (utility != null) {
				String[] emailTo = new String[3];
				if (utility.getSendToContractor()) {
					// emailTo[0] = formObject.getContractorDetails().getEmail();
					if (new AbstractValidator().validateEmail(userName)) {
						emailTo[0] = userName;
					}
					else {
						logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "The username: " + userName + " is not a valid email... Sending error.");
						emailUser.sendUsernameNotValidEmail(userName);
					}
				}
				if (utility.getSendToPremise()) {
					emailTo[1] = formObject.getCustomerDetails().getEmail();
				}
				if (utility.getSendToPrimary() && formObject.getAppDetails().getRebateToDifferentPerson().equals("true")) {
					emailTo[2] = formObject.getCustomerDetails().getCustomerAddress().getEmail();
				}
				
				logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "terms and conditions: " + tacImage);
				
				emailUser.sendTACs(emailTo, tacImage, imageType);
			}
		} catch (InternalServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Get json content as string
	 * 
	 * @param inputStream
	 * @throws IOException 
	 */
	private StringBuffer getJsonInputAsStringBuffer(ZipInputStream inputStream) throws IOException
	{
		final String THIS_METHOD = "getJsonInput";
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
		try {
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Converting form_data.json to JAXB classes.");
			br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD, "Input Form data = " + sb.toString());
		} catch (IOException e) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while processing zip file:", e);
			throw e;
		}
			return sb;
	}
	
	/**
	 * Demarshals json
	 * 
	 * @param inputStream - zip input stream
	 * @param GUUID - unique identifier
	 * @param companyName
	 * @throws IOException 
	 */
	
	private FormData demarshalJson(StringBuffer buffer) throws IOException
	{
		FormData formData = null;
		final String THIS_METHOD = "demarshalJson";
		Gson gson = new Gson();
		logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD, "Input Form data = " + buffer.toString());
		formData = gson.fromJson(buffer.toString(), FormData.class);
		return formData;
	}
	
	/**
	 * Convert Json to xml file
	 * 
	 * @param inputStream
	 *            - zip input stream
	 * @param GUUID
	 *            -- Unique Identifier
	 * @param companyName
	 * @return xml and zip file name
	 * @throws JAXBException
	 * @throws IOException
	 */
	private String convertJsonToXml(FormData formObject, StringBuffer buffer, String GUUID, String companyName, Map<String, String> userProperties)
			throws JAXBException, IOException {
			
			String ZipName = null;
			final String THIS_METHOD = "convertJsonToXml";
			try {
			ZipName = mapJsonToXml(formObject, GUUID, companyName, userProperties);
			if (Boolean.parseBoolean(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
					RebateApiConstant.STORE_FORM_DATA_JSON))) {
				UtilManager.createParentDirectory(configManager.getPropertyAsString(
						RebateApiConstant.REBATE_API_PROPERTY, RebateApiConstant.TEMP_FILE_LOCATION));
				String jsonLocation = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
						RebateApiConstant.TEMP_FILE_LOCATION) + ZipName + ".json";
				BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(jsonLocation)));
				bwr.write(buffer.toString());
				bwr.flush();
				bwr.close();
			}
		} catch (IOException ex) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while processing zip file:", ex);
			throw ex;
		} catch (JAXBException ex) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while processing zip file:", ex);
			throw ex;
		}
		return ZipName;
	}

	/**
	 * Marshall jaxb classes to xml
	 * 
	 * @param formObject
	 *            - form_data.json equivalent java bean
	 * @param GUUID
	 *            - unique identifier
	 * @param companyName
	 * @return xml and zip file name
	 * @throws JAXBException
	 */
	public String mapJsonToXml(FormData formObject, String GUUID, String companyName, Map<String, String> userProperties) throws JAXBException {
		final String THIS_METHOD = "mapJsonToXml";
		JAXBContext jc = JAXBContext.newInstance("com.icf.rebate.api.stub");
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		String prepareXmlName = companyName + "_" + formObject.getCustomerDetails().getLastName()
				+ UtilManager.getRemoteFileName() + "_" + GUUID;
		SystemRecord systemRecord = new SystemRecord();
		ImportRecord importRecord = new ImportRecord();

		//capture today's date to be used as submission/application date
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
		String today = sdf.format(Calendar.getInstance().getTime()); 
		
		// Program number support
		String utilityCompanyName = formObject.getContractorDetails().getCompany();
		logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "utility company name= " + utilityCompanyName);
		try {
			importRecord.setProgramNumber(companyManager.getCompanyByName(utilityCompanyName).getProgramNumber());
		} catch (InternalServerException e) {
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
		               "InternalServerException occured while retrieving company." + e.getMessage(), e);
			importRecord.setProgramNumber("Internal server error occured");
		}
		importRecord.setRefID(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
				RebateApiConstant.REFERENCE_ID_PREFIX) + "_" + GUUID);
		importRecord.setCustomerFirstname(formObject.getCustomerDetails().getFirstName());
		importRecord.setCustomerLastname(formObject.getCustomerDetails().getLastName());
		// Added 11/17/2015
		importRecord.setStartDate(formObject.getAppDetails().getStartDate());
		importRecord.setSubmissionDate(today);
		importRecord.setApplicationDate(today);
		// ------------------------------------------------------------------------------------------

		importRecord.setCustomerAccountNumber(formObject.getCustomerDetails().getAccountNumber());
		importRecord.setCustomerAccountNumber2(formObject.getCustomerDetails().getAccountNumber2());
		importRecord.setContractorAccountNumber(formObject.getContractorDetails().getContractorAccountNumber());
		importRecord.setPremiseContactId(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
				RebateApiConstant.REFERENCE_ID_PREFIX) + "_" + GUUID + "_prem");
		importRecord.setContractorContactId(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
				RebateApiConstant.REFERENCE_ID_PREFIX) + "_" + GUUID + "_ctr");
		if (formObject.getAppDetails().getRebateToDifferentPerson().equals("true")
				&& formObject.getCustomerDetails().getCustomerAddress().getFirstName() != null
				&& formObject.getCustomerDetails().getCustomerAddress().getFirstName().trim().length() > 0) {
			importRecord.setPrimaryContactId(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
					RebateApiConstant.REFERENCE_ID_PREFIX) + "_" + GUUID + "_prim");
		} 
		importRecord.setStatus("Application Received");

		// setting details for contractor
		ImportContacts importContacts = new ImportContacts();
		ImportContact importContactContractor = new ImportContact();
		if (formObject.getContractorDetails() != null) {
			importContactContractor
					.setContactRefId(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
							RebateApiConstant.REFERENCE_ID_PREFIX) + "_" + GUUID + "_ctr");
			importContactContractor.setContactType("Contractor");
			importContactContractor.setUserName(userProperties.get(USERNAME_KEY));
			importContactContractor.setTaxId(userProperties.get(TAX_ID_KEY));
			importContactContractor.setContractorAccountNumber(formObject.getContractorDetails().getContractorAccountNumber());
			importContactContractor.setFirstName(formObject.getContractorDetails().getFirstName());
			importContactContractor.setLastName(formObject.getContractorDetails().getLastName());
			importContactContractor.setCompany(formObject.getContractorDetails().getContractorCompany());
			// importContactContractor.setAddress(formObject.getContractorDetails().getAddress());
			// importContactContractor.setContractorCompany(formObject.getContractorDetails().getContractorCompany());
			importContactContractor.setAddress1(formObject.getContractorDetails().getAddress1());
			importContactContractor.setAddress2(formObject.getContractorDetails().getAddress2());
			importContactContractor.setCity(formObject.getContractorDetails().getCity());
			importContactContractor.setState(formObject.getContractorDetails().getState());
			/*
			 * if (formObject.getContractorDetails().getZip() != null &&
			 * formObject.getContractorDetails().getZip().length() >0) {
			 * importContactContractor.setZip(Integer.parseInt(formObject.
			 * getContractorDetails().getZip())); } else {
			 * importContactContractor.setZip(0); }
			 */
			importContactContractor.setZip(formObject.getContractorDetails().getZip());
			importContactContractor.setCell(formObject.getContractorDetails().getCell());
			importContactContractor.setFax("");
			importContactContractor.setPhone(formObject.getContractorDetails().getPhone());
			importContactContractor.setEmail(formObject.getContractorDetails().getEmail());
			importContactContractor.setAddToDropDown("Yes");
		}
		importContacts.getImportContact().add(importContactContractor);
		
		// setting details for consumer
		ImportContact importContactConsumer = new ImportContact();
		importContactConsumer.setContactRefId(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
				RebateApiConstant.REFERENCE_ID_PREFIX) + "_" + GUUID + "_prem");
		importContactConsumer.setContactType("Premise");
		importContactConsumer.setFirstName(formObject.getCustomerDetails().getFirstName());
		importContactConsumer.setLastName(formObject.getCustomerDetails().getLastName());
		importContactConsumer.setAddress(formObject.getCustomerDetails().getAddress());
		importContactConsumer.setCity(formObject.getCustomerDetails().getCity());
		importContactConsumer.setState(formObject.getCustomerDetails().getState());
		importContactConsumer.setZip(formObject.getCustomerDetails().getZip());
		importContactConsumer.setAccountNumber(formObject.getCustomerDetails().getAccountNumber());
		importContactConsumer.setAccountNumber2(formObject.getCustomerDetails().getAccountNumber2());
		importContactConsumer.setCell(formObject.getCustomerDetails().getCell());
		importContactConsumer.setPhone(formObject.getCustomerDetails().getPhone());
		importContactConsumer.setFax("");
		importContactConsumer.setEmail(formObject.getCustomerDetails().getEmail());
		importContactConsumer.setGpsAddress(formObject.getCustomerDetails().getGpsAddress());
		importContactConsumer.setGeoLat(formObject.getCustomerDetails().getGeoLat());
		importContactConsumer.setGeoLong(formObject.getCustomerDetails().getGeoLong());
		importContactConsumer.setAddToDropDown("Yes");
		importContacts.getImportContact().add(importContactConsumer);
		
		// setting details for premise if he exists
		if (formObject.getAppDetails().getRebateToDifferentPerson().equals("true")
				&& formObject.getCustomerDetails().getCustomerAddress().getFirstName() != null
				&& formObject.getCustomerDetails().getCustomerAddress().getFirstName().trim().length() > 0) {

			ImportContact importContactPremise = new ImportContact();
			importContactPremise
					.setContactRefId(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
							RebateApiConstant.REFERENCE_ID_PREFIX) + "_" + GUUID + "_prim");
			importContactPremise.setContactType("Primary");
			importContactPremise.setFirstName(formObject.getCustomerDetails().getCustomerAddress().getFirstName());
			importContactPremise.setLastName(formObject.getCustomerDetails().getCustomerAddress().getLastName());
			importContactPremise.setAddress(formObject.getCustomerDetails().getCustomerAddress().getAddress());
			importContactPremise.setCity(formObject.getCustomerDetails().getCustomerAddress().getCity());
			importContactPremise.setState(formObject.getCustomerDetails().getCustomerAddress().getState());
			importContactPremise.setZip(formObject.getCustomerDetails().getCustomerAddress().getZip());
			importContactPremise.setPhone(formObject.getCustomerDetails().getCustomerAddress().getPhone());
			importContactPremise.setEmail(formObject.getCustomerDetails().getCustomerAddress().getEmail());
			importContacts.getImportContact().add(importContactPremise);
		}

		ImportApplication importApplication = new ImportApplication();

		ImportApplicationItem applicationStartDate = new ImportApplicationItem();
		applicationStartDate.setField("startDate");
		applicationStartDate.setValue(formObject.getAppDetails().getStartDate());
		importApplication.getImportApplicationItem().add(applicationStartDate);

		ImportApplicationItem applicationSubmitDate = new ImportApplicationItem();
		applicationSubmitDate.setField("submissionDate");
		applicationSubmitDate.setValue(today);
		importApplication.getImportApplicationItem().add(applicationSubmitDate);

		ImportApplicationItem applicationDate = new ImportApplicationItem();
		applicationDate.setField("applicationDate");
		applicationDate.setValue(today);
		importApplication.getImportApplicationItem().add(applicationDate);

		ImportApplicationItem heatingCoolingSystem = new ImportApplicationItem();
		heatingCoolingSystem.setField("heatingCoolingSystem");
		heatingCoolingSystem.setValue(formObject.getAppDetails().getHeatingCoolingSelection());
		importApplication.getImportApplicationItem().add(heatingCoolingSystem);
		
		// Building type
		ImportApplicationItem buildingType = new ImportApplicationItem();
		buildingType.setField("buildingType");
		buildingType.setValue(formObject.getAppDetails().getBuildingTypeSelection());
		importApplication.getImportApplicationItem().add(buildingType);

		// Heating cooling fuel type
		ImportApplicationItem heatingCoolingFuelType = new ImportApplicationItem();
		heatingCoolingFuelType.setField("heatingCoolingFuelType");
		heatingCoolingFuelType.setValue(formObject.getAppDetails().getHeatingCoolingFuelTypeSelection());
		importApplication.getImportApplicationItem().add(heatingCoolingFuelType);
		
		// Central a/c
		ImportApplicationItem centralAC = new ImportApplicationItem();
		centralAC.setField("centralAC");
		centralAC.setValue(formObject.getAppDetails().getCentralACSelection());
		importApplication.getImportApplicationItem().add(centralAC);
		
		// Water heating fuel type
		ImportApplicationItem waterHeatingFuelType = new ImportApplicationItem();
		waterHeatingFuelType.setField("waterHeatingFuelType");
		waterHeatingFuelType.setValue(formObject.getAppDetails().getWaterHeatingFuelTypeSelection());
		importApplication.getImportApplicationItem().add(waterHeatingFuelType);
		
		ImportApplicationItem customerTypeItem = new ImportApplicationItem();
		customerTypeItem.setField("customerType");
		customerTypeItem.setValue(formObject.getAppDetails().getCustomerType());
		importApplication.getImportApplicationItem().add(customerTypeItem);
		ImportApplicationItem houseTypeItem = new ImportApplicationItem();
		houseTypeItem.setField("houseType");
		houseTypeItem.setValue(formObject.getAppDetails().getHouseType());
		importApplication.getImportApplicationItem().add(houseTypeItem);
		ImportApplicationItem rebateToCurrentBillItem = new ImportApplicationItem();
		rebateToCurrentBillItem.setField("rebateToCurrentBill");
		rebateToCurrentBillItem.setValue(formObject.getAppDetails().getRebateToCurrentBill());
		importApplication.getImportApplicationItem().add(rebateToCurrentBillItem);
		ImportApplicationItem rebateToUtilityItem = new ImportApplicationItem();
		rebateToUtilityItem.setField("rebateToUtility");
		rebateToUtilityItem.setValue(formObject.getAppDetails().getRebateToUtility());
		importApplication.getImportApplicationItem().add(rebateToUtilityItem);
		ImportApplicationItem rebateToDifferentPersonItem = new ImportApplicationItem();
		rebateToDifferentPersonItem.setField("rebateToDifferentPerson");
		rebateToDifferentPersonItem.setValue(formObject.getAppDetails().getRebateToDifferentPerson());
		importApplication.getImportApplicationItem().add(rebateToDifferentPersonItem);
		ImportApplicationItem rebateAmountValueItem = new ImportApplicationItem();
		rebateAmountValueItem.setField("rebateAmountValue");
		rebateAmountValueItem.setValue(formObject.getAppDetails().getRebateAmountValue());
		importApplication.getImportApplicationItem().add(rebateAmountValueItem);
		ImportApplicationItem rebateIncentiveDecisionItem = new ImportApplicationItem();
		rebateIncentiveDecisionItem.setField("rebateIncentiveDecision");
		rebateIncentiveDecisionItem.setValue(formObject.getAppDetails().getRebateIncentiveDecision());
		importApplication.getImportApplicationItem().add(rebateIncentiveDecisionItem);
		ImportApplicationItem approximateSizeofHouseItem = new ImportApplicationItem();
		approximateSizeofHouseItem.setField("approximateSizeofHouse");
		approximateSizeofHouseItem.setValue(formObject.getAppDetails().getApproximateSizeofHouse());
		importApplication.getImportApplicationItem().add(approximateSizeofHouseItem);
		
		ImportApplicationItem basementItem = new ImportApplicationItem();
		basementItem.setField("basement");
		basementItem.setValue(formObject.getAppDetails().getBasement());
		importApplication.getImportApplicationItem().add(basementItem);
		
		// New construction
		ImportApplicationItem newConstructionItem = new ImportApplicationItem();
		newConstructionItem.setField("newConstruction");
		newConstructionItem.setValue(formObject.getAppDetails().getNewConstruction());
		importApplication.getImportApplicationItem().add(newConstructionItem);
		
		// Permit number
		ImportApplicationItem permitNumber = new ImportApplicationItem();
		permitNumber.setField("permitNumber");
		permitNumber.setValue(formObject.getAppDetails().getPermitNumber());
		importApplication.getImportApplicationItem().add(permitNumber);
		
		//auditorCompanyName
		ImportApplicationItem auditorCompanyName = new ImportApplicationItem();
		auditorCompanyName.setField("auditorCompanyName");
		auditorCompanyName.setValue(formObject.getAppDetails().getAuditorCompanyNameSelection());
		importApplication.getImportApplicationItem().add(auditorCompanyName);
		
		//distributor
		ImportApplicationItem distributor = new ImportApplicationItem();
		distributor.setField("distributor");
		distributor.setValue(formObject.getAppDetails().getDistributorSelection());
		importApplication.getImportApplicationItem().add(distributor);
		
		//year built (range of years)
		ImportApplicationItem approximateYearHouseBuiltItem = new ImportApplicationItem();
		approximateYearHouseBuiltItem.setField("approximateYearHouseBuilt");
		String yearBuilt = formObject.getAppDetails().getApproximateYearHouseBuilt();
		if (yearBuilt == null || yearBuilt.isEmpty()) {
			yearBuilt = formObject.getAppDetails().getYearBuiltSelection();
		}
		approximateYearHouseBuiltItem.setValue(yearBuilt);
		importApplication.getImportApplicationItem().add(approximateYearHouseBuiltItem);

		//Following block not verified for production yet
		//date installed
		ImportApplicationItem dateInstalled = new ImportApplicationItem();
		dateInstalled.setField("dateInstalled");
		dateInstalled.setValue(formObject.getAppDetails().getDateInstalled());
		importApplication.getImportApplicationItem().add(dateInstalled);
		
		//deviceType
		ImportApplicationItem deviceType = new ImportApplicationItem();
		deviceType.setField("deviceType");
		deviceType.setValue(formObject.getAppDetails().getDeviceType());
		importApplication.getImportApplicationItem().add(deviceType);
		
		//mobileAppVersion
		ImportApplicationItem mobileAppVersion = new ImportApplicationItem();
		mobileAppVersion.setField("mobileAppVersion");
		mobileAppVersion.setValue(formObject.getAppDetails().getMobileAppVersion());
		importApplication.getImportApplicationItem().add(mobileAppVersion);
		
		//Technician Info
		ImportApplicationItem techNameItem = new ImportApplicationItem();
		techNameItem.setField("technicianName");
		techNameItem.setValue(formObject.getCustomerDetails().getTechnicianName());
		importApplication.getImportApplicationItem().add(techNameItem);
		
		ImportApplicationItem techIdItem = new ImportApplicationItem();
		techIdItem.setField("technicianId");
		techIdItem.setValue(formObject.getCustomerDetails().getTechnicianId());
		importApplication.getImportApplicationItem().add(techIdItem);

		ImportApplicationItem techPhoneItem = new ImportApplicationItem();
		techPhoneItem.setField("technicianPhoneNumber");
		techPhoneItem.setValue(formObject.getCustomerDetails().getTechnicianPhone());
		importApplication.getImportApplicationItem().add(techPhoneItem);
		
		//Project Info
		ImportApplicationItem projIncentiveItem = new ImportApplicationItem();
		projIncentiveItem.setField("projectIncentive");
		projIncentiveItem.setValue(formObject.getCustomerDetails().getCustomerIncentive());
		importApplication.getImportApplicationItem().add(projIncentiveItem);
		
		ImportApplicationItem projTypeItem = new ImportApplicationItem();
		projTypeItem.setField("projectTypes");
		projTypeItem.setValue(formObject.getAppDetails().getProjectTypeSelection());
		importApplication.getImportApplicationItem().add(projTypeItem);
		
		// Comments
		ImportApplicationItem commentsItem = new ImportApplicationItem();
		commentsItem.setField("comments");
		commentsItem.setValue(formObject.getAppDetails().getComments());
		importApplication.getImportApplicationItem().add(commentsItem);

		// Document Later
		ImportApplicationItem documentLaterItem = new ImportApplicationItem();
		documentLaterItem.setField("documentLater");
		documentLaterItem.setValue(formObject.getAppDetails().getDocumentLater());
		importApplication.getImportApplicationItem().add(documentLaterItem);

		// Order Number
		ImportApplicationItem orderNumberItem = new ImportApplicationItem();
		orderNumberItem.setField("orderNumber");
		orderNumberItem.setValue(formObject.getAppDetails().getOrderNumber());
		importApplication.getImportApplicationItem().add(orderNumberItem);

		if (formObject.getCustomerDetails() != null) {
			if (StringUtils.isNotBlank(formObject.getCustomerDetails().getGpsAddress())) {
				ImportApplicationItem gpsAdress = new ImportApplicationItem();
				gpsAdress.setField("gpsAddress");
				gpsAdress.setValue(formObject.getCustomerDetails().getGpsAddress());
				importApplication.getImportApplicationItem().add(gpsAdress);
			}

			if (StringUtils.isNotBlank(formObject.getCustomerDetails().getGeoLat())) {
				ImportApplicationItem geolat = new ImportApplicationItem();
				geolat.setField("geoLat");
				geolat.setValue(formObject.getCustomerDetails().getGeoLat());
				importApplication.getImportApplicationItem().add(geolat);
			}

			if (StringUtils.isNotBlank(formObject.getCustomerDetails().getGeoLong())) {
				ImportApplicationItem geolong = new ImportApplicationItem();
				geolong.setField("geoLong");
				geolong.setValue(formObject.getCustomerDetails().getGeoLong());
				importApplication.getImportApplicationItem().add(geolong);
			}

		}
		
		
	   // ---------------------------------- Set survey data ---------------------------------
		ImportSurvey importSurvey = new ImportSurvey();
		List<Submission> items = formObject.getSubmission();
		if (items != null && items.size() > 0) {
			// Debug
			for (Submission s : items) {
				logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Submission item: " + s.toString());
			}
			// Find survey
			int index = items.indexOf(new Submission(Submission.SURVEY_ID));
			if (index != -1) {
				logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Found surveyDetails");
				Submission survey = items.get(index);
				List<SubmissionItem> surveyItems = survey.getItems();
				// See form_data.json surveyDetail should be at index 0 for SubmissionItem list
				if (surveyItems != null && surveyItems.get(0) != null) {
					Category surveyDetail = surveyItems.get(0).getSurveyDetail();
					if (surveyDetail != null && surveyDetail.getParts() != null) {
							SurveyAttributes surveyAttributes = new SurveyAttributes();
							prepareAttributes(surveyDetail.getParts(), surveyAttributes);
							if (surveyAttributes.getAttribute() != null && surveyAttributes.getAttribute().size() > 0) {
								importSurvey.setImportSurveyAttributes(surveyAttributes);
							}
					}
				}
			}
		}
		// ----------------------------------------------------------------------------------
		
		importRecord.setImportContacts(importContacts);
		importRecord.setImportApplication(importApplication);
		importRecord.setImportSurvey(importSurvey);
		importRecord.setImportEquipment(getImportEquipment(formObject, GUUID));
		systemRecord.getImportRecord().add(importRecord);
		this.stringWriter = new StringWriter();
		marshaller.marshal(systemRecord, stringWriter);
		System.out.println("Created xml");
		return prepareXmlName;
	}

	/**
	 * Prepare equipment list
	 * 
	 * @param formObject
	 * @param GUUID
	 * @return
	 */
	private ImportEquipment getImportEquipment(FormData formObject, String GUUID) {
		final String THIS_METHOD = "getImportEquipment";
		
		ImportEquipment importEquipment = new ImportEquipment();
		if (formObject.getEquipments() != null && formObject.getEquipments().size() > 0) {
			List<Equipment> equipmentList = formObject.getEquipments();
			for (Equipment equipment : equipmentList) {
				List<Item> itemList = equipment.getItems();
				int i = 1;
				for (Item item : itemList) {
					ImportEquipmentItem importEquipmentItem = new ImportEquipmentItem();
					importEquipmentItem.setEquipmentRefId(equipment.getName() + "_" + i + "_" + GUUID);
					EquipmentAttributes equipmentAttributes = new EquipmentAttributes();
					Category replaceOnFail = item.getReplaceOnFail();
					List<Part> replaceOnFailPartList = replaceOnFail.getParts();
					prepareAttributes(replaceOnFailPartList, equipmentAttributes);
					if (item.getEarlyRetirement() != null) {
						Category earlyRetirement = item.getEarlyRetirement();
						List<Part> earlyRetirementPartList = earlyRetirement.getParts();
						prepareAttributes(earlyRetirementPartList, equipmentAttributes);
					}
					importEquipmentItem.setEquipmentAttributes(equipmentAttributes);
					importEquipment.getImportEquipmentItem().add(importEquipmentItem);
					i++;

					// As per mail from Jonathan.Lewis for every equipment we
					// must hardcode an attribute name as "quantity"and value as
					// "1"
					Attribute attribute = new Attribute();
					attribute.setAttributeName("quantity");
					attribute.setAttributeValue("1");
					importEquipmentItem.getEquipmentAttributes().getAttribute().add(attribute);
				}
			}
		}
		// KVB 12/9/2015 - set insulation data
		if (formObject.getInsulation() != null && formObject.getInsulation().size() > 0) {
			List<Insulation> insulationList = formObject.getInsulation();
			for (Insulation insulation : insulationList) {
				List<InsulationItem> items = insulation.getItems();
				int i = 1;
				for (InsulationItem item : items) {
					ImportEquipmentItem importInsulationItem = new ImportEquipmentItem();
					importInsulationItem.setEquipmentRefId(insulation.getName() + "_" + i + "_" + GUUID);
					EquipmentAttributes insulationAttributes = new EquipmentAttributes();
					Category insulationDetail = item.getInsulationDetail();
					List<Part> parts = insulationDetail.getParts();
					prepareAttributes(parts, insulationAttributes);
					if (insulationAttributes.getAttribute() != null && insulationAttributes.getAttribute().size() > 0) {
						importInsulationItem.setEquipmentAttributes(insulationAttributes);
						importEquipment.getImportEquipmentItem().add(importInsulationItem);
						i++;
					}
				}
			}
		}
		// KVB 12/21/2015 - set window and door data
		if (formObject.getWindowAndDoor() != null && formObject.getWindowAndDoor().size() > 0) {
			List<WindowAndDoor> wdList = formObject.getWindowAndDoor();
			for (WindowAndDoor wd : wdList) {
				List<WDItem> items = wd.getItems();
				int i = 1;
				for (WDItem item : items) {
					ImportEquipmentItem importWDItem = new ImportEquipmentItem();
					importWDItem.setEquipmentRefId(wd.getName() + "_" + i + "_" + GUUID);
					EquipmentAttributes wdAttributes = new EquipmentAttributes();
					Category wdDetail = item.getWindowDoorDetail();
					List<Part> parts = wdDetail.getParts();
					prepareAttributes(parts, wdAttributes);
					if (wdAttributes.getAttribute() != null && wdAttributes.getAttribute().size() > 0) {
						importWDItem.setEquipmentAttributes(wdAttributes);
						importEquipment.getImportEquipmentItem().add(importWDItem);
						i++;
					}
				}
			}
		}
		// set air & duct sealing data
		for (AirDuctSealingItem.SealingType typ : AirDuctSealingItem.SealingType.values()) {
			List<AirDuctSealing> sealingList = null;
			switch (typ) {
			case AIR: sealingList = formObject.getAirSealing(); break;
			case DUCT: sealingList = formObject.getDuctSealing(); break;
			}
			if (sealingList != null && sealingList.size() > 0) {
				for (AirDuctSealing sealing : sealingList) {
					List<AirDuctSealingItem> items = sealing.getItems();
					int i = 1;
					for (AirDuctSealingItem item : items) {
						ImportEquipmentItem importSealingItem = new ImportEquipmentItem();
						importSealingItem.setEquipmentRefId(sealing.getName() + "_" + i + "_" + GUUID);
						EquipmentAttributes sealingAttributes = new EquipmentAttributes();
						Category sealingDetail = item.getDetail(typ);
						List<Part> parts = sealingDetail.getParts();
						prepareAttributes(parts, sealingAttributes);
						if (sealingAttributes.getAttribute() != null && sealingAttributes.getAttribute().size() > 0) {
							importSealingItem.setEquipmentAttributes(sealingAttributes);
							importEquipment.getImportEquipmentItem().add(importSealingItem);
							i++;
						}
					}
				}
			}
		}
//		// set duct sealing data
//		if (formObject.getDuctSealing() != null && formObject.getDuctSealing().size() > 0) {
//			List<ImportEquipmentItem> ductImportEquipmentItemList = populateAirDuctSealingData(formObject, GUUID, DUCT_SEALING);
//			for (ImportEquipmentItem ductEquipment : ductImportEquipmentItemList) {
//				importEquipment.getImportEquipmentItem().add(ductEquipment);
//			}
//		}
//		// set air sealing data
//		if (formObject.getAirSealing() != null && formObject.getAirSealing().size() > 0) {
//			List<ImportEquipmentItem> airImportEquipmentItemList = populateAirDuctSealingData(formObject, GUUID, AIR_SEALING);
//			for (ImportEquipmentItem airEquipment : airImportEquipmentItemList) {
//				importEquipment.getImportEquipmentItem().add(airEquipment);
//			}
//		}
		// set qualityInstallVerification data
		if (formObject.getQualityInstallVerification() != null
				&& formObject.getQualityInstallVerification().size() > 0) {
			List<ImportEquipmentItem> qualityInstallVerificationItemList = populateQualityInstallVerificationData(
					formObject, GUUID);
			for (ImportEquipmentItem qivEquipment : qualityInstallVerificationItemList) {
				importEquipment.getImportEquipmentItem().add(qivEquipment);
			}
		}
		// set tune up data
		/*if (formObject.getTuneUp() != null && formObject.getTuneUp().size() > 0) {
			List<ImportEquipmentItem> tuneUpItemList = populateTuneUpData(formObject, GUUID);
			for (ImportEquipmentItem tuneUpEquipment : tuneUpItemList) {
				importEquipment.getImportEquipmentItem().add(tuneUpEquipment);
			}
		}*/
		// tune up
		logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "formObjectData data: " + formObject.toString());
		if (formObject.getTuneUp() != null && formObject.getTuneUp().size() > 0) {
					List<TuneUpBean> tuneUpList = formObject.getTuneUp();
					if (tuneUpList != null) {
					for (TuneUpBean tuneup : tuneUpList) {
						List<TuneUpItem> items = tuneup.getItems();
						int i = 1;
						if (items != null) {
						for (TuneUpItem item : items) {
							ImportEquipmentItem importItem = new ImportEquipmentItem();
							importItem.setEquipmentRefId(tuneup.getName() + "_" + i + "_" + GUUID);
							EquipmentAttributes attributes = new EquipmentAttributes();
							Split split = item.getSplit();
							Packages packages = item.getPackages();
							List<Part> parts;
							if (split == null) {
								parts = packages.getParts();
							}
							else {
								parts = split.getParts();
							}
							prepareAttributes(parts, attributes);
							if (attributes.getAttribute() != null && attributes.getAttribute().size() > 0) {
								importItem.setEquipmentAttributes(attributes);
								importEquipment.getImportEquipmentItem().add(importItem);
								i++;
							}
						}
					}
					}
		}
				}
		// set thermostatReferral data
		if (formObject.getThermostatReferral() != null && formObject.getThermostatReferral().size() > 0) {
			ImportEquipmentItem referalEquipmentItem = populateThermostatReferralData(formObject, GUUID);
			importEquipment.getImportEquipmentItem().add(referalEquipmentItem);
		}
		// set thermostatInstall data
		if (formObject.getThermostatInstall() != null && formObject.getThermostatInstall().size() > 0) {
			List<ImportEquipmentItem> thermoInstallItemList = populateThermostatInstallData(formObject, GUUID);
			for (ImportEquipmentItem installEquipment : thermoInstallItemList) {
				importEquipment.getImportEquipmentItem().add(installEquipment);
			}
		}
		return importEquipment;
	}

	/**
	 * Prepare duct sealing List
	 * 
	 * @param formObject
	 * @param GUUID
	 * @return ductSealing equipment list
	 */
//	public List<ImportEquipmentItem> populateAirDuctSealingData(FormData formObject, String GUUID, int sealingType) {
//		List<ImportEquipmentItem> airDuctItemList = new ArrayList<ImportEquipmentItem>();
//		ImportEquipmentItem airDuctItem = null;
//		EquipmentAttributes equipmentAttributes = null;
//		List<FormDataBean> airDuctList = null;
//		switch (sealingType) {
//		case DUCT_SEALING:
//			airDuctList = formObject.getDuctSealing();
//			break;
//		case AIR_SEALING:
//			airDuctList = formObject.getAirSealing();
//			break;
//		}
//		int i = 1;
//		for (FormDataBean airDuctBean : airDuctList) {
//			airDuctItem = new ImportEquipmentItem();
//			equipmentAttributes = new EquipmentAttributes();
//			airDuctItem.setEquipmentRefId(airDuctBean.getName() + "_" + i + "_" + GUUID);
//			List<Part> ductPartList = airDuctBean.getParts();
//			prepareAttributes(ductPartList, equipmentAttributes);
//			airDuctItem.setEquipmentAttributes(equipmentAttributes);
//			airDuctItemList.add(airDuctItem);
//			i++;
//		}
//		return airDuctItemList;
//	}

	/**
	 * Prepare Thermostat Install Data
	 * 
	 * @param formObject
	 *            - contains form_data.json details
	 * @param GUUID
	 *            - Unique id for each request
	 * @return equipmentObject list
	 */

	public List<ImportEquipmentItem> populateThermostatInstallData(FormData formObject, String GUUID) {
		List<ImportEquipmentItem> tInsatalltemList = new ArrayList<ImportEquipmentItem>();
		ImportEquipmentItem thermoInstallItem = null;
		EquipmentAttributes equipmentAttributes = null;
		List<FormDataBean> installList = formObject.getThermostatInstall();
		int i = 1;
		for (FormDataBean thermoInstallBean : installList) {
			thermoInstallItem = new ImportEquipmentItem();
			equipmentAttributes = new EquipmentAttributes();
			thermoInstallItem.setEquipmentRefId(thermoInstallBean.getName() + "_" + i + "_" + GUUID);
			List<Part> thermoInstallPartList = thermoInstallBean.getParts();
			prepareAttributes(thermoInstallPartList, equipmentAttributes);
			thermoInstallItem.setEquipmentAttributes(equipmentAttributes);
			tInsatalltemList.add(thermoInstallItem);
			i++;
		}
		return tInsatalltemList;
	}

	/**
	 * Prepare Thermostat Referral Data
	 * 
	 * @param formObject
	 *            - contains form_data.json details
	 * @param GUUID
	 *            - Unique id for each request
	 * @return equipment object
	 */
	public ImportEquipmentItem populateThermostatReferralData(FormData formObject, String GUUID) {
		ImportEquipmentItem referalEquipmentItem = new ImportEquipmentItem();
		EquipmentAttributes equipmentAttributes = new EquipmentAttributes();
		referalEquipmentItem.setEquipmentRefId("thermostatReferral_" + GUUID);
		FormDataBean thermostatReferralBean = formObject.getThermostatReferral().get(0);
		List<Part> thermostatReferralBeanPartList = thermostatReferralBean.getParts();
		prepareAttributes(thermostatReferralBeanPartList, equipmentAttributes);
		referalEquipmentItem.setEquipmentAttributes(equipmentAttributes);
		return referalEquipmentItem;
	}

	/**
	 * Prepare tune up Data
	 * 
	 * @param formObject
	 *            - contains form_data.json details
	 * @param GUUID
	 *            - Unique id for each request
	 * @return equipment object list
	 */

	// Unused after tuneup update
	
	/*	public List<ImportEquipmentItem> populateTuneUpData(FormData formObject, String GUUID) {
		List<ImportEquipmentItem> tuneUpXmlList = new ArrayList<ImportEquipmentItem>();
		ImportEquipmentItem tuneUpItem = null;
		EquipmentAttributes equipmentAttributes = null;
		List<TuneUpBean> tuneUpList = formObject.getTuneUp();
		int i = 1;
		for (TuneUpBean tuneUpBean : tuneUpList) {
			tuneUpItem = new ImportEquipmentItem();
			equipmentAttributes = new EquipmentAttributes();
			tuneUpItem.setEquipmentRefId("tuneUp_" + i + "_" + GUUID);
			List<Part> tuneUpBeanPartList = new ArrayList<Part>();
			// check if available tuneUpbean is of type split or package
			if (tuneUpBean.getSplit() != null && tuneUpBean.getSplit().getParts() != null
					&& tuneUpBean.getSplit().getParts().size() > 0)
				tuneUpBeanPartList = tuneUpBean.getSplit().getParts();
			else if (tuneUpBean.getPackages() != null && tuneUpBean.getPackages().getParts() != null
					&& tuneUpBean.getPackages().getParts().size() > 0)
				tuneUpBeanPartList = tuneUpBean.getPackages().getParts();
			prepareAttributes(tuneUpBeanPartList, equipmentAttributes);
			tuneUpItem.setEquipmentAttributes(equipmentAttributes);
			tuneUpXmlList.add(tuneUpItem);
			i++;
		}
		return tuneUpXmlList;
	}*/

	/**
	 * Prepare Quality Install Verification Data
	 * 
	 * @param formObject
	 *            - contains form_data.json details
	 * @param GUUID
	 *            - Unique id for each request
	 * @return equipment object list
	 */
	public List<ImportEquipmentItem> populateQualityInstallVerificationData(FormData formObject, String GUUID) {
		List<ImportEquipmentItem> qivXmlList = new ArrayList<ImportEquipmentItem>();
		ImportEquipmentItem qualityInstallVerificationItem = null;
		EquipmentAttributes equipmentAttributes = null;
		List<FormDataBean> qivList = formObject.getQualityInstallVerification();
		for (FormDataBean qivBean : qivList) {
			qualityInstallVerificationItem = new ImportEquipmentItem();
			equipmentAttributes = new EquipmentAttributes();
			qualityInstallVerificationItem.setEquipmentRefId(qivBean.getName() + "_" + GUUID);
			List<Part> qivBeanPartList = qivBean.getParts();
			prepareAttributes(qivBeanPartList, equipmentAttributes);
			qualityInstallVerificationItem.setEquipmentAttributes(equipmentAttributes);
			qivXmlList.add(qualityInstallVerificationItem);
		}
		return qivXmlList;
	}

	/**
	 * Prepare attributes names and value based on partlist
	 * 
	 * @param partList
	 *            - contains each equipment part list
	 * @param equipmentAttributes
	 *            - to which set the attributes.
	 */
	private void prepareAttributes(List<Part> partList, EquipmentAttributes equipmentAttributes) {
		if (partList != null) {
			int counter = 1;
			AttributeScrubber scrubber = AttributeScrubber.getInstance();
			for (Part part : partList) {
				List<Options> options = part.getOptions();
				if (options != null && options.size() > 0) {
					for (Options option : options) {
						if (option.getStoreSavedValue() != null && option.getStoreSavedValue().trim().length() > 0) {
							Attribute attribute = new Attribute();
							if (option.getXmlName() != null && option.getXmlName().length() > 0) {
								String xmlName = option.getXmlName();
								if (part.getId() != null && part.getId().equals("newAirHandlerUnit")) {
									xmlName = xmlName + "_" + counter;
								}
								attribute.setAttributeName(xmlName);
								attribute.setAttributeValue(scrubber.scrub(xmlName, option.getStoreSavedValue()));
							} else {
								String xmlName = part.getXmlName();
								if (part.getId() != null && part.getId().equals("newAirHandlerUnit")) {
									xmlName = xmlName + "_" + counter;
								}
								attribute.setAttributeName(xmlName);
								attribute.setAttributeValue(scrubber.scrub(xmlName, option.getStoreSavedValue()));
							}
							equipmentAttributes.getAttribute().add(attribute);
//							For barcode scanned feature
//							if (option.isBarcodeScanned()) {
//								Attribute wasScanned = new Attribute();
//								wasScanned.setAttributeName(option.getXmlName() + "_BarcodeScanned");
//								wasScanned.setAttributeValue(String.valueOf(option.isBarcodeScanned()));
//							}
						}
					}
				}
				if (part.getId() != null && part.getId().equals("newAirHandlerUnit")) {
					counter++;
				}
			}
		}
	}
	
	/**
	 * Prepare attributes names and value based on partlist
	 * 
	 * @param partList
	 *            - contains each equipment part list
	 * @param attributes
	 *            - to which set the attributes.
	 */
	private void prepareAttributes(List<Part> partList, BaseAttributes attributes) {
		if (partList != null) {
			int counter = 1;
			AttributeScrubber scrubber = AttributeScrubber.getInstance();
			for (Part part : partList) {
				List<Options> options = part.getOptions();
				if (options != null && options.size() > 0) {
					for (Options option : options) {
						if (option.getStoreSavedValue() != null && option.getStoreSavedValue().trim().length() > 0) {
							Attribute attribute = new Attribute();
							if (option.getXmlName() != null && option.getXmlName().length() > 0) {
								String xmlName = option.getXmlName();
								if (part.getId() != null && part.getId().equals("newAirHandlerUnit")) {
									xmlName = xmlName + "_" + counter;
								}
								attribute.setAttributeName(xmlName);
								attribute.setAttributeValue(scrubber.scrub(xmlName, option.getStoreSavedValue()));
							} else {
								String xmlName = part.getXmlName();
								if (part.getId() != null && part.getId().equals("newAirHandlerUnit")) {
									xmlName = xmlName + "_" + counter;
								}
								attribute.setAttributeName(xmlName);
								attribute.setAttributeValue(scrubber.scrub(xmlName, option.getStoreSavedValue()));
							}
							attributes.getAttribute().add(attribute);
						}
					}
				}
				if (part.getId() != null && part.getId().equals("newAirHandlerUnit")) {
					counter++;
				}
			}
		}
	}
	
	public ILoginValidator getLoginValidator() {
		return loginValidator;
	}

	public void setLoginValidator(ILoginValidator loginValidator) {
		this.loginValidator = loginValidator;
	}

	public ILoginAdapter getLoginAdapter() {
		return loginAdapter;
	}

	public void setLoginAdapter(ILoginAdapter loginAdapter) {
		this.loginAdapter = loginAdapter;
	}

	/*
	 * public void createParentDirectory(String path) { File file = new
	 * File(path); if (!file.exists()) { file.mkdirs(); } }
	 */
	/**
	 * Extracts a zip file specified by the zipFilePath to a directory specified
	 * by destDirectory (will be created if does not exists)
	 * 
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 */
	public void unzip(InputStream inputStream, String destDirectory) throws IOException {
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		ZipInputStream zipIn = new ZipInputStream(inputStream, Charset.forName("Cp437"));
		ZipEntry entry = zipIn.getNextEntry();
		// iterates over entries in the zip file
		while (entry != null) {
			String filePath = destDirectory + File.separator + entry.getName();
			if (!entry.isDirectory()) {
				extractFile(zipIn, filePath);
			} else {
				// if the entry is a directory, make the directory
				File dir = new File(filePath);
				dir.mkdir();
			}
			zipIn.closeEntry();
			entry = zipIn.getNextEntry();
		}
		zipIn.close();
	}

	/**
	 * Extracts a zip entry (file entry)
	 * 
	 * @param zipIn
	 * @param filePath
	 * @throws IOException
	 */
	private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
		byte[] bytesIn = new byte[BUFFER_SIZE];
		int read = 0;
		while ((read = zipIn.read(bytesIn)) != -1) {
			bos.write(bytesIn, 0, read);
		}
		bos.close();
	}

	/**
	 * Extracts a zip file specified by the zipFilePath to a directory specified
	 * by destDirectory (will be created if does not exists)
	 * 
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 * @throws InternalServerException
	 */
	public Map<String, Object> processZipFile(InputStream inputStream, String GUUID, String companyName, Map<String, String> userProperties)
			throws IOException, JAXBException, InternalServerException, JsonNotPresentException {
		final String THIS_METHOD = "processZipFile";
		ZipOutputStream zout = null;
		ZipInputStream zipIn = null;
		String zipName = null;
		ByteArrayOutputStream bos;
		Map<String, Object> zipContent = new HashMap<String, Object>();
		// Demarshaled formdata object
		FormData formData = null;
		StringBuffer jsonBuffer;
		// For terms and condition image
		byte[] tacImage = null;
		String imageType = null;
		
		try {
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
					"Converting inputStream to ByteArrayOutputStream for comapny: " + companyName);
			zipIn = new ZipInputStream(inputStream, Charset.forName("Cp437"));
			ZipEntry entry = zipIn.getNextEntry();
			bos = new ByteArrayOutputStream();
			zout = new ZipOutputStream(bos);
			if (entry == null) {
				if (zipIn != null)
					zipIn.close();
				if (zout != null)
					zout.close();
				logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Invalid Zip file");
				throw new InternalServerException("Invalid Zip file");
			}
			// iterates over entries in the zip file
			boolean zipContainsJson = false;
			while (entry != null) {
				if (!entry.isDirectory()) {
					String fineName = entry.getName();
					if (fineName.contains("form_data") && fineName.endsWith(".json")) {
						// demarshal
						jsonBuffer = getJsonInputAsStringBuffer(zipIn);
						formData = demarshalJson(jsonBuffer);
						zipName = convertJsonToXml(formData, jsonBuffer, GUUID, companyName, userProperties);
						fineName = zipName + ".xml";
						zout.putNextEntry(new ZipEntry(fineName));
						zout.write(stringWriter.toString().getBytes());
						zipContainsJson = true;
					} else {
						if (fineName.endsWith(".png")) {
							fineName = fineName.substring(0, fineName.lastIndexOf(".")) + "_" + GUUID + ".png";
						}
						else if (fineName.endsWith(".jpg")) {
							fineName = fineName.substring(0, fineName.lastIndexOf(".")) + "_" + GUUID + ".jpg";
						}
						else if (fineName.toLowerCase().contains("measurequick")) {
							//Measure Quick files will be in a format like 
							//acTuneUp_0_MeasureQuick_1545168512400578.json
							// or
							//tuneUp_0_measureQuick1545168862413.pdf

							String mqName = fineName.toLowerCase();
							//strip out unique file id created by mobile app to replace with GUUID
							int l = mqName.lastIndexOf("measurequick") + 12;
							mqName = fineName.substring(0, l);
							if (fineName.endsWith(".json")) fineName = mqName + "_" + GUUID + ".json";
							if (fineName.endsWith(".pdf")) fineName = mqName + "_" + GUUID + ".pdf";
						}
						
						// Grab terms and conditions image
						if (fineName.contains("TermsAndConditions")) {
							imageType = fineName.contains(".jpg") ? ".jpg" : ".png";
							
							ByteArrayOutputStream out = new ByteArrayOutputStream();
							int data = 0;
							while((data = zipIn.read()) != -1) {
								out.write(data);
							}
							out.flush();
							out.close();
							
							tacImage = out.toByteArray();
							zout.putNextEntry(new ZipEntry(fineName));
							// zipIn.read(termsAndConditions);
							zout.write(tacImage);
							
						}
						else{
						zout.putNextEntry(new ZipEntry(fineName));
						int length;
						byte[] buffer = new byte[4096];
						while ((length = zipIn.read(buffer)) > 0) {
							zout.write(buffer, 0, length);
						}
						}
					}
					zout.closeEntry();
				}
				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
			}
			zipIn.close();
			zout.close();
			if (!zipContainsJson) {
				logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "JSON file not available in the zip");
				throw new JsonNotPresentException();
			}
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "zipName:=====>" + zipName);
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "data:=====>" + bos.toByteArray());
			zipContent.put("zipName", zipName);
			zipContent.put("data", bos.toByteArray());
			// Email TAC image
			if (tacImage != null && imageType != null) {
				sendTACs(formData, tacImage, imageType, userProperties.get(USERNAME_KEY));
			}
			
		} catch (OutOfMemoryError memoryError) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
					"Since file size is more thorwing out of memory error :", memoryError);
			memoryError.printStackTrace();
			throw memoryError;
		} catch (ZipException ex) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while uploading zip file:", ex);
			throw ex;
		} catch (IOException ex) {
			logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while uploading zip file:", ex);
			throw ex;
		} finally {
			try {
				zipIn.close();
			} catch (IOException e) {
				logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while uploading zip file:", e);
			}
			try {
				zout.close();
			} catch (IOException e) {
				logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while uploading zip file:", e);
			}
		}
		return zipContent;
	}

	public EmailUser getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(EmailUser emailUser) {
		this.emailUser = emailUser;
	}
	
	
}
