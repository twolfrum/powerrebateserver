package com.icf.rebate.api.company.exception;

import com.icf.rebate.api.util.UtilManager;
/**
 * This class CompanyHasUsersException represents use case cannot delete company with users associated to it. 
 *
 */
public class CompanyHasUsersException extends GenericCompanyException
{
   /**
    * 
    */
   private static final long serialVersionUID = 85452779982390614L;
   private String errorCode = "ECOM1001";
   private String errorMessage = UtilManager.getErrorMessage(errorCode);

   /**
    * Constructs an instance of <code>InvalidConfiguration</code>.
    */
   public CompanyHasUsersException ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    */
   public CompanyHasUsersException ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String icfIdentifier = "05131966";

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public CompanyHasUsersException ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public CompanyHasUsersException ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.icf.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.icf.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }
}
