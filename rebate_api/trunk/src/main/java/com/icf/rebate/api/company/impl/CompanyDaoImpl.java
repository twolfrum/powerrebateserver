package com.icf.rebate.api.company.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.icf.rebate.api.company.intrface.ICompanyDao;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.util.RebateApiConstant;
/**
 * This class implements company CRUD operations.
 *
 */
public class CompanyDaoImpl extends JdbcDaoSupport implements ICompanyDao
{
   /**
    * Returns Login Details for the given user name.
    * 
    * @param userName - User name for which login details will be retrieved.
    * @return User - User details for the given user name.
    */
   public Company getCompanyDetails( String companyName )
   {
      Company company = null;
      company = this.getJdbcTemplate().queryForObject(GET_COMPANY_DETAILS, new Object[] { companyName },
            new CompanyDetailMapper());
      return company;
   }

   public class CompanyDetailMapper implements RowMapper<Company>
   {
      public Company mapRow( ResultSet rs, int rowNum ) throws SQLException
      {
         Company company = null;
         if (rs != null)
         {
            company = new Company();
            company.setCompanyId(rs.getInt("COMPANY_ID"));
            company.setCompanyName(rs.getString("COMPANY_NAME"));
            //company.setIcon(rs.getString("COMPANY_ID") + "_logo.png");
            // KVB - Adding program number
            company.setProgramNumber(rs.getString("PROGRAM_NUMBER"));
            company.setSendToContractor(rs.getBoolean("SEND_TOC_TO_CONTRACTOR"));
            company.setSendToPremise(rs.getBoolean("SEND_TOC_TO_PREMISE"));
            company.setSendToPrimary(rs.getBoolean("SEND_TOC_TO_PRIMARY"));
            company.setState(rs.getString("STATE"));
            company.setAddress(rs.getString("ADDRESS"));
            company.setCompanyDisplayName(rs.getString("COMPANY_NAME"));
            if (company.getCompanyDisplayName() != null && company.getCompanyDisplayName().length() > 37)
            {
               company.setCompanyDisplayName(company.getCompanyDisplayName().substring(0, 37) + "...");
            }
            try {
				company.setAbbrvCompanyName(rs.getString("ABBRV_COMPANY_NAME"));
			} catch (Exception e) {
				//Not all queries include column ABBRV_COMPANY_NAME in the resultset 
			}
         }
         return company;
      }
   }

   /**
    * Get all companies
    * 
    * @return Company map with key as company id and value as corresponding
    *         company bean
    * 
    */
   public Map<Integer, Company> getAllCompanies()
   {
      final Map<Integer, Company> companyMap = new HashMap<Integer, Company>();
      Object[] params = new Object[] { RebateApiConstant.USER_DELETED_CODE };
      this.getJdbcTemplate().query(GET_ALL_COMPANY_DETAILS, params, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            Company company = null;
            if (rs != null)
            {
               company = new Company();
               company.setCompanyId(rs.getInt("COMPANY_ID"));
               company.setCompanyName(rs.getString("COMPANY_NAME"));
               company.setProgramNumber(rs.getString("PROGRAM_NUMBER"));
               company.setIcon(rs.getString("COMPANY_NAME") + ".png");
               company.setState(rs.getString("STATE"));
               company.setAddress(rs.getString("ADDRESS"));
            }
            companyMap.put(rs.getInt("COMPANY_ID"), company);
         }
      });
      return companyMap;
   }

   /**
    * Retrieve company List
    * @return company list
    */
   public List<Company> getCompanyList()
   {
      return getJdbcTemplate().query(GET_ALL_COMPANY_DETAILS, new CompanyDetailMapper());
   }

   /**
    * Retrieve company list for specific user
    * 
    * @param userId - get company list for this user id
    * @return company list
    */
   public List<Company> getCompanyList( final long userId )
   {
      Object[] params = new Object[] { userId };
      return getJdbcTemplate().query(GET_ALL_COMPANY_DETAILS_FOR_USER, params, new CompanyDetailMapper());
   }

   public List<Long> getUsersForCompany( final long companyId )
   {
      final List<Long> userList = new ArrayList<Long>();
      Object[] params = new Object[] { companyId };
      this.getJdbcTemplate().query(GET_USERS_FOR_COMPANY, params, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            if (rs != null)
            {
               userList.add(( rs.getLong("USER_ID") ));
            }
         }
      });
      return userList;
   }

   /**
    * Retrieve User details for given company id
    * 
    * @param companyId - get User details for this Id
    * @return User list
    */
   public List<User> getUserDetailsForComapny( final long companyId )
   {
      final List<User> userList = new ArrayList<User>();
      Object[] params = new Object[] { companyId };
      this.getJdbcTemplate().query(GET_USERS_DETAILS_FOR_COMPANY, params, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            User user = null;
            if (rs != null)
            {
               user = new User();
               user.setUserId(rs.getLong("USER_ID"));
               user.setUserName(rs.getString("USER_NAME"));
               user.setFirstName(rs.getString("FIRST_NAME"));
               user.setLastName(rs.getString("LAST_NAME"));
               userList.add(user);
            }
         }
      });
      return userList;
   }

   /**
    * Add new company into DB
    * 
    * @param company - contains company details
    */
   public void addCompany( final Company company )
   {
      getJdbcTemplate().update(new PreparedStatementCreator()
      {
         public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
         {
            PreparedStatement ps = conn.prepareStatement(ADD_COMPANY);
            ps.setLong(1, getMaxCompanyId() + 1);
            ps.setString(2, company.getCompanyName());
            ps.setString(3, company.getState());
            ps.setString(4, company.getAddress());
            ps.setString(5, company.getProgramNumber());
            ps.setBoolean(6, company.getSendToPrimary());
            ps.setBoolean(7, company.getSendToPremise());
            ps.setBoolean(8, company.getSendToContractor());
            return ps;
         }
      });
   }

   /**
    * get company details by company id
    * 
    * @param companyId - get company details for this id
    * @return Company - contains company details
    */
   public Company getCompanyById( long companyId )
   {
      return this.getJdbcTemplate().queryForObject(GET_COMPANY_DETAILS_BY_ID, new Object[] { companyId },
            new CompanyDetailMapper());
   }

   /**
    * get company details by company name
    * 
    * @param companyName - get company details for this name
    * @return Company - contains company details
    */
   public Company getCompanyByName( String companyName )
   {
      return this.getJdbcTemplate().queryForObject(GET_COMPANY_DETAILS_BY_NAME, new Object[] { companyName },
            new CompanyDetailMapper());
   }

   /**
    * 
    * @return long - maximum company Id
    */
   public long getMaxCompanyId()
   {
      long companyId = 0;
      final List<Long> companyIdList = new ArrayList<Long>();
      this.getJdbcTemplate().query(GET_MAX_COMPANY_ID, new RowCallbackHandler()
      {
         /*
          * (non-Javadoc)
          * 
          * @see
          * org.springframework.jdbc.core.RowCallbackHandler#processRow(java
          * .sql.ResultSet)
          */
         public void processRow( ResultSet rs ) throws SQLException
         {
            long companyId = rs.getLong("COMPANY_ID");
            companyIdList.add(companyId);
         }
      });
      if (! companyIdList.isEmpty())
      {
         companyId = companyIdList.get(0);
      }
      return companyId;
   }

   /**
    * @return long - indicates company id
    */
   @Override
   public long getCompanyById( String companyName )
   {

      long companyId = 0;
      final List<Long> conpanyIdList = new ArrayList<Long>();
      this.getJdbcTemplate().query(GET_COMPANY_ID, new Object[] { companyName }, new RowCallbackHandler()
      {
         /*
          * (non-Javadoc)
          * 
          * @see
          * org.springframework.jdbc.core.RowCallbackHandler#processRow(java
          * .sql.ResultSet)
          */
         public void processRow( ResultSet rs ) throws SQLException
         {
            long companyId = rs.getLong("COMPANY_ID");
            conpanyIdList.add(companyId);
         }
      });
      if (! conpanyIdList.isEmpty())
      {
         companyId = conpanyIdList.get(0);
      }
      return companyId;
   }

   /**
    * Update existing company with new details.
    * 
    * @param company
    */
   public void updateCompany( final Company company )
   {
      getJdbcTemplate().update(new PreparedStatementCreator()
      {
         public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
         {
            PreparedStatement ps = conn.prepareStatement(UPDATE_COMPANY);
            ps.setString(1, company.getState());
            ps.setString(2, company.getAddress());
            ps.setString(3, company.getProgramNumber());
            ps.setBoolean(4, company.getSendToPrimary());
            ps.setBoolean(5, company.getSendToPremise());
            ps.setBoolean(6, company.getSendToContractor());
            ps.setLong(7, company.getCompanyId());
            return ps;
         }
      });
   }

   /**
    * Delete company for given company Id.
    * 
    * @param company
    */
   public void deleteCompany( final long companyId )
   {
      getJdbcTemplate().update(DELETE_COMPANY, companyId);
   }

   /**
    * Get all companies
    * 
    * @return Company map with key as company name and value as corresponding
    *         company id
    */
   public Map<String, Long> getAllCompanyNames()
   {
      final Map<String, Long> companyMap = new HashMap<String, Long>();
      // Object[] params = new Object[] {RebateApiConstant.USER_DELETED_CODE};
      this.getJdbcTemplate().query(GET_ALL_COMPANY_DETAILS, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            if (rs != null)
            {
               companyMap.put(rs.getString("COMPANY_NAME"), rs.getLong("COMPANY_ID"));
            }
         }
      });
      return companyMap;
   }
   
   /**
    * Get all companies for manager
    * 
    * @return Company map with key as company name and value as corresponding
    *         company id
    */
   
   public Map<String, Long> getAllCompanyNamesForManager(long managerId) {
	   final Map<String, Long> companyMap = new HashMap<>();
	   getJdbcTemplate().query(GET_ALL_COMPANY_DETAILS_FOR_MANAGER, new RowCallbackHandler() {
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			if (rs != null) {
				companyMap.put(rs.getString("COMPANY_NAME"), rs.getLong("COMPANY_ID"));
			}
		}
	}, managerId);
	   return companyMap;
   }

   /**
    * Get all companies
    * 
    * @return company Map with key as company Id and value as company name
    */
   @Override
   public Map<Long, String> getAllCompany()
   {
      final Map<Long, String> companyMap = new HashMap<Long, String>();
      this.getJdbcTemplate().query(GET_ALL_COMPANY_DETAILS, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            if (rs != null)
            {
               companyMap.put(rs.getLong("COMPANY_ID"), rs.getString("COMPANY_NAME"));
            }
         }
      });
      return companyMap;
   }
}
