package com.icf.rebate.api.user.impl;

import java.util.List;
import java.util.Map;

import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.user.exception.EmailIdAlreadyExist;
import com.icf.rebate.api.user.exception.InvalidPasswordException;
import com.icf.rebate.api.user.exception.UserAlreadyExist;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.intrface.IUserAdapter;
import com.icf.rebate.api.user.intrface.IUserValidator;
/**
 * This class implements User CRUD opertion
 * 
 */
public class UserManager
{
   private final String THIS_CLASS = "UserManager";
   private IUserAdapter userAdapter;
   private IUserValidator userValidator;
   /**
    * logger object to log the messages.
    * 
    */
   private IMPLogger logger;

   /**
    * Retrieve all active users
    * 
    * @return User Map - key - User_id and value-User Bean
    * @throws InternalServerException
    */
   public Map<Integer, User> getAllUsers() throws InternalServerException
   {
      final String THIS_METHOD = "getAllUsers";
      try
      {
         return userAdapter.getAllUsers();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Subscriber." + e.getMessage(), e);
      }
   }

   // public void updateUserMock()
   // {
   // User user= new User();
   // user.setUserId(1005);
   // user.setUserName("niv");
   // user.setPassword("niv");
   // user.setVerifyPassword("niv");
   // user.setFirstName("niv2");
   // user.setLastName("niv2");
   // user.setEmailId("niv2@mportal.com");
   // user.setUserRole(1);
   // user.setUserStatus(9001);
   // try
   // {
   // updateUser(user);
   // }
   // catch(Exception ex)
   // {
   // ex.printStackTrace();
   // }
   //
   // }
   // public void deleteUserMock()
   // {
   // try
   // {
   // deleteUser(1005);
   // }
   // catch(Exception ex)
   // {
   // ex.printStackTrace();
   // }
   //
   // }
   /**
    * Retrieve all active users that belong to this manager
    * 
    * @return User List
    * @throws InternalServerException
    */
   
   public List<User> getUserListForManager(long userId) throws InternalServerException {
	   final String THIS_METHOD = "getUserListForManager";
	      try
	      {
	         return userAdapter.getUserListForManager(userId);
	      }
	      catch (Exception e)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	               "InternalServerException occured while retrieving Subscriber." + e.getMessage(), e);
	         throw new InternalServerException("Exception occured while retrieving Subscriber." + e.getMessage(), e);
	      }
   }
   
   /**
    * Retrieve all active users List
    * 
    * @return User List
    * @throws InternalServerException
    */
   public List<User> getUserList() throws InternalServerException
   {
      final String THIS_METHOD = "getUserList";
      try
      {
         return userAdapter.getUserList();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Subscriber." + e.getMessage(), e);
      }
   }

   public IUserAdapter getUserAdapter()
   {
      return userAdapter;
   }

   public void setUserAdapter( IUserAdapter userAdapter )
   {
      this.userAdapter = userAdapter;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   /**
    * Add New User into DB
    * 
    * @param user - contains user details
    * @throws InvalidDataException - throws if data is invalid
    * @throws InvalidPasswordException - throws if password is invalid
    * @throws UserAlreadyExist - throws if duplicate user trying to insert
    * @throws InternalServerException - throws if any server error occurs.
    * @throws EmailIdAlreadyExist - throws email id already associated with one
    *            user
    */
   public void addUser( User user ) throws InvalidDataException, InvalidPasswordException, UserAlreadyExist,
         InternalServerException, EmailIdAlreadyExist
   {
      final String THIS_METHOD = "addUser";
      try
      {
         this.userValidator.validateAddSubscriber(user);
         this.userAdapter.addUser(user);
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException Exception occured while adding subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (UserAlreadyExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "SubscriberAlreadyExists Exception occured while adding subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (EmailIdAlreadyExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "EmailIdAlreadyExist Exception occured while adding subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (InvalidPasswordException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidPasswordException Exception occured while adding subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while adding subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while adding Subscriber." + e.getMessage(), e);
      }
   }
   
   public void assignUserToManager(User manager, User user) throws InternalServerException {
	   final String THIS_METHOD = "assignUserToManager";
	   
	   try {
		   this.userAdapter.addUserToManager(manager, user);
	   } catch (Exception e)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	               "InternalServerException occured while adding subscriber." + e.getMessage(), e);
	         throw new InternalServerException("Exception occured while adding Subscriber." + e.getMessage(), e);
	      }
   }

   /**
    * Get Role Map with Key as Role name and value as Role Id - For manager
    * 
    * @return Map
    * @throes InteralServerException = throws if any interal server error occurs
    */
   public Map<String, Long> getRoleMapForManager() throws InternalServerException
   {
      final String THIS_METHOD = "getRoleMapForManager";
      try
      {
         return userAdapter.getRoleMapForManager();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Role Map." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Role Map." + e.getMessage(), e);
      }
   }
   
   /**
    * Get Role Map with Key as Role name and value as Role Id
    * 
    * @return Map
    * @throws InternalServerException- throws if any internal server occurs
    */
   public Map<String, Long> getRoleMap() throws InternalServerException
   {
      final String THIS_METHOD = "getRoleMap";
      try
      {
         return userAdapter.getRoleMap();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Role Map." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Role Map." + e.getMessage(), e);
      }
   }

   /**
    * Retrieve user details for given usedId
    * 
    * @param userId - get User details for this id
    * @return User - contains user details.
    * @throws InternalServerException - throws if any internal server error
    *            occurs.
    */
   public User getUserById( long userId ) throws InternalServerException
   {
      final String THIS_METHOD = "getUserById";
      try
      {
         return userAdapter.getUserById(userId);
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Subscriber." + e.getMessage(), e);
      }
   }

   public void setUserValidator( IUserValidator userValidator )
   {
      this.userValidator = userValidator;
   }

   /**
    * Update existing user for given User object
    * 
    * @param user - contains user details
    * @throws InvalidDataException - throws if data is invalid
    * @throws InvalidPasswordException - throws if password is invalid
    * @throws UserAlreadyExist - throws if duplicate user trying to insert
    * @throws InternalServerException - throws if any server error occurs.
    * @throws EmailIdAlreadyExist - throws email id already associated with one
    *            user
    */
   public void updateUser( User user ) throws InvalidDataException, UserDoesNotExist, InternalServerException,
         EmailIdAlreadyExist, InvalidPasswordException
   {
      final String THIS_METHOD = "updateSubscriber";
      try
      {
         this.userValidator.validateUpdateUser(user);
         this.userAdapter.updateUser(user);
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException Exception occured while updating subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (UserDoesNotExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserDoesNotExist Exception occured while updating subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (EmailIdAlreadyExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "EmailIdAlreadyExist Exception occured while updating subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (InvalidPasswordException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidPassword Exception occured while updating the subscriber " + e.getErrorMessage(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while updating subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while adding Subscriber." + e.getMessage(), e);
      }
   }

   /**
    * Delete User for given user id
    * 
    * @param userId - delete user for this id
    * @throws InvalidDataException - throws if invalid data.
    * @throws UserDoesNotExist - throws if user does not exists.
    * @throws InternalServerException - throws if any internal server occurs.
    */
   public void deleteUser( long userId ) throws InvalidDataException, UserDoesNotExist, InternalServerException
   {
      final String THIS_METHOD = "deleteSubscriber";
      try
      {
         this.userValidator.validateDeleteUserInfo(userId);
         this.userAdapter.deleteUser(userId);
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException Exception occured while deleting Subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (UserDoesNotExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "SubscriberDoesNotExist Exception occured while deleting Subscriber." + e.getErrorMessage(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while deleting Subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while deleting Subscriber." + e.getMessage(), e);
      }
   }

   public Map<String, Long> getUserStatusMap()
   {
      return this.userAdapter.getUserStatusMap();
   }

   /**
    * Search users for given value
    * 
    * @param searchString - search user for this value
    * @return User List
    */
   public List<User> searchUser( String searchUser, String searchCompany )
   {
      return this.userAdapter.searchUser(searchUser, searchCompany);
   }

   /**
    * get User details by UserName
    * 
    * @param userName
    * @return User Bean
    * @throws UserDoesNotExist - throws if user does not exists
    */
   public User getUserDetails( String userName ) throws UserDoesNotExist
   {
      return this.userAdapter.getUserDetails(userName);
   }

   /**
    * Update Password reset flag for given used Id based on flag
    * 
    * @param flag
    * @param userId
    */
   public void modifyPwdResetFlag( boolean flag, long userId )
   {
      this.userAdapter.modifyPwdResetFlag(flag, userId);
   }
}
