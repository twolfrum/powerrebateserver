package com.icf.rebate.api.logger;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.icf.rebate.api.util.AppRepositorySelector;
import com.icf.rebate.api.util.ThreadLocalImpl;
/**
 * 
 */
public class MPLoggerImpl implements IMPLogger
{
   private Logger logger;
   private static String MDC_CLASS = "Class";
   private static String MDC_METHOD = "Method";
   private static String MDC_USER = "User";
   private static String MDC_TID = "tId";
   private String loggerName = null;
   private LogConfigLocator logConfigLocator = null;

   // private static final ThreadLocal<String> threadLocal = new
   // ThreadLocal<String>();
   /**
    * sets given object to threadLocal
    * 
    * @param object
    */
   /*
    * public static void setThreadLocalUserInfo(String object) {
    * threadLocal.set(object); }
    *//**
    * gets required object from threadLocal.
    * 
    * @return
    */
   /*
    * public static String getThreadLocalUserInfo() { return
    * threadLocal.get()==null?"":threadLocal.get(); }
    * 
    * public static void unsetThreadLocalUserInfo() { threadLocal.remove(); }
    */
   public MPLoggerImpl ( String loggerName, LogConfigLocator logConfigLocator )
   {
      this.loggerName = loggerName;
      this.logConfigLocator = logConfigLocator;
      loadLogInstance();
   }

   public MPLoggerImpl ( String loggerName )
   {
      this.loggerName = loggerName;
      loadLogInstance();
   }

   public MPLoggerImpl ( Logger logger )
   {
      this.logger = logger;
   }

   /**
    * Load the log4j logger instance based on the loggerConfigFilePath and
    * loggerName
    */
   private void loadLogInstance()
   {
      // PropertyConfigurator.configure(loggerConfigFilePath);
      /*
       * If the AppRepositorySelector is not initialized, then initialize before
       * getting the logger.
       */
      if (! AppRepositorySelector.isAppRepositoryInitialized())
      {
         AppRepositorySelector.init(logConfigLocator.getLog4jXMLPath());
      }
      logger = Logger.getLogger(loggerName);
   }

   /**
    * Logs the given level and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param msg - Message to be logged.
    */
   public void log( int level, String msg )
   {
      log(level, msg, null);
   }

   /**
    * Logs the given level, class, method, message and throwable.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    * @param throwable - Throwable to be logged.
    */
   public void log( int level, String curClass, String method, String msg, Throwable throwable )
   {
      addFieldValueToLogMessage(MDC_CLASS, curClass);
      addFieldValueToLogMessage(MDC_METHOD, method);
      addFieldValueToLogMessage(MDC_USER, ThreadLocalImpl.getUserId());
      addFieldValueToLogMessage(MDC_TID, ThreadLocalImpl.getTransactionId());
      log(level, msg, throwable);
      /**
       * To remove all the key/values set so as to not affect the next log
       * message.
       */
      resetMDC();
   }

   /**
    * Logs the given level, class, method and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    */
   public void log( int level, String curClass, String method, String msg )
   {
      addFieldValueToLogMessage(MDC_CLASS, curClass);
      addFieldValueToLogMessage(MDC_METHOD, method);
      addFieldValueToLogMessage(MDC_USER, ThreadLocalImpl.getUserId());
      addFieldValueToLogMessage(MDC_TID, ThreadLocalImpl.getTransactionId());
      log(level, msg);
      /**
       * To remove all the key/values set so as to not affect the next log
       * message.
       */
      resetMDC();
   }

   /**
    * Logs the given level, user, class, method and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param user - User info to be logged.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    */
   public void log( int level, String user, String curClass, String method, String msg )
   {
      addFieldValueToLogMessage(MDC_CLASS, curClass);
      addFieldValueToLogMessage(MDC_METHOD, method);
      addFieldValueToLogMessage(MDC_USER, ThreadLocalImpl.getUserId());
      addFieldValueToLogMessage(MDC_TID, ThreadLocalImpl.getTransactionId());
      log(level, msg);
      /**
       * To remove all the key/values set so as to not affect the next log
       * message.
       */
      resetMDC();
   }

   /**
    * Logs the given level, user, class, method and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param user - User info to be logged.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    * @param throwable - Throwable to be logged.
    */
   public void log( int level, String user, String curClass, String method, String msg, Throwable excep )
   {
      addFieldValueToLogMessage(MDC_CLASS, curClass);
      addFieldValueToLogMessage(MDC_METHOD, method);
      addFieldValueToLogMessage(MDC_USER, ThreadLocalImpl.getUserId());
      addFieldValueToLogMessage(MDC_TID, ThreadLocalImpl.getTransactionId());
      log(level, msg, excep);
      /**
       * To remove all the key/values set so as to not affect the next log
       * message.
       */
      resetMDC();
   }

   /**
    * This method will log the msg.
    * 
    * @param level
    * @param msg
    * @param throwable
    */
   public void log( int level, String msg, Throwable throwable )
   {
      switch (level)
      {
         case IMPLogger.TRACE:
            logger.trace(msg);
            break;
         case IMPLogger.DEBUG:
            if (throwable != null)
            {
               logger.debug(msg, throwable);
            }
            else
            {
               logger.debug(msg);
            }
            break;
         case IMPLogger.INFO:
            if (throwable != null)
            {
               logger.info(msg, throwable);
            }
            else
            {
               logger.info(msg);
            }
            break;
         case IMPLogger.WARN:
            if (throwable != null)
            {
               logger.warn(msg, throwable);
            }
            else
            {
               logger.warn(msg);
            }
            break;
         case IMPLogger.ERROR:
            if (throwable != null)
            {
               logger.error(msg, throwable);
            }
            else
            {
               logger.error(msg);
            }
            break;
         case IMPLogger.FATAL:
            if (throwable != null)
            {
               logger.fatal(msg, throwable);
            }
            else
            {
               logger.fatal(msg);
            }
            break;
         default:
            // System.out.println("Invalid Level, Please specify the level from the CDMLogger interface.");
            break;
      }
   }

   /**
    * Adds the key/value pair for the next log message. Note that the key has to
    * be pre-configured.
    * 
    * 
    * @param key - Key name to which the value has to be appended.
    * @param value - Value for the key which has to go into the log message.
    */
   public void addFieldValueToLogMessage( String key, Object value )
   {
      MDC.put(key, value);
   }

   /**
    * Adds a list of key/value pair for the next log message. Note that the keys
    * has to be pre-configured.
    * 
    * @param mdcMap - Map of key/value pairs which will be used for the next log
    *           message.
    */
   public void addFieldValueListToLogMessage( Map<String, Object> mdcMap )
   {
      if (mdcMap != null && mdcMap.size() > 0)
      {
         Iterator<String> itr = mdcMap.keySet().iterator();
         while (itr.hasNext())
         {
            String key = itr.next();
            Object value = mdcMap.get(key);
            MDC.put(key, value);
         }
      }
   }

   /**
    * Removes the given Key's value which will be reflected in the next log
    * message call.
    * 
    * @param key - key whose value has to be removed.
    */
   public void removeFieldValueForLogMessage( String key )
   {
      MDC.remove(key);
   }

   /**
    * Removes all the Keys values which will be reflected in the next log
    * message call.
    * 
    */
   public void resetMDC()
   {
      Enumeration<String> keyItr = MDC.getContext().keys();
      while (keyItr.hasMoreElements())
      {
         MDC.remove(keyItr.nextElement());
      }
   }

   public void reloadLogger()
   {
      loadLogInstance();
   }
}
