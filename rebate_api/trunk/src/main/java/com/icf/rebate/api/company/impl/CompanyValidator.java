package com.icf.rebate.api.company.impl;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.api.company.intrface.ICompanyDao;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.user.exception.EmailIdAlreadyExist;
import com.icf.rebate.api.user.exception.InvalidPasswordException;
import com.icf.rebate.api.user.exception.UserAlreadyExist;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.UtilManager;
/**
 * This Class LoginValidator Validates all the required Login fields.If any
 * field is not valid ,then it will add the corresponding error message to the
 * errorlist. This class extends {@link AbstractValidator}.
 */
public class CompanyValidator extends AbstractValidator
{
   private ICompanyDao companyDao = null;
   List<String> errorsList = new ArrayList<String>();
   public static final String VLOGIN1001 = "VLOGIN1001";
   public static final String VLOGIN1002 = "VLOGIN1002";
   public static final String VLOGIN1003 = "VLOGIN1003";
   public static final String VLOGIN1004 = "VLOGIN1004";
   public static final String VLOGIN1005 = "VLOGIN1005";
   public static final String VLOGIN1006 = "VLOGIN1006";
   public static final String VLOGIN1007 = "VLOGIN1007";
   public static final String VLOGIN1008 = "VLOGIN1008";
   public static final String VLOGIN1009 = "VLOGIN1009";
   public static final String VSUB1006 = "VSUB1006";

   /**
    * Validates the user name and password.
    * 
    * @param userName - user name to be validated.
    * @param password - password to be validated.
    * @throws InvalidDataException - If userName or password contains invalid
    *            data.
    */
   public void validateLoginFields( String companyName ) throws InvalidDataException
   {
      if (! isNotNull(companyName))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1001));
      }
      checkInvalidDataException();
   }

   /**
    * This method validates company details
    * @param company - contains company details
    * @throws UserAlreadyExist - throws if user already exists.
    * @throws InvalidDataException- throws if data is invalid
    * @throws InvalidPasswordException - throws if password is invalid
    * @throws EmailIdAlreadyExist - throws if duplicated email id
    */
   public void validateAddCompany( Company company ) throws UserAlreadyExist, InvalidDataException,
         InvalidPasswordException, EmailIdAlreadyExist
   {
      CompanyProfileValidator validator = new CompanyProfileValidator();
      validator.validateCompanyProfile(company, false);
      if (isCompanyAlreadyExist(company, false))
      {
         throw new UserAlreadyExist("Company [" + company.getCompanyName() + "] already exist in Data source.");
      }
   }
   /**
    * Check is company exists or not
    * @param company - company details
    * @param isUpdate
    * @return boolean - if company exists return true else false
    */
   private boolean isCompanyAlreadyExist( Company company, boolean isUpdate )
   {
      boolean companyExists = false;
      long companyId = 0;
      companyId = companyDao.getCompanyById(company.getCompanyName());
      if (isUpdate && companyId > 0 && ! ( company.getCompanyId() == companyId ))
      {
         companyExists = true;
      }
      else if (! isUpdate && companyId > 0)
      {
         companyExists = true;
      }
      return companyExists;
   }
}
