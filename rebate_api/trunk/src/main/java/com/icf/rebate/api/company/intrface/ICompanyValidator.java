package com.icf.rebate.api.company.intrface;

import com.icf.rebate.api.company.exception.CompanyAlreadyExist;
import com.icf.rebate.api.company.exception.CompanyHasUsersException;
import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.exception.InvalidDataException;
/**
 * This interface ILoginValidator will expose validation api for login
 * functionalities.
 * 
 */
public interface ICompanyValidator
{
   /**
    * Validates the given userName and password.
    * 
    * @param userName - User name to be validated.
    * @param password - Password to be validated.
    * @throws InvalidDataException - If user name or password fields contain
    *            invalid data.
    */
   // public void validate( String userName, String password ) throws
   // InvalidDataException;
   /**Validate company details before adding into DB.
    * @param company - contains company details
    */
   public void validateAddCompany( Company company ) throws CompanyAlreadyExist, InvalidDataException;

   /**
    *  Validate company details before updating into DB.
    * @param company - contains company details
    */
   public void validateUpdateCompany( Company company ) throws CompanyAlreadyExist, InvalidDataException;

   /**
    * Validate company details before deleting from DB.
    * @param company - contains company details
    */
   public void validateDeleteCompanyInfo( long companyId ) throws InvalidDataException, CompanyHasUsersException;
}
