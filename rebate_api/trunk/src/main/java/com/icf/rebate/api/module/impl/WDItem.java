package com.icf.rebate.api.module.impl;

import java.io.IOException;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@XmlRootElement
@XmlType(propOrder = { "replaceOnFail", "earlyRetirement" })
public class WDItem {
	
		@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
		private Category windowDoorDetail;
		
		   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
		   {
		      ObjectMapper objectMapper = new ObjectMapper();
		      return objectMapper.writeValueAsString(this);
		   }

		public Category getWindowDoorDetail() {
			return windowDoorDetail;
		}

		public void setWindowDoorDetail(Category windowDoorDetail) {
			this.windowDoorDetail = windowDoorDetail;
		}

}
