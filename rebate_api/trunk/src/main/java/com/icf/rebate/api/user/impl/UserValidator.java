package com.icf.rebate.api.user.impl;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.user.exception.EmailIdAlreadyExist;
import com.icf.rebate.api.user.exception.InvalidPasswordException;
import com.icf.rebate.api.user.exception.UserAlreadyExist;
import com.icf.rebate.api.user.intrface.IUserDao;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.UtilManager;
/**
 * This Class LoginValidator Validates all the required Login fields.If any
 * field is not valid ,then it will add the corresponding error message to the
 * errorlist. This class extends {@link AbstractValidator}.
 */
public class UserValidator extends AbstractValidator
{
   private IUserDao userDao = null;
   List<String> errorsList = new ArrayList<String>();
   public static final String VLOGIN1001 = "VLOGIN1001";
   public static final String VLOGIN1002 = "VLOGIN1002";
   public static final String VLOGIN1003 = "VLOGIN1003";
   public static final String VLOGIN1004 = "VLOGIN1004";
   public static final String VLOGIN1005 = "VLOGIN1005";
   public static final String VLOGIN1006 = "VLOGIN1006";
   public static final String VLOGIN1007 = "VLOGIN1007";
   public static final String VLOGIN1008 = "VLOGIN1008";
   public static final String VLOGIN1009 = "VLOGIN1009";
   public static final String VSUB1006 = "VSUB1006";

   /**
    * Validates the user name and password.
    * 
    * @param userName - user name to be validated.
    * @param password - password to be validated.
    * @throws InvalidDataException - If userName or password contains invalid
    *            data.
    */
   public void validateLoginFields( String userName, String password ) throws InvalidDataException
   {
      if (! isNotNull(userName))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1001));
      }
      if (! isNotNull(password))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1002));
      }
      checkInvalidDataException();
   }

   /**
    * Validate User before adding into DB
    * @param user - contains user details
    * @throws UserAlreadyExist - throws if user already exists in DB
    * @throws InvalidDataException - throws if data is invalid
    * @throws InvalidPasswordException - throws if password is invalid
    * @throws EmailIdAlreadyExist - throws if email id already exists.
    */
   public void validateAddSubscriber( User user ) throws UserAlreadyExist, InvalidDataException,
         InvalidPasswordException, EmailIdAlreadyExist
   {
      UserProfileValidator validator = new UserProfileValidator();
      validator.validateUserProfile(user, false);
      if (isUserAlreadyExist(user, false))
      {
         throw new UserAlreadyExist("Subscriber [" + user.getUserName() + "] already exist in Data source.");
      }
      // if (isUserAlreadyExistWithEmail(user, false))
      // {
      // throw new EmailIdAlreadyExist("Subscriber [" + user.getEmailId() +
      // "] already exist in Data source.");
      // }
      //
      String passWord = user.getPassword();
      String verifyPassword = user.getVerifyPassword();
      if (! passWord.equals(verifyPassword))
      {
         throw new InvalidPasswordException("Password and Confirm Password must be same");
      }
   }

   private boolean isSubscriberAlreadyExistWithEmail( User user, boolean isUpdate )
   {
      boolean subscriberExists = false;
      long subscriberId = 0;
      try
      {
         // User existingUser = userDao.getUser(user.getEmailId());
         User existingUser = userDao.getUserDetails(user.getUserName());
         subscriberId = existingUser.getUserId();
         if (isUpdate && subscriberId > 0 && ! ( user.getUserId() == subscriberId ))
         {
            subscriberExists = true;
         }
         else if (! isUpdate && subscriberId > 0)
         {
            subscriberExists = true;
         }
         return subscriberExists;
      }
      catch (Exception e)
      {
         return subscriberExists;
      }
   }

   /**
    * This methods checks if user exists in DB or not
    * @param user
    * @param isUpdate
    * @return boolean - true if user exists else false.
    */
   private boolean isUserAlreadyExist( User user, boolean isUpdate )
   {
      boolean subscriberExists = false;
      long userId = 0;
      userId = userDao.getUserById(user.getUserName());
      if (isUpdate && userId > 0 && ! ( user.getUserId() == userId ))
      {
         subscriberExists = true;
      }
      else if (! isUpdate && userId > 0)
      {
         subscriberExists = true;
      }
      return subscriberExists;
   }
}
