package com.icf.rebate.api.login.intrface;

import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.login.impl.UserPasswordTrackingBean;
import com.icf.rebate.api.user.impl.User;
/**
 * This interface ILoginValidator will expose validation api for login
 * functionalities.
 * 
 */
public interface ILoginValidator
{
   /**
    * Validates the given userName and password.
    * 
    * @param userName - User name to be validated.
    * @param password - Password to be validated.
    * @throws InvalidDataException - If user name or password fields contain
    *            invalid data.
    */
   public void validate( String userName, String password ) throws InvalidDataException;

   public void validateUserName( String userName ) throws InvalidDataException;

   /**
    * Validates the user password tracking fields for valid values.
    * 
    * @param userBean - is the user password tracking details.
    * @throws InvalidDataException
    */
   void validatePasswordCorrect( UserPasswordTrackingBean userBean ) throws InvalidDataException;

   /**
    * Validate the password Tracking Bean details
    * 
    * @param userBean - is the user password tracking details.
    * @throws InvalidDataException
    */
   void validatePasswordTrackingBean( UserPasswordTrackingBean userBean ) throws InvalidDataException;

   /**Validate user details before authenticate.
    * 
    * @param userDetails - contains user information
    * @throws InvalidDataException - throws if data is invalid
    */
   public void validate( User userDetails ) throws InvalidDataException;

   /**Validate user details before change password.
    * 
    * @param userDetails - contains user information
    * @throws InvalidDataException - throws if data is invalid
    */
   public void validateChangePwd( User userDetails ) throws InvalidDataException;
}
