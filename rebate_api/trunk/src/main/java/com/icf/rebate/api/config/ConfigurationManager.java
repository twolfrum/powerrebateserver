package com.icf.rebate.api.config;

import java.util.List;

import com.icf.rebate.api.subsystem.GenericManager;
/**
 * Configuration Manager maintains the configuration files that are present in
 * the sub system. It manages all the properties files in the subsystem by
 * default, if the client wants this manager class to handle more types of files
 * then it has to inject the required functionality through a implementation
 * class of {@link IConfiguration} interface.
 * 
 */
public class ConfigurationManager extends GenericManager
{
   private IConfiguration configAdapter = null;

   /**
    * Initilize the Configuration Manager.
    */
   public void initialize()
   {
   }

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method reports an
    * error in the form of run time exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsString( String propertyFileName, String propertyName )
   {
      return configAdapter.getPropertyAsString(propertyFileName, propertyName);
   }

   /**
    * Returns the property value as boolean for the given property name from the
    * given property file. If the given property is not found, this method
    * reports an error in the form of run time exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - boolean value of the property if found in the property file.
    */
   public boolean getPropertyAsBoolean( String propertyFileName, String propertyName )
   {
      return Boolean.parseBoolean(configAdapter.getPropertyAsString(propertyFileName, propertyName));
   }

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method assumes the
    * default and returns it to the caller.<br>
    * If the given property configured with a comma separated value, then the
    * first String before comma will be returned.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsStringWithDefault( String propertyFileName, String propertyName, String defaultValue )
   {
      return configAdapter.getPropertyAsStringWithDefault(propertyFileName, propertyName, defaultValue);
   }

   /**
    * Returns the property value as integer. If the value read from the property
    * file is not integer, this method tries to return the default. If the
    * default value is not a proper integer value, this method throws a run time
    * exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - int value of the property.
    */
   public int getPropertyAsInt( String propertyFileName, String propertyName, int defaultValue )
   {
      return configAdapter.getPropertyAsInt(propertyFileName, propertyName, defaultValue);
   }

   /**
    * Returns the property value as long. If the value read from the property
    * file is not long, this method tries to return the default. If the default
    * value is not a proper long value, this method throws a run time exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - long value of the property.
    */
   public long getPropertyAsLong( String propertyFileName, String propertyName, long defaultValue )
   {
      return configAdapter.getPropertyAsLong(propertyFileName, propertyName, defaultValue);
   }

   /**
    * Returns the property value as double. If the value read from the property
    * file is not double, this method tries to return the default. If the
    * default value is not a proper double value, this method throws a run time
    * exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - double value of the property.
    */
   public double getPropertyAsDouble( String propertyFileName, String propertyName, double defaultValue )
   {
      return configAdapter.getPropertyAsDouble(propertyFileName, propertyName, defaultValue);
   }

   /**
    * Returns a list of strings which are separated by comma for the given
    * property.
    * 
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - list of all string which are separated by comma.
    */
   public List<String> getPropertyAsList( String propertyFileName, String propertyName )
   {
      return configAdapter.getPropertyAsList(propertyFileName, propertyName);
   }

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method reports an
    * error in the form of run time exception. If the given property configured
    * with a comma separated value, then it will return the String with comma.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyWithComma( String propertyFileName, String propertyName )
   {
      StringBuffer propertyValue = new StringBuffer();
      List<String> propertyValueList = configAdapter.getPropertyAsList(propertyFileName, propertyName);
      if (propertyValueList != null && ! propertyValueList.isEmpty())
      {
         for (int i = 0; i < propertyValueList.size(); i++)
         {
            if (i == 0)
            {
               propertyValue.append(propertyValueList.get(i));
            }
            else
            {
               propertyValue.append("," + propertyValueList.get(i));
            }
         }
      }
      if (propertyValue.length() <= 0)
      {
         return null;
      }
      return propertyValue.toString();
   }

   /**
    * Reloads all the Configuration files.
    * 
    * @return true - If successfully reloaded.<br>
    *         false - If reload fails.
    */
   public boolean reLoadAllConfiguration()
   {
      return configAdapter.reLoadAllConfiguration();
   }

   public IConfiguration getConfigAdapter()
   {
      return configAdapter;
   }

   public void setConfigAdapter( IConfiguration configAdapter )
   {
      this.configAdapter = configAdapter;
   }

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method assumes the
    * default and returns it to the caller.<br>
    * If the given property configured with a comma separated value, then the
    * first String before comma will be returned.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsStringWithDefault( String propertyName, String defaultValue )
   {
      return configAdapter.getPropertyAsStringWithDefault(propertyName, defaultValue);
   }
}
