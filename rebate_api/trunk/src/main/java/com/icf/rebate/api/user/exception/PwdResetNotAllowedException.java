package com.icf.rebate.api.user.exception;

import com.icf.rebate.api.util.UtilManager;

public class PwdResetNotAllowedException extends GenericUserException {

	public final String errorCode = "ESUB1008";
	private String errorMessage = UtilManager.getErrorMessage(errorCode);
	
	   /**
	    * Constructs an instance of <code>PwdResetNotAllowedException</code>.
	    */
	   public PwdResetNotAllowedException ()
	   {
	      super();
	   }

	   /**
	    * Constructs an instance of <code>PwdResetNotAllowedException</code> with the
	    * specified detail message.
	    */
	   public PwdResetNotAllowedException ( String msg )
	   {
	      super(msg);
	   }

	 
	   /**
	    * Constructs an instance of <code>PwdResetNotAllowedException</code> with the
	    * specified detail message.
	    * 
	    * @param e is the actual Exception, that is wrapped in this Custom
	    *           Exception.
	    */
	   public PwdResetNotAllowedException ( Exception e )
	   {
	      super(e);
	   }

	   /**
	    * Constructs an instance of <code>PwdResetNotAllowedException</code> with the
	    * specified detail message.
	    * 
	    * @param e is the actual Exception, that is wrapped in this Custom
	    *           Exception.
	    */
	   public PwdResetNotAllowedException ( String msg, Throwable e )
	   {
	      super(msg, e);
	   }

	   /*
	    * (non-Javadoc)
	    * 
	    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
	    */
	   @Override
	   public String getErrorCode()
	   {
	      return errorCode;
	   }

	   /*
	    * (non-Javadoc)
	    * 
	    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
	    */
	   @Override
	   public String getErrorMessage()
	   {
	      return errorMessage;
	   }

}
