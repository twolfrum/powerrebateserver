package com.icf.rebate.api.login.impl;

public class UserPasswordTrackingBean
{
   private String userName = "";
   private long userId = 0L;
   private String oldPassword = "";
   private String newPassword = "";
   private String confirmNewPassword = "";
   private String previousLoginTime = "";
   private String currentLoginTime = "";
   private String passwordUpdatedOn = "";
   private String resetFlag = "";
   private boolean isUserLoggedFirstTime = true;
   private boolean dataAvailable;

   /**
    * @return the userName
    */
   public String getUserName()
   {
      return userName;
   }

   /**
    * @param userName the userName to set
    */
   public void setUserName( String userName )
   {
      this.userName = userName;
   }

   /**
    * @return the userId
    */
   public long getUserId()
   {
      return userId;
   }

   /**
    * @param userId the userId to set
    */
   public void setUserId( long userId )
   {
      this.userId = userId;
   }

   /**
    * @return the oldPassword
    */
   public String getOldPassword()
   {
      return oldPassword;
   }

   /**
    * @param oldPassword the oldPassword to set
    */
   public void setOldPassword( String oldPassword )
   {
      this.oldPassword = oldPassword;
   }

   /**
    * @return the newPassword
    */
   public String getNewPassword()
   {
      return newPassword;
   }

   /**
    * @param newPassword the newPassword to set
    */
   public void setNewPassword( String newPassword )
   {
      this.newPassword = newPassword;
   }

   /**
    * @return the confirmNewPassword
    */
   public String getConfirmNewPassword()
   {
      return confirmNewPassword;
   }

   /**
    * @param confirmNewPassword the confirmNewPassword to set
    */
   public void setConfirmNewPassword( String confirmNewPassword )
   {
      this.confirmNewPassword = confirmNewPassword;
   }

   /**
    * @return the previousLoginTime
    */
   public String getPreviousLoginTime()
   {
      return previousLoginTime;
   }

   /**
    * @param previousLoginTime the previousLoginTime to set
    */
   public void setPreviousLoginTime( String previousLoginTime )
   {
      this.previousLoginTime = previousLoginTime;
   }

   /**
    * @return the currentLoginTime
    */
   public String getCurrentLoginTime()
   {
      return currentLoginTime;
   }

   /**
    * @param currentLoginTime the currentLoginTime to set
    */
   public void setCurrentLoginTime( String currentLoginTime )
   {
      this.currentLoginTime = currentLoginTime;
   }

   /**
    * @return the passwordUpdatedOn
    */
   public String getPasswordUpdatedOn()
   {
      return passwordUpdatedOn;
   }

   /**
    * @param passwordUpdatedOn the passwordUpdatedOn to set
    */
   public void setPasswordUpdatedOn( String passwordUpdatedOn )
   {
      this.passwordUpdatedOn = passwordUpdatedOn;
   }

   /**
    * @return the resetFlag
    */
   public String getResetFlag()
   {
      return resetFlag;
   }

   /**
    * @param resetFlag the resetFlag to set
    */
   public void setResetFlag( String resetFlag )
   {
      this.resetFlag = resetFlag;
   }

   /**
    * @return the isUserLoggedFirstTime
    */
   public boolean isUserLoggedFirstTime()
   {
      return isUserLoggedFirstTime;
   }

   /**
    * @param isUserLoggedFirstTime the isUserLoggedFirstTime to set
    */
   public void setUserLoggedFirstTime( boolean isUserLoggedFirstTime )
   {
      this.isUserLoggedFirstTime = isUserLoggedFirstTime;
   }

   public boolean getDataAvailable()
   {
      return dataAvailable;
   }

   public void setDataAvailable( boolean dataAvailable )
   {
      this.dataAvailable = dataAvailable;
   }
}
