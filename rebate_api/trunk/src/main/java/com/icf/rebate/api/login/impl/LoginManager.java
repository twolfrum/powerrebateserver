package com.icf.rebate.api.login.impl;

import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.login.exception.InActiveUserException;
import com.icf.rebate.api.login.exception.UserAuthenticationException;
import com.icf.rebate.api.login.exception.UserBlockedException;
import com.icf.rebate.api.login.exception.UserNameNotFoundException;
import com.icf.rebate.api.login.exception.UserNotAllowedException;
import com.icf.rebate.api.login.intrface.ILoginAdapter;
import com.icf.rebate.api.login.intrface.ILoginValidator;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
/**
 * This class implements Login operations against datasource.
 * 
 */
public class LoginManager
{
   private final String THIS_CLASS = "LoginManager";
   /**
    * Validator class to use to check the functionality.
    */
   private ILoginValidator loginValidator;
   /**
    * Plug in class which implements the login functionality. Login Manager
    * performs all its actions using this implementation class.
    */
   private ILoginAdapter loginAdapter;
   /**
    * logger object to log the messages.
    * 
    */
   private IMPLogger logger;
   private ConfigurationManager configManager;

   /**
    * Authenticates a user with the given user name and password.
    * 
    * @param userName - User to be authenticated with this user name.
    * @param password - User to be authenticated with this password.
    * @return user - User Profile for the given user.
    * @throws InternalServerException - If any runtime exception occurs.
    * @throws InvalidDataException - If user name or password fields contain
    *            invalid data.
    * @throws UserNameNotFoundException - If user does not exist for the given
    *            user name.
    * @throws UserAuthenticationException - If user enters Invalid password.
    * @throws UserBlockedException - throws if user is blocked.
    * 
    */
   public User authenticateUser( String userName, String password, boolean forWebConsole )
         throws InternalServerException, InvalidDataException, UserNameNotFoundException, UserAuthenticationException,
         InActiveUserException, UserBlockedException, UserNotAllowedException
   {
      final String THIS_METHOD = "authenticateUser";
      try
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Validating userName");
         this.loginValidator.validate(userName, password);
         return this.loginAdapter.authenticateAndGetUser(userName, password, true);
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException  occured while validating userName " , e);
         throw e;
      }
      catch (UserNameNotFoundException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserNameNotFoundException  occured while authenticating userName " , e);
         throw e;
      }
      catch (InActiveUserException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InActiveUserException  occured while authenticating userName " , e);
         throw e;
      }
      catch (UserAuthenticationException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserAuthenticationException  occured while authenticating userName " , e);
         throw e;
      }
      catch (UserBlockedException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserBlockedException  occured while authenticating userName " , e);
         throw e;
      }
      catch (UserNotAllowedException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserNotAllowedException  occured while authenticating userName " , e);
         throw e;
      }
      catch (InternalServerException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException  occured while authenticating userName " , e);
         throw e;
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception  occured while authenticating userName"
               , excep);
         throw new InternalServerException("Exception while authenticating userName " + excep);
      }
   }

   public ILoginValidator getLoginValidator()
   {
      return loginValidator;
   }

   public void setLoginValidator( ILoginValidator loginValidator )
   {
      this.loginValidator = loginValidator;
   }

   public ILoginAdapter getLoginAdapter()
   {
      return loginAdapter;
   }

   public void setLoginAdapter( ILoginAdapter loginAdapter )
   {
      this.loginAdapter = loginAdapter;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   public User forgetPasswordFromUserName( String userName ) throws UserDoesNotExist, InvalidDataException,
         InternalServerException
   {
      String THIS_METHOD = "forgetPasswordFromUserName";
      try
      {
         this.loginValidator.validateUserName(userName);
         return loginAdapter.forgetPasswordWithUserName(userName);
      }
      catch (UserDoesNotExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "SubscriberDoesNotExist occured while while resetting the password", e);
         throw e;
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException occured while while resetting the password", e);
         throw e;
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while retrieving last login message :"
               + ex.getMessage(), ex);
         throw new InternalServerException("Exception occured while resetting the password : " + ex.getMessage(), ex);
      }
   }

   public void resetPwdInternally( User user ) throws InternalServerException
   {
      String THIS_METHOD = "resetPwd";
      try
      {
         this.loginAdapter.resetPwdInternally(user);
      }
      catch (InternalServerException ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Exception occured while resetting pwd as part of edit user :" + ex.getMessage(), ex);
         throw new InternalServerException("Exception occured while resetting the password : " + ex.getMessage(), ex);
      }
   }

   public void deleteUserLoginData( long userId )
   {
      this.loginAdapter.deleteUserLoginData(userId);
   }

   /**
    * Update the logged in user details in to data source
    * 
    * @param pwdTrackingBean - UserPasswordTrackingBean info of user password
    *           tracking
    * @param isPwdUpdate - If true then update the password , If false then
    *           don't update password. just update other information
    * @param isAnswerUpdate - if true then encrypt the answer and update else
    *           don't encrypt
    * @throws InternalServerException - If any exception occurred while updating
    *            logged in user details
    * @throws InvalidDataException - Throws if any validation error
    */
   public PasswordResponse updateLoggedInUserDetail( UserPasswordTrackingBean pwdTrackingBean, boolean isPwdUpdate )
         throws InvalidDataException, InternalServerException
   {
      final String THIS_METHOD = "updateLoggedInUserDetail";
      try
      {
         PasswordResponse response = isPasswordCorrect(pwdTrackingBean);
         if (response.isSuccess())
         {
            this.loginValidator.validatePasswordTrackingBean(pwdTrackingBean);
            this.loginAdapter.updateLoggedInUserDetail(pwdTrackingBean, isPwdUpdate);
         }
         return response;
      }
      catch (InvalidDataException ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException occured while updating logged user details", ex);
         throw ex;
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while updating logged user details :"
               + ex.getMessage(), ex);
         throw new InternalServerException(
               "Exception occured while updating logged user details  : " + ex.getMessage(), ex);
      }
   }

   /**
    * Check that password is correct or not
    * 
    * @param userBean - UserPasswordTrackingBean bean contain password info
    * @return PasswordResponse bean proper flag and error massage
    * @throws InternalServerException - Throws if any exception occurred
    * @throws InvalidDataException - Throws if any validation error
    */
   public PasswordResponse isPasswordCorrect( UserPasswordTrackingBean userBean ) throws InvalidDataException,
         InternalServerException
   {
      final String THIS_METHOD = "isPasswordCorrect";
      try
      {
         loginValidator.validatePasswordCorrect(userBean);
         return this.loginAdapter.isPasswordCorrect(userBean);
      }
      catch (InvalidDataException ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException occured while checking password correct or not", ex);
         throw ex;
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Exception occured while checking password correct or not :" + ex.getMessage(), ex);
         throw new InternalServerException("Exception occured while checking password correct or not : "
               + ex.getMessage(), ex);
      }
   }

   public ConfigurationManager getConfigManager()
   {
      return configManager;
   }

   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }
}
