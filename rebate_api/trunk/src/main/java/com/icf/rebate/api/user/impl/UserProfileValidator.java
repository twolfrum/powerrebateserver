package com.icf.rebate.api.user.impl;

import java.util.regex.Pattern;

import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.UtilManager;
public class UserProfileValidator extends AbstractValidator
{
   public static final String VSUB1001 = "VSUB1001";
   public static final String VSUB1005 = "VSUB1005";
   public static final String VSUB1006 = "VSUB1006";
   public static final String VSUB1007 = "VSUB1007";
   public static final String VSUB1010 = "VSUB1010";
   public static final String VSUB1012 = "VSUB1012";
   public static final String VSUB1013 = "VSUB1013";
   public static final String VSUB1035 = "VSUB1035";
   public static final String VSUB1036 = "VSUB1036";
   public static final String VSUB1037 = "VSUB1037";
   public static final String VSUB1038 = "VSUB1038";
   public static final String VSUB1039 = "VSUB1039";

   /**
    * This validate method validates all the required Subscriber Profile
    * fields.if any field is not valid means it will add the corresponding
    * errormessages to errorlist.
    * 
    * @param profile - SubscriberProfile object.
    * @param flag - is true means it is update operation,we need to validate
    *           userId also. - is false means it is insert operation.
    * @throws InvalidDataException occurs when atleast one error occurs while
    *            validation
    */
   public void validateUserProfile( User profile, boolean prevalid ) throws InvalidDataException
   {
      if (profile != null)
      {
         if (! prevalid)
         {
            validateUserName(profile.getUserName());
         }
         validateFirstName(profile.getFirstName());
         validateLastName(profile.getLastName());
         validateUserStatus(profile.getUserStatus());
         validateUserRoleId(profile.getUserRole());
         validateEmail(profile.getEmailId());
         validatePhoneNumber(profile.getPhone());
         validatePassword(profile.getPassword());
         validatePassword(profile.getVerifyPassword());
         validateTaxId(profile.getTaxId());
         if (profile.getUserId() != 0)
         {
            validateUserId(profile.getUserId());
         }
         /*
          * if (profile.getPassword() != null &&
          * profile.getPassword().length()>0) {
          * validatePassword(profile.getPassword()); }
          */
      }
      else
      {
         addError(UtilManager.getErrorMessage(VSUB1001));
      }
      checkInvalidDataException();
   }

   /**
    * This validateUserId method is used to verify whether userID is valid or
    * not.if userID is not valid,then corresponding ErrorMessage is added to
    * errorList.
    * 
    * @param userID of selected SubscriberProfile
    * @throws InvalidDataException occurs when atleast one error occurs while
    *            validation
    */
   public void validateUserId( long userID ) throws InvalidDataException
   {
      if (! isValidNumber(userID))
      {
         addError(UtilManager.getErrorMessage(VSUB1005));
      }
      checkInvalidDataException();
   }

   /**
    * This validateUserName method is used to verify whether userName is valid
    * or not.if userName is not valid,then corresponding ErrorMessage is added
    * to errorList.
    * 
    * @param userName of selected SubscriberProfile
    */
   public void validateUserName( String userName )
   {
      if (! isNotNull(userName))
      {
         addError(UtilManager.getErrorMessage(VSUB1006));
      }
   }

   public void validateFirstName( String firstName )
   {
      if (! isNotNull(firstName))
      {
         addError(UtilManager.getErrorMessage(VSUB1035));
      }
   }

   public void validateLastName( String lastName )
   {
      if (! isNotNull(lastName))
      {
         addError(UtilManager.getErrorMessage(VSUB1036));
      }
   }

   public boolean validateEmail( String email )
   {
      boolean isValid = false;
	  String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w-]+\\.)+[\\w]+[\\w]$";
      if (isNotNull(email))
      {
         if (! email.matches(EMAIL_REGEX)) {
        	 addError(UtilManager.getErrorMessage(VSUB1013));
         }
         else {
        	 isValid = true;
         }
      }
      else
      {
         addError(UtilManager.getErrorMessage(VSUB1013));
      }
      return isValid;
   }

   public void validatePhoneNumber( String phoneNo )
   {
      if (isNotNull(phoneNo))
      {
         if (! phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}"))
         {
            addError(UtilManager.getErrorMessage(VSUB1038));
         }
      }
   }

   /**
    * This validatePassword method is used to verify whether password is valid
    * or not.if password is not valid,then corresponding ErrorMessage is added
    * to errorList.
    * 
    * @param password of selected SubscriberProfile
    * @throws InvalidDataException occurs when atleast one error occurs while
    *            validation
    */
   public void validatePassword( String password ) throws InvalidDataException
   {
      if (! isNotNull(password))
      {
         addError(UtilManager.getErrorMessage(VSUB1007));
      }
      checkInvalidDataException();
   }

   /**
    * This validateStatusCD method is used to verify whether statusCD is valid
    * or not.if statusCD is not valid,then corresponding ErrorMessage is added
    * to errorList.
    * 
    * @param statusCD of selected SubscriberProfile
    * @throws InvalidDataException occurs when atleast one error occurs while
    *            validation
    */
   public void validateUserStatus( long statusCD ) throws InvalidDataException
   {
      if (! isValidNumber(statusCD))
      {
         addError(UtilManager.getErrorMessage(VSUB1012));
      }
      checkInvalidDataException();
   }

   /**
    * Validate the user's role
    * 
    * @param userRoleId
    * @throws InvalidDataException
    */
   public void validateUserRoleId( long userRoleId ) throws InvalidDataException
   {
      if (! isValidNumber(userRoleId))
      {
         addError(UtilManager.getErrorMessage(VSUB1010));
      }
      checkInvalidDataException();
   }
   
   /**
    * Validates the user's taxId
    * @param taxID
    * @throws InvalidDataException
    */
   
   public void validateTaxId(String taxId) throws InvalidDataException
   {
	   if (isNotNull(taxId)) {
		   	Pattern pattern = Pattern.compile("[\\d-]{1,255}");
		   	if (!pattern.matcher(taxId).matches()) {
		   		addError(UtilManager.getErrorMessage(VSUB1039));
		   	}
	   }
	   checkInvalidDataException();
   }

   public void validateChangePassword( String password ) throws InvalidDataException
   {
      if (! isNotNull(password))
      {
         addError(UtilManager.getErrorMessage(VSUB1037));
      }
      checkInvalidDataException();
   }
}
