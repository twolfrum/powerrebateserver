package com.icf.rebate.api.user.exception;

import com.icf.rebate.api.util.UtilManager;
/**
 * This class UserAlreadyExist  represents use case User already exists with this name.
 *
 */
public class UserAlreadyExist extends GenericUserException
{
   /**
    * 
    */
   private static final long serialVersionUID = 85452779982390614L;
   private String errorCode = "ESUB1001";
   private String errorMessage = UtilManager.getErrorMessage(errorCode);

   /**
    * Constructs an instance of <code>InvalidConfiguration</code>.
    */
   public UserAlreadyExist ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>UserAlreadyExist</code> with the
    * specified detail message.
    */
   public UserAlreadyExist ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String mportalIdentifier = "05131966";

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public UserAlreadyExist ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public UserAlreadyExist ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }
}
