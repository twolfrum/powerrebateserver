package com.icf.rebate.api.login.impl;

import java.nio.charset.Charset;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.login.exception.InActiveUserException;
import com.icf.rebate.api.login.exception.UserAuthenticationException;
import com.icf.rebate.api.login.exception.UserBlockedException;
import com.icf.rebate.api.login.exception.UserNameNotFoundException;
import com.icf.rebate.api.login.intrface.ILoginAdapter;
import com.icf.rebate.api.login.intrface.ILoginValidator;
import com.icf.rebate.api.restservice.AbstractManager;
import com.icf.rebate.api.restservice.BuildResponse;
import com.icf.rebate.api.user.exception.PwdResetNotAllowedException;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.user.impl.UserManager;
import com.icf.rebate.api.util.BStatus;
import com.icf.rebate.api.util.CharacterSets;
import com.icf.rebate.api.util.PasswordGenerator;
import com.icf.rebate.api.util.PasswordGenerator.PasswordCharacterSet;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.api.util.UtilManager;
/**
 * This class contains implementation for login,logout,change password and reset
 * request
 * 
 */
public class LoginRestManager extends AbstractManager
{
   private final String THIS_CLASS = "LoginRestManager";
   /**
    * Plug in class which implements the login functionality. Login Manager
    * performs all its actions using this implementation class.
    */
   private ILoginAdapter loginAdapter;
   /**
    * logger object to log the messages.
    * 
    */
   private IMPLogger logger;
   private ConfigurationManager configManager;
   private LoginManager loginManager;
   private UserManager userManager;
   /**
    * Validator class to use to check the functionality.
    */
   private ILoginValidator loginValidator;

   public ILoginValidator getLoginValidator()
   {
      return loginValidator;
   }

   public void setLoginValidator( ILoginValidator loginValidator )
   {
      this.loginValidator = loginValidator;
   }

   public ILoginAdapter getLoginAdapter()
   {
      return loginAdapter;
   }

   public void setLoginAdapter( ILoginAdapter loginAdapter )
   {
      this.loginAdapter = loginAdapter;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   public ConfigurationManager getConfigManager()
   {
      return configManager;
   }

   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }

   /**
    * Authenticates a user with the given user name and password.
    * 
    * @param userName - User to be authenticated with this user name.
    * @param password - User to be authenticated with this password.
    * @return user - User Profile for the given user.
    * @throws InternalServerException - If any runtime exception occurs.
    * @throws InvalidDataException - If user name or password fields contain
    *            invalid data.
    * @throws UserNameNotFoundException - If user does not exist for the given
    *            user name.
    * @throws UserAuthenticationException - If user enters Invalid password.
    * @throws UserBlockedException - throws if user is blocked.
    * 
    */
   public Response authenticateUser( HttpServletRequest request, User userDetails ) throws Exception
   {
      final String THIS_METHOD = "authenticateUser";
      Response response = null;
      byte[] responseData = null;
      User user = new User();
      BStatus status = new BStatus();
      BuildResponse responseBuilder = new BuildResponse();
      try
      {
         UtilManager.userNameValidation(userDetails);
         loginValidator.validate(userDetails);
         logger.log(
               IMPLogger.INFO,
               THIS_CLASS,
               THIS_METHOD,
               "Validating userName for userName " + userDetails.getUserName() + " with deviceId "
                     + userDetails.getDeviceId());
         this.loginValidator.validate(userDetails.getUserName(), userDetails.getPassword());
         user = this.loginAdapter.authenticateAndGetUser(userDetails.getUserName(), userDetails.getPassword());
         // need to set the pwd explicitly as null so that it is not shown to
         // client
         user.setPassword(null);
         user.setVerifyPassword(null);
         // if user is admin not allow to login
         // Changed as of 1/19/2016 - admin is allowed to log in
         /*if (user.getUserRole() == RebateApiConstant.ROLE_ADMIN)
         {
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "admin user is not allowed to logn.");
            status.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
                  "CODE_FAILURE_INTERNAL"));
            status.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
                  "MSG_ADMIN_LOGIN"));
            responseData = status.formatJSONString().getBytes(Charset.forName("UTF-8"));
            responseBuilder.setResponseCode(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FAILURE_INTERNAL"));
            responseBuilder.setResponseMsg(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_ADMIN_LOGIN"));
            responseBuilder.setResponseObject(responseData);
            response = buildRESTResponse(responseBuilder, user.getUserId());
            return response;
         }*/
         // check if user is logged in 1st time through client and if so send
         // back change password as message
         if (user.getResetFlag().equals("N"))
         {
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
                  "User is logging in for the 1st time and must change password");
            status.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
                  "CODE_FIRST_LOGIN_FAILURE"));
            status.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
                  "MSG_FIRST_LOGIN_FAILURE"));
            responseData = status.formatJSONString().getBytes(Charset.forName("UTF-8"));
            responseBuilder.setResponseCode(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_FIRST_LOGIN_FAILURE"));
            responseBuilder.setResponseMsg(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_FIRST_LOGIN_FAILURE"));
            responseBuilder.setResponseObject(responseData);
            response = buildRESTResponse(responseBuilder, user.getUserId());
            return response;
         }
         responseData = user.formatJSONString().getBytes(Charset.forName("UTF-8"));
         responseBuilder.setResponseCode(user.getStatusCode());
         responseBuilder.setResponseMsg(user.getStatusMsg());
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException  occured while validating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (UserNameNotFoundException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserNameNotFoundException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (InActiveUserException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InActiveUserException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INACTIVE_USER"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE_INACTIVE_USER"));
      }
      catch (UserAuthenticationException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserAuthenticationException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (UserBlockedException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserBlockedException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (InternalServerException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE"));
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception  occured while authenticating userName"
               , excep);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE"));
      }
      responseBuilder.setResponseObject(responseData);
      response = buildRESTResponse(responseBuilder, user.getUserId());
      return response;
   }

   
   /**
    * Reset password - make password flag false, and reset password
    * @param request = HttpServletRequest
    * @param userDetails
    * @return success - status code and message, failure - error code
    * @throws Exception
    */
   public Response loginResetPassword(HttpServletRequest request, User userDetails) throws Exception {
	   final String THIS_METHOD = "loginResetPassword";
	   
	   Response response = null;
	   BuildResponse responseBuilder = new BuildResponse();
	   byte[] responseData = null;
	   responseBuilder.setResponseObject("Reset password failed.".getBytes()); 
	   try {
		   // Null check
		   if (userDetails == null) {
			   logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "ERROR tried to reset the password of a null User reference");
		       throw new Exception();
		   }
		   
		   // responseData = userDetails.formatJSONString().getBytes(Charset.forName("UTF-8"));
		   logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Begin password reset for user: " + userDetails.getUserName() + " with deviceId: " + userDetails.getDeviceId());
		   
		   loginAdapter.forgetPassword(userDetails.getUserName());
		   responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
		   responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_CHANGE_PWD_SUCCESS"));
		   responseBuilder.setResponseObject("Password reset successful...".getBytes());
	   }
	   	  catch (PwdResetNotAllowedException e) {
	   		  logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "PwdResetNotAllowedException occured while resetting password for userName");
	   		  responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
		               "CODE_FAILURE_PWD_RESET_NOT_ALLOWED"));
		      responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
		               "MSG_FAILURE_PWD_RESET_NOT_ALLOWED"));
	   	  }
	      catch (UserNameNotFoundException e)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	               "UserNameNotFoundException occured while resetting password for userName " , e);
	         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "CODE_FAILURE_INVALID_USER_PASSWORD"));
	         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "MSG_LOGIN_FAILURE"));
	      }
	      catch (InActiveUserException e)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	               "InActiveUserException occured while resetting password for userName " , e);
	         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "CODE_FAILURE_INACTIVE_USER"));
	         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "MSG_FAILURE_INACTIVE_USER"));
	      }
	      catch (UserBlockedException e)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	               "UserBlockedException occured while resetting password for userName " , e);
	         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "CODE_FAILURE_INVALID_USER_PASSWORD"));
	         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "MSG_LOGIN_FAILURE"));
	      }
	      catch (InternalServerException e)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	               "InternalServerException occured while resetting password for userName " , e);
	         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "CODE_FAILURE_INTERNAL"));
	         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "MSG_FAILURE"));
	      }
	      catch (Exception excep)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while resetting password for userName"
	               , excep);
	         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "CODE_FAILURE_INTERNAL"));
	         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
	               "MSG_FAILURE"));
	      }
	   response = buildRESTResponse(responseBuilder, userDetails != null ? userDetails.getUserId() : -1);
	   return response;
   }
   
   /**
    * Change existing password with the new password.
    * 
    * @param request - HttpServletRequest
    * @param userDetails - oldpassowrd for authentication
    * @param userDetails - newpassword update with oldpassword
    * @return Success response with user profile for given user
    * @throws UserAuthenticationException - throws if user validation fails
    * @throws UserDoesNotExist - throws if user does not exists
    * @throws Exception
    */
   public Response changePwd( HttpServletRequest request, User userDetails ) throws Exception
   {
      final String THIS_METHOD = "changePwd";
      Response response = null;
      byte[] responseData = null;
      BuildResponse responseBuilder = new BuildResponse();
      User exisitngUserDetails = null;
      BStatus status = new BStatus();
      try
      {
         UtilManager.userNameValidation(userDetails);
         loginValidator.validateChangePwd(userDetails);
         UserPasswordTrackingBean userBean = new UserPasswordTrackingBean();
         exisitngUserDetails = this.loginAdapter.authenticateAndGetUser(userDetails.getUserName(),
               userDetails.getOldPassword());
         userBean.setUserId(exisitngUserDetails.getUserId());
         userBean.setUserName(exisitngUserDetails.getUserName());
         userBean.setOldPassword(exisitngUserDetails.getPassword());
         userBean.setNewPassword(userDetails.getNewPassword());
         userBean.setConfirmNewPassword(userDetails.getNewPassword());
         PasswordResponse pwdResponse = loginManager.updateLoggedInUserDetail(userBean, true);
         if (! pwdResponse.isSuccess())
         {
            List<String> errorMessages = pwdResponse.getErrorMesseages();
            if (errorMessages != null && errorMessages.size() > 0)
            {
               logger.log(IMPLogger.ERROR, userDetails.getUserName(), THIS_CLASS, THIS_METHOD,
                     "Error reported from API:" + errorMessages.get(0));
               responseBuilder.setResponseCode(configManager.getPropertyAsString(
                     RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_CHANGE_PWD_FAILURE"));
               responseBuilder.setResponseMsg(errorMessages.get(0));
            }
         }
         else
         {
            logger.log(IMPLogger.INFO, userDetails.getUserName(), THIS_CLASS, THIS_METHOD,
                  "Action:Password changed successfully.");
            // modify the RESET_FLAG in user_profile table
            userManager.modifyPwdResetFlag(true, userBean.getUserId());
            exisitngUserDetails.setPassword(null);
            exisitngUserDetails.setVerifyPassword(null);
            exisitngUserDetails.setStatusCode(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
            exisitngUserDetails.setStatusMsg(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_CHANGE_PWD_SUCCESS"));
            responseData = exisitngUserDetails.formatJSONString().getBytes(Charset.forName("UTF-8"));
            responseBuilder.setResponseCode(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
            responseBuilder.setResponseMsg(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_CHANGE_PWD_SUCCESS"));
         }
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException  occured while validating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         status.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         if (e.getErrorList() != null)
         {
            responseBuilder.setResponseMsg(e.getErrorList().get(0));
            status.setStatusMsg(e.getErrorList().get(0));
         }
         else
         {
            responseBuilder.setResponseMsg(configManager.getPropertyAsString(
                  RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_LOGIN_FAILURE"));
            status.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
                  "MSG_LOGIN_FAILURE"));
         }
         responseData = status.formatJSONString().getBytes(Charset.forName("UTF-8"));
      }
      catch (InternalServerException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE"));
      }
      catch (UserDoesNotExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserDoesNotExist  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (UserAuthenticationException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserDoesNotExist  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         // responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,"MSG_LOGIN_FAILURE"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_INCORRECT_PASSWORD"));
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception  occured while authenticating userName"
               , excep);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE"));
      }
      responseBuilder.setResponseObject(responseData);
      response = buildRESTResponse(responseBuilder, 0);
      return response;
   }

   public LoginManager getLoginManager()
   {
      return loginManager;
   }

   public void setLoginManager( LoginManager loginManager )
   {
      this.loginManager = loginManager;
   }

   public UserManager getUserManager()
   {
      return userManager;
   }

   public void setUserManager( UserManager userManager )
   {
      this.userManager = userManager;
   }

   /**
    * User logout request
    * @param request - HttpServletRequest
    * @param userDetails - contains userName for logout
    * @return success - status code and message , failure - error code and message
    */
   public Response logoutUser( HttpServletRequest request, User userDetails )
   {
      final String THIS_METHOD = "logoutUser";
      Response response = null;
      User user = new User();
      BuildResponse responseBuilder = new BuildResponse();
      try
      {
         UtilManager.userNameValidation(userDetails);
         logger.log(
               IMPLogger.INFO,
               THIS_CLASS,
               THIS_METHOD,
               "Validating userName for userName " + userDetails.getUserName() + " with deviceId "
                     + userDetails.getDeviceId());
         this.loginValidator.validate(userDetails.getUserName(), userDetails.getPassword());
         user = this.loginAdapter.authenticateAndGetUser(userDetails.getUserName(), userDetails.getPassword());
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_SUCCESS"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGOUT_SUCCESS"));
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception  occured while user logout" , excep);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGOUT_FAILURE"));
      }
      response = buildRESTResponse(responseBuilder, user.getUserId());
      return response;
   }

   /**
    * Reset login -  make password flag as false
    * @param request - HttpServletRequest
    * @param userDetails
    * @return success - status code and message , failure - error code and message
    */
   public Response loginReset( HttpServletRequest request, User userDetails )
   {
      final String THIS_METHOD = "loginReset";
      Response response = null;
      User user = new User();
      BuildResponse responseBuilder = new BuildResponse();
      try
      {
         UtilManager.userNameValidation(userDetails);
         logger.log(
               IMPLogger.INFO,
               THIS_CLASS,
               THIS_METHOD,
               "Validating userName for userName " + userDetails.getUserName() + " with deviceId "
                     + userDetails.getDeviceId());
         this.loginValidator.validate(userDetails.getUserName(), userDetails.getPassword());
         user = this.loginAdapter.authenticateAndGetUser(userDetails.getUserName(), userDetails.getPassword());
         this.loginAdapter.resetPasswordFlag(user.getUserId(), false);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_SUCCESS"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGOUT_SUCCESS"));
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception  occured while user logout" , excep);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGOUT_FAILURE"));
      }
      response = buildRESTResponse(responseBuilder, user.getUserId());
      return response;
   }
}
