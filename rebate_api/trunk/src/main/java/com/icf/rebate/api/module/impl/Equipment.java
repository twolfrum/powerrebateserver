package com.icf.rebate.api.module.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "name", "items" })
public class Equipment implements Serializable
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String name;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<Item> items;

   public String getName()
   {
      return name;
   }

   public void setName( String name )
   {
      this.name = name;
   }

   public List<Item> getItems()
   {
      return items;
   }

   public void setItems( List<Item> items )
   {
      this.items = items;
   }

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }
}
