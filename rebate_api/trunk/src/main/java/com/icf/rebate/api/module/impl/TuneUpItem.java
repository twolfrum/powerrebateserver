package com.icf.rebate.api.module.impl;

import java.io.IOException;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@XmlType(propOrder = { "split", "packages" })

public class TuneUpItem implements Serializable {

	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private Split split;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private Packages packages;

	public Split getSplit() {
		return split;
	}

	public void setSplit(Split split) {
		this.split = split;
	}

	public Packages getPackages() {
		return packages;
	}

	public void setPackages(Packages packages) {
		this.packages = packages;
	}

	public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(this);
	}

	@Override
	public String toString() {
		return "TuneUpItem [split size=" + split + ", packages=" + packages + "]";
	}
}
