package com.icf.rebate.api.restservice;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.icf.rebate.api.login.impl.LoginRestManager;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.util.ThreadLocalImpl;
import com.sun.jersey.api.client.ClientResponse.Status;
@Path("/user")
public class LoginRestService
{
   @Autowired
   private LoginRestManager loginRestManager;
   
   @Autowired
   private JavaMailSender mailSender;

   @POST
   @Path("/login")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   public Response authenticateUserService( User userDetails, @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
	  System.out.println("Login Requested"); 
 
      ThreadLocalImpl.setThreadLocalUserInfo(request, userDetails.getUserName());
      Response response = loginRestManager.authenticateUser(request, userDetails);
      return response;
   }

   @POST
   @Path("/changepwd")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   public Response changepwdUserService( User userDetails, @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
      ThreadLocalImpl.setThreadLocalUserInfo(request, userDetails.getUserName());
      Response response = loginRestManager.changePwd(request, userDetails);
      return response;
   }

   @POST
   @Path("/logout")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   public Response logoutUser( User userDetails, @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
      ThreadLocalImpl.setThreadLocalUserInfo(request, userDetails.getUserName());
      Response response = loginRestManager.logoutUser(request, userDetails);
      return response;
   }

   @POST
   @Path("/loginreset")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   public Response loginReset( User userDetails, @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
      ThreadLocalImpl.setThreadLocalUserInfo(request, userDetails.getUserName());
      Response response = loginRestManager.loginReset(request, userDetails);
      return response;
   }
   
   @POST
   @Path("resetpassword")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   public Response resetPassword(User userDetails, @Context HttpHeaders httpHeaders, @Context HttpServletRequest request) throws Exception
   {
	   ThreadLocalImpl.setThreadLocalUserInfo(request, userDetails.getUserName());
	   Response response = null;
	   try { 
//	   SimpleMailMessage email = new SimpleMailMessage();
//	   email.setTo("foo@bar.com");
//	   email.setSubject("Power Rebate Password Reset");
//	   email.setText("Your new password is: passw0rd!");
//	   mailSender.send(email);
	   response = loginRestManager.loginResetPassword(request, userDetails);
	   } catch (Exception e) {
		   // Fail safe
		   response = Response.status(Status.OK)
				   .entity("Password reset failed")
				   .build();
	   }
	   return response;
   }
}
