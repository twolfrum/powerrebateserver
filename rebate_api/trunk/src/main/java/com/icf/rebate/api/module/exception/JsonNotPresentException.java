package com.icf.rebate.api.module.exception;

import com.icf.rebate.api.user.exception.GenericUserException;
import com.icf.rebate.api.util.UtilManager;

/**
 * THis class EmailIdAlreadyExist represents use case email id already exists 
 *
 */
public class JsonNotPresentException extends GenericUserException
{
   /**
    * 
    */
   private static final long serialVersionUID = - 1282958732041617557L;
   private String errorCode = "ESUB1004";
   private String errorMessage = UtilManager.getErrorMessage(errorCode);

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code>.
    */
   public JsonNotPresentException ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code> with the
    * specified detail message.
    */
   public JsonNotPresentException ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String mportalIdentifier = "05131966";

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public JsonNotPresentException ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public JsonNotPresentException ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }
}
