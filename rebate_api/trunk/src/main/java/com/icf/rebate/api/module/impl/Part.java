package com.icf.rebate.api.module.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "id", "name", "xmlName", "picture", "options", "mandatory" })
public class Part implements Serializable
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String id;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String name;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String xmlName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String picture;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<Options> options;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String mandatory;

   public String getId()
   {
      return id;
   }

   public void setId( String id )
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName( String name )
   {
      this.name = name;
   }

   public String getPicture()
   {
      return picture;
   }

   public void setPicture( String picture )
   {
      this.picture = picture;
   }

   public List<Options> getOptions()
   {
      return options;
   }

   public void setOptions( List<Options> options )
   {
      this.options = options;
   }

   public String getMandatory()
   {
      return mandatory;
   }

   public void setMandatory( String mandatory )
   {
      this.mandatory = mandatory;
   }

   public String getXmlName()
   {
      return xmlName;
   }

   public void setXmlName( String xmlName )
   {
      this.xmlName = xmlName;
   }

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }
}
