package com.icf.rebate.api.user.intrface;

import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.user.exception.EmailIdAlreadyExist;
import com.icf.rebate.api.user.exception.InvalidPasswordException;
import com.icf.rebate.api.user.exception.UserAlreadyExist;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
/**
 * This interface ILoginValidator will expose validation api for login
 * functionalities.
 * 
 */
public interface IUserValidator
{
   /**
    * Validates the given userName and password.
    * 
    * @param userName - User name to be validated.
    * @param password - Password to be validated.
    * @throws InvalidDataException - If user name or password fields contain
    *            invalid data.
    */
   // public void validate( String userName, String password ) throws
   // InvalidDataException;
   /**
    * Validates the addition of a user against the datasource.
    * 
    * @param user - User details bean
    * @throws UserAlreadyExist , is thrown if the user already present in
    *            the data store.
    * @throws InvalidDataException ,is thrown if any of the user
    *            information is not valid.
    * @throws InvalidPasswordException - throws if password and verifyPassword
    *            does not match
    */
   public void validateAddSubscriber( User user ) throws UserAlreadyExist, InvalidDataException, EmailIdAlreadyExist,
         InvalidPasswordException;

   /**
    * Validates the updation of a user against the datasource.
    * 
    * @param user - User details bean
    * @throws UserAlreadyExist , is thrown if the user already present in
    *            the data store.
    * @throws InvalidDataException ,is thrown if any of the user
    *            information is not valid.
    * @throws InvalidPasswordException - throws if password and verifyPassword
    *            does not match
    */
   public void validateUpdateUser( User user ) throws UserDoesNotExist, InvalidDataException, EmailIdAlreadyExist,
         InvalidPasswordException;

   /**Validate User Id before deleting from DB
    * 
    * @param userId - to which validate 
    * @throws InvalidDataException - throws if data is invalid.
    */
   public void validateDeleteUserInfo( long userId ) throws InvalidDataException;
}
