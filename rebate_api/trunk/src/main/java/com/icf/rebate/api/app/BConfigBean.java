package com.icf.rebate.api.app;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.util.BStatus;
@XmlRootElement
@XmlType(propOrder = { "language", "changePassword", "sessionExpTime", "sessionExpTimeLogout", "appVersion", "upgradeURL", "serverURL",
      "analyticsKey","reportIssue","emailTo","noOfSubmittedJobs","utilityCompanies" })
public class BConfigBean extends BStatus
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String language;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String changePassword;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   int sessionExpTime;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   int sessionExpTimeLogout;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String appVersion;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String upgradeURL;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String serverURL;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String analyticsKey;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String termsAndCond;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   boolean reportIssue;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String emailTo;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   int noOfSubmittedJobs;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<Company> utilityCompanies;
   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }

   public String getLanguage()
   {
      return language;
   }

   public void setLanguage( String language )
   {
      this.language = language;
   }

   public String getChangePassword()
   {
      return changePassword;
   }

   public void setChangePassword( String changePassword )
   {
      this.changePassword = changePassword;
   }

   public String getAppVersion()
   {
      return appVersion;
   }

   public void setAppVersion( String appVersion )
   {
      this.appVersion = appVersion;
   }

   public String getUpgradeURL()
   {
      return upgradeURL;
   }

   public void setUpgradeURL( String upgradeURL )
   {
      this.upgradeURL = upgradeURL;
   }

   public String getServerURL()
   {
      return serverURL;
   }

   public void setServerURL( String serverURL )
   {
      this.serverURL = serverURL;
   }

   public String getAnalyticsKey()
   {
      return analyticsKey;
   }

   public void setAnalyticsKey( String analyticsKey )
   {
      this.analyticsKey = analyticsKey;
   }

   public List<Company> getUtilityCompanies()
   {
      return utilityCompanies;
   }

   public void setUtilityCompanies( List<Company> utilityCompanies )
   {
      this.utilityCompanies = utilityCompanies;
   }

   public String getTermsAndCond()
   {
      return termsAndCond;
   }

   public void setTermsAndCond( String termsAndCond )
   {
      this.termsAndCond = termsAndCond;
   }

   public int getSessionExpTime()
   {
      return sessionExpTime;
   }

   public void setSessionExpTime( int sessionExpTime )
   {
      this.sessionExpTime = sessionExpTime;
   }

   public boolean isReportIssue()
   {
      return reportIssue;
   }

   public void setReportIssue( boolean reportIssue )
   {
      this.reportIssue = reportIssue;
   }

   public String getEmailTo()
   {
      return emailTo;
   }

   public void setEmailTo( String emailTo )
   {
      this.emailTo = emailTo;
   }

   public int getNoOfSubmittedJobs()
   {
      return noOfSubmittedJobs;
   }

   public void setNoOfSubmittedJobs( int noOfSubmittedJobs )
   {
      this.noOfSubmittedJobs = noOfSubmittedJobs;
   }

   public int getSessionExpTimeLogout() {
	   return sessionExpTimeLogout;
   }

   public void setSessionExpTimeLogout(int sessionExpTimeLogout) {
	   this.sessionExpTimeLogout = sessionExpTimeLogout;
   }
   
}
