package com.icf.rebate.api.user.intrface;

import java.security.DigestException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
/**
 * This interface ILoginAdapter will expose all the login operation needs to be
 * done.
 * 
 */
public interface IUserAdapter
{
   public Map<Integer, User> getAllUsers();

   public List<User> getUserList();

   public void addUser( User user ) throws DigestException, NoSuchAlgorithmException, 
   			CloneNotSupportedException, InternalServerException;

   public Map<String, Long> getRoleMap();

   public User getUserById( long userId );

   public void updateUser( User user ) throws InternalServerException;

   public void deleteUser( long userId ) throws UserDoesNotExist;

   public Map<String, Long> getUserStatusMap();

   public List<User> searchUser( String searchUser, String searchCompany );

   public User getUserDetails( String userName ) throws UserDoesNotExist;

   public void modifyPwdResetFlag( boolean flag, long userId );
   
   public List<User> getUserListForManager(long userId);
   
   public Map<String, Long> getRoleMapForManager();
   
   public void addUserToManager(User manager, User user);
}
