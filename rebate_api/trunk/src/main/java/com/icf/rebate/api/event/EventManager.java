package com.icf.rebate.api.event;

import java.util.ArrayList;

import com.icf.rebate.api.reload.IRefreshListener;
import com.icf.rebate.api.util.UtilManager;
/**
 * 
 * This class manages all the events and update the interested clients when
 * triggered by specific event.
 */
public class EventManager
{
   private ArrayList<IRefreshListener> refreshListenerList = new ArrayList<IRefreshListener>();
   private boolean dataRefreshListenerThrownError = false;
   private String THIS_CLASS = EventManager.class.getName();

   /**
    * Add IReloadResourceListener instance to the list.
    * 
    * @param addInstanceList , IReloadResourceListener class.
    */
   public void addRefreshListner( IRefreshListener addInstanceList )
   {
      this.refreshListenerList.add(addInstanceList);
   }

   public ArrayList<IRefreshListener> getRefreshListnerList()
   {
      return this.refreshListenerList;
   }

   /**
    * Notifies Refresh Listener.
    * 
    */
   public void notifyRefreshListeners()
   {
      /*
       * Clear the JVM Resource Bundle cache first and then notify the
       * listeners.
       */
      UtilManager.clearJVMResourceBundleCache();
      for (IRefreshListener refreshListner : refreshListenerList)
      {
         refreshListner.refreshNotify();
      }
   }
}
