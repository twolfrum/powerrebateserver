package com.icf.rebate.api.company.impl;

import java.util.List;

import com.icf.rebate.api.company.exception.CompanyAlreadyExist;
import com.icf.rebate.api.company.exception.CompanyHasUsersException;
import com.icf.rebate.api.company.intrface.ICompanyDao;
import com.icf.rebate.api.company.intrface.ICompanyValidator;
import com.icf.rebate.api.exception.InvalidDataException;
/**
 * This class provides the basic validation rules that are applicable for
 * creating the company.
 * 
 */
public class DefaultCompanyValidator implements ICompanyValidator
{
   private ICompanyDao companyDao = null;

   /**Validate company details before adding into DB.
    * @param company - contains company details
    */
   public void validateAddCompany( Company company ) throws CompanyAlreadyExist, InvalidDataException
   {
      CompanyProfileValidator validator = new CompanyProfileValidator();
      validator.validateCompanyProfile(company, false);
      if (isCompanyAlreadyExist(company, false))
      {
         throw new CompanyAlreadyExist("company [" + company.getCompanyName() + "] already exist in Data source.");
      }
   }

   /**
    * This method checks company already exists in DB or not
    * @param company - contains company details
    * @param isUpdate
    * @return boolean - if company exists return true else false.
    */
   private boolean isCompanyAlreadyExist( Company company, boolean isUpdate )
   {
      boolean companyExists = false;
      long companyId = 0;
      companyId = companyDao.getCompanyById(company.getCompanyName());
      if (isUpdate && companyId > 0 && ! ( company.getCompanyId() == companyId ))
      {
         companyExists = true;
      }
      else if (! isUpdate && companyId > 0)
      {
         companyExists = true;
      }
      return companyExists;
   }

   /**
    *  Validate company details before updating into DB.
    * @param company - contains company details
    */
   @Override
   public void validateUpdateCompany( Company company ) throws CompanyAlreadyExist, InvalidDataException
   {
      // TODO Auto-generated method stub
      CompanyProfileValidator validator = new CompanyProfileValidator();
      validator.validateCompanyProfile(company, false);
   }

   /**
    * Validate company details before deleting from DB.
    * @param company - contains company details
    */
   @Override
   public void validateDeleteCompanyInfo( long companyId ) throws InvalidDataException, CompanyHasUsersException
   {
      // chk if there are users associated to the company and if so throw
      // exception
      List<Long> userList = companyDao.getUsersForCompany(companyId);
      if (userList != null && userList.size() > 0)
      {
         throw new CompanyHasUsersException("Cannot delete a company with users.");
      }
   }

   public ICompanyDao getCompanyDao()
   {
      return companyDao;
   }

   public void setCompanyDao( ICompanyDao companyDao )
   {
      this.companyDao = companyDao;
   }
}
