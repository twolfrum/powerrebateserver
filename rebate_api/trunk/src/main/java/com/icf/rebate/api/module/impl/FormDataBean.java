package com.icf.rebate.api.module.impl;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "name", "options" })
public class FormDataBean implements Serializable
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String name;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<Part> parts;

   public List<Part> getParts()
   {
      return parts;
   }

   public void setParts( List<Part> parts )
   {
      this.parts = parts;
   }

   public String getName()
   {
      return name;
   }

   public void setName( String name )
   {
      this.name = name;
   }
}
