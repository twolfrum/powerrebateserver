package com.icf.rebate.api.module.impl;

import java.io.IOException;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@XmlType(propOrder = { "replaceOnFail", "earlyRetirement" })
public class InsulationItem {

	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private Category insulationDetail;
	
	   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
	   {
	      ObjectMapper objectMapper = new ObjectMapper();
	      return objectMapper.writeValueAsString(this);
	   }

	public Category getInsulationDetail() {
		return insulationDetail;
	}

	public void setInsulationDetail(Category insulationDetail) {
		this.insulationDetail = insulationDetail;
	}
	   
}
