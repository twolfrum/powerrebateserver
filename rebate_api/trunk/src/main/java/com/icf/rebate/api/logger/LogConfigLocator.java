package com.icf.rebate.api.logger;
/**
 * This class is used to locate the log4j xml file path
 * 
 */
public class LogConfigLocator
{
   private String log4jXMLPath = null;

   public LogConfigLocator ( String log4jXMLPath )
   {
      this.log4jXMLPath = log4jXMLPath;
   }

   /**
    * @return the log4jXMLPath
    */
   public String getLog4jXMLPath()
   {
      return log4jXMLPath;
   }

   /**
    * @param log4jXMLPath the log4jXMLPath to set
    */
   public void setLog4jXMLPath( String log4jXMLPath )
   {
      this.log4jXMLPath = log4jXMLPath;
   }
}
