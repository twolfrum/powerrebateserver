package com.icf.rebate.api.user.exception;

import com.icf.rebate.api.util.UtilManager;
/**
 * THis class EmailIdAlreadyExist represents use case email id already exists 
 *
 */
public class EmailIdAlreadyExist extends GenericUserException
{
   /**
    * 
    */
   private static final long serialVersionUID = - 1282958732041617557L;
   private String errorCode = "ESUB1004";
   private String errorMessage = UtilManager.getErrorMessage(errorCode);

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code>.
    */
   public EmailIdAlreadyExist ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code> with the
    * specified detail message.
    */
   public EmailIdAlreadyExist ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String mportalIdentifier = "05131966";

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public EmailIdAlreadyExist ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>EmailIdAlreadyExist</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public EmailIdAlreadyExist ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }
}
