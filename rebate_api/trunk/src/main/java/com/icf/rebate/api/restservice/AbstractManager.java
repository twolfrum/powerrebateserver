package com.icf.rebate.api.restservice;

import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.util.BErrorResponse;
import com.icf.rebate.api.util.RebateApiConstant;
/**
 * This class provides common response for all API's
 *
 */
public class AbstractManager
{
   private final String THIS_CLASS = "AbstractManager";
   /**
    * logger object to log the messages.
    * 
    */
   private IMPLogger logger;
   private ConfigurationManager configManager;

   /**
    * Prepare response to be sent to device
    * @param responseBuilder - contains response details
    * @param userId - Id send response details for this user id
    * @return Response object - to be sent to device
    */
   public Response buildRESTResponse( BuildResponse responseBuilder, long userId )
   {
      final String THIS_METHOD = "buildRESTResponse";
      ResponseBuilder jerseyResponseBuilder = Response.status(Status.OK);
      Map<String, String> responseHeaderMap = responseBuilder.getResponseHeaderMap();
      User userDetails = responseBuilder.getUserDetails();
      if (userDetails != null && userDetails.getHttpHeaders() != null)
      {
         String playbackValue = userDetails.getHttpHeaders().getFirst(RebateApiConstant.HEADER_PLAYBACK_KEY);
         if (playbackValue != null)
         {
            logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Setting playback header with value : {}",
                  playbackValue);
            jerseyResponseBuilder.header(RebateApiConstant.HEADER_PLAYBACK_KEY, playbackValue);
         }
         // To resolve cross platform issue added Origin in response header if
         // request is coming from web client
         List<String> originList = userDetails.getHttpHeaders().get("origin");
         if (originList != null)
         {
            jerseyResponseBuilder.header("Access-Control-Allow-Origin",
                  userDetails.getHttpHeaders().get("origin").get(0));
         }
      }
      if (responseHeaderMap != null && ! responseHeaderMap.isEmpty())
      {
         for (String headerStr : responseHeaderMap.keySet())
         {
            jerseyResponseBuilder.header(headerStr, responseHeaderMap.get(headerStr));
         }
      }
      // if(LocalIdentity.groupIdentity.get() != null)
      // {
      // jerseyResponseBuilder.header(Constants.HEADER_GROUP,
      // LocalIdentity.groupIdentity.get().getName());
      // }
      // try
      // {
      // if(replayVersion)
      // {
      // getVersion(jerseyResponseBuilder, bUserDetails, deviceIdentity);
      // if (userId != 0 &&
      // configManager.getPropertyAsBoolean(Constants.REPLAY_RB_VERSION))
      // {
      // getRbVersion(jerseyResponseBuilder, bUserDetails, userId,
      // deviceIdentity);
      // }
      // }
      // }
      // catch (Exception excep)
      // {
      // if(Boolean.valueOf(configManager.getPropertyAsString(Constants.IGNORE_VERSION_ERRORS))
      // == false)
      // {
      // jerseyResponseBuilder.header(Constants.RESPONSE_HEADER_STATUS_CODE,
      // configManager.getPropertyAsString(Constants.CODE_FAILURE_INTERNAL));
      // jerseyResponseBuilder.header(Constants.RESPONSE_HEADER_STATUS_MESSAGE,
      // configManager.getPropertyAsString(Constants.MSG_FAILURE));
      // return jerseyResponseBuilder.build();
      // }
      // }
      jerseyResponseBuilder.header(RebateApiConstant.RESPONSE_HEADER_STATUS_CODE, responseBuilder.getResponseCode());
      jerseyResponseBuilder.header(RebateApiConstant.RESPONSE_HEADER_STATUS_MESSAGE, responseBuilder.getResponseMsg());
      // jerseyResponseBuilder.header(Constants.RESPONSE_HEADER_TITLE,
      // configManager.getPropertyAsStringWithDefault("TITLE_"+responseBuilder.getResponseCode(),Constants.DEFAULT_RESPONSE_HEADER_TITLE_VALUE));
      if (responseBuilder.getResponseObject() != null && responseBuilder.getResponseObject().length > 0)
      {
         jerseyResponseBuilder.header(RebateApiConstant.CONTENT_LENGTH, responseBuilder.getResponseObject().length);
      }
      jerseyResponseBuilder.type(( responseBuilder.getContentType() != null ) ? responseBuilder.getContentType()
            : MediaType.APPLICATION_JSON + " ; charset=UTF-8");
      if (( responseBuilder.getResponseObject() == null || responseBuilder.getResponseObject().length < 0 )
            && responseBuilder.getResponseCode().startsWith("E"))
      {
         BErrorResponse errorResponse = new BErrorResponse();
         errorResponse.setStatusCode(responseBuilder.getResponseCode());
         // errorResponse.setTitle(configManager.getPropertyAsStringWithDefault("TITLE_"+responseBuilder.getResponseCode(),Constants.DEFAULT_RESPONSE_HEADER_TITLE_VALUE));
         errorResponse.setStatusMsg(responseBuilder.getResponseMsg());
         // errorResponse.setDetails(configManager.getPropertyAsStringWithDefault("HTML_"+responseBuilder.getResponseCode(),
         // responseBuilder.getResponseMsg()));
         errorResponse.setDetails("set details");
         try
         {
            jerseyResponseBuilder.entity(errorResponse.formatJSONString().getBytes());
         }
         catch (Exception e)
         {
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
                  "Exception while adding error response to response data ", e);
         }
      }
      else
         jerseyResponseBuilder.entity(responseBuilder.getResponseObject());
      return jerseyResponseBuilder.build();
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   public ConfigurationManager getConfigManager()
   {
      return configManager;
   }

   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }
}
