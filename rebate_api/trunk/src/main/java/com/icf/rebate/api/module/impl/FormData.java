package com.icf.rebate.api.module.impl;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "appDetails", "customerDetails", "contractorDetails", "equipments", "insulations",
      "qualityInstallVerification", "tuneUp", "thermostatReferral" })
public class FormData implements Serializable
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   AppDetails appDetails;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   FormUserBean customerDetails;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   FormUserBean contractorDetails;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<Equipment> equipments;
   // KVB 12/9/2015
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<Insulation> insulation;
   //--------------------------------------------------------
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<WindowAndDoor> windowAndDoor;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<Submission> submission;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<AirDuctSealing> ductSealing;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<AirDuctSealing> airSealing;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<FormDataBean> qualityInstallVerification;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<TuneUpBean> tuneUp;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<FormDataBean> thermostatReferral;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<FormDataBean> thermostatInstall;

   public List<AirDuctSealing> getDuctSealing()
   {
      return ductSealing;
   }

   public void setDuctSealing( List<AirDuctSealing> ductSealing )
   {
      this.ductSealing = ductSealing;
   }

   public List<AirDuctSealing> getAirSealing()
   {
      return airSealing;
   }

   public void setAirSealing( List<AirDuctSealing> airSealing )
   {
      this.airSealing = airSealing;
   }

   public List<FormDataBean> getQualityInstallVerification()
   {
      return qualityInstallVerification;
   }

   public void setQualityInstallVerification( List<FormDataBean> qualityInstallVerification )
   {
      this.qualityInstallVerification = qualityInstallVerification;
   }

   public List<FormDataBean> getThermostatReferral()
   {
      return thermostatReferral;
   }

   public void setThermostatReferral( List<FormDataBean> thermostatReferral )
   {
      this.thermostatReferral = thermostatReferral;
   }

   public List<TuneUpBean> getTuneUp()
   {
      return tuneUp;
   }

   public void setTuneUp( List<TuneUpBean> tuneUp )
   {
      this.tuneUp = tuneUp;
   }

   public List<Equipment> getEquipments()
   {
      return equipments;
   }

   public void setEquipments( List<Equipment> equipments )
   {
      this.equipments = equipments;
   }

   public FormUserBean getCustomerDetails()
   {
      return customerDetails;
   }

   public void setCustomerDetails( FormUserBean customerDetails )
   {
      this.customerDetails = customerDetails;
   }

   public FormUserBean getContractorDetails()
   {
      return contractorDetails;
   }

   public void setContractorDetails( FormUserBean contractorDetails )
   {
      this.contractorDetails = contractorDetails;
   }

   public AppDetails getAppDetails()
   {
      return appDetails;
   }

   public void setAppDetails( AppDetails appDetails )
   {
      this.appDetails = appDetails;
   }

   public List<FormDataBean> getThermostatInstall()
   {
      return thermostatInstall;
   }

   public void setThermostatInstall( List<FormDataBean> thermostatInstall )
   {
      this.thermostatInstall = thermostatInstall;
   }

public List<Insulation> getInsulation() {
	return insulation;
}

public void setInsulation(List<Insulation> insulation) {
	this.insulation = insulation;
}


public List<WindowAndDoor> getWindowAndDoor() {
	return windowAndDoor;
}

public void setWindowAndDoor(List<WindowAndDoor> windowAndDoor) {
	this.windowAndDoor = windowAndDoor;
}

public List<Submission> getSubmission() {
	return submission;
}

public void setSubmission(List<Submission> submission) {
	this.submission = submission;
}

@Override
public String toString() {
	return "FormData [appDetails=" + appDetails + ", customerDetails=" + customerDetails + ", contractorDetails="
			+ contractorDetails + ", equipments=" + equipments + ", insulation=" + insulation + ", windowAndDoor="
			+ windowAndDoor + ", submission=" + submission + ", ductSealing=" + ductSealing + ", airSealing=" + airSealing
			+ ", qualityInstallVerification=" + qualityInstallVerification + ", tuneUp=" + tuneUp
			+ ", thermostatReferral=" + thermostatReferral + ", thermostatInstall=" + thermostatInstall + "]";
}  


}
