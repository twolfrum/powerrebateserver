package com.icf.rebate.api.login.exception;

import com.icf.rebate.api.exception.GenericAPIException;
import com.icf.rebate.api.util.UtilManager;
/**
 * This class UserNameNotFoundException represents an use case where a user
 * enters invalid user while login in to ICF.
 * 
 */
public class UserNameNotFoundException extends GenericAPIException
{
   /**
    * Represents error code for {@link UserNameNotFoundException}
    */
   private String errorCode = "ELOGIN1001";
   /**
    * Represents error msg for {@link UserNameNotFoundException}
    */
   private String errorMessage = UtilManager.getErrorMessage(errorCode);
   /**
    * Represents Serial Version UID.
    */
   private static final long serialVersionUID = 1L;

   /**
    * Constructs an instance of <code>UserNameNotFoundException</code>.
    */
   public UserNameNotFoundException ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>UserNameNotFoundException</code> with the
    * specified detail message.
    */
   public UserNameNotFoundException ( String msg )
   {
      super(msg);
   }

   /**
    * Constructs an instance of <code>UserNameNotFoundException</code> with the
    * specified detail message.
    * 
    * @param e - is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public UserNameNotFoundException ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>UserNameNotFoundException</code> with the
    * specified detail message.
    * 
    * @param e - is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public UserNameNotFoundException ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }

   /**
    * Sets the error message.
    * 
    * @param errorMessage - String Value
    */
   public void setErrorMessage( String errorMessage )
   {
      this.errorMessage = errorMessage;
   }
}
