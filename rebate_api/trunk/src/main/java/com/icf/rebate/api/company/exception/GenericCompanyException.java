package com.icf.rebate.api.company.exception;

import com.icf.rebate.api.exception.GenericAPIException;
public abstract class GenericCompanyException extends GenericAPIException
{
   /**
    * Constructs an instance of <code>InvalidConfiguration</code>.
    */
   public GenericCompanyException ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>GenericSubscriberException</code> with the
    * specified detail message.
    */
   public GenericCompanyException ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String icfIdentifier = "05131966";

   /**
    * Constructs an instance of <code>GenericSubscriberException</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public GenericCompanyException ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>GenericSubscriberException</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public GenericCompanyException ( String msg, Throwable e )
   {
      super(msg, e);
   }
}
