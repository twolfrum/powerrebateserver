package com.icf.rebate.api.exception;

import java.util.List;
/**
 * This class InvalidDataException represents use case for invalid data 
 *
 */
public class InvalidDataException extends GenericAPIException
{
   /**
    * This list will hold a set of error description of all the invalid data
    * fields.
    */
   private List<String> errorList;
   private String errorMessage = "Invalid data exception";

   /**
    * Contructs the exception object with the given list of error codes.
    * 
    * @param list
    */
   public InvalidDataException ( List<String> list )
   {
      setErrorList(list);
   }

   /**
    *
    */
   public InvalidDataException ()
   {
   }

   /**
    * @param e
    */
   public InvalidDataException ( Exception e )
   {
      super(e);
   }

   /**
    * @param errorCode
    * @param e
    */
   public InvalidDataException ( String errorCode, Throwable e )
   {
      super(errorCode, e);
   }

   /**
    * Creates the exception class with the given message.
    * 
    * @param errorMsg
    */
   public InvalidDataException ( String errorMsg )
   {
      super(errorMsg);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return null;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      if (errorList != null && errorList.size() > 0)
      {
         return errorList.get(0);
      }
      else
      {
         return errorMessage;
      }
   }

   /**
    * @return the errorList
    */
   public List<String> getErrorList()
   {
      return errorList;
   }

   /**
    * @param errorList the errorList to set
    */
   public void setErrorList( List<String> errorList )
   {
      this.errorList = errorList;
   }

   public String getMessage()
   {
      if (errorList != null && errorList.size() > 0)
      {
         return errorList.get(0);
      }
      else
      {
         return errorMessage;
      }
   }

   /**
    * @param errorMessage the errorMessage to set
    */
   public void setErrorMessage( String errorMessage )
   {
      this.errorMessage = errorMessage;
   }
}
