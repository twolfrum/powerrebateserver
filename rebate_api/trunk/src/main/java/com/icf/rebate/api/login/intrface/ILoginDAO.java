package com.icf.rebate.api.login.intrface;

import java.util.List;

import com.icf.rebate.api.login.impl.UserPasswordTrackingBean;
/**
 * This interface ILoginDAO will expose all the data base operaion for login
 * functionalities.
 * 
 */
public interface ILoginDAO
{
   public static final String RESET_FLAG_YES = "Y";
   public static final String RESET_FLAG_NO = "N";
   public static final String GET_LOGGEDIN_USER_DETAIL = " SELECT USER_ID, PASSWORD, PREVIOUS_LOGIN_TIME,CURRENT_LOGIN_TIME,PASSWORD_UPDATED_ON, RESET_FLAG FROM USER_LOGIN_AND_PWD_TRACK WHERE USER_ID = ? ";
   public static final String DELETE_UNSUCCESS_ATTEMPT = " DELETE FROM USER_UNSUCCESS_LOGIN_ATTEMPTS  WHERE USER_ID = ? ";
   public static final String INSERT_PWD_HISTORY_QUERY = " INSERT INTO USER_PASSWORD_HISTORY  (USER_ID,PASSWORD,PASSWORD_UPDATED_ON)  VALUES (?,?,?)";
   public static final String UPDATE_LOGGEDIN_USER_DETAIL = " UPDATE USER_LOGIN_AND_PWD_TRACK  SET PASSWORD = ?, PREVIOUS_LOGIN_TIME = ?, "
         + " CURRENT_LOGIN_TIME =?, PASSWORD_UPDATED_ON =?, RESET_FLAG = ?  WHERE USER_ID = ? ";
   public static final String INSERT_LOGGEDIN_USER_DETAIL = " INSERT INTO USER_LOGIN_AND_PWD_TRACK  (USER_ID, PASSWORD, PREVIOUS_LOGIN_TIME, CURRENT_LOGIN_TIME, PASSWORD_UPDATED_ON, "
         + " RESET_FLAG ) VALUES (?,?,?,?,?,?) ";
   public static final String UPDATE_PWD = " UPDATE USER_PROFILE SET PWD = ? WHERE USER_ID = ? AND USER_STATUS != ?";
   public static final String DELETE_USER_PWD_TRACK = " DELETE FROM USER_LOGIN_AND_PWD_TRACK  WHERE USER_ID = ? ";
   public static final String DELETE_USER_PWD_HISTORY = " DELETE FROM USER_PASSWORD_HISTORY  WHERE USER_ID = ? ";
   public static final String GET_PWD_HISTORY_QUERY = " SELECT PASSWORD, PASSWORD_UPDATED_ON "
         + " FROM USER_PASSWORD_HISTORY WHERE USER_ID = ? ORDER BY PASSWORD_UPDATED_ON DESC ";
   public static final String UPDATE_PWD_RESET_FLAG = " UPDATE USER_PROFILE SET RESET_FLAG=? WHERE USER_ID = ?";

   /**
    * Get the logged in user details
    * 
    * @param userId - long unique id of user
    * @return Bean details of UserPasswordTrackingBean
    */
   public UserPasswordTrackingBean getLoggedInUserDetail( long userId );

   /**
    * Updated logged in user details in to data source
    * 
    * @param userTrackingBean -Bean details of UserPasswordTrackingBean
    * @param blnUpdatePwd - true the update the new password to data source,
    *           else don't update the new password
    */
   public void updateLoggedInUserDetail( UserPasswordTrackingBean userTrackingBean, boolean blnUpdatePwd );

   /**
    * Delete unsuccessful attempt from data source
    * 
    * @param userId - long unique id of user.
    */
   public void deleteUnsuccesssAttempt( long userId );

   /**
    * Insert user password information into data source
    * 
    * @param userTrackingBean - Bean of UserPasswordTrackingBean
    */
   public void insertUserPasswordHistory( UserPasswordTrackingBean userTrackingBean );

   /**
    * Update password in to data source
    * 
    * @param userId - unique Id of user
    * @param password - String password, which need to update in Data source
    */
   public void updatePasswordInUserProfile( long userId, String password );

   /**
    * Insert logged in user details data in to data source
    * 
    * @param userTrackingBean - Bean details of UserPasswordTrackingBean
    */
   public void insertUserPwdTrackDetail( UserPasswordTrackingBean pwdTrackingBean );

   /**
    * Delete user password History for given user id
    * 
    * @param userId
    */
   public void deleteUserPwdHistory( final long userId );

   /**
    * delete password Track for given user id
    * 
    * @param userId
    */
   public void deleteUserPwdTrack( final long userId );

   /**
    * get passwordBean List by give used Id
    * 
    * @param userId
    * @return List<UserPasswordTrackingBean>
    */
   public List<UserPasswordTrackingBean> retrievePasswordsUsedBytheUser( final long userId );

   /**
    * Reset password flag based on Input flag for given user Id
    * 
    * @param userId - reset flag for this user Id
    * @param flag - if true set Y else set N
    */
   public void resetPasswordFlag( final long userId, final boolean flag );
}
