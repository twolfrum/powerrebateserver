package com.icf.rebate.api.app;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.util.BStatus;
import com.icf.rebate.api.util.RebateApiConstant;
/**
 * 
 * This class has the implementation for save log request.
 */
public class MetricsLogAdapter
{
   private final String THIS_CLASS = "MetricsLogAdapter";
   @Autowired
   private ConfigurationManager configManager;
   /**
    * logger object to log the messages.
    * 
    */
   private IMPLogger logger;

   /**Save content into local file system
    * 
    * @param username -name of the file
    * @param logContent - content to store in file 
    * @return success or failure
    * @throws Exception
    */
   public BStatus logMetrics( String username, String logContent ) throws Exception
   {
      final String THIS_METHOD = "logMetrics";
      BStatus bStatus = new BStatus();
      // create Directory
      File logDir = new File(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            "METRICS_LOG_FOLDER") + File.separatorChar + username);
      try
      {
         boolean isDirExist = logDir.exists();
         if (! isDirExist)
         {
            logDir.mkdir();
         }
         else
         {
            logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Directory exist for username : " + username);
         }
      }
      catch (SecurityException Se)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Error while creating directory in Java: " + Se.getMessage(), Se);
         throw new Exception(Se);
      }
      // Create file having name as current timestamp
      // Instantiate a Date object
      Date today = Calendar.getInstance().getTime();
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss");
      String filename = formatter.format(today) + ".txt";
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "File name to store logs : " + filename);
      File logFile = new File(logDir, filename);
      try
      {
         FileUtils.writeStringToFile(logFile, logContent);
      }
      catch (IOException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception while writing to logfile : " , e);
         throw new Exception(e);
      }
      bStatus.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
            "CODE_SUCCESS"));
      bStatus.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
            "MSG_METRICS_LOG_SUCCESS"));
      return bStatus;
   }

   public ConfigurationManager getConfigManager()
   {
      return configManager;
   }

   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }
}
