package com.icf.rebate.api.util;
public class ServerInfo
{
   String remoteHost;
   String remotePort;
   String protocol;
   String userId;
   String webContext;
   String transactionID;
   String reportUserName;

   /**
    * @return the webContext
    */
   public String getWebContext()
   {
      return webContext;
   }

   /**
    * @param webContext the webContext to set
    */
   public void setWebContext( String webContext )
   {
      this.webContext = webContext;
   }

   /**
    * @return the userId
    */
   public String getUserId()
   {
      return userId;
   }

   /**
    * @param userId the userId to set
    */
   public void setUserId( String userId )
   {
      this.userId = userId;
   }

   /**
    * @return the remoteHost
    */
   public String getRemoteHost()
   {
      return remoteHost;
   }

   /**
    * @param remoteHost the remoteHost to set
    */
   public void setRemoteHost( String remoteHost )
   {
      this.remoteHost = remoteHost;
   }

   /**
    * @return the remotePort
    */
   public String getRemotePort()
   {
      return remotePort;
   }

   /**
    * @param remotePort the remotePort to set
    */
   public void setRemotePort( String remotePort )
   {
      this.remotePort = remotePort;
   }

   /**
    * @return the protocol
    */
   public String getProtocol()
   {
      return protocol;
   }

   /**
    * @param protocol the protocol to set
    */
   public void setProtocol( String protocol )
   {
      this.protocol = protocol;
   }

   public String getTransactionID()
   {
      return transactionID;
   }

   public void setTransactionID( String transactionID )
   {
      this.transactionID = transactionID;
   }

   /**
    * @return the reportUserName
    */
   public String getReportUserName()
   {
      return reportUserName;
   }

   /**
    * @param reportUserName the reportUserName to set
    */
   public void setReportUserName( String reportUserName )
   {
      this.reportUserName = reportUserName;
   }
}
