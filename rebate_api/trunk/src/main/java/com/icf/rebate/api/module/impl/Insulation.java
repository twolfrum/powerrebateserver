package com.icf.rebate.api.module.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@XmlType(propOrder = { "name", "items" })
public class Insulation implements Serializable {
	 @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	   String name;
	   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	   List<InsulationItem> items;

	   public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}



	public List<InsulationItem> getItems() {
		return items;
	}



	public void setItems(List<InsulationItem> items) {
		this.items = items;
	}



	public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
	   {
	      ObjectMapper objectMapper = new ObjectMapper();
	      return objectMapper.writeValueAsString(this);
	   }
}
