package com.icf.rebate.api.reload;
/**
 * This Interface is developed for reloading properties and XML files for
 * servers.
 * 
 */
public interface IRefreshListener
{
   /**
    * This method is used to reload all the XML or property files which are in
    * server cache.
    */
   public void refreshNotify();
}
