package com.icf.rebate.api.module.impl;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "id", "name", "xmlName", "dataType", "barcode", "barcodeScanned", "reference", "values", "mandatory", "inputType",
      "storeSavedValue" })
public class Options
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String id;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String name;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String xmlName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String dataType;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String barcode;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   boolean barcodeScanned;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   Reference reference;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   List<String> values;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String mandatory;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String inputType;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String storeSavedValue;

   public String getId()
   {
      return id;
   }

   public void setId( String id )
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName( String name )
   {
      this.name = name;
   }

   public String getDataType()
   {
      return dataType;
   }

   public void setDataType( String dataType )
   {
      this.dataType = dataType;
   }

   public String getBarcode()
   {
      return barcode;
   }

   public void setBarcode( String barcode )
   {
      this.barcode = barcode;
   }

   public Reference getReference()
   {
      return reference;
   }

   public void setReference( Reference reference )
   {
      this.reference = reference;
   }

   public List<String> getValues()
   {
      return values;
   }

   public void setValues( List<String> values )
   {
      this.values = values;
   }

   public String getMandatory()
   {
      return mandatory;
   }

   public void setMandatory( String mandatory )
   {
      this.mandatory = mandatory;
   }

   public String getInputType()
   {
      return inputType;
   }

   public void setInputType( String inputType )
   {
      this.inputType = inputType;
   }

   public String getXmlName()
   {
      return xmlName;
   }

   public void setXmlName( String xmlName )
   {
      this.xmlName = xmlName;
   }

   public String getStoreSavedValue()
   {
      return storeSavedValue;
   }

   public void setStoreSavedValue( String storeSavedValue )
   {
      this.storeSavedValue = storeSavedValue;
   }

   public boolean isBarcodeScanned() {
	   return barcodeScanned;
   }

   public void setBarcodeScanned(boolean barcodeScanned) {
	   this.barcodeScanned = barcodeScanned;
   }

public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }
}
