package com.icf.rebate.api.user.impl;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.icf.rebate.api.util.BStatus;
@XmlRootElement
@XmlType(propOrder = { "userId", "userName", "password", "oldPassword", "newPassword", "deviceId", "verifyPassword",
      "firstName", "middleName", "lastName", "gender", "phone", "emailId", "address", "userStatus", "createdTime",
      "contactorAccountNumber", "lastUpdatedTime", "userRole", "role", "companyList", "resetFlag", "type", "density" })
public class User extends BStatus
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private long userId;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String userName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String password;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String oldPassword;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String newPassword;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String deviceId;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String verifyPassword;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String firstName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String middleName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String lastName;
   @JsonIgnore
   private String gender;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String phone;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String emailId;
   @JsonIgnore 
   private String taxId; // Never going to be sent to the app
   
   /*@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String address; */
   // KVB - adding multiple fields for address
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String address1;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String address2;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String city;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String state;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String zip;
   //----------------------------------------------------------
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String contractorCompany;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String contractorAccountNumber;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private long userStatus;
   @JsonIgnore
   private String createdTime;
   @JsonIgnore
   private String lastUpdatedTime;
   @JsonIgnore
   private long userRole;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String role;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private List<Long> companyList;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String resetFlag;
   @JsonIgnore
   private String comapnyUtility;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String fullName;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String type;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   private String density;
   @JsonIgnore
   MultivaluedMap<String, String> httpHeaders;

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }

   public long getUserId()
   {
      return userId;
   }

   public void setUserId( long userId )
   {
      this.userId = userId;
   }

   public String getUserName()
   {
      return userName;
   }

   public void setUserName( String userName )
   {
      this.userName = userName;
   }

   public String getPassword()
   {
      return password;
   }

   public void setPassword( String password )
   {
      this.password = password;
   }

   public String getVerifyPassword()
   {
      return verifyPassword;
   }

   public void setVerifyPassword( String verifyPassword )
   {
      this.verifyPassword = verifyPassword;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public void setFirstName( String firstName )
   {
      this.firstName = firstName;
   }

   public String getMiddleName()
   {
      return middleName;
   }

   public void setMiddleName( String middleName )
   {
      this.middleName = middleName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName( String lastName )
   {
      this.lastName = lastName;
   }

   public String getGender()
   {
      return gender;
   }

   public void setGender( String gender )
   {
      this.gender = gender;
   }

   public String getPhone()
   {
      return phone;
   }

   public void setPhone( String phone )
   {
      this.phone = phone;
   }

   public String getEmailId()
   {
      return emailId;
   }

   public void setEmailId( String emailId )
   {
      this.emailId = emailId;
   }

   /*public String getAddress()
   {
      return address;
   }

   public void setAddress( String address )
   {
      this.address = address;
   }*/
   

   public long getUserStatus()
   {
      return userStatus;
   }

   public String getAddress1() {
	  return address1;
   }

   public void setAddress1(String address1) {
	  this.address1 = address1;
   }

   public String getAddress2() {
	  return address2;
   }

   public void setAddress2(String address2) {
	  this.address2 = address2;
   }

   public String getCity() {
	  return city;
   }

   public void setCity(String city) {
	  this.city = city;
   }

   public String getState() {
	  return state;
   }

   public void setState(String state) {
	  this.state = state;
   }

   public String getZip() {
	  return zip;
   }

   public void setZip(String zip) {
	  this.zip = zip;
   }

   public void setUserStatus( long userStatus )
   {
      this.userStatus = userStatus;
   }

   public String getCreatedTime()
   {
      return createdTime;
   }

   public void setCreatedTime( String createdTime )
   {
      this.createdTime = createdTime;
   }

   public String getLastUpdatedTime()
   {
      return lastUpdatedTime;
   }

   public void setLastUpdatedTime( String lastUpdatedTime )
   {
      this.lastUpdatedTime = lastUpdatedTime;
   }

   public long getUserRole()
   {
      return userRole;
   }

   public void setUserRole( long userRole )
   {
      this.userRole = userRole;
   }

   public String getRole()
   {
      return role;
   }

   public void setRole( String role )
   {
      this.role = role;
   }

   public List<Long> getCompanyList()
   {
      return companyList;
   }

   public void setCompanyList( List<Long> companyList )
   {
      this.companyList = companyList;
   }

   public MultivaluedMap<String, String> getHttpHeaders()
   {
      return httpHeaders;
   }

   public void setHttpHeaders( MultivaluedMap<String, String> httpHeaders )
   {
      this.httpHeaders = httpHeaders;
   }

   public String getDeviceId()
   {
      return deviceId;
   }

   public void setDeviceId( String deviceId )
   {
      this.deviceId = deviceId;
   }

   public String getResetFlag()
   {
      return resetFlag;
   }

   public void setResetFlag( String resetFlag )
   {
      this.resetFlag = resetFlag;
   }

   public String getComapnyUtility()
   {
      return comapnyUtility;
   }

   public void setComapnyUtility( String comapnyUtility )
   {
      this.comapnyUtility = comapnyUtility;
   }

   public String getFullName()
   {
      return fullName;
   }

   public void setFullName( String fullName )
   {
      this.fullName = fullName;
   }

   public String getOldPassword()
   {
      return oldPassword;
   }

   public void setOldPassword( String oldPassword )
   {
      this.oldPassword = oldPassword;
   }

   public String getNewPassword()
   {
      return newPassword;
   }

   public void setNewPassword( String newPassword )
   {
      this.newPassword = newPassword;
   }

   public String getType()
   {
      return type;
   }

   public void setType( String type )
   {
      this.type = type;
   }

   public String getDensity()
   {
      return density;
   }

   public void setDensity( String density )
   {
      this.density = density;
   }

   public String getContractorCompany() {
	   return contractorCompany;
   }

   public void setContractorCompany(String contractorCompany) {
	   this.contractorCompany = contractorCompany;
   }

public String getContractorAccountNumber() {
	return contractorAccountNumber;
}

public void setContractorAccountNumber(String contractorAccountNumber) {
	this.contractorAccountNumber = contractorAccountNumber;
}

public String getTaxId() {
	return taxId;
}

public void setTaxId(String taxId) {
	this.taxId = taxId;
}

@Override
public String toString() {
	return "User [userId=" + userId + ", userName=" + userName + ", deviceId=" + deviceId + ", verifyPassword="
			+ verifyPassword + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName
			+ ", gender=" + gender + ", phone=" + phone + ", emailId=" + emailId + ", address1=" + address1
			+ ", address2=" + address2 + ", city=" + city + ", state=" + state + ", zip=" + zip + ", contractorCompany="
			+ contractorCompany + ", contractorAccountNumber=" + contractorAccountNumber + ", userStatus=" + userStatus
			+ ", createdTime=" + createdTime + ", lastUpdatedTime=" + lastUpdatedTime + ", userRole=" + userRole
			+ ", role=" + role + ", companyList=" + companyList + ", resetFlag=" + resetFlag + ", comapnyUtility="
			+ comapnyUtility + ", fullName=" + fullName + ", type=" + type + ", density=" + density + ", httpHeaders="
			+ httpHeaders + "]";
}
   


}
