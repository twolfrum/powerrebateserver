package com.icf.rebate.api.module.intrface;

public interface IModuleProcessor
{
   public void process( String filePath );
}
