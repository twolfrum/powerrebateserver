package com.icf.rebate.api.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.jar.JarOutputStream;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;

import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.company.impl.DashboardItems;
import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.user.impl.User;
/**
 * This class contains all the utility methods.
 */
public class UtilManager
{
   private final String THIS_CLASS = "UtilManager";
   private static final String REBATE_MESSAGES_PROPERTY = "rebatemessages";
   private static ConfigurationManager configManager;
   public static String CONTEXT_XML = "applicationContext.xml";
   private static AbstractApplicationContext _context;
   private static Object mutex = new Object();

   public static void setContext( AbstractApplicationContext context )
   {
      _context = context;
   }

   /**
    * This method is used to get the ErrorMessage for given ErrorCode from
    * Property file.
    * 
    * @param errorCode - String object contains errorCode
    * 
    * @return errorMessage String object
    */
   public static String getErrorMessage( String errorCode )
   {
      return configManager.getPropertyAsString(REBATE_MESSAGES_PROPERTY, errorCode);
      // return "error msg";
   }

   /**
    * Encrypts the given password.
    * 
    * @param password - Password to be encrypt.
    * @return encryptPassword - Encrypted password.
    * @throws InternalServerException - If any exception occurs while encrypting
    *            password.
    */
   public static String encrypt( String password ) throws InternalServerException
   {
      StringBuffer buffer = null;
      try
      {
         byte[] toChapterDigest = null;
         MessageDigest md = MessageDigest.getInstance("MD5");
         byte[] dataBytes = password.getBytes();
         md.update(dataBytes);
         MessageDigest tc = (MessageDigest) md.clone();
         toChapterDigest = tc.digest();
         buffer = new StringBuffer();
         for (int i = 0; i < toChapterDigest.length; i++)
         {
            String s = Integer.toHexString(0xFF & toChapterDigest[i]).toUpperCase();
            buffer.append(s.length() == 1 ? "0" + s : s);
         }
      }
      catch (NoSuchAlgorithmException e)
      {
         throw new InternalServerException("NoSuchAlgorithmException occured while encrypting password:", e);
      }
      catch (CloneNotSupportedException e)
      {
         throw new InternalServerException("CloneNotSupportedException occured while encrypting password:", e);
      }
      catch (Exception e)
      {
         throw new InternalServerException("Exception occured while encrypting password:", e);
      }
      return buffer.toString();
   }

   public static void clearJVMResourceBundleCache()
   {
      try
      {
         Class type = ResourceBundle.class;
         Field cacheList = type.getDeclaredField("cacheList");
         cacheList.setAccessible(true);
         ( (Map) cacheList.get(ResourceBundle.class) ).clear();
      }
      catch (Exception e)
      {
         e.printStackTrace();
         // System.out.println("Exception while reloading Resource Bundle..."+e);
      }
   }

   /**
    * 
    * @param milliSeconds
    * @param pattern
    * @return
    */
   public static String formatDateMilliSecondsToString( long milliSeconds, String pattern )
   {
      if (milliSeconds <= 0)
         return null;
      try
      {
         DateFormat obj = new SimpleDateFormat(pattern);
         String dateStr = "";
         dateStr = obj.format(new Date(milliSeconds));
         return dateStr;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
      return null;
   }

   public ConfigurationManager getConfigManager()
   {
      return configManager;
   }

   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }

   public static String generateRandomString( int length )
   {
      int randomIndex = 20;
      String randomCharacters = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
            RebateApiConstant.RANDOM_CHARACTERS);// "aijklmnIJKOopqryz[=|/DE&()bLMNcdUVWXYefgh_+';stuvwx:?.,<-]ABCF~!@#%GHP>{}QRSTZ"
                                                 // ;
      String alphabets = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD, RebateApiConstant.ALPHABETS);// "abcdefghijklmnoprstuvwxyz";
      String randomNumbers = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
            RebateApiConstant.RANDOM_NUMBERS); // "8609731245";
      Random random = new Random();
      char[] text = new char[length];
      for (int i = 0; i < length; i++)
      {
         if (( random.nextInt(randomIndex) > 5 && random.nextInt(randomIndex) < 10 )
               || ( random.nextInt(randomIndex) > 15 && random.nextInt(randomIndex) < 20 ))
            text[i] = randomNumbers.charAt(random.nextInt(randomNumbers.length()));
         else
            text[i] = randomCharacters.charAt(random.nextInt(randomCharacters.length()));
      }
      String randomString = new String(text);
      if (isNumeric(randomString))
      {
         char c = randomCharacters.charAt(random.nextInt(randomCharacters.length()));
         randomString = randomString.replace(randomString.charAt(length - 1), c);
      }
      else
      {
         if (! isStringContainsNumber(randomString))
         {
            char c = randomNumbers.charAt(random.nextInt(randomNumbers.length()));
            randomString = randomString.replace(randomString.charAt(length - 1), c);
         }
         if (isNotHavingAlphabet(randomString))
         {
            char c = alphabets.charAt(random.nextInt(alphabets.length()));
            randomString = randomString.replace(randomString.charAt(length - 2), c);
         }
      }
      // failsafe check to ensure password is 8 chars.
      if (randomString.length() < length)
      {
         randomString = "tA82#1jl";
      }
      return randomString;
   }

   /**
    * Check given String is Numeric or not
    * @param str - input String value
    * @return boolean - true value is numeric else false.
    */
   public static boolean isNumeric( String str )
   {
      try
      {
         Double.parseDouble(str);
      }
      catch (NumberFormatException nfe)
      {
         return false;
      }
      return true;
   }

   public static boolean isStringContainsNumber( String str )
   {
      if (str.matches(".*\\d.*"))
      {
         return true;
      }
      return false;
   }

   public static boolean isNotHavingAlphabet( String str )
   {
      String allowed_apla = configManager.getPropertyAsString(RebateApiConstant.WEB_PASSWORD,
            RebateApiConstant.PASSWORD_ALLOWED_ALPHABETS);
      if (Pattern.matches(allowed_apla, str))
      {
         return false;
      }
      return true;
   }

   public static String getStringFromList( List<Long> list )
   {
      StringBuffer items = new StringBuffer();
      if (list != null && list.size() > 0)
      {
         for (int i = 0; i < list.size(); i++)
         {
            if (i == 0)
            {
               items.append(list.get(i));
            }
            else
            {
               items.append("," + list.get(i));
            }
         }
      }
      return items.toString();
   }

   public static List<Long> getCompanyIdList( String commaSeparatedId )
   {
      List<Long> companyIdList = new ArrayList<Long>();
      if (commaSeparatedId != null && commaSeparatedId.contains(","))
      {
         String[] ids = commaSeparatedId.split(",");
         for (String id : ids)
         {
            companyIdList.add(Long.parseLong(id));
         }
      }
      else if (commaSeparatedId != null && ! commaSeparatedId.isEmpty())
      {
         companyIdList.add(Long.parseLong(commaSeparatedId));
      }
      return companyIdList;
   }

   public static List<String> getAllStates()
   {
      List<String> statesList = new ArrayList<String>();
      StringTokenizer tokenizer = new StringTokenizer(RebateApiConstant.STATES, ",");
      while (tokenizer.hasMoreTokens())
      {
         statesList.add(tokenizer.nextToken());
      }
      Collections.sort(statesList);
      return statesList;
   }

   /**
    * This method will used for converting String into date(java.util.Date)
    * 
    * @param sDate - String object contains Date
    * @param pattern - required DateFormat String object
    * 
    * @return Date - Return java.util.Date object
    */
   public static Date formatDateFromString( String sDate, String pattern )
   {
      if (sDate == null)
         return null;
      try
      {
         SimpleDateFormat obj = new SimpleDateFormat(pattern);
         java.util.Date date1 = null;
         date1 = obj.parse(sDate.trim());
         return date1;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
      return null;
   }

   /**
    * Returns the requested bean object from the Spring container.The
    * application context xml is assumed to be present in the class path.
    * 
    * @param String - Name of the object that has to be retrived. The name
    *           should match exactly to the id mentioned in the application
    *           context xml.
    * 
    * @return Object - bean object, configured in application context XML file
    *         and has to be type casted to appropriate object instance by the
    *         caller.
    */
   public static Object getBeanInstance( String beanName )
   {
      Object beanInstance = null;
      if (_context == null)
      {
         synchronized (mutex)
         {
            if (_context == null)
            {
               // DefaultResourceLoader loader = new
               // DefaultResourceLoader();
               // Resource res = loader.getResource(CONTEXT_XML);
               // _factory = new XmlBeanFactory(new
               // FileSystemResource(temp_str));
               // _factory = new XmlBeanFactory(res);
               _context = new ClassPathXmlApplicationContext(new String[] { CONTEXT_XML });
               // _context = new FileSystemXmlApplicationContext(
               // "E:\\workspace\\HEAD\\cdmapi\\config\\properties\\classpath\\applicationContext.xml"
               // );
               // ResourceBundle bundle =
               // ResourceBundle.getBundle(PROPERTY_FILE_NAME);
               // String filePath =
               // bundle.getString(APPLICATION_CONTEXT_PATH);
               // _context = new FileSystemXmlApplicationContext(filePath);
            }
         }
      }
      beanInstance = _context.getBean(beanName);
      // beanInstance = _factory.getBean(beanName);
      return beanInstance;
   }

   /**
    * Get dashboard items for each company
    * @param utilityCompanies - for which get the dash bored details.
    */
   public static void getDashboardItems( List<Company> utilityCompanies )
   {
         for (Company company : utilityCompanies)
         {
            //company.setDashboardItems(parseDashboardItems(company.getCompanyName(), company.getCompanyId()));
            JsonParser jParser = null;
            BufferedReader br = null;
            StringBuffer buffer = new StringBuffer();
            List<DashboardItems> dashboardItems = new ArrayList<DashboardItems>();
            String filePath = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
                  RebateApiConstant.DASHBOARD_JSON_PATH);
            System.out.println("Looking for company: " +company.getCompanyName());
            if (! UtilManager.checkFileExisits(filePath, company.getCompanyName()))
            {
               filePath = filePath.replace("${companyname}", "default");
            }
            else
            {
               filePath = filePath.replace("${companyname}", company.getCompanyName());
            }
            /*** read from file ***/
            try
            {
               br = new BufferedReader(new FileReader(filePath));
               String line = null;
               while (( line = br.readLine() ) != null)
               {
                  buffer.append(line);
               }
               JSONArray response = new JSONArray(buffer.toString());
               JSONObject obj = response.getJSONObject(0);
               String icon = obj.getString("icon");
               company.setIcon(company.getCompanyId()+"_"+icon);
               JSONArray jsonItems = obj.getJSONArray("dashboardItems");
               for (int i = 0; i < jsonItems.length(); i++)
               {
                  JSONObject dashboardJson = jsonItems.getJSONObject(i);
                  DashboardItems item = new DashboardItems();
                  item.setId(dashboardJson.getString("id"));
                  item.setName(dashboardJson.getString("name"));
                  item.setIcon(company.getCompanyId() + "_" + dashboardJson.getString("icon"));
                  JSONArray optionsList;
                  try
                  {
                     optionsList = dashboardJson.getJSONArray("options");
                  }
                  catch (JSONException ex)
                  {
                     dashboardItems.add(item);
                     continue;
                  }
                  if (optionsList != null && optionsList.length() > 0)
                  {
                     for (int j = 0; j < optionsList.length(); j++)
                     {
                        JSONObject optionJson = optionsList.getJSONObject(j);
                        DashboardItems optionitem = new DashboardItems();
                        optionitem.setId(optionJson.getString("id"));
                        optionitem.setName(optionJson.getString("name"));
                        optionitem.setIcon(company.getCompanyId() + "_" + optionJson.getString("icon"));
                        item.setOptions(optionitem);
                     }
                  }
                  dashboardItems.add(item);
               }
               company.setDashboardItems(dashboardItems);
            }
            catch (Exception ex)
            {
               ex.printStackTrace();
            }
            finally
            {
               try
               {
                  br.close();
               }
               catch (Exception ex)
               {
                  ex.printStackTrace();
               }
            }
         }     
   }
   /**
    * Parse dashboared item against dashboared.json
    * @param companyName - company to which json need to parse.
    * @param companyId - Id to which json need to parse.
    * @return dashboaredItem list for specified company.
    */
   private static List<DashboardItems> parseDashboardItems( String companyName, long companyId )
   {
      JsonParser jParser = null;
      BufferedReader br = null;
      StringBuffer buffer = new StringBuffer();
      List<DashboardItems> dashboardItems = new ArrayList<DashboardItems>();
      String filePath = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.DASHBOARD_JSON_PATH);
      if (! UtilManager.checkFileExisits(filePath, companyName))
      {
         filePath = filePath.replace("${companyname}", "default");
      }
      else
      {
         filePath = filePath.replace("${companyname}", companyName);
      }
      /*** read from file ***/
      try
      {
         br = new BufferedReader(new FileReader(filePath));
         String line = null;
         while (( line = br.readLine() ) != null)
         {
            buffer.append(line);
         }
         JSONArray response = new JSONArray(buffer.toString());
         JSONObject obj = response.getJSONObject(0);
         String name = obj.getString("name");
         JSONArray jsonItems = obj.getJSONArray("dashboardItems");
         for (int i = 0; i < jsonItems.length(); i++)
         {
            JSONObject dashboardJson = jsonItems.getJSONObject(i);
            DashboardItems item = new DashboardItems();
            item.setId(dashboardJson.getString("id"));
            item.setName(dashboardJson.getString("name"));
            item.setIcon(companyId + "_" + dashboardJson.getString("icon"));
            JSONArray optionsList;
            try
            {
               optionsList = dashboardJson.getJSONArray("options");
            }
            catch (JSONException ex)
            {
               dashboardItems.add(item);
               continue;
            }
            if (optionsList != null && optionsList.length() > 0)
            {
               for (int j = 0; j < optionsList.length(); j++)
               {
                  JSONObject optionJson = optionsList.getJSONObject(j);
                  DashboardItems optionitem = new DashboardItems();
                  optionitem.setId(optionJson.getString("id"));
                  optionitem.setName(optionJson.getString("name"));
                  optionitem.setIcon(companyId + "_" + optionJson.getString("icon"));
                  item.setOptions(optionitem);
               }
            }
            dashboardItems.add(item);
         }
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
         System.out.println("FileNotFoundException for compnay " + companyName + " with path " + filePath);
      }
      finally
      {
         try
         {
            br.close();
         }
         catch (Exception ex)
         {
            ex.printStackTrace();
         }
      }
      return dashboardItems;
   }

   /**
    * Check give file exists in specified path or not
    * @return boolean -  if file exists return true else false.
    */
   public static boolean checkFileExisits( String filePath, String companyName, String type, String density )
   {
      filePath = filePath.replace("${companyname}", companyName);
      filePath = filePath.replace("${type}", type);
      filePath = filePath.replace("${density}", density);
      File f = new File(filePath);
      if (f.exists())
         return true;
      else
         return false;
   }

   /**
    * Check is specified directory contains files or not.
    */
   public static boolean isDirecoryContainFiles( String filePath, String companyName, String type, String density )
   {
      filePath = filePath.replace("${companyname}", companyName);
      filePath = filePath.replace("${type}", type);
      filePath = filePath.replace("${density}", density);
      File file = new File(filePath);
      if (file.list().length > 0)
         return true;
      else
         return false;
   }

   /**
    * Check give file exists in specified path or not
    * @return boolean -  if file exists return true else false.
    */
   public static boolean checkFileExisits( String filePath, String companyName )
   {
      filePath = filePath.replace("${companyname}", companyName);
      File f = new File(filePath);
      System.out.println(f.getAbsolutePath() + (f.exists() ? " exists." : "does not exist."));
      if (f.exists())
         return true;
      else
         return false;
   }

   /**
    * Check is specified directory contains files or not.
    */
   public static boolean isDirecoryContainFiles( String filePath, String companyName )
   {
      filePath = filePath.replace("${companyname}", companyName);
      File file = new File(filePath);
      if (file.list().length > 0)
         return true;
      else
         return false;
   }

   public static boolean isValidString( String strValue )
   {
      if (strValue != null && strValue.trim().length() > 0)
      {
         return true;
      }
      return false;
   }

   public static void prepareReportMap( ArrayList<String> apSiteReportPdfPaths, Map<String, byte[]> reportMap,
         long companyId )
   {
      for (String reportPath : apSiteReportPdfPaths)
      {
         File file = new File(reportPath);
         if (file.exists())
         {
            byte[] bytes = readBytes(reportPath);
            String reportName = new File(reportPath).getName();
            reportMap.put(companyId + "_" + reportName, bytes);
         }
      }
   }

   /**
    * 
    * @param path
    * @return
    */
   public static final byte[] readBytes( String path )
   {
      final String THIS_METHOD = "readBytes";
      byte[] imageBytes = null;
      try
      {
         imageBytes = FileUtils.readFileToByteArray(new File(path));
      }
      catch (IOException e)
      {
         // logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
         // "No Image found at this location : "+path);
      }
      return imageBytes;
   }

   /**
    * This method is used to create a jar at the specified location for the
    * provided map.
    * 
    * @param pathContentMap This mao will have key as path and value as byte
    *           array.
    * @throws Exception
    */
   public static byte[] prepareInMemoryJar( Map<String, byte[]> pathContentMap ) throws Exception
   {
      byte[] content = null;
      byte[] jarContent = null;
      ByteArrayOutputStream outputStream = null;
      JarOutputStream out = null;
      try
      {
         outputStream = new ByteArrayOutputStream();
         out = new JarOutputStream(outputStream);
         for (String path : pathContentMap.keySet())
         {
            content = (byte[]) pathContentMap.get(path);
            if (content != null)
            {
               if (! path.endsWith("/"))
               {
                  ZipEntry entry = new ZipEntry(path);
                  entry.setMethod(ZipEntry.DEFLATED);
                  // entry.setCompressedSize(content.length);
                  entry.setSize(content.length);
                  CRC32 crc = new CRC32();
                  crc.update(content);
                  entry.setCrc(crc.getValue());
                  out.putNextEntry(entry);
                  out.write(content, 0, content.length);
               }
               out.closeEntry();
            }
         }
      }
      catch (Exception excep)
      {
         excep.printStackTrace();
         String key = "";
         byte[] value = null;
         Iterator itr = pathContentMap.keySet().iterator();
         while (itr.hasNext())
         {
            key = (String) itr.next();
            value = pathContentMap.get(key);
            if (value == null || value.length == 0)
            {
               System.out.println("NULL FOUND: Key:==" + key + " Value:==" + value);
            }
            else
            {
               System.out.println("FOUND: Key:==" + key + " Value:==" + value.length);
            }
         }
         throw excep;
      }
      finally
      {
         try
         {
            if (out != null)
            {
               out.close();
            }
         }
         catch (Exception excep)
         {
            excep.printStackTrace();
         }
      }
      jarContent = outputStream.toByteArray();
      return jarContent;
   }

   public static ArrayList<String> getFilesToZip( String filepath, ArrayList<String> reportPaths )
   {
      File folder = new File(filepath);
      File[] listOfFiles = folder.listFiles();
      for (File file : listOfFiles)
      {
         if (file.isFile())
         {
            reportPaths.add(file.getAbsolutePath());
         }
      }
      return reportPaths;
   }

   public static void deleteFile( String filepath )
   {
      File f = new File(filepath);
      if (f.exists())
      {
         f.delete();
      }
   }

   public static String getModuleData( String filepath )
   {
      BufferedReader br = null;
      StringBuffer buffer = new StringBuffer();
      try
      {
         br = new BufferedReader(new FileReader(filepath));
         String line = null;
         while (( line = br.readLine() ) != null)
         {
            buffer.append(line);
         }
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
      finally
      {
         try
         {
            br.close();
         }
         catch (Exception ex)
         {
         }
      }
      return buffer.toString();
   }

   public static String getRemoteFileName()
   {
      String fileName = null;
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
      Date date = new Date();
      fileName = "_" + dateFormat.format(date);
      return fileName;
   }

   private static String getRandomNumber()
   {
      String randomNumber = UUID.randomUUID().toString();
      return randomNumber;
   }

   /**
    * 
    * @param userDetails
    * @throws InvalidDataException
    */
   public static void userNameValidation( User userDetails ) throws InvalidDataException
   {
      if (userDetails != null)
      {
         if (userDetails.getUserName() != null && userDetails.getUserName().contains(" "))
         {
            throw new InvalidDataException("Invalid Username or Password.");
         }
      }
   }

   /**
    * Check is user contains space or not.
    * @param username - validate against space.
    * @throws InvalidDataException - throws if data is invalid.
    */
   public static void userNameValidation( String username ) throws InvalidDataException
   {
      if (username != null && username.contains(" "))
      {
         throw new InvalidDataException("Invalid Username or Password.");
      }
   }
   /**
    * Creates directory for give specified path
    */
   public static void companyDirecory( String companyName )
   {
      String companyDirecoryName = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.UTILITY_COMPANY_CONFIG_PATH) + companyName;
      new File(companyDirecoryName).mkdir();
      String dashboaredDirecoryName = companyDirecoryName + "/" + "dashboard";
      new File(dashboaredDirecoryName).mkdir();
      String resourceDirecoryName = companyDirecoryName + "/" + "resourcebundle";
      new File(resourceDirecoryName).mkdir();
      new File(resourceDirecoryName +"/"+"json").mkdir();
      new File(resourceDirecoryName + "/" + "phone").mkdir();
      new File(resourceDirecoryName + "/" + "tablet").mkdir();
      String densities = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.UTILITY_COMPANY_DENSITY_NAME);
      String[] densityList = densities.split(",");
      for (String densityName : densityList)
      {
         new File(resourceDirecoryName + "/" + "phone" + "/" + densityName.trim()).mkdir();
         new File(resourceDirecoryName + "/" + "tablet" + "/" + densityName.trim()).mkdir();
      }
   }

   public static void createParentDirectory( String path )
   {
      File file = new File(path);
      if (! file.exists())
      {
         file.mkdirs();
      }
   }
   /**Create file for given path
    * 
    */
   public static void createFile( String path ) throws IOException
   {
      File file = new File(path);
      file.createNewFile();
   }

   /**Create directory for given path
    * 
    */
   public static void createDirectory( String companyName )
   {
      File file = new File(companyName);
      if (! file.exists())
      {
         file.mkdir();
      }
   }
}
