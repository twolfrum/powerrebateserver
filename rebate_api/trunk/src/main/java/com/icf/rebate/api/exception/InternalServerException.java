package com.icf.rebate.api.exception;
/**
 * This class InternalServerException represents use case if any internal server exceptions occurs 
 *
 */
public class InternalServerException extends GenericAPIException
{
   String errorMessage = "An internal server error occurred. Please check the logs for details.";

   /**
    * Instantiates a new InternalServerException exception.
    * 
    * @param msg the msg
    */
   public InternalServerException ( String msg )
   {
      super(msg);
   }

   /**
    * 
    */
   public InternalServerException ()
   {
      super();
   }

   /**
    * @param e
    */
   public InternalServerException ( Exception e )
   {
      super(e);
   }

   /**
    * Instantiates a new InternalServerException exception.
    * 
    * @param msg the msg
    * @param e the e
    */
   public InternalServerException ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      // TODO Auto-generated method stub
      return null;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      // TODO Auto-generated method stub
      return errorMessage;
   }
}
