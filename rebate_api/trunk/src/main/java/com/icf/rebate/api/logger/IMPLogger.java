package com.icf.rebate.api.logger;

import java.util.Map;
/**
 * IMPLogger interface has to be inherited by the Logger implementation class.
 * 
 */
public interface IMPLogger
{
   public static int TRACE = 1;
   public static int DEBUG = 2;
   public static int INFO = 3;
   public static int WARN = 4;
   public static int ERROR = 5;
   public static int FATAL = 6;

   /**
    * Logs the given level and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param msg - Message to be logged.
    */
   public void log( int level, String msg );

   /**
    * Logs the given level and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param msg - Message to be logged.
    * @param throwable - Throwable to be logged.
    */
   public void log( int level, String msg, Throwable throwable );

   /**
    * Logs the given level, class, method, message and throwable.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    * @param throwable - Throwable to be logged.
    */
   public void log( int level, String curClass, String method, String msg, Throwable throwable );

   /**
    * Logs the given level, class, method and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    */
   public void log( int level, String curClass, String method, String msg );

   /**
    * Logs the given level, user, class, method and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param user - User info to be logged.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    */
   public void log( int level, String user, String curClass, String method, String msg );

   /**
    * Logs the given level, user, class, method and message.
    * 
    * @param level - Severity level, should be a value from IMPLogger.
    * @param user - User info to be logged.
    * @param curClass - Current class name to be logged.
    * @param method - Method name to be logged.
    * @param msg - Message to be logged.
    * @param throwable - Throwable to be logged.
    */
   public void log( int level, String user, String curClass, String method, String msg, Throwable excep );

   /**
    * Adds the key/value pair for the next log message. Note that the key has to
    * be pre-configured.
    * 
    * 
    * @param key - Key name to which the value has to be appended.
    * @param value - Value for the key which has to go into the log message.
    */
   public void addFieldValueToLogMessage( String key, Object value );

   /**
    * Adds a list of key/value pair for the next log message. Note that the keys
    * has to be pre-configured.
    * 
    * @param mdcMap - Map of key/value pairs which will be used for the next log
    *           message.
    */
   public void addFieldValueListToLogMessage( Map<String, Object> mdcMap );

   /**
    * Removes the given Key's value which will be reflected in the next log
    * message call.
    * 
    * @param key - key whose value has to be removed.
    */
   public void removeFieldValueForLogMessage( String key );

   /**
    * Removes all the Keys values which will be reflected in the next log
    * message call.
    * 
    */
   public void resetMDC();

   /**
    * Reloads the logger instance
    */
   public void reloadLogger();
}
