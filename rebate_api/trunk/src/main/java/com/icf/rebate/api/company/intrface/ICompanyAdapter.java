package com.icf.rebate.api.company.intrface;

import java.security.DigestException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.exception.InternalServerException;
/**
 * This interface contains all company related declarations for CRUD operations.
 *
 */
public interface ICompanyAdapter
{
   public Map<Integer, Company> getAllCompanies();

   public List<Company> getCompanyList();

   public List<Company> getCompanyListForUser( long userId );

   public void addCompany( Company company ) throws DigestException, NoSuchAlgorithmException,
         CloneNotSupportedException, InternalServerException;

   public Company getCompanyById( long companyId );

   public Company getCompanyByName( String companyName );

   public void updateCompany( Company company );

   public void deleteCompany( long companyId );

   public Map<String, Long> getAllCompanyNames();
   
   public Map<String, Long> getAllCompanyNamesForManager(long managerId);

   public Map<Long, String> getAllCompany();
}
