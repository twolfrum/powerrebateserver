package com.icf.rebate.api.login.impl;

import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.login.intrface.ILoginValidator;
import com.icf.rebate.api.user.impl.User;
/**
 * This class DefaultLoginValidator will validate all the input data for login
 * functionalities. This class implements {@link ILoginValidator}
 * 
 */
public class DefaultLoginValidator implements ILoginValidator
{
   /**
    * Validates the given userName and password.
    * 
    * @param userName - User name to be validated.
    * @param password - Password to be validated.
    * @throws InvalidDataException - If user name or password fields contain
    *            invalid data.
    */
   public void validate( String userName, String password ) throws InvalidDataException
   {
      LoginValidator validator = new LoginValidator();
      validator.validateLoginFields(userName, password);
   }

   /**
    * Validates the given userName .
    * 
    * @param userName - User name to be validated.
    * @param password - Password to be validated.
    * @throws InvalidDataException - If user name or password fields contain
    *            invalid data.
    */
   public void validateUserName( String userName ) throws InvalidDataException
   {
      LoginValidator validator = new LoginValidator();
      validator.validateUser(userName);
   }

   /**
    * Validates UserPasswordTrackingBean
    * @param userBean - contains user details
    */
   public void validatePasswordTrackingBean( UserPasswordTrackingBean userBean ) throws InvalidDataException
   {
      LoginValidator validator = new LoginValidator();
      validator.validatePasswordTrackingBean(userBean);
   }

   /**
    * Validates password
    * @param userBean - contains user details
    */
   public void validatePasswordCorrect( UserPasswordTrackingBean userBean ) throws InvalidDataException
   {
      LoginValidator validator = new LoginValidator();
      validator.validatePasswordCorrect(userBean);
   }

   /**
    * Validate user details
    * @param userDetails - contains user information
    */
   public void validate( User userDetails ) throws InvalidDataException
   {
      LoginValidator validator = new LoginValidator();
      validator.validate(userDetails);
   }

   /**
    * Validate user details for change password
    * @param userDetails - contains user information
    */
   public void validateChangePwd( User userDetails ) throws InvalidDataException
   {
      LoginValidator validator = new LoginValidator();
      validator.validateChangePwd(userDetails);
   }
}
