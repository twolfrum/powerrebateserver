package com.icf.rebate.api.module.impl;

import java.io.IOException;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
@XmlRootElement
@XmlType(propOrder = { "replaceOnFail", "earlyRetirement" })
public class Item implements Serializable
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   Category replaceOnFail;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   Category earlyRetirement;

   public Category getReplaceOnFail()
   {
      return replaceOnFail;
   }

   public void setReplaceOnFail( Category replaceOnFail )
   {
      this.replaceOnFail = replaceOnFail;
   }

   public Category getEarlyRetirement()
   {
      return earlyRetirement;
   }

   public void setEarlyRetirement( Category earlyRetirement )
   {
      this.earlyRetirement = earlyRetirement;
   }

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }
}
