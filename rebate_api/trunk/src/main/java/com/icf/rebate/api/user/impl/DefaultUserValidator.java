package com.icf.rebate.api.user.impl;

import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.user.exception.EmailIdAlreadyExist;
import com.icf.rebate.api.user.exception.InvalidPasswordException;
import com.icf.rebate.api.user.exception.UserAlreadyExist;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.intrface.IUserDao;
import com.icf.rebate.api.user.intrface.IUserValidator;
/**
 * This class provides the basic validation rules that are applicable for
 * creating the user.
 * 
 */
public class DefaultUserValidator implements IUserValidator
{
   private IUserDao userDao = null;

   public void setUserDao( IUserDao userDao )
   {
      this.userDao = userDao;
   }

   /*
    * (non-Javadoc)
    * 
    * @see
    * com.mportal.api.subscriber.validate.ISubscriberValidator#validateAddSubscriber
    * ()
    */
   public void validateAddSubscriber( User user ) throws UserAlreadyExist, InvalidDataException,
         InvalidPasswordException, EmailIdAlreadyExist
   {
      UserProfileValidator validator = new UserProfileValidator();
      validator.validateUserProfile(user, false);
      if (isUserAlreadyExist(user, false))
      {
         throw new UserAlreadyExist("Subscriber [" + user.getUserName() + "] already exist in Data source.");
      }
      // if (isSubscriberAlreadyExistWithEmail(profile, false))
      // {
      // throw new EmailIdAlreadyExist("Subscriber [" + profile.getEmailId() +
      // "] already exist in Data source.");
      // }
      String passWord = user.getPassword();
      String verifyPassword = user.getVerifyPassword();
      if (! passWord.equals(verifyPassword))
      {
         throw new InvalidPasswordException("Password and Confirm Password must be same");
      }
   }

   private boolean isUserAlreadyExist( User user, boolean isUpdate )
   {
      boolean subscriberExists = false;
      long userId = 0;
      userId = userDao.getUserById(user.getUserName());
      if (isUpdate && userId > 0 && ( user.getUserId() == userId ))
      {
         subscriberExists = true;
      }
      else if (! isUpdate && userId > 0)
      {
         subscriberExists = true;
      }
      return subscriberExists;
   }

   // private boolean isSubscriberAlreadyExistWithEmail(User profile, boolean
   // isUpdate)
   // {
   // boolean subscriberExists = false;
   // long subscriberId = 0;
   //
   // try
   // {
   // User user = userDao.getSubscriber(profile.getEmailId());
   //
   // subscriberId = user.getUserId();
   //
   // if (isUpdate && subscriberId > 0 && !(profile.getUserId() ==
   // subscriberId))
   // {
   // subscriberExists = true;
   // }
   // else if (!isUpdate && subscriberId > 0)
   // {
   // subscriberExists = true;
   // }
   // return subscriberExists;
   // }
   // catch (UserDoesNotExist e)
   // {
   // return userExists;
   // }
   //
   // }
   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.sport.api.subscriber.validate.ISubscriberValidator#
    * validateUpdateSubscriber(com.mportal.sport.api.subscriber.bean.Subscriber)
    */
   @Override
   public void validateUpdateUser( User user ) throws UserDoesNotExist, InvalidDataException, EmailIdAlreadyExist,
         InvalidPasswordException
   {
      UserProfileValidator validator = new UserProfileValidator();
      validator.validateUserProfile(user, false);
      if (! isUserAlreadyExist(user, true))
      {
         throw new UserDoesNotExist("Subscriber Profile [" + user.getUserName() + "] dosen't exist in Data source.");
      }
      //
      // if (isSubscriberAlreadyExistWithEmail(profile, true))
      // {
      // throw new EmailIdAlreadyExist("Subscriber Profile [" +
      // profile.getEmailId()
      // + "] already exist in Data source.");
      // }
      String password = user.getPassword();
      String verifyPassword = user.getVerifyPassword();
      if (password != null && password.length() > 0)
      {
         if (! password.equals(verifyPassword))
         {
            throw new InvalidPasswordException("New Password and Confirm Password must be same");
         }
      }
   }

   @Override
   public void validateDeleteUserInfo( long userId ) throws InvalidDataException
   {
      UserProfileValidator validator = new UserProfileValidator();
      validator.validateUserId(userId);
   }
}
