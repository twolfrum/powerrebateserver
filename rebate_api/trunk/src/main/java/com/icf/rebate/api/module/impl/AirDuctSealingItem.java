package com.icf.rebate.api.module.impl;

import java.io.IOException;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@XmlType(propOrder = { "replaceOnFail", "earlyRetirement" })
public class AirDuctSealingItem {
	public enum SealingType {AIR, DUCT}

	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private Category airSealingDetail;

	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private Category ductSealingDetail;

	public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(this);
	}

	public Category getDetail(SealingType typ) {
		Category detail = null;
		switch (typ) {
		case AIR: detail = airSealingDetail; break;
		case DUCT: detail = ductSealingDetail; break;
		}
		return detail;
	}

	public void setDetail(Category detail, SealingType typ) {
		switch (typ) {
		case AIR: airSealingDetail = detail; break;
		case DUCT: ductSealingDetail = detail; break;
		}
	}
}
