package com.icf.rebate.api.stub;

import java.util.HashMap;
import java.util.Map;

public class AttributeScrubber {
	static private AttributeScrubber mInstance;
	final private Map<String, Scrub[]> mScrubs;

	//implement as singleton 
	private AttributeScrubber() {
		mScrubs = new HashMap<String, Scrub[]>();
		
		//key to mScrubs is the xml name of the attribute whose value needs "scrubbing"
		mScrubs.put("System_Effective_Efficiency_Before", new Scrub[]{new Scrub("%", "")});
		mScrubs.put("System_Effective_Efficiency_After", new Scrub[]{new Scrub("%", "")});
	}
	
	static public AttributeScrubber getInstance() {
		if (mInstance == null) mInstance = new AttributeScrubber();
		return mInstance;
	}
	
	public String scrub (String xmlName, String attributeValue) {
		Scrub[] scrubDefs = mScrubs.get(xmlName);
		if (scrubDefs == null) return attributeValue;
		
		String scrubbedValue = attributeValue;
		for (Scrub scrubDef : scrubDefs) {
			scrubbedValue = scrubbedValue.replace(scrubDef.dirty, scrubDef.clean);
		}
		return scrubbedValue;
	}
	
	private class Scrub {
		String dirty;
		String clean;
		
		public Scrub(String dirty, String clean) {
			super();
			this.dirty = dirty;
			this.clean = clean;
		}
	}
}
