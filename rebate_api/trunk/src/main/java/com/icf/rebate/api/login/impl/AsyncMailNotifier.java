package com.icf.rebate.api.login.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.codemonkey.simplejavamail.Email;
import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;

import com.icf.rebate.api.logger.IMPLogger;
public class AsyncMailNotifier
{
   ExecutorService executorService = null;
   private IMPLogger logger;

   public AsyncMailNotifier ()
   {
      executorService = Executors.newSingleThreadExecutor();
   }

   public void sendMail( final Mailer mailer, final Email email )
   {
      executorService.submit(new Runnable()
      {
         @Override
         public void run()
         {
            try
            {
               // Send mail in a thread
               mailer.sendMail(email);
            }
            catch (Exception ex)
            {
               logger.log(IMPLogger.ERROR, "Error while sending emails", ex);
            }
         }
      });
   }

   public void sendMail( final String host, final int port, final String userName, final String password,
         final Email email )
   {
      executorService.submit(new Runnable()
      {
         @Override
         public void run()
         {
            try
            {
               // Send mail in a thread
               new Mailer(host, port, userName, password, TransportStrategy.SMTP_TLS).sendMail(email);
            }
            catch (Exception ex)
            {
               logger.log(IMPLogger.ERROR, "Error while sending emails", ex);
            }
         }
      });
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }
}
