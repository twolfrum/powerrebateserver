package com.icf.rebate.api.config;

import java.util.List;
/**
 * This interface describes the way the users access configuration from the
 * subsystem.
 */
public interface IConfiguration
{
   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method reports an
    * error in the form of run time exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsString( String propertyFileName, String propertyName );

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method assumes the
    * default and returns it to the caller.<br>
    * If the given prperty configured with a comma separated value, then the
    * first String before comma will be returned.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsStringWithDefault( String propertyFileName, String propertyName, String defaultValue );

   /**
    * Returns the property value as integer. If the value read from the property
    * file is not integer, this method tries to return the default. If the
    * default value is not a proper integer value, this method throws a run time
    * exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - int value of the property.
    */
   public int getPropertyAsInt( String propertyFileName, String propertyName, int defaultValue );

   /**
    * Returns the property value as long. If the value read from the property
    * file is not long, this method tries to return the default. If the default
    * value is not a proper long value, this method throws a run time exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - long value of the property.
    */
   public long getPropertyAsLong( String propertyFileName, String propertyName, long defaultValue );

   /**
    * Returns the property value as double. If the value read from the property
    * file is not double, this method tries to return the default. If the
    * default value is not a proper double value, this method throws a run time
    * exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - double value of the property.
    */
   public double getPropertyAsDouble( String propertyFileName, String propertyName, double defaultValue );

   /**
    * Returns a list of strings which are separated by comma for the given
    * property.
    * 
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - list of all string which are separated by comma.
    */
   public List<String> getPropertyAsList( String propertyFileName, String propertyName );

   /**
    * Reloads all the Configuration files.
    * 
    * @return true - If successfully reloaded.<br>
    *         false - If reload fails.
    */
   public boolean reLoadAllConfiguration();

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method assumes the
    * default and returns it to the caller.<br>
    * If the given prperty configured with a comma separated value, then the
    * first String before comma will be returned.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsStringWithDefault( String propertyName, String defaultValue );
}
