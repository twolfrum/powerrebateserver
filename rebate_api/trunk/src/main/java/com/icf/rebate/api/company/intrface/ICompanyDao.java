package com.icf.rebate.api.company.intrface;

import java.util.List;
import java.util.Map;

import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.user.impl.User;
public interface ICompanyDao
{
   String GET_COMPANY_DETAILS = "SELECT COMPANY_ID,COMPANY_NAME ,STATE ,ADDRESS, PROGRAM_NUMBER, SEND_TOC_TO_PRIMARY, SEND_TOC_TO_PREMISE, SEND_TOC_TO_CONTRACTOR, ABBRV_COMPANY_NAME FROM COMPANY_PROFILE WHERE COMPANY_NAME =?";
   String GET_COMPANY_DETAILS_BY_ID = "SELECT COMPANY_ID,COMPANY_NAME ,STATE, ADDRESS, PROGRAM_NUMBER, SEND_TOC_TO_PRIMARY, SEND_TOC_TO_PREMISE, SEND_TOC_TO_CONTRACTOR, ABBRV_COMPANY_NAME FROM COMPANY_PROFILE WHERE COMPANY_ID =?";
   String GET_COMPANY_DETAILS_BY_NAME = "SELECT COMPANY_ID,COMPANY_NAME ,STATE, ADDRESS, PROGRAM_NUMBER, SEND_TOC_TO_PRIMARY, SEND_TOC_TO_PREMISE, SEND_TOC_TO_CONTRACTOR, ABBRV_COMPANY_NAME  FROM COMPANY_PROFILE WHERE COMPANY_NAME =?";
   String GET_ALL_COMPANY_DETAILS = "SELECT COMPANY_ID,COMPANY_NAME ,STATE, ADDRESS, PROGRAM_NUMBER, SEND_TOC_TO_PRIMARY, SEND_TOC_TO_PREMISE, SEND_TOC_TO_CONTRACTOR, ABBRV_COMPANY_NAME FROM COMPANY_PROFILE ORDER BY COMPANY_NAME";
   String GET_ALL_COMPANY_DETAILS_FOR_MANAGER = "SELECT DISTINCT COMPANY_PROFILE.COMPANY_ID, COMPANY_NAME, STATE, ADDRESS, PROGRAM_NUMBER, SEND_TOC_TO_PRIMARY, SEND_TOC_TO_PREMISE, SEND_TOC_TO_CONTRACTOR FROM COMPANY_PROFILE JOIN USER_TO_COMPANY ON USER_TO_COMPANY.USER_ID = ? AND USER_TO_COMPANY.COMPANY_ID = COMPANY_PROFILE.COMPANY_ID ORDER BY COMPANY_NAME";
   String GET_ALL_COMPANY_DETAILS_FOR_USER = "SELECT CP.COMPANY_ID,COMPANY_NAME ,STATE, ADDRESS, PROGRAM_NUMBER, SEND_TOC_TO_PRIMARY, SEND_TOC_TO_PREMISE, SEND_TOC_TO_CONTRACTOR FROM COMPANY_PROFILE CP, USER_TO_COMPANY UTC  WHERE "
         + "CP.COMPANY_ID=UTC.COMPANY_ID AND UTC.USER_ID=? ORDER BY COMPANY_NAME";
   String ADD_COMPANY = "INSERT INTO COMPANY_PROFILE(COMPANY_ID,COMPANY_NAME ,STATE, ADDRESS, PROGRAM_NUMBER, SEND_TOC_TO_PRIMARY, SEND_TOC_TO_PREMISE, SEND_TOC_TO_CONTRACTOR) VALUES(?,?,?,?,?,?,?,?)";
   String GET_COMPANY_ID = "SELECT COMPANY_ID FROM COMPANY_PROFILE WHERE COMPANY_NAME =?";
   String GET_MAX_COMPANY_ID = "SELECT MAX(COMPANY_ID) AS COMPANY_ID FROM COMPANY_PROFILE";
   String UPDATE_COMPANY = "UPDATE COMPANY_PROFILE SET STATE=?, ADDRESS=?, PROGRAM_NUMBER=?, SEND_TOC_TO_PRIMARY=?, SEND_TOC_TO_PREMISE=?, SEND_TOC_TO_CONTRACTOR=? WHERE COMPANY_ID=?";
   String DELETE_COMPANY = "DELETE FROM COMPANY_PROFILE  WHERE COMPANY_ID=?";
   String GET_USERS_FOR_COMPANY = "SELECT USER_ID FROM USER_TO_COMPANY  WHERE COMPANY_ID=?";
   String GET_USERS_DETAILS_FOR_COMPANY = "SELECT UP.USER_ID , UP.USER_NAME , UP.FIRST_NAME, UP.LAST_NAME FROM USER_TO_COMPANY UTC , USER_PROFILE UP "
         + "WHERE UTC.COMPANY_ID=? AND UTC.USER_ID=UP.USER_ID";

   /**
    * Returns Login Details for the given user name.
    * 
    * @param userName - User name for which login details will be retrieved.
    * @return User - User details for the given user name.
    */
   Company getCompanyDetails( String companyName );

   /**
    * Get all companies
    * 
    * @return Company map with key as company id and value as corresponding
    *         company bean
    * 
    */
   public Map<Integer, Company> getAllCompanies();

   /**
    * Retrieve company List
    * 
    * @return company list
    */
   public List<Company> getCompanyList();

   /**
    * Retrieve company list for specific user
    * 
    * @param userId - get company list for this user id
    * @return company list
    */
   public List<Company> getCompanyList( long userId );

   /**
    * Add new company into DB
    * 
    * @param company - contains company details
    */
   public void addCompany( Company company );

   /**
    * get company details by company id
    * 
    * @param companyId - get company details for this id
    * @return Company - contains company details
    */
   public Company getCompanyById( long userId );

   public long getCompanyById( String userName );

   /**
    * get company details by company name
    * 
    * @param companyName - get company details for this name
    * @return Company - contains company details
    */
   public Company getCompanyByName( String companyName );

   /**
    * Update existing company with new details.
    * 
    * @param company
    */
   public void updateCompany( Company company );

   /**
    * Delete company for given company Id.
    * 
    * @param company
    */
   public void deleteCompany( long companyId );

   /**
    * Get all companies
    * 
    * @return Company map with key as company name and value as corresponding
    *         company id
    */
   public Map<String, Long> getAllCompanyNames();
   
   /**
    * Get all companies bound to manager
    * 
    * @return Company map with key as company name and value as corresponding
    *         company id
    */
   public Map<String, Long> getAllCompanyNamesForManager(long managerId);

   public List<Long> getUsersForCompany( long companyId );

   /**
    * Retrieve User details for given company id
    * 
    * @param companyId - get User details for this Id
    * @return User list
    */
   public List<User> getUserDetailsForComapny( long companyId );

   /**
    * Get all companies
    * 
    * @return company Map with key as company Id and value as company name
    */
   Map<Long, String> getAllCompany();
}
