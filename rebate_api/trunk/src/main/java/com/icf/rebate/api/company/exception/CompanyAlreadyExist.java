package com.icf.rebate.api.company.exception;

import com.icf.rebate.api.util.UtilManager;
/**
 * This class CompanyAlreadyExist represents use case company already exists with this name 
 *
 */
public class CompanyAlreadyExist extends GenericCompanyException
{
   /**
    * 
    */
   private static final long serialVersionUID = 85452779982390614L;
   private String errorCode = "ECOM1001";
   private String errorMessage = UtilManager.getErrorMessage(errorCode);

   /**
    * Constructs an instance of <code>InvalidConfiguration</code>.
    */
   public CompanyAlreadyExist ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    */
   public CompanyAlreadyExist ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String icfIdentifier = "05131966";

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public CompanyAlreadyExist ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>SubscriberAlreadyExists</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public CompanyAlreadyExist ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.icf.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.icf.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }
}
