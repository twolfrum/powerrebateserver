package com.icf.rebate.api.login.impl;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.codemonkey.simplejavamail.Email;
import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;

import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.util.RebateApiConstant;
public class EmailUser
{
   private int port;
   private String host;
   private String emailUserName;
   private String password;
   private String from;
   private String fromDisplayName;
   private ConfigurationManager configManager;
   private AsyncMailNotifier asyncMailNotifier;
   Mailer mailer;

   /**
    * @param configManager the configManager to set
    */
   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }

   /**
    * Initialize required parameters for sending an email.
    */
   public void initializeParameters()
   {
      if (mailer == null)
      {
         host = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY, RebateApiConstant.EMAIL_HOST);
         port = configManager.getPropertyAsInt(RebateApiConstant.REBATE_API_PROPERTY, RebateApiConstant.EMAIL_PORT, 0);
         emailUserName = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
               RebateApiConstant.EMAIL_USERNAME);
         password = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
               RebateApiConstant.EMAIL_PASSWORD);
         from = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY, RebateApiConstant.EMAIL_FROM);
         fromDisplayName = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
               RebateApiConstant.EMAIL_FROM_DISPLAY_NAME);
         mailer = new Mailer(host, port, emailUserName, password, TransportStrategy.SMTP_TLS);
      }
   }

   /**
    * Sends an email to administrator if username is not a valid email
    * 
    * @param username
    */
   
   public void sendUsernameNotValidEmail(String username) {
	   initializeParameters();
	   String subject = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY, 
			   RebateApiConstant.EMAIL_USERNAME_INVALID_SUBJECT);
	   String message = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY, 
			   RebateApiConstant.EMAIL_USERNAME_INVALID_MESSAGE);
	   String messageFooter = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
	            RebateApiConstant.EMAIL_FOOTER);
	   String toAddress = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
	            RebateApiConstant.EMAIL_ICF_ADMIN);
	   message += messageFooter;
	   message = message.replace("<1>", username); // Replace token is <1>
	   Email email = getEmailInstance(from, fromDisplayName, subject, toAddress, message);
	   asyncMailNotifier.sendMail(mailer, email);
   }
   
   /**
    * Sends an email after adding a user.
    * 
    * @param toEmailId
    * @param newPassword
    */
   public void sendEmailForAddUser( String toEmailId, String newPassword, String userName, String firstName,
         String lastName )
   {
      initializeParameters();
      String subject = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_ADD_USER_SUBJECT);
      String message = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_ADD_USER_MESSAGE);
      String messageFooter = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_FOOTER);
      message += messageFooter;
      message = getMessageforUser(message, userName, newPassword, firstName, lastName);
      Email email = getEmailInstance(from, fromDisplayName, subject, toEmailId, message);
      /*
       * new Mailer(host, port, emailUserName, password,
       * TransportStrategy.SMTP_TLS).sendMail(email);
       */
      asyncMailNotifier.sendMail(mailer, email);
   }
   
   public void sendTACs(String[] to, byte[] image, String imageType)
	   {
	      initializeParameters();
	      String subject = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
	            RebateApiConstant.EMAIL_SEND_TOC_SUBJ);
	      String message = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
	            RebateApiConstant.EMAIL_SEND_TOC_MSG);
	      String messageFooter = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
	            RebateApiConstant.EMAIL_FOOTER);
	      message += messageFooter;
	      Email email = getEmailInstance(from, fromDisplayName, subject, to, message, image, imageType);
	      /*
	       * new Mailer(host, port, emailUserName, password,
	       * TransportStrategy.SMTP_TLS).sendMail(email);
	       */
	      asyncMailNotifier.sendMail(mailer, email);
	   }

   /**
    * Sends an email after updating a user.
    * 
    * @param toEmailId
    * @param newPassword
    * @param userName
    */
   public void sendEmailForPasswordUpdateUser( String toEmailId, String newPassword, String userName, String firstName,
         String lastName )
   {
      initializeParameters();
      String subject = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_UPDATE_USER_PASSWORD_SUBJECT);
      String message = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_UPDATE_USER_PASSWORD_MESSAGE);
      String messageFooter = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_FOOTER);
      message += messageFooter;
      message = getMessageforUser(message, userName, newPassword, firstName, lastName);
      Email email = getEmailInstance(from, fromDisplayName, subject, toEmailId, message);
      /*
       * new Mailer(host, port, emailUserName, password,
       * TransportStrategy.SMTP_TLS).sendMail(email);
       */
      asyncMailNotifier.sendMail(mailer, email);
   }

   /**
    * Sends an email after updating a user.
    * 
    * @param toEmailId
    * @param userName
    */
   public void sendEmailForUpdateUser( String toEmailId, String newPassword, String userName, String firstName,
         String lastName )
   {
      initializeParameters();
      String subject = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_UPDATE_USER_SUBJECT);
      String message = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_UPDATE_USER_MESSAGE);
      String messageFooter = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_FOOTER);
      message += messageFooter;
      message = getMessageforUser(message, userName, newPassword, firstName, lastName);
      Email email = getEmailInstance(from, fromDisplayName, subject, toEmailId, message);
      /*
       * new Mailer(host, port, emailUserName, password,
       * TransportStrategy.SMTP_TLS).sendMail(email);
       */
      asyncMailNotifier.sendMail(mailer, email);
   }

   /**
    * Sends an email when user forgets the password.
    * 
    * @param toEmailId
    * @param newPassword
    * @param userName
    */
   public void sendEmailForForgotPwd( String toEmailId, String newPassword, String userName )
   {
      initializeParameters();
      String subject = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_FORGOT_PWD_SUBJECT);
      String message = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_FORGOT_PWD_MESSAGE);
      String messageFooter = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
            RebateApiConstant.EMAIL_FOOTER);
      message += messageFooter;
      message = getMessage(message, userName, newPassword);
      Email email = getEmailInstance(from, fromDisplayName, subject, toEmailId, message);
      
      //new Mailer(host, port, emailUserName, password,TransportStrategy.SMTP_TLS).sendMail(email);
       
      asyncMailNotifier.sendMail(mailer, email);
   }

   /**
    * Returns an email instance with given subject, message
    * 
    * @param from
    * @param fromDisplayName
    * @param subject
    * @param toEmailId
    * @param message
    * @return
    */
   private Email getEmailInstance( String from, String fromDisplayName, String subject, String toEmailId, String message )
   {
      Email email = new Email();
      email.setFromAddress(fromDisplayName, from);
      email.setSubject(subject);
      email.addRecipient(toEmailId, toEmailId, RecipientType.TO);
      email.setTextHTML(message);
      return email;
   }
   
   private Email getEmailInstance( String from, String fromDisplayName, String subject, String[] toEmailId, String message, byte[] img, String imageType )
   {
      Email email = new Email();
      email.setFromAddress(fromDisplayName, from);
      email.setSubject(subject);
      for (int i = 0; i < toEmailId.length; i++) {
    	  if (toEmailId[i] != null){
    		  email.addRecipient(toEmailId[i], toEmailId[i], RecipientType.TO);
    	  }
      }
      
      if (img != null){
    	  email.addAttachment("TermsAndConditions"+imageType, img, "image/gif");
      }
      email.setTextHTML(message);
      return email;
   }

   /**
    * Add the argument values in message and Generate the dynamic message,
    * 
    * @param message - String static message.
    * @param args - list of argument values, which need to add in message
    * @return Dynamic message with argument value
    */
   public String getMessage( String message, String userName, String newPassword )
   {
      List<String> argsList = new ArrayList<String>();
      argsList.add(userName);
      argsList.add(newPassword);
      if (message != null)
      {
         if (argsList.size() > 0)
         {
            for (int i = 0; i < argsList.size(); i++)
            {
               String strVariable = "<" + ( i + 1 ) + ">";
               message = message.replaceAll(strVariable, argsList.get(i));
            }
         }
      }
      return message;
   }

   /**
    * Add the argument values in message and Generate the dynamic message,
    * 
    * @param message - String static message.
    * @param args - list of argument values, which need to add in message
    * @return Dynamic message with argument value
    */
   public String getMessageforUser( String message, String userName, String newPassword, String firstName,
         String lastName )
   {
      List<String> argsList = new ArrayList<String>();
      argsList.add(firstName);
      argsList.add(lastName);
      argsList.add(userName);
      argsList.add(newPassword);
      if (message != null)
      {
         if (argsList.size() > 0)
         {
            for (int i = 0; i < argsList.size(); i++)
            {
               String strVariable = "<" + ( i + 1 ) + ">";
               message = message.replaceAll(strVariable, argsList.get(i));
            }
         }
      }
      return message;
   }

   /**
    * Add the argument values in message and Generate the dynamic message,
    * 
    * @param message - String static message.
    * @param args - list of argument values, which need to add in message
    * @return Dynamic message with argument value
    */
   public String getUpdateMessage( String message, String userName )
   {
      List<String> argsList = new ArrayList<String>();
      argsList.add(userName);
      if (message != null)
      {
         if (argsList.size() > 0)
         {
            for (int i = 0; i < argsList.size(); i++)
            {
               String strVariable = "<" + ( i + 1 ) + ">";
               message = message.replaceAll(strVariable, argsList.get(i));
            }
         }
      }
      return message;
   }

   public AsyncMailNotifier getAsyncMailNotifier()
   {
      return asyncMailNotifier;
   }

   public void setAsyncMailNotifier( AsyncMailNotifier asyncMailNotifier )
   {
      this.asyncMailNotifier = asyncMailNotifier;
   }
}
