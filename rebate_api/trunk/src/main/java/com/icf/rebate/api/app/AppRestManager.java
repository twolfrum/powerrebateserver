package com.icf.rebate.api.app;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.company.impl.CompanyManager;
import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.login.exception.InActiveUserException;
import com.icf.rebate.api.login.exception.UserAuthenticationException;
import com.icf.rebate.api.login.exception.UserBlockedException;
import com.icf.rebate.api.login.exception.UserNameNotFoundException;
import com.icf.rebate.api.login.intrface.ILoginAdapter;
import com.icf.rebate.api.login.intrface.ILoginValidator;
import com.icf.rebate.api.restservice.AbstractManager;
import com.icf.rebate.api.restservice.BuildResponse;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.user.impl.UserManager;
import com.icf.rebate.api.util.BStatus;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.api.util.UtilManager;

/**
 * 
 * This class has the implementation for various user requests like config request, log request and get resource bundle request.
 */

public class AppRestManager extends AbstractManager
{
   private final String THIS_CLASS = "AppRestManager";
   /**
    * logger object to log the messages.
    * 
    */
   private IMPLogger logger;
   private ConfigurationManager configManager;
   private CompanyManager companyManager;
   private MetricsLogAdapter metricsLogAdapter;
   private ILoginValidator loginValidator;
   private ILoginAdapter loginAdapter;
   private UserManager userManager;

   public UserManager getUserManager()
   {
      return userManager;
   }

   public void setUserManager( UserManager userManager )
   {
      this.userManager = userManager;
   }

   public CompanyManager getCompanyManager()
   {
      return companyManager;
   }

   public void setCompanyManager( CompanyManager companyManager )
   {
      this.companyManager = companyManager;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   public ConfigurationManager getConfigManager()
   {
      return configManager;
   }

   public void setConfigManager( ConfigurationManager configManager )
   {
      this.configManager = configManager;
   }
   
   /**
    * Gets the user config response.
    * 
    * @param request - Http request .
    * @param userDetails - User details like user name and password for which we need to fetch config details.
    * @return response - Config response for user based on the companies associated to him or error response in case of any error.
    * @throws Exception - thrown if there is an authentication issue or and other issue while fetching config details.
    * 
    */

   public Response getUserConfig( HttpServletRequest request, User userDetails ) throws Exception
   {
      final String THIS_METHOD = "getUserConfig";
      Response response = null;
      byte[] responseData = null;
      BuildResponse responseBuilder = new BuildResponse();
      BConfigBean configBean;
      User user = new User();
      try
      {
         UtilManager.userNameValidation(userDetails);
         this.loginValidator.validate(userDetails.getUserName(), userDetails.getPassword());
         user = this.loginAdapter.authenticateAndGetUser(userDetails.getUserName(), userDetails.getPassword());
         logger.log(
               IMPLogger.INFO,
               THIS_CLASS,
               THIS_METHOD,
               "Getting Config data  for userName " + userDetails.getUserName() + " with deviceId "
                     + userDetails.getDeviceId());
         configBean = new BConfigBean();
         configBean.setLanguage(request.getHeader("accept-language"));
        // configBean.setChangePassword(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,"CONFIG_CHANGE_PWD"));
         configBean.setSessionExpTime(configManager.getPropertyAsInt(RebateApiConstant.REBATE_API_PROPERTY,
               "CONFIG_SESSION_EXPIRY_TIME", 0));
         configBean.setSessionExpTimeLogout(configManager.getPropertyAsInt(RebateApiConstant.REBATE_API_PROPERTY,
               "CONFIG_SESSION_EXPIRY_TIME_LOGOUT", 0));
         configBean.setAppVersion(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
               "CONFIG_APP_VERSION"));
        // configBean.setUpgradeURL(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,"ICF_SERVER_URL")+ configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY, "CONFIG_UPGRADE_URL"));
       //  configBean.setServerURL(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,"ICF_SERVER_URL"));
         configBean.setAnalyticsKey(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
               "CONFIG_ANALYTICS_KEY"));
       //  configBean.setTermsAndCond(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY, "CONFIG_TERMS_AND_CONDITIONS"));
         configBean.setReportIssue(configManager.getPropertyAsBoolean(RebateApiConstant.REBATE_API_PROPERTY,"REPORT_ISSUE"));
         configBean.setEmailTo(configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,"EMAIL_TO"));
         configBean.setNoOfSubmittedJobs(configManager.getPropertyAsInt(RebateApiConstant.REBATE_API_PROPERTY,"NO_OF_SUBMITTED_JOBS",0));
         List<Company> utilityCompanies = companyManager.getCompanyListForUser(user.getUserId());
         // set the dashboard items for each company
         UtilManager.getDashboardItems(utilityCompanies);
         configBean.setUtilityCompanies(utilityCompanies);
         configBean.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_SUCCESS"));
         configBean.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_CONFIG_SUCCESS"));
         responseData = configBean.formatJSONString().getBytes(Charset.forName("UTF-8"));
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_SUCCESS"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_CONFIG_SUCCESS"));
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException  occured while validating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (UserNameNotFoundException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserNameNotFoundException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (InActiveUserException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InActiveUserException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INACTIVE_USER"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE_INACTIVE_USER"));
      }
      catch (UserAuthenticationException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserAuthenticationException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (UserBlockedException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserBlockedException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (InternalServerException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE"));
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception  occured while authenticating userName"
               , excep);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE"));
      }
      responseBuilder.setResponseObject(responseData);
      response = buildRESTResponse(responseBuilder, user.getUserId());
      return response;
   }
   
   /**
    * Stores the log response posted by user based on the username in local file system.
    * 
    * @param username - Http request .
    * @param logContent - User details like user name and password for which we need to fetch config details.
    * @return response - Success or error response based on whether logging was successful.
    * @throws Exception - thrown if there is an issue while storing the use log.
    * 
    */

   public Response logResponse( String username, String logContent ) throws Exception
   {
      final String THIS_METHOD = "logResponse";
      Response response = null;
      BStatus bStatus = null;
      BuildResponse responseBuilder = new BuildResponse();
      try
      {
         UtilManager.userNameValidation(username);
         this.userManager.getUserDetails(username);
         bStatus = metricsLogAdapter.logMetrics(username, logContent);
         responseBuilder.setResponseCode(bStatus.getStatusCode());
         responseBuilder.setResponseMsg(bStatus.getStatusMsg());
      }
      catch (UserDoesNotExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserDoesNotExist  occured while authenticating userName " , e);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INVALID_USER_PASSWORD"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_LOGIN_FAILURE"));
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while saving metrics log:" , excep);
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "CODE_FAILURE_INTERNAL"));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               "MSG_FAILURE"));
      }
      response = buildRESTResponse(responseBuilder, 0);
      return response;
   }
   
   /**
    * Stores the log response posted by user based on the username in local file system.
    * 
    * @param username - Http request .
    * @param userDetails - User details like user name and password for which we need to fetch resource bundle details.
    * @return response -  Success will give zip file with asset details of all companies associated to the user.Error will give json error response.
    * @throws Exception - thrown if there is an issue while fetching the resource bundle.
    * 
    */

   public Response getRBZip( @Context
   HttpServletRequest request, User userDetails ) throws Exception
   {
      final String THIS_METHOD = "getRBZip";
      ResponseBuilder rbResposne = null;
      byte[] responseData = null;
      byte[] reportBytes = null;
      User user = new User();
      BuildResponse responseBuilder = new BuildResponse();
      Map<String, byte[]> reportMap = new HashMap<String, byte[]>();
      ArrayList<String> reportPaths = null;
      try
      {
         UtilManager.userNameValidation(userDetails);
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
               "Getting Resource bundle begins for user " + userDetails.getUserName());
         this.loginValidator.validate(userDetails.getUserName(), userDetails.getPassword());
         user = this.loginAdapter.authenticateAndGetUser(userDetails.getUserName(), userDetails.getPassword());
         List<Company> utilityCompanies = companyManager.getCompanyListForUser(user.getUserId());
         for (Company company : utilityCompanies)
         {
            logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Getting Resource bundle begins for user for company "
                  + company.getCompanyName());
            String filePath = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
                  RebateApiConstant.RESOURCEBUNDLE_PATH);
            reportPaths = new ArrayList<String>();
            if (UtilManager.checkFileExisits(filePath, company.getCompanyName(), userDetails.getType(),
                  userDetails.getDensity())
                  && UtilManager.isDirecoryContainFiles(filePath, company.getCompanyName(), userDetails.getType(),
                        userDetails.getDensity()))
            {
               filePath = filePath.replace("${companyname}", company.getCompanyName());
               filePath = filePath.replace("${type}", userDetails.getType());
               filePath = filePath.replace("${density}", userDetails.getDensity());
            }
            else
            {
               filePath = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
                     RebateApiConstant.RESOURCEBUNDLE_DEFAULT_PATH) + "rb/";
               filePath = filePath.replace("${companyname}", "default");
            }
            // code to create zip file
            reportPaths = UtilManager.getFilesToZip(filePath, reportPaths);
            // get the company json and csv files also to be bundled.
            filePath = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
                  RebateApiConstant.RESOURCEBUNDLE_JSON_PATH);
            if (UtilManager.checkFileExisits(filePath, company.getCompanyName())
                  && UtilManager.isDirecoryContainFiles(filePath, company.getCompanyName()))
            {
               filePath = filePath.replace("${companyname}", company.getCompanyName());
            }
            else
            {
               filePath = configManager.getPropertyAsString(RebateApiConstant.REBATE_API_PROPERTY,
                     RebateApiConstant.RESOURCEBUNDLE_DEFAULT_JSON_PATH);
               filePath = filePath.replace("${companyname}", "default");
            }
            reportPaths = UtilManager.getFilesToZip(filePath, reportPaths);
            UtilManager.prepareReportMap(reportPaths, reportMap, company.getCompanyId());
            /*
             * UtilManager.deleteFile(filePath+"rb.zip"); File f = new
             * File(filePath+"rb.zip"); FileOutputStream fileOuputStream = new
             * FileOutputStream(f); fileOuputStream.write(reportBytes);
             * fileOuputStream.close();
             */
         }
         try
         {
            logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
                  "Getting Resource bundle begins for user for company reportMap ==" + reportMap);
            reportBytes = UtilManager.prepareInMemoryJar(reportMap);
         }
         catch (Exception e)
         {
            // TODO Auto-generated catch block
            logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception  occured while fetching rb " , e);
         }
         rbResposne = Response.ok((Object) reportBytes);
         rbResposne.header("Content-Disposition", "attachment; filename=rb.zip");
         rbResposne.header(RebateApiConstant.RESPONSE_HEADER_STATUS_CODE,
               configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "CODE_SUCCESS"));
         rbResposne.header(RebateApiConstant.RESPONSE_HEADER_STATUS_MESSAGE,
               configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, "MSG_RG_SUCCESS"));
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException  occured while validating userName " , e);
         return buildRBFailResponse("CODE_FAILURE_INVALID_USER_PASSWORD", "MSG_LOGIN_FAILURE");
      }
      catch (UserNameNotFoundException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserNameNotFoundException  occured while authenticating userName " , e);
         return buildRBFailResponse("CODE_FAILURE_INVALID_USER_PASSWORD", "MSG_LOGIN_FAILURE");
      }
      catch (InActiveUserException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InActiveUserException  occured while authenticating userName " , e);
         return buildRBFailResponse("CODE_FAILURE_INACTIVE_USER", "MSG_FAILURE_INACTIVE_USER");
      }
      catch (UserAuthenticationException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserAuthenticationException  occured while authenticating userName " , e);
         return buildRBFailResponse("CODE_FAILURE_INVALID_USER_PASSWORD", "MSG_LOGIN_FAILURE");
      }
      catch (UserBlockedException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "UserBlockedException  occured while authenticating userName " , e);
         return buildRBFailResponse("CODE_FAILURE_INVALID_USER_PASSWORD", "MSG_LOGIN_FAILURE");
      }
      catch (InternalServerException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException  occured while authenticating userName " , e);
         return buildRBFailResponse("CODE_FAILURE_INTERNAL", "MSG_FAILURE");
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while getting Resource Bundle:"
               , excep);
         return buildRBFailResponse("CODE_FAILURE_INTERNAL", "MSG_FAILURE");
      }
      return rbResposne.build();
   }

   public MetricsLogAdapter getMetricsLogAdapter()
   {
      return metricsLogAdapter;
   }

   public void setMetricsLogAdapter( MetricsLogAdapter metricsLogAdapter )
   {
      this.metricsLogAdapter = metricsLogAdapter;
   }

   public ILoginValidator getLoginValidator()
   {
      return loginValidator;
   }

   public void setLoginValidator( ILoginValidator loginValidator )
   {
      this.loginValidator = loginValidator;
   }

   public ILoginAdapter getLoginAdapter()
   {
      return loginAdapter;
   }

   public void setLoginAdapter( ILoginAdapter loginAdapter )
   {
      this.loginAdapter = loginAdapter;
   }
   
   /**
    * This method is used to build the failure json response for getRBRequest.
    * 
    * @param code - Error code to be used in json response .
    * @param msg - Error message to be used in json response.
    * @return response - This is the json error response for getRBRequest with given code and message .
    * 
    */

   private Response buildRBFailResponse( String code, String msg )
   {
      final String THIS_METHOD = "buildRBFailResponse";
      BStatus bStatus = new BStatus();
      BuildResponse responseBuilder = new BuildResponse();
      Response response = null;
      byte[] responseData = null;
      try
      {
         responseBuilder.setResponseCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               code));
         responseBuilder.setResponseMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY,
               msg));
         bStatus.setStatusCode(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, code));
         bStatus.setStatusMsg(configManager.getPropertyAsString(RebateApiConstant.REBATE_MESSAGES_PROPERTY, msg));
         responseData = bStatus.formatJSONString().getBytes(Charset.forName("UTF-8"));
         responseBuilder.setResponseObject(responseData);
         response = buildRESTResponse(responseBuilder, 0);
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occured while getting Resource Bundle:" , ex);
      }
      return response;
   }
}
