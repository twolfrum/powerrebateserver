package com.icf.rebate.api.module.impl;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@XmlType(propOrder = { "id", "name", "items" })
public class TuneUpBean implements Serializable {

	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private List<TuneUpItem> items;
	
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private String name;
	
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private String id;

	public List<TuneUpItem> getItems() {
		return items;
	}

	public void setItems(List<TuneUpItem> items) {
		this.items = items;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TuneUpBean [items=" + items + ", name=" + name + ", id=" + id + "]";
	}
	
	
}
