package com.icf.rebate.api.login.impl;

import java.util.ArrayList;
import java.util.List;

import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.user.intrface.IUserDao;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.UtilManager;
/**
 * This Class LoginValidator Validates all the required Login fields.If any
 * field is not valid ,then it will add the corresponding error message to the
 * errorlist. This class extends {@link AbstractValidator}.
 * 
 */
public class LoginValidator extends AbstractValidator
{
   List<String> errorsList = new ArrayList<String>();
   public static final String VLOGIN1001 = "VLOGIN1001";
   public static final String VLOGIN1002 = "VLOGIN1002";
   public static final String VLOGIN1003 = "VLOGIN1003";
   public static final String VLOGIN1004 = "VLOGIN1004";
   public static final String VLOGIN1005 = "VLOGIN1005";
   public static final String VLOGIN1006 = "VLOGIN1006";
   public static final String VLOGIN1007 = "VLOGIN1007";
   public static final String VLOGIN1008 = "VLOGIN1008";
   public static final String VLOGIN1009 = "VLOGIN1009";
   public static final String VSUB1006 = "VSUB1006";

   /**
    * Validates the user name and password.
    * 
    * @param userName - user name to be validated.
    * @param password - password to be validated.
    * @throws InvalidDataException - If userName or password contains invalid
    *            data.
    */
   public void validateLoginFields( String userName, String password ) throws InvalidDataException
   {
      if (! isNotNull(userName))
      {
         addError("User Name is mandatory.");
      }
      if (! isNotNull(password))
      {
         addError("Password is mandatory.");
      }
      checkInvalidDataException();
   }

   /**
    * Validates the user name and password.
    * 
    * @param userName - user name to be validated.
    * @throws InvalidDataException - If userName or password contains invalid
    *            data.
    */
   public void validateUser( String userName ) throws InvalidDataException
   {
      if (! isNotNull(userName))
      {
         addError("User Name is mandatory.");
      }
      checkInvalidDataException();
   }

   /**
    * Validates user name for valid value.
    * 
    * @param userId - user id.
    * @throws InvalidDataException
    */
   public void validateUserId( long userId ) throws InvalidDataException
   {
      if (! isPositiveNumber(userId))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1006));
      }
      // if there's an error, throw it here.
      checkInvalidDataException();
   }

   /**
    * Validates the password tracking beans fields for valid values.
    * 
    * @param userBean - is the user password tracking details.
    * @throws InvalidDataException
    */
   public void validatePasswordTrackingBean( UserPasswordTrackingBean userBean ) throws InvalidDataException
   {
      // validateNewAndConfirmPassword(userBean.getNewPassword(),
      // userBean.getConfirmNewPassword());
      validateUserId(userBean.getUserId());
      // validateUserName(userBean.getUserName());
      /*
       * validateQuestionId(userBean.getQuestionId());
       * validateAnswer(userBean.getAnswer());
       */
      // if there's an error, throw it here.
      checkInvalidDataException();
   }

   /**
    * Validates the user password tracking fields for valid values.
    * 
    * @param userBean - is the user password tracking details.
    * @throws InvalidDataException
    */
   public void validatePasswordCorrect( UserPasswordTrackingBean userBean ) throws InvalidDataException
   {
      validateOldPassword(userBean.getOldPassword(), userBean.getUserName());
      validateNewAndConfirmPassword(userBean.getNewPassword(), userBean.getConfirmNewPassword());
      validateOldAndNewPassword(userBean);
      validateUserId(userBean.getUserId());
      validateUserName(userBean.getUserName());
      // if there's an error, throw it here.
      checkInvalidDataException();
   }

   /**
    * Validates old password for valid value.
    * 
    * @param oldPassword - old password value is invalid.
    */
   private void validateOldPassword( String oldPassword, String userName )
   {
      if (! isNotNull(oldPassword))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1003));
      }
      else
      {
         try
         {
            // String encryptedPassword =
            // UtilManager.encrypt(oldPassword.trim());
            IUserDao userDao = (IUserDao) UtilManager.getBeanInstance("userDao");
            User user = userDao.getUserDetails(userName);
            if (user == null)
            {
               addError(UtilManager.getErrorMessage(VSUB1006));
            }
            else
            {
               if (! oldPassword.equals(user.getPassword()))
               {
                  addError(UtilManager.getErrorMessage(VLOGIN1003));
               }
            }
         }
         catch (Exception e)
         {
            addError("Exception occured while encrypting password");
         }
      }
   }

   /**
    * Validates new and confirm password for correct values and checks if new
    * and confirm password values are same.
    * 
    * @param newPassword - new password value.
    * @param confirmNewPassword - confirm password value.
    */
   private void validateNewAndConfirmPassword( String newPassword, String confirmNewPassword )
         throws InvalidDataException
   {
      validateNewPassword(newPassword);
      validateConfirmPassword(confirmNewPassword);
      // check if new and confirm new password values are proper, else throw
      // error.
      /*
       * if (! newPassword.equals(confirmNewPassword)) {
       * addError(UtilManager.getErrorMessage(VLOGIN1007)); }
       */
   }

   /**
    * Checks wether old and new password are same
    * 
    * @param userBean
    */
   private void validateOldAndNewPassword( UserPasswordTrackingBean userBean ) throws InvalidDataException
   {
      String password = null;
      try
      {
         password = UtilManager.encrypt(userBean.getNewPassword());
      }
      catch (InternalServerException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      if (userBean.getOldPassword().equalsIgnoreCase(password))
      {
         addError("Old password and New password are same");
      }
      checkInvalidDataException();
   }

   /**
    * Validates user name for valid value.
    * 
    * @param userName - user name.
    * @throws InvalidDataException - when user name is invalid.
    */
   public void validateUserName( String userName ) throws InvalidDataException
   {
      if (! isNotNull(userName))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1001));
      }
      // if there's an error, throw it here.
      checkInvalidDataException();
   }

   /**
    * Validates new password for valid value.
    * 
    * @param newPassword - c
    */
   private void validateNewPassword( String newPassword ) throws InvalidDataException
   {
      if (! isNotNull(newPassword))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1004));
      }
      // if there's an error, throw it here.
      checkInvalidDataException();
   }

   /**
    * Validates confirm password for valid value.
    * 
    * @param confirmNewPassword - new confirm password for valid value.
    */
   private void validateConfirmPassword( String confirmNewPassword ) throws InvalidDataException
   {
      if (! isNotNull(confirmNewPassword))
      {
         addError(UtilManager.getErrorMessage(VLOGIN1005));
      }
      // if there's an error, throw it here.
      checkInvalidDataException();
   }

   public void validate( User userDetails ) throws InvalidDataException
   {
      String message = "Invalid Username or Password";
      if (( ! UtilManager.isValidString(userDetails.getUserName()) )
            || ( ! UtilManager.isValidString(userDetails.getPassword()) ))
      {
         throw new InvalidDataException(message);
      }
   }

   public void validateChangePwd( User userDetails ) throws InvalidDataException
   {
      String message = "Invalid Username or Password";
      if (( ! UtilManager.isValidString(userDetails.getUserName()) )
            || ( ! UtilManager.isValidString(userDetails.getOldPassword()) )
            || ( ! UtilManager.isValidString(userDetails.getNewPassword()) ))
      {
         throw new InvalidDataException(message);
      }
   }
}
