package com.icf.rebate.api.restservice;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.icf.rebate.api.app.AppRestManager;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.util.ThreadLocalImpl;
@Path("/app")
public class AppRestService
{
   @Autowired
   private AppRestManager appRestManager;

   @POST
   @Path("/config")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   public Response configUserService( User userDetails, @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
      ThreadLocalImpl.setThreadLocalUserInfo(request, userDetails.getUserName());
      Response response = appRestManager.getUserConfig(request, userDetails);
      return response;
   }

   @POST
   @Path("/logs/save")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM })
   public Response saveLog( @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request, @QueryParam("_username")
   String username, String logContent ) throws Exception
   {
      ThreadLocalImpl.setThreadLocalUserInfo(request, username);
      Response response = appRestManager.logResponse(username, logContent);
      return response;
   }

   @POST
   @Path("/rb/get")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM, "application/zip" })
   public Response getRBZip( User userDetails, @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
      ThreadLocalImpl.setThreadLocalUserInfo(request, userDetails.getUserName());
      Response response = appRestManager.getRBZip(request, userDetails);
      return response;
   }
}
