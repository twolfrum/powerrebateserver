package com.icf.rebate.api.module.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.icf.rebate.api.util.UtilManager;
/**
 * This class represents upload zip file in server location on success and on failure store in local transaction path
 *
 */
public class CreateFileThread implements Runnable
{
   Map<String, Object> zipContent;

   CreateFileThread ( Map<String, Object> zipContent )
   {
      this.zipContent = zipContent;
   }

   @Override
   public void run()
   {
      BufferedOutputStream bs = null;
      try
      {
         String basePath = (String) zipContent.get("ICF_BASE_DIRECTORY");
         String companyName = (String) zipContent.get("companyName");
         String fileName = (String) zipContent.get("zipName");
         String filePath = basePath + companyName + "/" + fileName + ".zip";
         UtilManager.createParentDirectory(basePath);
         UtilManager.createDirectory(basePath + companyName);
         FileOutputStream fs = new FileOutputStream(new File(filePath));
         bs = new BufferedOutputStream(fs);
         bs.write((byte[]) zipContent.get("data"));
         bs.close();
      }
      catch (Exception e)
      {
         try
         {
            String basePath = (String) zipContent.get("MPORTAL_BASE_DIRECTORY");
            String companyName = (String) zipContent.get("companyName");
            String fileName = (String) zipContent.get("zipName");
            // String filePath =basePath+ companyName+"/"+fileName+".zip";
            UtilManager.createParentDirectory(basePath);
            UtilManager.createDirectory(basePath + companyName);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            Date date = new Date();
            String failureDate = dateFormat.format(date);
            UtilManager.createDirectory(basePath + companyName);
            UtilManager.createDirectory(basePath + companyName + "/" + failureDate);
            FileOutputStream fs = new FileOutputStream(new File(basePath + companyName + "/" + failureDate + "/"
                  + fileName + ".zip"));
            bs = new BufferedOutputStream(fs);
            bs.write((byte[]) zipContent.get("data"));
            bs.close();
         }
         catch (Exception ex)
         {
            ex.printStackTrace();
         }
         e.printStackTrace();
      }
      finally
      {
         zipContent = null;
         if (bs != null)
            try
            {
               bs.close();
            }
            catch (Exception e)
            {
               e.printStackTrace();
            }
      }
      
   }
}
