package com.icf.rebate.api.user.exception;

import com.icf.rebate.api.exception.GenericAPIException;
import com.icf.rebate.api.util.UtilManager;
/**
 * 
 * 
 * InvalidPasswordException exception represents an use case where the
 * subscriber password entered and confirm passwords are not equal.
 */
public class InvalidPasswordException extends GenericAPIException
{
   private String errorCode = "ESUB1003";
   private String errorMessage = UtilManager.getErrorMessage(errorCode);

   /**
    * Constructs an instance of <code>InvalidConfiguration</code>.
    */
   public InvalidPasswordException ()
   {
      super();
   }

   /**
    * Constructs an instance of <code>InvalidPasswordException</code> with the
    * specified detail message.
    */
   public InvalidPasswordException ( String msg )
   {
      super(msg);
   }

   // Security number - As per requirements in section 7.3 in 564 FRD v1.1 ref
   // bug#6609
   public String mportalIdentifier = "05131966";

   /**
    * Constructs an instance of <code>InvalidPasswordException</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public InvalidPasswordException ( Exception e )
   {
      super(e);
   }

   /**
    * Constructs an instance of <code>InvalidPasswordException</code> with the
    * specified detail message.
    * 
    * @param e is the actual Exception, that is wrapped in this Custom
    *           Exception.
    */
   public InvalidPasswordException ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorCode()
    */
   @Override
   public String getErrorCode()
   {
      return errorCode;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.mportal.api.exception.GenericAPIException#getErrorMessage()
    */
   @Override
   public String getErrorMessage()
   {
      return errorMessage;
   }
}
