package com.icf.rebate.api.stub;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "surveyAttributes")
public class SurveyAttributes extends BaseAttributes {

	public SurveyAttributes() {
		super();
	}
	
}
