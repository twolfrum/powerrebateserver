package com.icf.rebate.api.config;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.commons.configuration.FileConfiguration;

import com.icf.rebate.api.event.EventManager;
import com.icf.rebate.api.reload.IRefreshListener;
public class PropertyConfigImpl implements IConfiguration, IRefreshListener
{
   /**
    * Constants represents the API property file name, api.properties file
    * should be in class path.
    */
   public String configFilePath = "";
   /**
    * Default configuration builder.
    */
   DefaultConfigurationBuilder builder = null;
   /**
    * Combined configuration.
    */
   CombinedConfiguration combinedConfiguration = null;
   /**
    * Holds all the configuration object.
    */
   HashMap<String, FileConfiguration> configMap = null;
   /**
    * Event manager for reloading property files ,xmls etc.,
    */
   private EventManager eventManager;

   /**
    * Creates instance of PropertyConfigImpl.
    * 
    * @param configFilePath - path of the configuration file.
    */
   public PropertyConfigImpl ( String configFilePath )
   {
      this.configFilePath = configFilePath;
      configMap = new HashMap<String, FileConfiguration>();
   }

   public void initialize()
   {
      /**
       * The below code is used for adding current class instance in Event
       * manager list, for refreshing properties and xml files.
       */
      eventManager.addRefreshListner(this);
      loadConfiguration();
   }

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method reports an
    * error in the form of run time exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsString( String propertyFileName, String propertyName )
   {
      String propertyValue = null;
      validate(propertyFileName, propertyName);
      FileConfiguration fileConfiguration = configMap.get(propertyFileName);
      if (fileConfiguration.getProperty(propertyName) instanceof List)
      {
         propertyValue = fileConfiguration.getProperty(propertyName).toString();
         propertyValue = propertyValue.substring(1, propertyValue.length() - 1);
      }
      else
      {
         propertyValue = (String) fileConfiguration.getProperty(propertyName);
      }
      return propertyValue;
   }

   /**
    * Returns the property configured for the given property name from the given
    * property file. If the given property is not found, this method assumes the
    * default and returns it to the caller.<br>
    * If the given prperty configured with a comma separated value, then the
    * first String before comma will be returned.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - String value of the property if found in the property file.
    */
   public String getPropertyAsStringWithDefault( String propertyFileName, String propertyName, String defaultValue )
   {
      validate(propertyFileName, propertyName);
      FileConfiguration fileConfiguration = configMap.get(propertyFileName);
      String value = fileConfiguration.getString(propertyName);
      if (value == null)
      {
         value = defaultValue;
      }
      return value;
   }

   /**
    * Returns a list of strings which are separated by comma for the given
    * property.
    * 
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @return - list of all string which are separated by comma.
    */
   public List<String> getPropertyAsList( String propertyFileName, String propertyName )
   {
      List<String> valueList = null;
      validate(propertyFileName, propertyName);
      FileConfiguration fileConfiguration = configMap.get(propertyFileName);
      valueList = fileConfiguration.getList(propertyName);
      return valueList;
   }

   /**
    * Returns the property value as integer. If the value read from the property
    * file is not integer, this method tries to return the default. If the
    * default value is not a proper integer value, this method throws a run time
    * exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - int value of the property.
    */
   public int getPropertyAsInt( String propertyFileName, String propertyName, int defaultValue )
   {
      validate(propertyFileName, propertyName);
      FileConfiguration fileConfiguration = configMap.get(propertyFileName);
      return fileConfiguration.getInt(propertyName, defaultValue);
   }

   /**
    * Returns the property value as long. If the value read from the property
    * file is not long, this method tries to return the default. If the default
    * value is not a proper long value, this method throws a run time exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - long value of the property.
    */
   public long getPropertyAsLong( String propertyFileName, String propertyName, long defaultValue )
   {
      validate(propertyFileName, propertyName);
      FileConfiguration fileConfiguration = configMap.get(propertyFileName);
      return fileConfiguration.getLong(propertyName, defaultValue);
   }

   /**
    * Returns the property value as double. If the value read from the property
    * file is not double, this method tries to return the default. If the
    * default value is not a proper double value, this method throws a run time
    * exception.
    * 
    * @param propertyFileName - the file name where this property is present.
    * @param propertyName - name of the property to look for.
    * @param defaultValue - default value to be assumed, if property is
    *           un-available.
    * @return - double value of the property.
    */
   public double getPropertyAsDouble( String propertyFileName, String propertyName, double defaultValue )
   {
      validate(propertyFileName, propertyName);
      FileConfiguration fileConfiguration = configMap.get(propertyFileName);
      return fileConfiguration.getDouble(propertyName, defaultValue);
   }

   /**
    * Loads all the configuration details from the configuration file.
    */
   private void loadConfiguration()
   {
      if (configFilePath == null || configFilePath.trim().length() < 0)
      {
         throw new IllegalArgumentException("Invalid Config file path:" + configFilePath);
      }
      try
      {
         // reLoadAllResourceBundle();
         builder = new DefaultConfigurationBuilder(configFilePath);
         combinedConfiguration = new CombinedConfiguration();
         combinedConfiguration = builder.getConfiguration(true);
         Set<String> configNames = combinedConfiguration.getConfigurationNames();
         Iterator<String> itr = configNames.iterator();
         while (itr.hasNext())
         {
            String fileConfigName = itr.next();
            FileConfiguration configuration = (FileConfiguration) combinedConfiguration
                  .getConfiguration(fileConfigName);
            configMap.put(fileConfigName.toLowerCase(), configuration);
         }
      }
      catch (ConfigurationException e)
      {
         throw new IllegalArgumentException("Invalid Config file:" + configFilePath, e);
      }
   }

   /**
    * Validates the property file name and property.
    * 
    * @param propertyFileName - property file name to be validated.
    * @param propertyName - property name to be validated.
    */
   private void validate( String propertyFileName, String propertyName )
   {
      if (propertyFileName == null || propertyFileName.trim().length() == 0
            || ( ! configMap.containsKey(propertyFileName.trim().toLowerCase()) ))
      {
         throw new IllegalArgumentException("Invalid property file:" + propertyFileName);
      }
      if (propertyName == null || propertyName.trim().length() == 0)
      {
         throw new IllegalArgumentException("Invalid property:" + propertyName + " for the property file:"
               + propertyFileName);
      }
   }

   /**
    * Reloads all the Configuration files.
    * 
    * @return true - If successfully reloaded.<br>
    *         false - If reload fails.
    */
   public boolean reLoadAllConfiguration()
   {
      boolean isSuccess = false;
      try
      {
         synchronized (configMap)
         {
            loadConfiguration();
            isSuccess = true;
         }
      }
      catch (Exception excep)
      {
         // TODO log ths exception stack trace.
      }
      return isSuccess;
   }

   private static void reLoadAllResourceBundle()
   {
      try
      {
         Class type = ResourceBundle.class;
         Field cacheList = type.getDeclaredField("cacheList");
         cacheList.setAccessible(true);
         ( (Map) cacheList.get(ResourceBundle.class) ).clear();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Reloading the all the properties and xml files.
    */
   public void refreshNotify()
   {
      final String THIS_METHOD = "reloadResource";
      try
      {
         loadConfiguration();
         // UtilManager.refreshApplicationContext();
      }
      catch (Exception exc)
      {
         exc.printStackTrace();
      }
   }

   /**
    * @param eventManager the eventManager to set
    */
   public void setEventManager( EventManager eventManager )
   {
      this.eventManager = eventManager;
   }

   @Override
   public String getPropertyAsStringWithDefault( String propertyName, String defaultValue )
   {
      // TODO Auto-generated method stub
      return "";
   }
}
