package com.icf.rebate.api.module.impl;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 */
@XmlRootElement
@XmlType(propOrder = {"startDate", "submissionDate", "customerType", "houseType",
		"rebateToCurrentBill", "rebateToUtility", "rebateToDifferentPerson", "rebateAmountValue" })
public class AppDetails implements Serializable {
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String startDate;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String submissionDate;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String customerType;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String houseType;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String rebateToCurrentBill;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String rebateToUtility;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String rebateToDifferentPerson;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String rebateAmountValue;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String rebateIncentiveDecision;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String approximateSizeofHouse;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String approximateYearHouseBuilt;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String basement;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String permitNumber;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String newConstruction;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String comments;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String orderNumber;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String documentLater;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String heatingCoolingSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String buildingTypeSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String heatingCoolingFuelTypeSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String centralACSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String waterHeatingFuelTypeSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String auditorCompanyNameSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String yearBuiltSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String distributorSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String dateInstalled;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String projectTypeSelection;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String deviceType;
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	String mobileAppVersion;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDocumentLater() {
		return documentLater;
	}

	public void setDocumentLater(String documentLater) {
		this.documentLater = documentLater;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getBasement() {
		return basement;
	}

	public void setBasement(String basement) {
		this.basement = basement;
	}

	public String getRebateIncentiveDecision() {
		return rebateIncentiveDecision;
	}

	public void setRebateIncentiveDecision(String rebateIncentiveDecision) {
		this.rebateIncentiveDecision = rebateIncentiveDecision;
	}

	public String getApproximateSizeofHouse() {
		return approximateSizeofHouse;
	}

	public void setApproximateSizeofHouse(String approximateSizeofHouse) {
		this.approximateSizeofHouse = approximateSizeofHouse;
	}

	public String getApproximateYearHouseBuilt() {
		return approximateYearHouseBuilt;
	}

	public void setApproximateYearHouseBuilt(String approximateYearHouseBuilt) {
		this.approximateYearHouseBuilt = approximateYearHouseBuilt;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getHouseType() {
		return houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public String getRebateToCurrentBill() {
		return rebateToCurrentBill;
	}

	public void setRebateToCurrentBill(String rebateToCurrentBill) {
		this.rebateToCurrentBill = rebateToCurrentBill;
	}

	public String getRebateToUtility() {
		return rebateToUtility;
	}

	public void setRebateToUtility(String rebateToUtility) {
		this.rebateToUtility = rebateToUtility;
	}

	public String getRebateToDifferentPerson() {
		return rebateToDifferentPerson;
	}

	public void setRebateToDifferentPerson(String rebateToDifferentPerson) {
		this.rebateToDifferentPerson = rebateToDifferentPerson;
	}

	public String getRebateAmountValue() {
		return rebateAmountValue;
	}

	public void setRebateAmountValue(String rebateAmountValue) {
		this.rebateAmountValue = rebateAmountValue;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getHeatingCoolingSelection() {
		return heatingCoolingSelection;
	}

	public void setHeatingCoolingSelection(String heatingCoolingSelection) {
		this.heatingCoolingSelection = heatingCoolingSelection;
	}

	public String getBuildingTypeSelection() {
		return buildingTypeSelection;
	}

	public void setBuildingTypeSelection(String buildingTypeSelection) {
		this.buildingTypeSelection = buildingTypeSelection;
	}

	public String getPermitNumber() {
		return permitNumber;
	}

	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}

	public String getNewConstruction() {
		return newConstruction;
	}

	public void setNewConstruction(String newConstruction) {
		this.newConstruction = newConstruction;
	}

	public String getHeatingCoolingFuelTypeSelection() {
		return heatingCoolingFuelTypeSelection;
	}

	public void setHeatingCoolingFuelTypeSelection(String heatingCoolingFuelTypeSelection) {
		this.heatingCoolingFuelTypeSelection = heatingCoolingFuelTypeSelection;
	}

	public String getCentralACSelection() {
		return centralACSelection;
	}

	public void setCentralACSelection(String centralACSelection) {
		this.centralACSelection = centralACSelection;
	}

	public String getWaterHeatingFuelTypeSelection() {
		return waterHeatingFuelTypeSelection;
	}

	public void setWaterHeatingFuelTypeSelection(String waterHeatingFuelTypeSelection) {
		this.waterHeatingFuelTypeSelection = waterHeatingFuelTypeSelection;
	}

	public String getAuditorCompanyNameSelection() {
		return auditorCompanyNameSelection;
	}

	public void setAuditorCompanyNameSelection(String auditorCompanyNameSelection) {
		this.auditorCompanyNameSelection = auditorCompanyNameSelection;
	}

	public String getYearBuiltSelection() {
		return yearBuiltSelection;
	}

	public void setYearBuiltSelection(String yearBuiltSelection) {
		this.yearBuiltSelection = yearBuiltSelection;
	}

	public String getDistributorSelection() {
		return distributorSelection;
	}

	public void setDistributorSelection(String distributorSelection) {
		this.distributorSelection = distributorSelection;
	}
	
	public String getProjectTypeSelection() {
		return projectTypeSelection;
	}

	public void setProjectTypeSelection(String projectTypeSelection) {
		this.projectTypeSelection = projectTypeSelection;
	}

	public String getDateInstalled() {
		return dateInstalled;
	}

	public void setDateInstalled(String dateInstalled) {
		this.dateInstalled = dateInstalled;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getMobileAppVersion() {
		return mobileAppVersion;
	}

	public void setMobileAppVersion(String mobileAppVersion) {
		this.mobileAppVersion = mobileAppVersion;
	}

	@Override
	public String toString() {
		return "AppDetails [startDate=" + startDate + ", submissionDate="
				+ submissionDate + ", customerType=" + customerType + ", houseType=" + houseType
				+ ", rebateToCurrentBill=" + rebateToCurrentBill + ", rebateToUtility=" + rebateToUtility
				+ ", rebateToDifferentPerson=" + rebateToDifferentPerson + ", rebateAmountValue=" + rebateAmountValue
				+ ", rebateIncentiveDecision=" + rebateIncentiveDecision + ", approximateSizeofHouse="
				+ approximateSizeofHouse + ", approximateYearHouseBuilt=" + approximateYearHouseBuilt + ", basement="
				+ basement + ", permitNumber=" + permitNumber + ", newConstruction=" + newConstruction + ", comments="
				+ comments + ", documentLater=" + documentLater + ", heatingCoolingSelection=" + heatingCoolingSelection
				+ ", buildingTypeSelection=" + buildingTypeSelection + ", heatingCoolingFuelTypeSelection="
				+ heatingCoolingFuelTypeSelection + ", centralACSelection=" + centralACSelection
				+ ", waterHeatingFuelTypeSelection=" + waterHeatingFuelTypeSelection
				+ ", auditorCompanyNameSelection=" + auditorCompanyNameSelection
				+ ", yearBuiltSelection=" + yearBuiltSelection
				+ ", distributorSelection=" + distributorSelection
				+ ", dateInstalled=" + dateInstalled
				+ "]";
	}

}
