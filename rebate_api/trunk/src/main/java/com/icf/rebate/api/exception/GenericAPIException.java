package com.icf.rebate.api.exception;
/**
 * Generic API exception denotes the super class of all the exceptions thrown by
 * the API layer. Each exception that inherits this exception must specify the
 * error code and a message to describe the error. The error code might be used
 * by the callers to determine the appropriate error message to show the user.
 * 
 */
public abstract class GenericAPIException extends Exception
{
   /**
    * Instantiates a new generic api exception.
    * 
    * @param msg the msg
    */
   public GenericAPIException ( String msg )
   {
      super(msg);
   }

   /**
    * 
    */
   public GenericAPIException ()
   {
      super();
   }

   /**
    * @param e
    */
   public GenericAPIException ( Exception e )
   {
      super(e);
   }

   /**
    * Instantiates a new generic api exception.
    * 
    * @param msg the msg
    * @param e the e
    */
   public GenericAPIException ( String msg, Throwable e )
   {
      super(msg, e);
   }

   /**
    * @return the _errorCode
    */
   public abstract String getErrorCode();

   /**
    * @return the _errorMessage
    */
   public abstract String getErrorMessage();
}
