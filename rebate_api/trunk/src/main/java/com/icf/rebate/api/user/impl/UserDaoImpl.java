package com.icf.rebate.api.user.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.icf.rebate.api.user.intrface.IUserDao;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.api.util.UtilManager;
/**
 * This class implements user CRUD operation
 * 
 */
public class UserDaoImpl extends JdbcDaoSupport implements IUserDao
{
   /**
    * Returns Login Details for the given user name.
    * 
    * @param userName - User name for which login details will be retrieved.
    * @return User - User details for the given user name.
    */
   public User getUserDetails( String userName )
   {
      User userDetails = null;
      int result = this.getJdbcTemplate().queryForInt(IS_USER_EXTST, new Object[] { userName });
      if (result > 0)
      {
         userDetails = this.getJdbcTemplate().queryForObject(GET_USER_DETAILS, new Object[] { userName },
               new UserDetailMapper());
      }
      return userDetails;
   }

   /**
    * Get the unsuccessful attempt of user
    * 
    * @param userId - long unique id of user.
    * @return int count of unsuccessful attempt
    */
   public int getUnsuccesssAttempt( long userId )
   {
      final List<Integer> attemptList = new ArrayList<Integer>();
      int attempts = 0;
      Object[] params = new Object[] { userId };
      this.getJdbcTemplate().query(GET_UNSUCCESS_ATTEMPT, params, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            if (rs != null)
            {
               attemptList.add(rs.getInt("NO_OF_UNSUCCESSFUL_ATEMPTS"));
            }
         }
      });
      if (! attemptList.isEmpty())
      {
         attempts = attemptList.get(0);
      }
      return attempts;
   }

   /**
    * insert unsuccessful attempt of user into data source
    * 
    * @param userId - long unique id of user.
    * @return int count of unsuccessful attempt
    */
   public int insertUnsuccessAttempt( final long userId )
   {
      getJdbcTemplate().update(new PreparedStatementCreator()
      {
         public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
         {
            PreparedStatement ps = conn.prepareStatement(INSERT_UNSUCCESS_ATTEMPT);
            ps.setLong(1, userId);
            ps.setInt(2, 1);
            ps.setString(3,
                  UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(), RebateApiConstant.DATE_FORMAT));
            return ps;
         }
      });
      return 1;
   }

   /**
    * Update unsuccessful attempt of user into data source
    * 
    * @param userId - long unique id of user.
    * @param unSuccessAttemptCount - int count of unsuccessful attempt
    */
   public void updateUnsuccesssAttempt( final long userId, final int unSuccessAttemptCount )
   {
      getJdbcTemplate().update(new PreparedStatementCreator()
      {
         public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
         {
            PreparedStatement ps = conn.prepareStatement(UPDATE_UNSUCCESS_ATTEMPT);
            ps.setInt(1, unSuccessAttemptCount);
            ps.setString(2,
                  UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(), RebateApiConstant.DATE_FORMAT));
            ps.setLong(3, userId);
            return ps;
         }
      });
   }

   /**
    * Make User InActive for given user Id
    * 
    * @param userId - for this change status to inactive
    */
   public void UpdateUserStatusToInActive( final long userId )
   {
      getJdbcTemplate().update(new PreparedStatementCreator()
      {
         public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
         {
            PreparedStatement ps = conn.prepareStatement(UPDATE_USER_STATUS);
            ps.setLong(1, USER_INACTIVE);
            ps.setLong(2, userId);
            return ps;
         }
      });
   }

   public class UserDetailMapper implements RowMapper<User>
   {
      public User mapRow( ResultSet rs, int rowNum ) throws SQLException
      {
         User user = null;
         if (rs != null)
         {
            user = new User();
            user.setUserId(rs.getInt("USER_ID"));
            user.setUserName(rs.getString("USER_NAME"));
            user.setPassword(rs.getString("PWD"));
            user.setVerifyPassword(rs.getString("PWD"));
            user.setFirstName(rs.getString("FIRST_NAME"));
            user.setMiddleName(rs.getString("MIDDLE_NAME"));
            user.setLastName(rs.getString("LAST_NAME"));
            user.setGender(rs.getString("GENDER"));
            user.setPhone(rs.getString("PHONE"));
            user.setEmailId(rs.getString("EMAIL"));
            //user.setAddress(rs.getString("ADDRESS"));
            // KVB adding multiple field address entry
            user.setContractorCompany(rs.getString("CONTRACTOR_COMPANY"));
            user.setAddress1(rs.getString("ADDRESS1"));
            user.setAddress2(rs.getString("ADDRESS2"));
            user.setCity(rs.getString("CITY"));
            user.setState(rs.getString("STATE"));
            user.setZip(rs.getString("ZIP"));
            //-----------------------------------------
            user.setUserStatus(rs.getInt("USER_STATUS"));
            user.setUserRole(rs.getInt("USER_ROLE"));
            user.setCreatedTime(rs.getString("CREATED_TIME"));
            user.setLastUpdatedTime(rs.getString("LAST_UPDATED_TIME"));
            user.setContractorAccountNumber(rs.getString("ACCOUNT_NUMBER"));
            user.setResetFlag(rs.getString("RESET_FLAG"));
            user.setFullName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
            user.setTaxId(rs.getString(("TAX_ID")));
            if (user.getFullName() != null && user.getFullName().length() > 37)
            {
               user.setFullName(user.getFullName().substring(0, 37) + "...");
            }
         }
         return user;
      }
   }

   /**
    * Retrieve all active users
    * 
    * @return User Map - key - User_id and value-User Bean
    * 
    */
   public Map<Integer, User> getAllUsers()
   {
      final Map<Integer, User> userMap = new HashMap<Integer, User>();
      Object[] params = new Object[] { RebateApiConstant.USER_DELETED_CODE };
      this.getJdbcTemplate().query(GET_ALL_USER_DETAILS, params, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            User user = null;
            if (rs != null)
            {
               user = new User();
               user.setUserId(rs.getInt("USER_ID"));
               user.setUserName(rs.getString("USER_NAME"));
               user.setPassword(rs.getString("PWD"));
               user.setFirstName(rs.getString("FIRST_NAME"));
               user.setMiddleName(rs.getString("MIDDLE_NAME"));
               user.setLastName(rs.getString("LAST_NAME"));
               user.setGender(rs.getString("GENDER"));
               user.setPhone(rs.getString("PHONE"));
               user.setEmailId(rs.getString("EMAIL"));
               //user.setAddress(rs.getString("ADDRESS"));
               user.setContractorCompany(rs.getString("CONTRACTOR_COMPANY"));
               user.setAddress1(rs.getString("ADDRESS1"));
               user.setAddress2(rs.getString("ADDRESS2"));
               user.setCity(rs.getString("CITY"));
               user.setState(rs.getString("STATE"));
               user.setZip(rs.getString("ZIP"));
               user.setUserStatus(rs.getInt("USER_STATUS"));
               user.setUserRole(rs.getInt("USER_ROLE"));
               user.setCreatedTime(rs.getString("CREATED_TIME"));
               user.setLastUpdatedTime(rs.getString("LAST_UPDATED_TIME"));
               user.setContractorAccountNumber(rs.getString("ACCOUNT_NUMBER"));
               user.setResetFlag(rs.getString("RESET_FLAG"));
            }
            userMap.put(rs.getInt("USER_ID"), user);
         }
      });
      return userMap;
   }

   /**
    * Retrieve all active users List for manager
    * 
    * @param manager id
    * @return User List
    */
   public List<User> getUserListForManager(long managerId)
   {
      return getJdbcTemplate().query(GET_ALL_USER_DETAILS_FOR_MANAGER, new UserDetailMapper(),
            managerId);
   }

   
   /**
    * Retrieve all active users List
    * 
    * @return User List
    * 
    */
   public List<User> getUserList()
   {
      return getJdbcTemplate().query(GET_ALL_USER_DETAILS, new UserDetailMapper(),
            new Long(RebateApiConstant.USER_DELETED_CODE));
   }

   /**
    * Retrieve user status from DB
    * @return status map - key will be status_name and value will be status_id
    */
   public Map<String, Long> getUserStatusMap()
   {
      final Map<String, Long> userStatusMap = new HashMap<String, Long>();
      Object[] params = new Object[] { RebateApiConstant.USER_STATUS_CODE_TYPE };
      this.getJdbcTemplate().query(GET_USER_STATUS_LIST, params, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            if (rs != null)
            {
               userStatusMap.put(rs.getString("CODE_VALUE"), rs.getLong("CODE_ID"));
            }
         }
      });
      return userStatusMap;
   }

   /**
    * Add a new user to a manager
    * 
    * @param manager - contains manager details
    * @param user - contains user details
    */
   // TODO check or delete
   @Override
   public void addUserToManager(final User manager, final User user) {
	   getJdbcTemplate().update(new PreparedStatementCreator() {
		
		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement ps = con.prepareStatement(ADD_USER_TO_MANAGER);
			ps.setLong(1, user.getUserId());
			ps.setLong(2, manager.getUserId());
			return ps;
		}
	}); 
   }
   
   /**
    * Add New User into DB
    * 
    * @param user - contains user details
    */
   public void addUser( final User user )
   {
      getJdbcTemplate().update(new PreparedStatementCreator()
      {
         public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
         {
            PreparedStatement ps = conn.prepareStatement(ADD_USER);
            ps.setLong(1, getMaxUserId() + 1);
            ps.setString(2, user.getUserName().toLowerCase());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getFirstName());
            ps.setString(5, user.getMiddleName());
            ps.setString(6, user.getLastName());
            ps.setString(7, user.getGender());
            ps.setString(8, user.getPhone());
            ps.setString(9, user.getEmailId());
            //ps.setString(10, user.getAddress());
            ps.setString(10, user.getContractorCompany());
            ps.setString(11, user.getAddress1());
            ps.setString(12, user.getAddress2());
            ps.setString(13, user.getCity());
            ps.setString(14, user.getState());
            ps.setString(15, user.getZip());
            ps.setLong(16, user.getUserStatus());
            ps.setLong(17, user.getUserRole());
            ps.setString(18,
                  UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(), RebateApiConstant.DATE_FORMAT));
            ps.setString(19,
                  UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(), RebateApiConstant.DATE_FORMAT));
            ps.setString(20, user.getContractorAccountNumber());
            ps.setString(21, user.getTaxId());
            return ps;
         }
      });
   }

   /**
    * Add Companies associated to user
    */
   public void addUserCompany( final User user )
   {
      List<Long> companyList = user.getCompanyList();
      if (companyList != null && companyList.size() > 0)
      {
         for (int i = 0; i < companyList.size(); i++)
         {
            final String companyId = companyList.get(i) + "";
            getJdbcTemplate().update(new PreparedStatementCreator()
            {
               public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
               {
                  PreparedStatement ps = conn.prepareStatement(ADD_USER_COMPANY);
                  ps.setLong(1, user.getUserId());
                  ps.setLong(2, new Long(companyId));
                  return ps;
               }
            });
         }
      }
   }

   /**
    * Retrieve user map from DB for manager
    * @return status map - key will be role_name and value will be role_id
    */
   @Override
   public Map<String, Long> getRoleMapForManager()
   {
      final Map<String, Long> roleMap = new LinkedHashMap<String, Long>();
      this.getJdbcTemplate().query(GET_MANAGER_ROLE_QUERY, new RowCallbackHandler()
      {
         @Override
         public void processRow( ResultSet rs ) throws SQLException
         {
            roleMap.put(rs.getString("ROLE_NAME"), rs.getLong("ROLE_ID"));
         }
      });
      return roleMap;
   }
   
   /**
    * Retrieve user map from DB
    * @return status map - key will be role_name and value will be role_id
    */
   @Override
   public Map<String, Long> getRoleMap()
   {
      final Map<String, Long> roleMap = new LinkedHashMap<String, Long>();
      this.getJdbcTemplate().query(GET_ALL_ROLE_QUERY, new RowCallbackHandler()
      {
         @Override
         public void processRow( ResultSet rs ) throws SQLException
         {
            roleMap.put(rs.getString("ROLE_NAME"), rs.getLong("ROLE_ID"));
         }
      });
      return roleMap;
   }

   /**
    * Retrieve user details for given usedId
    * 
    * @param userId - get User details for this id
    * @return User - contains user details.
    * 
    */
   public User getUserById( long userId )
   {
      return this.getJdbcTemplate().queryForObject(GET_USER_DETAILS_BY_ID, new Object[] { userId },
            new UserDetailMapper());
   }

   public String getPwd( long userId )
   {
      String pwd = "";
      final List<String> pwdList = new ArrayList<String>();
      this.getJdbcTemplate().query(GET_PASSWORD, new Object[] { userId }, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            String passwd = rs.getString("PWD");
            pwdList.add(passwd);
         }
      });
      if (! pwdList.isEmpty())
      {
         pwd = pwdList.get(0);
      }
      return pwd;
   }

   /**
    * Returns Login Details for the given user name.
    * 
    * @param userName - User name for which login details will be retrieved.
    * @return User - User details for the given user name.
    */
   public long getUserById( String userName )
   {
      long userId = 0;
      final List<Long> userIdList = new ArrayList<Long>();
      /*
       * List subscriberStatusList = getSimpleJdbcTemplate().queryForList(
       * GET_SUBSCRIBER_CURRENT_STATUS, UserId); if
       * (!subscriberStatusList.isEmpty()) { Map subscriberMap =
       * (ListOrderedMap) subscriberStatusList.get(0);
       * 
       * currentStatus = Long.parseLong((subscriberMap.get("STATUS_CD") + ""));
       * } return currentStatus;
       */
      this.getJdbcTemplate().query(GET_USER_ID, new Object[] { userName }, new RowCallbackHandler()
      {
         /*
          * (non-Javadoc)
          * 
          * @see
          * org.springframework.jdbc.core.RowCallbackHandler#processRow(java
          * .sql.ResultSet)
          */
         public void processRow( ResultSet rs ) throws SQLException
         {
            long userId = rs.getLong("USER_ID");
            userIdList.add(userId);
         }
      });
      if (! userIdList.isEmpty())
      {
         userId = userIdList.get(0);
      }
      return userId;
   }
   
   public long getMaxUserId()
   {
      long userId = 0;
      final List<Long> userIdList = new ArrayList<Long>();
      this.getJdbcTemplate().query(GET_MAX_USER_ID, new RowCallbackHandler()
      {
         /*
          * (non-Javadoc)
          * 
          * @see
          * org.springframework.jdbc.core.RowCallbackHandler#processRow(java
          * .sql.ResultSet)
          */
         public void processRow( ResultSet rs ) throws SQLException
         {
            long userId = rs.getLong("USER_ID");
            userIdList.add(userId);
         }
      });
      if (! userIdList.isEmpty())
      {
         userId = userIdList.get(0);
      }
      return userId;
   }

   /**
    * Update existing user for given User object
    * 
    * @param user - contains user details
    */
   public void updateUser( final User user )
   {
      getJdbcTemplate().update(new PreparedStatementCreator()
      {
         public PreparedStatement createPreparedStatement( Connection conn ) throws SQLException
         {
            PreparedStatement ps = conn.prepareStatement(UPDATE_USER);
            ps.setString(1, user.getPassword());
            ps.setString(2, user.getFirstName());
            ps.setString(3, user.getMiddleName());
            ps.setString(4, user.getLastName());
            ps.setString(5, user.getGender());
            ps.setString(6, user.getPhone());
            ps.setString(7, user.getEmailId());
            //ps.setString(8, user.getAddress());
            ps.setString(8, user.getContractorCompany());
            ps.setString(9, user.getAddress1());
            ps.setString(10, user.getAddress2());
            ps.setString(11, user.getCity());
            ps.setString(12, user.getState());
            ps.setString(13, user.getZip());
            ps.setLong(14, user.getUserStatus());
            ps.setLong(15, user.getUserRole());
            ps.setString(16,
                  UtilManager.formatDateMilliSecondsToString(System.currentTimeMillis(), RebateApiConstant.DATE_FORMAT));
            ps.setString(17, user.getContractorAccountNumber());
            ps.setString(18, user.getTaxId());
            ps.setString(19, user.getUserName());
            ps.setLong(20, user.getUserId());
            return ps;
         }
      });
   }

   public void deleteUnsuccesssAttempt( final long userId )
   {
      getJdbcTemplate().update(DELETE_UNSUCCESS_ATTEMPT, userId);
   }

   /**
    * Delete User for given user id
    * 
    * @param userId - delete user for this id
    */
   public void deleteUser( final long userId )
   {
      getJdbcTemplate().update(DELETE_USER, userId);
   }

   /**
    * DisAssocaite companies for given used id
    */
   public void deleteUserCompany( final long userId )
   {
      getJdbcTemplate().update(DELETE_USER_COMPANY, userId);
   }

   /**
    * Retrieve company id's for given user id
    */
   public List<Long> getUserCompany( final long userId )
   {
      final List<Long> companyIdList = new ArrayList<Long>();
      this.getJdbcTemplate().query(GET_USER_COMPANY, new Object[] { userId }, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            long companyId = rs.getLong("COMPANY_ID");
            companyIdList.add(companyId);
         }
      });
      return companyIdList;
   }

   /**
    * Retrieve company Name for give user id
    */
   public List<String> getUserCompanyNames( final long userId )
   {
      final List<String> companyNameList = new ArrayList<String>();
      this.getJdbcTemplate().query(GET_USER_COMPANY_NAME, new Object[] { userId }, new RowCallbackHandler()
      {
         public void processRow( ResultSet rs ) throws SQLException
         {
            String companyName = rs.getString("COMPANY_NAME");
            companyNameList.add(companyName);
         }
      });
      return companyNameList;
   }

   /**
    * Search users for given value
    * 
    * @param searchString - search user for this value
    * @return User List
    */
   @Override
   public List<User> searchUser( String searchUser, String searchCompany )
   {
      String regExUsr = "%" + searchUser + "%";
      String regExCompany = "%" + searchCompany + "%";
      Object[] params = new Object[] { regExUsr, regExUsr, regExUsr, regExCompany };
      return getJdbcTemplate().query(GET_SEARCH_USER_DETAILS, params, new UserDetailMapper());
   }

   /**
    * Update Password reset flag for given used Id based on flag
    * 
    * @param flag
    * @param userId
    */
   public void modifyPwdResetFlag( boolean flag, long userId )
   {
      if (flag)
      {
         getJdbcTemplate().update(RESET_PASSWORD_FLAG_Y, new Object[] { userId });
      }
      else
      {
         getJdbcTemplate().update(RESET_PASSWORD_FLAG_N, new Object[] { userId });
      }
   }
}
