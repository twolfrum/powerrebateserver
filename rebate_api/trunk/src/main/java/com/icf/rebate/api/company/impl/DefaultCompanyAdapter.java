package com.icf.rebate.api.company.impl;

import java.security.DigestException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import com.icf.rebate.api.company.intrface.ICompanyAdapter;
import com.icf.rebate.api.company.intrface.ICompanyDao;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.user.intrface.IUserAdapter;
/**
 * This class DefaultUserAdapter will handle all login operaion.This class
 * implements {@link IUserAdapter}
 * 
 */
public class DefaultCompanyAdapter implements ICompanyAdapter
{
   private final String THIS_CLASS = "DefaultCompanyAdapter";
   private IMPLogger logger;
   /**
    * represents the User DAO.
    */
   private ICompanyDao companyDao = null;

   /**
    * Get all companies
    * 
    * @return Company map with key as company id and value as corresponding
    *         company bean
    * 
    */
   @Override
   public Map<Integer, Company> getAllCompanies()
   {
      return companyDao.getAllCompanies();
   }
   
   /**
    * Retrieve User details for given company id
    * 
    * @param companyId - get User details for this Id
    * @return User list
    */
   @Override
   public List<Company> getCompanyList()
   {
      List<Company> companyList = companyDao.getCompanyList();
      for (Company company : companyList)
      {
         company.setUsersList(companyDao.getUserDetailsForComapny(company.getCompanyId()));
      }
      return companyList;
   }

   /**
    * Retrieve company list for specific user
    * 
    * @param userId - get company list for this user id
    * @return company list
    */
   @Override
   public List<Company> getCompanyListForUser( long userId )
   {
      List<Company> companyList = companyDao.getCompanyList(userId);
      return companyList;
   }

   /**
    * get company details by company id
    * 
    * @param companyId - get company details for this id
    * @return Company - contains company details
    */
   @Override
   public Company getCompanyById( long companyId )
   {
      Company company = companyDao.getCompanyById(companyId);
      company.setUsersList(companyDao.getUserDetailsForComapny(company.getCompanyId()));
      return company;
   }

   /**
    * get company details by company name
    * 
    * @param companyName - get company details for this name
    * @return Company - contains company details
    */
   @Override
   public Company getCompanyByName( String companyName )
   {
      Company company = companyDao.getCompanyByName(companyName);
      company.setUsersList(companyDao.getUserDetailsForComapny(company.getCompanyId()));
      return company;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   public ICompanyDao getCompanyDao()
   {
      return companyDao;
   }

   public void setCompanyDao( ICompanyDao companyDao )
   {
      this.companyDao = companyDao;
   }

   /**
    * Add new company into DB
    * 
    * @param company - contains company details
    */
   public void addCompany( Company company ) throws DigestException, NoSuchAlgorithmException,
         CloneNotSupportedException, InternalServerException
   {
      companyDao.addCompany(company);
   }

   /**
    * Update existing company with new details.
    * 
    * @param company
    */
   @Override
   public void updateCompany( Company company )
   {
      companyDao.updateCompany(company);
   }

   /**
    * Delete company for given company Id.
    * 
    * @param company
    */
   @Override
   public void deleteCompany( long companyId )
   {
      // TODO Auto-generated method stub
      companyDao.deleteCompany(companyId);
   }

   /**
    * Get all companies
    * 
    * @return Company map with key as company name and value as corresponding
    *         company id
    */
   @Override
   public Map<String, Long> getAllCompanyNames()
   {
      return companyDao.getAllCompanyNames();
   }
   
   
   /**
    * Get all companies bound to managerId
    * 
    * @return Company map with key as company name and value as corresponding
    *         company id
    */
   
   @Override
   public Map<String, Long> getAllCompanyNamesForManager(long managerId) {
	   return companyDao.getAllCompanyNamesForManager(managerId);
   }


   /**
    * Get all companies
    * 
    * @return company Map with key as company Id and value as company name
    */
   @Override
   public Map<Long, String> getAllCompany()
   {
      return companyDao.getAllCompany();
   }
}
