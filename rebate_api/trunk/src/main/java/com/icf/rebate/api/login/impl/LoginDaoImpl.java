package com.icf.rebate.api.login.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.icf.rebate.api.login.intrface.ILoginDAO;
import com.icf.rebate.api.util.RebateApiConstant;
/**
 * This class LoginDAOImpl will handle all the data base operaion for login
 * functionalities. This class implements {@link ILoginDAO}
 * 
 */
public class LoginDaoImpl extends JdbcDaoSupport implements ILoginDAO
{
   public static final String RESET_FLAG_NO = "N";

   /**
    * Return the user password tracking bean for given user id
    * 
    * @param userId - for this user id return the password bean
    * @return UserPasswordTrackingBean
    */
   public UserPasswordTrackingBean getLoggedInUserDetail( final long userId )
   {
      final UserPasswordTrackingBean userTrackingBean = new UserPasswordTrackingBean();
      this.getJdbcTemplate().query(GET_LOGGEDIN_USER_DETAIL, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(1, userId);
         }
      }, new RowCallbackHandler()
      {
         @Override
         public void processRow( ResultSet rs ) throws SQLException
         {
            userTrackingBean.setUserId(rs.getLong("USER_ID"));
            userTrackingBean.setOldPassword(rs.getString("PASSWORD"));
            userTrackingBean.setPreviousLoginTime(rs.getString("PREVIOUS_LOGIN_TIME"));
            userTrackingBean.setCurrentLoginTime(rs.getString("CURRENT_LOGIN_TIME"));
            userTrackingBean.setPasswordUpdatedOn(rs.getString("PASSWORD_UPDATED_ON"));
            userTrackingBean.setResetFlag(rs.getString("RESET_FLAG"));
            if (RESET_FLAG_NO.equals(userTrackingBean.getResetFlag()))
            {
               userTrackingBean.setUserLoggedFirstTime(false);
            }
            userTrackingBean.setDataAvailable(true);
         }
      });
      return userTrackingBean;
   }

   /**
    * delete unsuccess attempts for given user id
    * @param userId
    */
   public void deleteUnsuccesssAttempt( final long userId )
   {
      this.getJdbcTemplate().update(DELETE_UNSUCCESS_ATTEMPT, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(1, userId);
         }
      });
   }
   /**
    * Insert user password History  using UserPasswordTrackingBean
    * @param userTrackingBean - contains complete User detaills
    */
   public void insertUserPasswordHistory( final UserPasswordTrackingBean userTrackingBean )
   {
      this.getJdbcTemplate().update(INSERT_PWD_HISTORY_QUERY, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(1, userTrackingBean.getUserId());
            ps.setString(2, userTrackingBean.getNewPassword());
            ps.setString(3, userTrackingBean.getPasswordUpdatedOn());
         }
      });
   }

   /**
    * Update logged in user details using UserPasswordTrackingBean
    * 
    * @param blnUpdatePwd - newpasswod/oldpassword
    */
   public void updateLoggedInUserDetail( final UserPasswordTrackingBean userTrackingBean, final boolean blnUpdatePwd )
   {
      this.getJdbcTemplate().update(UPDATE_LOGGEDIN_USER_DETAIL, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            if (blnUpdatePwd)
            {
               ps.setString(1, userTrackingBean.getNewPassword());
            }
            else
            {
               ps.setString(1, userTrackingBean.getOldPassword());
            }
            ps.setString(2, userTrackingBean.getPreviousLoginTime());
            ps.setString(3, userTrackingBean.getCurrentLoginTime());
            ps.setString(4, userTrackingBean.getPasswordUpdatedOn());
            ps.setString(5, userTrackingBean.getResetFlag());
            ps.setLong(6, userTrackingBean.getUserId());
         }
      });
   }

   public void insertUserPwdTrackDetail( final UserPasswordTrackingBean pwdTrackingBean )
   {
      this.getJdbcTemplate().update(INSERT_LOGGEDIN_USER_DETAIL, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(1, pwdTrackingBean.getUserId());
            ps.setString(2, pwdTrackingBean.getNewPassword());
            ps.setString(3, pwdTrackingBean.getPreviousLoginTime());
            ps.setString(4, pwdTrackingBean.getCurrentLoginTime());
            ps.setString(5, pwdTrackingBean.getPasswordUpdatedOn());
            ps.setString(6, pwdTrackingBean.getResetFlag());
         }
      });
   }
   /**
    * update password in user_profile table for input user_id
    * @param userId - User unique id
    * @param password - password
    */
   public void updatePasswordInUserProfile( final long userId, final String password )
   {
      this.getJdbcTemplate().update(UPDATE_PWD, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setString(1, password);
            ps.setLong(2, userId);
            ps.setLong(3, RebateApiConstant.USER_DELETED_CODE);
         }
      });
   }

   /**
    * delete password Track for given user id
    * 
    * @param userId - user unique id
    */
   public void deleteUserPwdTrack( final long userId )
   {
      this.getJdbcTemplate().update(DELETE_USER_PWD_TRACK, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(1, userId);
         }
      });
   }

   /**
    * Delete user password History for given user id
    * 
    * @param userId -  user unique id
    */
   public void deleteUserPwdHistory( final long userId )
   {
      this.getJdbcTemplate().update(DELETE_USER_PWD_HISTORY, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(1, userId);
         }
      });
   }

   /**
    * get passwordBean List by give used Id
    * 
    * @param userId - user unique id
    * @return List<UserPasswordTrackingBean> object
    */
   public List<UserPasswordTrackingBean> retrievePasswordsUsedBytheUser( final long userId )
   {
      final List<UserPasswordTrackingBean> userPwdHistoryList = new ArrayList<UserPasswordTrackingBean>();
      this.getJdbcTemplate().query(GET_PWD_HISTORY_QUERY, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(1, userId);
         }
      }, new RowCallbackHandler()
      {
         @Override
         public void processRow( ResultSet rs ) throws SQLException
         {
            UserPasswordTrackingBean pwdHistoryBean = new UserPasswordTrackingBean();
            pwdHistoryBean.setOldPassword(rs.getString("PASSWORD"));
            pwdHistoryBean.setPasswordUpdatedOn(rs.getString("PASSWORD_UPDATED_ON"));
            userPwdHistoryList.add(pwdHistoryBean);
         }
      });
      return userPwdHistoryList;
   }

   /**
    * Reset password flag based on Input flag for given user Id
    * 
    * @param userId - reset flag for this user Id
    * @param flag - if true set Y else set N
    */
   public void resetPasswordFlag( final long userId, final boolean flag )
   {
      this.getJdbcTemplate().update(UPDATE_PWD_RESET_FLAG, new PreparedStatementSetter()
      {
         @Override
         public void setValues( PreparedStatement ps ) throws SQLException
         {
            ps.setLong(2, userId); 
            if (flag)
               ps.setString(1, "Y");
            else
               ps.setString(1, "N");
         }
      });
   }
}
