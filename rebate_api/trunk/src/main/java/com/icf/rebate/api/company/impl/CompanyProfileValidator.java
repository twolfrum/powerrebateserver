package com.icf.rebate.api.company.impl;

import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.UtilManager;
/**
 * This class provides basic company profile validation
 *
 */
public class CompanyProfileValidator extends AbstractValidator
{
   public static final String VSUB1001 = "VSUB1001";
   public static final String VSUB1005 = "VSUB1005";
   public static final String VSUB1006 = "VSUB1006";
   public static final String VSUB1007 = "VSUB1007";
   public static final String VSUB1010 = "VSUB1010";
   public static final String VSUB1012 = "VSUB1012";
   public static final String VSUB1013 = "VSUB1013";
   public static final String VCOM1001 = "VCOM1001";

   /**
    * This validate method validates all the required Subscriber Profile
    * fields.if any field is not valid means it will add the corresponding
    * errormessages to errorlist.
    * 
    * @param profile - SubscriberProfile object.
    * @param flag - is true means it is update operation,we need to validate
    *           userId also. - is false means it is insert operation.
    * @throws InvalidDataException occurs when atleast one error occurs while
    *            validation
    */
   public void validateCompanyProfile( Company company, boolean prevalid ) throws InvalidDataException
   {
      if (company != null)
      {
         if (! prevalid)
         {
            validateCompanyName(company.getCompanyName());
         }
         if (company.getCompanyId() != 0)
         {
            validateCompanyId(company.getCompanyId());
         }
         validateCompanyState(company.getState());
      }
      else
      {
         addError(UtilManager.getErrorMessage(VSUB1001));
      }
      checkInvalidDataException();
   }

   /**
    * This validateUserId method is used to verify whether userID is valid or
    * not.if userID is not valid,then corresponding ErrorMessage is added to
    * errorList.
    * 
    * @param userID of selected user
    * @throws InvalidDataException occurs when atleast one error occurs while
    *            validation
    */
   public void validateCompanyId( long userID ) throws InvalidDataException
   {
      if (! isValidNumber(userID))
      {
         addError(UtilManager.getErrorMessage(VSUB1005));
      }
      checkInvalidDataException();
   }

   /**
    * This validateUserName method is used to verify whether userName is valid
    * or not.if userName is not valid,then corresponding ErrorMessage is added
    * to errorList.
    * 
    * @param userName of selected user
    */
   public void validateCompanyName( String userName )
   {
      if (! isNotNull(userName))
      {
         addError(UtilManager.getErrorMessage(VSUB1006));
      }
   }

   /**
    * This validateUserName method is used to verify whether userName is valid
    * or not.if userName is not valid,then corresponding ErrorMessage is added
    * to errorList.
    * 
    * @param userName of selected user
    */
   public void validateCompanyState( String userName )
   {
      if (! isNotNull(userName))
      {
         addError(UtilManager.getErrorMessage(VCOM1001));
      }
   }
}
