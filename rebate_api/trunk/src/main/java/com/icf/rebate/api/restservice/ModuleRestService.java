package com.icf.rebate.api.restservice;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.icf.rebate.api.module.impl.ModuleRestManager;
import com.icf.rebate.api.util.ThreadLocalImpl;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
@Path("/module")
public class ModuleRestService
{
   ModuleRestManager moduleRestManager;

   /*
   @POST
   @Path("/fields/save")
   @Consumes(MediaType.MULTIPART_FORM_DATA)
   @Produces({ MediaType.APPLICATION_JSON })
   public Response uploadZipFile( @FormDataParam("file")
   InputStream uploadedInputStream, @FormDataParam("file")
   FormDataContentDisposition fileDetail, @FormDataParam("companyName")
   String companyName, @FormDataParam("username")
   String username, @FormDataParam("password")
   String password, @FormDataParam("modulename")
   String modulename, @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
      Map<String, String> userDetails = new HashMap<String, String>();
      userDetails.put("username", username);
      userDetails.put("password", password);
      userDetails.put("companyName", companyName);
      userDetails.put("modulename", modulename);
      userDetails.put("fileName", fileDetail.getFileName());
      Response response = moduleRestManager.uploadZipFile(request, userDetails, uploadedInputStream);
      return response;
   }
   */

   @GET
   @Path("/fields/get")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   public Response getModuleFields( @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request, @QueryParam("_companyname")
   String companyName ) throws Exception
   {
      Response response = moduleRestManager.getModuleFields(request, companyName);
      return response;
   }
   
   /* when testing the following with SoapUI,
    * you must manually edit the both the Media Type and Content type of the attached 
    * file to be application/octet-stream  
    */

   @POST
   @Path("/fields/uploadZip")
   @Consumes({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON })
   @Produces({ MediaType.APPLICATION_JSON })
   public Response uploadZipFile( @Context
   HttpHeaders httpHeaders, @Context
   HttpServletRequest request ) throws Exception
   {
      ThreadLocalImpl.setThreadLocalUserInfo(request, httpHeaders.getRequestHeader("username").get(0));
      Response response = moduleRestManager.uploadZipFile(httpHeaders, request);
      return response;
   }

   public ModuleRestManager getModuleRestManager()
   {
      return moduleRestManager;
   }

   public void setModuleRestManager( ModuleRestManager moduleRestManager )
   {
      this.moduleRestManager = moduleRestManager;
   }
}
