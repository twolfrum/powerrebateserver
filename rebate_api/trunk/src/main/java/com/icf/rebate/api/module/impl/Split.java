package com.icf.rebate.api.module.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
@XmlRootElement
@XmlType(propOrder = { "parts" })
public class Split implements Serializable
{
   List<Part> parts;

   public List<Part> getParts()
   {
      return parts;
   }

   public void setParts( List<Part> parts )
   {
      this.parts = parts;
   }

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }
}
