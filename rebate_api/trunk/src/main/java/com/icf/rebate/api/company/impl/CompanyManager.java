package com.icf.rebate.api.company.impl;

import java.util.List;
import java.util.Map;

import com.icf.rebate.api.company.exception.CompanyAlreadyExist;
import com.icf.rebate.api.company.exception.CompanyHasUsersException;
import com.icf.rebate.api.company.intrface.ICompanyAdapter;
import com.icf.rebate.api.company.intrface.ICompanyValidator;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
/**
 * This class implements company CRUD operations.
 * 
 */
public class CompanyManager
{
   private final String THIS_CLASS = "CompanyManager";
   private ICompanyAdapter companyAdapter;
   private ICompanyValidator companyValidator;
   /**
    * logger object to log the messages.
    * 
    */
   private IMPLogger logger;

   /**
    * Get all companies
    * 
    * @return Company map with key as company id and value as corresponding
    *         company bean
    * @throws InternalServerException - throws if any server error occurs.
    */
   public Map<Integer, Company> getAllCompanies() throws InternalServerException
   {
      final String THIS_METHOD = "getAllCompanies";
      try
      {
         return companyAdapter.getAllCompanies();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Companies." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Companies." + e.getMessage(), e);
      }
   }

   /**
    * Get all companies
    * 
    * @return Company map with key as company name and value as corresponding
    *         company id
    * @throws InternalServerException - throws if any server error occurs.
    */
   public Map<String, Long> getAllCompanyNames() throws InternalServerException
   {
      final String THIS_METHOD = "getAllCompanyNames";
      try
      {
         return companyAdapter.getAllCompanyNames();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Companies." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Companies." + e.getMessage(), e);
      }
   }
   
   /**
    * Get all companies associated with id
    * 
    * @param id - Id specifying the user who's companies will be retrieved
    * @return Map with key as company name and value as company id
    * @throws InternalServerException - if server error occurs
    */
   
   public Map<String, Long> getAllCompanyNamesForManager(long id) throws InternalServerException {
	   final String THIS_METHOD = "getAllCompanyNamesForManager";
	   try
	      {
	         return companyAdapter.getAllCompanyNamesForManager(id);
	      }
	      catch (Exception e)
	      {
	         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
	               "InternalServerException occured while retrieving Companies." + e.getMessage(), e);
	         throw new InternalServerException("Exception occured while retrieving Companies." + e.getMessage(), e);
	      }
   }

   /**
    * Retrieve company List
    * 
    * @return company list
    * @throws InternalServerException - throws if any server error occurs.
    */
   public List<Company> getCompanyList() throws InternalServerException
   {
      final String THIS_METHOD = "getCompanyList";
      try
      {
         return companyAdapter.getCompanyList();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Subscriber." + e.getMessage(), e);
      }
   }

   /**
    * Retrieve company list for specific user
    * 
    * @param userId - get company list for this user id
    * @return company list
    * @throws InternalServerException
    */
   public List<Company> getCompanyListForUser( long userId ) throws InternalServerException
   {
      final String THIS_METHOD = "getCompanyListForUser";
      try
      {
         return companyAdapter.getCompanyListForUser(userId);
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Subscriber." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Subscriber." + e.getMessage(), e);
      }
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   public ICompanyAdapter getCompanyAdapter()
   {
      return companyAdapter;
   }

   public void setCompanyAdapter( ICompanyAdapter companyAdapter )
   {
      this.companyAdapter = companyAdapter;
   }

   public ICompanyValidator getCompanyValidator()
   {
      return companyValidator;
   }

   public void setCompanyValidator( ICompanyValidator companyValidator )
   {
      this.companyValidator = companyValidator;
   }

   /**
    * Add new company into DB
    * 
    * @param company - contains company details
    * @throws CompanyAlreadyExist - throws if company already exists.
    * @throws InvalidDataException - throws if data is invalid.
    * @throws InternalServerException - throws if server error occurs.
    */
   public void addCompany( Company company ) throws CompanyAlreadyExist, InvalidDataException, InternalServerException
   {
      final String THIS_METHOD = "addUser";
      try
      {
         this.companyValidator.validateAddCompany(company);
         this.companyAdapter.addCompany(company);
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException Exception occured while adding company." + e.getErrorMessage(), e);
         throw e;
      }
      catch (CompanyAlreadyExist e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "CompanyAlreadyExists Exception occured while adding company." + e.getErrorMessage(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "InternalServerException occured while adding company."
               + e.getMessage(), e);
         throw new InternalServerException("Exception occured while adding company." + e.getMessage(), e);
      }
   }

   /**
    * Get company details by id
    * 
    * @param companyId
    * @return Company bean - containing company details.
    * @throws InternalServerException - throws if server error occurs.
    */
   public Company getCompanyById( long companyId ) throws InternalServerException
   {
      final String THIS_METHOD = "getCompanyById";
      try
      {
         return companyAdapter.getCompanyById(companyId);
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving company." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving company." + e.getMessage(), e);
      }
   }

   /**
    * Get company details by name
    * 
    * @param companyId
    * @return Company bean - containing company details.
    * @throws InternalServerException - throws if server error occurs.
    */
   public Company getCompanyByName( String companyName ) throws InternalServerException
   {
      final String THIS_METHOD = "getCompanyById";
      try
      {
         return companyAdapter.getCompanyByName(companyName);
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving company." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving company." + e.getMessage(), e);
      }
   }

   /**
    * Update existing company with new details.
    * 
    * @param company
    * @throws InvalidDataException - throw if data is invalid.
    * @throws InternalServerException - throws if server error occurs.
    */
   public void updateCompany( Company company ) throws InvalidDataException, InternalServerException
   {
      final String THIS_METHOD = "updateCompany";
      try
      {
         this.companyValidator.validateUpdateCompany(company);
         this.companyAdapter.updateCompany(company);
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException Exception occured while updating company." + e.getErrorMessage(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "InternalServerException occured while updating company."
               + e.getMessage(), e);
         throw new InternalServerException("Exception occured while adding company." + e.getMessage(), e);
      }
   }

   /**
    * Delete company for given company Id.
    * 
    * @param company
    * @throws InvalidDataException - throw if data is invalid.
    * @throws InternalServerException - throws if server error occurs.
    */
   public void deleteCompany( long companyId ) throws InvalidDataException, InternalServerException,
         CompanyHasUsersException
   {
      final String THIS_METHOD = "deleteSubscriber";
      try
      {
         this.companyValidator.validateDeleteCompanyInfo(companyId);
         this.companyAdapter.deleteCompany(companyId);
      }
      catch (InvalidDataException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InvalidDataException Exception occured while deleting company." + e.getErrorMessage(), e);
         throw e;
      }
      catch (CompanyHasUsersException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "CompanyHasUsersException Exception occured while deleting company." + e.getErrorMessage(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "InternalServerException occured while deleting company."
               + e.getMessage(), e);
         throw new InternalServerException("Exception occured while deleting company." + e.getMessage(), e);
      }
   }

   /**
    * Get all companies
    * 
    * @return company Map with key as company Id and value as company name
    * @throws InternalServerException - throws if server error occurs.
    */
   public Map<Long, String> getAllCompany() throws InternalServerException
   {
      final String THIS_METHOD = "getAllCompany";
      try
      {
         return companyAdapter.getAllCompany();
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "InternalServerException occured while retrieving Companies." + e.getMessage(), e);
         throw new InternalServerException("Exception occured while retrieving Companies." + e.getMessage(), e);
      }
   }
}
