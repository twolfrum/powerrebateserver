package com.icf.rebate.api.module.impl;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;

import com.icf.rebate.api.util.UtilManager;

public class ArchiveUploadedFileThread implements Runnable {
	private byte[] zipData;
	private String basePath;
	private String companyName;
	private String processedZipName;
	
	public ArchiveUploadedFileThread(byte[] zipData, String basePath, 
						String companyName, String processedZipName) {
		this.zipData = zipData;
		this.basePath = basePath;
		this.companyName = companyName;
		this.processedZipName = processedZipName;
	}
	
	@Override
	public void run() {
		BufferedOutputStream bs = null;
	      try
	      {
	         String filePath = basePath + companyName + "/" + 
	        		 "/archived_uploads/" + processedZipName + "_uploaded.zip";
	         UtilManager.createParentDirectory(basePath);
	         UtilManager.createDirectory(basePath + companyName);
	         UtilManager.createDirectory(basePath + companyName + "/archived_uploads");
	         ByteArrayInputStream input = new ByteArrayInputStream(zipData);
	         FileOutputStream fs = new FileOutputStream(new File(filePath));
	         try
				{
					byte buffer[] = new byte[1024];
					int cnt = input.read( buffer );
					while ( cnt != -1 )
					{
						fs.write( buffer, 0, cnt );
						cnt = input.read( buffer );
					}
				}
				finally
				{
					fs.close();
					input.close();
				}	         
	      }
	      catch (Exception e)
	      {
	    	  
	      }
	}

}
