package com.icf.rebate.api.module.impl;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@XmlType(propOrder = { "name", "parts" })
public class Submission {
	
	public static final String SURVEY_ID = "survey";
	
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private String name;
	
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private List<SubmissionItem> items;
	
	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	private String id;

	public Submission(String id) {
		this.id = id;
	}
	
	public Submission() {
		super();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SubmissionItem> getItems() {
		return items;
	}

	public void setItems(List<SubmissionItem> items) {
		this.items = items;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		boolean equal = false;
		if (obj instanceof Submission) {
			Submission submissionToCompare = (Submission) obj;
			if (this.getId().equals(submissionToCompare.getId())) {
				equal = true;
			}
		}
		return equal;
	}
	
	

	@Override
	public String toString() {
		return "Submission [name=" + name + ", items=" + items + ", id=" + id + "]";
	}



	public class SubmissionItem {
		
		@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
		private Category surveyDetail;

		public Category getSurveyDetail() {
			return surveyDetail;
		}

		public void setSurveyDetail(Category surveyDetail) {
			this.surveyDetail = surveyDetail;
		}

		@Override
		public String toString() {
			return "SubmissionItem [surveyDetail=" + surveyDetail + "]";
		}
	}
}
