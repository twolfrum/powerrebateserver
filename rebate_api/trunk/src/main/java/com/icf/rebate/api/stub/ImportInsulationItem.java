package com.icf.rebate.api.stub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insulationRefId",
    "insulationAttributes"
})
@XmlRootElement(name = "importInsulationItem")

public class ImportInsulationItem {

	 @XmlElement(required = true)
	    protected String insulationRefId;
	    @XmlElement(required = true)
	    protected InsulationAttributes insulationAttributes;
		
	    public String getInsulationRefId() {
			return insulationRefId;
		}
		public void setInsulationRefId(String insulationRefId) {
			this.insulationRefId = insulationRefId;
		}
		public InsulationAttributes getInsulationAttributes() {
			return insulationAttributes;
		}
		public void setInsulationAttributes(InsulationAttributes insulationAttributes) {
			this.insulationAttributes = insulationAttributes;
		}

	    

	
}
