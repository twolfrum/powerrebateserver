package com.icf.rebate.api.subsystem;
/**
 * 
 * Generic manager class implements the life cycle methods of a manager class,
 * All the manager classes must extend from this class and then define their own
 * behavior.
 * 
 */
public abstract class GenericManager
{
   public GenericManager ()
   {
      this.initialize();
   }

   /**
    * This is the initialization step given to allow the manager class to setup
    * its environment. This method is present only to force the managers to
    * initialize themselves, so its not suggestible to call this method directly
    * on the manager instance.
    */
   protected abstract void initialize();
}
