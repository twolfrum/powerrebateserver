package com.icf.rebate.api.util;

public class ThreadLocalImpl
{
   /*
    * private static ThreadLocalImpl instance = null;
    * 
    * private ThreadLocalImpl() { // Exists only to defeat instantiation. }
    * 
    * public static ThreadLocalImpl getInstance() {
    * 
    * if(instance==null) { synchronized (ThreadLocalImpl.class) { if (instance
    * == null) { instance = new ThreadLocalImpl(); } } } return instance; }
    */
   private static final ThreadLocal<ServerInfo> threadLocal = new ThreadLocal<ServerInfo>();

   public static void setThreadLocalUserInfo( javax.servlet.http.HttpServletRequest request ,String userName)
   {
      /*
       * System.out.println("::::::::::::::get::::::::::"+threadLocal.get());
       * System
       * .out.println("::::::::::::::request protocol::::::::::"+request.getProtocol
       * ());
       * System.out.println("::::::::::::::request schema::::::::::"+request
       * .getScheme());
       * System.out.println("::::::::::::::server name::::::::::"+
       * request.getServerName());
       * System.out.println("::::::::::::::remote address::::::::::"
       * +request.getRemoteAddr());
       * System.out.println("::::::::::::::server port::::::::::"
       * +request.getServerPort());
       * System.out.println("::::::::::::::remote host::::::::::"
       * +request.getRemoteHost());
       * System.out.println("::::::::::::::remote port::::::::::"
       * +request.getRemotePort());
       * System.out.println("::::::::::::::Local address::::::::::"
       * +request.getLocalAddr());
       * System.out.println("::::::::::::::Local port::::::::::"
       * +request.getLocalPort());
       * System.out.println("::::::::::::::Local Name::::::::::"
       * +request.getLocalName());
       * System.out.println("::::::::::::::web context::::::::::"
       * +request.getContextPath());
       * System.out.println("::::::::::::::remote is secure::::::::::"
       * +request.isSecure());
       * System.out.println("::::::::::::::request url::::::::::"
       * +request.getRequestURL().toString()); System.out.println(
       * "::::::::::::::request header forwarded protocol::::::::::"
       * +request.getHeader("x-forwarded-proto"));
       * System.out.println("::::::::::::::request header host ::::::::::"
       * +request.getHeader("host"));
       */
      ServerInfo serverInfo = new ServerInfo();
      serverInfo.setUserId(userName);
      serverInfo.setProtocol(getProtocol(request));
      serverInfo.setRemoteHost(getHost(request));
      serverInfo.setWebContext(request.getContextPath());
      serverInfo.setTransactionID(String.valueOf(System.currentTimeMillis()));
      threadLocal.set(serverInfo);
   }

   private static String getProtocol( javax.servlet.http.HttpServletRequest request )
   {
      String protocol = request.getHeader("x-forwarded-proto");
      if (protocol != null && protocol.length() > 0)
         return protocol + "://";
      else
      {
         if (request.isSecure())
            return "https://";
         else
            return "http://";
      }
   }

   private static String getHost( javax.servlet.http.HttpServletRequest request )
   {
      String host = request.getHeader("host");
      if (host != null && host.length() > 0)
         return host;
      else
      {
         return null;
      }
   }

   public static void setThreadLocalUserInfo( ServerInfo serverIn )
   {
      ServerInfo serverInfo = new ServerInfo();
      if (serverIn != null)
      {
         serverInfo.setUserId(serverIn.getUserId());
         serverInfo.setProtocol(serverIn.getProtocol());
         serverInfo.setRemoteHost(serverIn.getRemoteHost());
         serverInfo.setWebContext(serverIn.getWebContext());
      }
      threadLocal.set(serverInfo);
   }

   public static ServerInfo getThreadLocalServerInfo()
   {
      return threadLocal.get();
   }

   public static void unsetThreadLocalUserInfo()
   {
      threadLocal.remove();
   }

   public static String getUserId()
   {
      ServerInfo serverInfo = getThreadLocalServerInfo();
      if (serverInfo != null)
         return serverInfo.getUserId() == null ? "" : serverInfo.getUserId();
      else
         return "";
   }

   public static String getTransactionId()
   {
      ServerInfo serverInfo = getThreadLocalServerInfo();
      if (serverInfo != null)
         return serverInfo.getTransactionID() == null ? "" : serverInfo.getTransactionID();
      else
         return "";
   }

   public static void setUserName( String username )
   {
      ServerInfo serverInfo = getThreadLocalServerInfo();
      if (serverInfo == null)
      {
         serverInfo = new ServerInfo();
         threadLocal.set(serverInfo);
      }
      serverInfo.setUserId(username);
   }

   public static void setTransactionId( String transactionId )
   {
      ServerInfo serverInfo = getThreadLocalServerInfo();
      if (serverInfo == null)
      {
         serverInfo = new ServerInfo();
         threadLocal.set(serverInfo);
      }
      serverInfo.setTransactionID(transactionId);
   }

   public static void setReportUserName( String reportUserName )
   {
      ServerInfo serverInfo = getThreadLocalServerInfo();
      if (serverInfo == null)
      {
         serverInfo = new ServerInfo();
         threadLocal.set(serverInfo);
      }
      serverInfo.setReportUserName(reportUserName);
   }

   public static String getReportUserName()
   {
      ServerInfo serverInfo = getThreadLocalServerInfo();
      if (serverInfo != null)
         return serverInfo.getReportUserName() == null ? "" : serverInfo.getReportUserName();
      else
         return "";
   }
}
