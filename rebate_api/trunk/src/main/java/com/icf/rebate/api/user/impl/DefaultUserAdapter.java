package com.icf.rebate.api.user.impl;

import java.security.DigestException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.login.impl.EmailUser;
import com.icf.rebate.api.login.impl.LoginManager;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.intrface.IUserAdapter;
import com.icf.rebate.api.user.intrface.IUserDao;
import com.icf.rebate.api.util.AbstractValidator;
import com.icf.rebate.api.util.UtilManager;

/**
 * This class DefaultUserAdapter will handle all login operaion.This class
 * implements {@link IUserAdapter}
 * 
 */
public class DefaultUserAdapter implements IUserAdapter
{
   private final String THIS_CLASS = "DefaultUserAdapter";
   private IMPLogger logger;
   private EmailUser emailUser;
   /**
    * represents the User DAO.
    */
   private IUserDao userDao = null;
   private LoginManager loginManager = null;

   @Override
   public Map<Integer, User> getAllUsers()
   {
      return userDao.getAllUsers();
   }

   @Override
   public List<User> getUserListForManager(long managerId) {
	   List<User> userList = userDao.getUserListForManager(managerId);
	      // set the company list in user
	      for (User user : userList)
	      {
	         user.setCompanyList(userDao.getUserCompany(user.getUserId()));
	      }
	      return userList;
   }
   
   @Override
   public List<User> getUserList()
   {
      List<User> userList = userDao.getUserList();
      // set the company list in user
      for (User user : userList)
      {
         user.setCompanyList(userDao.getUserCompany(user.getUserId()));
      }
      return userList;
   }
   
   @Override
   public Map<String, Long> getRoleMapForManager()
   {
      return userDao.getRoleMapForManager();
   }

   @Override
   public Map<String, Long> getRoleMap()
   {
      return userDao.getRoleMap();
   }

   @Override
   public User getUserById( long userId )
   {
      User user = userDao.getUserById(userId);
      user.setCompanyList(userDao.getUserCompany(user.getUserId()));
      return user;
   }

   public IUserDao getUserDao()
   {
      return userDao;
   }

   public void setUserDao( IUserDao userDao )
   {
      this.userDao = userDao;
   }

   public IMPLogger getLogger()
   {
      return logger;
   }

   public void setLogger( IMPLogger logger )
   {
      this.logger = logger;
   }

   public void addUser( User user ) throws DigestException, NoSuchAlgorithmException, CloneNotSupportedException,
         InternalServerException
   {
      String password = user.getPassword();
      user.setPassword(UtilManager.encrypt(password));
      userDao.addUser(user);
      user.setUserId(userDao.getUserById(user.getUserName()));
      userDao.addUserCompany(user);
      
      if (new AbstractValidator().validateEmail(user.getUserName())) {
    	  emailUser.sendEmailForAddUser(user.getUserName(), password, user.getUserName(), user.getFirstName(),
    	            user.getLastName());
      } 
      else {
    	  emailUser.sendUsernameNotValidEmail(user.getUserName());
      }
      
   }
   @Override
   public void addUserToManager(User manager, User user) {
	   userDao.addUserToManager(manager, user);
   }

   @Override
   public void updateUser( User user ) throws InternalServerException
   {
      String password = user.getPassword();
      // this code is required to check if the pwd filed has been edited as part
      // of update. Only if edited encrypt. Else retain old pwd.
      String existingpwd = userDao.getPwd(user.getUserId());
      if (! existingpwd.equals(user.getPassword()))
      {
         user.setPassword(UtilManager.encrypt(user.getPassword()));
         user.setVerifyPassword(user.getPassword());
         loginManager.resetPwdInternally(user);
         emailUser.sendEmailForUpdateUser(user.getEmailId(), password, user.getUserName(), user.getFirstName(),
               user.getLastName());
      }
      // if(password!=null && password.length()>0)
      // {
      // user.setPassword(UtilManager.encrypt(user.getPassword()));
      // }
      userDao.updateUser(user);
      userDao.deleteUserCompany(user.getUserId());
      userDao.addUserCompany(user);
      if (user.getUserStatus() != userDao.USER_INACTIVE && user.getUserStatus() != userDao.USER_DELETE)
      {
         userDao.deleteUnsuccesssAttempt(user.getUserId());
      }
      // if(password!=null && password.length()>0)
      // {
      // //loginManager.sendEmailWithTLS(subscriber.getEmailId(), password);
      // emailUser.sendEmailForPasswordUpdateUser(user.getEmailId(), password,
      // user.getUserName());
      // }
   }

   @Override
   public void deleteUser( long userId ) throws UserDoesNotExist
   {
      loginManager.deleteUserLoginData(userId);
      userDao.deleteUserCompany(userId);
      userDao.deleteUser(userId);
   }

   public Map<String, Long> getUserStatusMap()
   {
      return userDao.getUserStatusMap();
   }

   public EmailUser getEmailUser()
   {
      return emailUser;
   }

   public void setEmailUser( EmailUser emailUser )
   {
      this.emailUser = emailUser;
   }

   public LoginManager getLoginManager()
   {
      return loginManager;
   }

   public void setLoginManager( LoginManager loginManager )
   {
      this.loginManager = loginManager;
   }

   public List<User> searchUser( String searchUser, String searchCompany )
   {
      List<User> userList = userDao.searchUser(searchUser, searchCompany);
      // set the company list in user
      for (User user : userList)
      {
         user.setComapnyUtility(StringUtils.join(userDao.getUserCompanyNames(user.getUserId()), ","));
      }
      return userList;
   }

   public User getUserDetails( String userName ) throws UserDoesNotExist
   {
      return userDao.getUserDetails(userName);
   }

   public void modifyPwdResetFlag( boolean flag, long userId )
   {
      userDao.modifyPwdResetFlag(flag, userId);
   }
}
