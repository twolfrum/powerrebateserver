/**
 * 
 */
package com.icf.rebate.api.stub;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Ken Butler
 * 12/9/2015
 * Copyright (c) ICF International
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	    "importInsulationItem"
	})
@XmlRootElement(name = "importInsulation")
public class ImportInsulation {
    @XmlElement(required = true)
    protected List<ImportInsulationItem> importInsulationItem;

    
    public List<ImportInsulationItem> getImportEquipmentItem() {
        if (importInsulationItem == null) {
            importInsulationItem = new ArrayList<ImportInsulationItem>();
        }
        return this.importInsulationItem;
    }
}
