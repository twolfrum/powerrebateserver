package com.icf.rebate.api.user.intrface;

import java.util.List;
import java.util.Map;

import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.user.impl.UserDaoImpl.UserDetailMapper;
public interface IUserDao
{
   public static final long USER_ACTIVE = 9001;
   public static final long USER_INACTIVE = 9002;
   public static final long USER_DELETE = 9003;
   public static final String GET_USER_DETAILS = "SELECT USER_ID,USER_NAME ,PWD,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,PHONE,EMAIL,ADDRESS,CONTRACTOR_COMPANY,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,USER_STATUS,USER_ROLE,CREATED_TIME,LAST_UPDATED_TIME,ACCOUNT_NUMBER, RESET_FLAG, TAX_ID FROM USER_PROFILE WHERE USER_NAME =?";
   public static final String GET_USER_DETAILS_BY_ID = "SELECT USER_ID,USER_NAME ,PWD,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,PHONE,EMAIL,ADDRESS,CONTRACTOR_COMPANY,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,USER_STATUS,USER_ROLE,CREATED_TIME,LAST_UPDATED_TIME, ACCOUNT_NUMBER, RESET_FLAG, TAX_ID FROM USER_PROFILE WHERE USER_ID =?";
   // public static final String GET_ALL_USER_DETAILS = "SELECT USER_ID,USER_NAME ,PWD,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,PHONE,EMAIL,ADDRESS,USER_STATUS,USER_ROLE,CREATED_TIME,LAST_UPDATED_TIME,RESET_FLAG FROM USER_PROFILE WHERE USER_STATUS !=? ORDER BY FIRST_NAME";
   public static final String GET_ALL_USER_DETAILS = "SELECT USER_ID,USER_NAME ,PWD,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,PHONE,EMAIL,CONTRACTOR_COMPANY,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,USER_STATUS,USER_ROLE,CREATED_TIME,LAST_UPDATED_TIME,ACCOUNT_NUMBER,RESET_FLAG, TAX_ID FROM USER_PROFILE WHERE USER_STATUS !=? ORDER BY FIRST_NAME";
   public static final String GET_ALL_USER_DETAILS_FOR_MANAGER = "SELECT DISTINCT USER_PROFILE.USER_ID, USER_NAME ,PWD,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,PHONE,EMAIL,CONTRACTOR_COMPANY,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,USER_STATUS,USER_ROLE,CREATED_TIME,LAST_UPDATED_TIME,ACCOUNT_NUMBER,RESET_FLAG, TAX_ID FROM USER_PROFILE JOIN USER_TO_COMPANY ON USER_TO_COMPANY.USER_ID = USER_PROFILE.USER_ID AND USER_TO_COMPANY.COMPANY_ID IN (SELECT COMPANY_ID FROM USER_TO_COMPANY WHERE USER_ID = ?) ORDER BY FIRST_NAME";
   public static final String GET_ALL_ROLE_QUERY = "SELECT ROLE_ID, ROLE_NAME FROM ROLE ORDER BY UPPER(ROLE_NAME)";
   public static final String GET_MANAGER_ROLE_QUERY = "SELECT ROLE_ID, ROLE_NAME FROM ROLE WHERE ROLE_ID = 2";
  /* public static final String ADD_USER = "INSERT INTO USER_PROFILE(USER_ID,USER_NAME,PWD,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,PHONE,EMAIL,ADDRESS,USER_STATUS,USER_ROLE,CREATED_TIME,LAST_UPDATED_TIME,RESET_FLAG)"
         + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,'N')"; */
   public static final String ADD_USER = "INSERT INTO USER_PROFILE(USER_ID,USER_NAME,PWD,FIRST_NAME,MIDDLE_NAME,LAST_NAME,GENDER,PHONE,EMAIL,CONTRACTOR_COMPANY,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,USER_STATUS,USER_ROLE,CREATED_TIME,LAST_UPDATED_TIME,ACCOUNT_NUMBER,TAX_ID,RESET_FLAG)"
	         + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'N')";
   public static final String ADD_USER_TO_MANAGER = "INSERT INTO USER_TO_MANAGER(USER_ID, MANAGER_ID) VALUES(?,?)";
   public static final String GET_USER_ID = "SELECT USER_ID FROM USER_PROFILE WHERE USER_NAME =?";
   public static final String GET_MAX_USER_ID = "SELECT MAX(USER_ID) AS USER_ID FROM USER_PROFILE";
   // public static final String UPDATE_USER = "UPDATE USER_PROFILE SET PWD=?,FIRST_NAME=?, MIDDLE_NAME=?, LAST_NAME=? ,GENDER=? ,PHONE=?, EMAIL=?,ADDRESS=?,USER_STATUS=?,USER_ROLE=?, LAST_UPDATED_TIME =?  WHERE USER_NAME=? AND USER_ID=?";
   public static final String UPDATE_USER = "UPDATE USER_PROFILE SET PWD=?,FIRST_NAME=?, MIDDLE_NAME=?, LAST_NAME=? ,GENDER=? ,PHONE=?, EMAIL=?,CONTRACTOR_COMPANY=?, ADDRESS1=?,ADDRESS2=?,CITY=?,STATE=?,ZIP=?,USER_STATUS=?,USER_ROLE=?, LAST_UPDATED_TIME =?, ACCOUNT_NUMBER=?, TAX_ID=?  WHERE USER_NAME=? AND USER_ID=?";
   public static final String DELETE_UNSUCCESS_ATTEMPT = " DELETE FROM USER_UNSUCCESS_LOGIN_ATTEMPTS "
         + " WHERE USER_ID = ? ";
   public static final String DELETE_USER = " DELETE FROM USER_PROFILE  WHERE USER_ID = ? ";
   public static final String IS_USER_EXTST = "SELECT COUNT(*) FROM USER_PROFILE WHERE USER_NAME = ?";
   public static final String GET_USER_STATUS_LIST = "SELECT CODE_VALUE , CODE_ID FROM CODE WHERE CODE_TYPE=?";
   public static final String GET_PASSWORD = "SELECT PWD FROM USER_PROFILE WHERE USER_ID=?";
   public static final String ADD_USER_COMPANY = "INSERT INTO USER_TO_COMPANY(USER_ID,COMPANY_ID) VALUES(?,?)";
   public static final String DELETE_USER_COMPANY = " DELETE FROM USER_TO_COMPANY  WHERE USER_ID = ? ";
   public static final String GET_USER_COMPANY = " SELECT COMPANY_ID FROM USER_TO_COMPANY  WHERE USER_ID = ? ";
   public static final String GET_USER_COMPANY_NAME = " SELECT COMPANY_NAME FROM USER_TO_COMPANY ,COMPANY_PROFILE WHERE USER_ID =? AND USER_TO_COMPANY.COMPANY_ID = COMPANY_PROFILE.COMPANY_ID";
   public static final String INSERT_UNSUCCESS_ATTEMPT = " INSERT INTO USER_UNSUCCESS_LOGIN_ATTEMPTS "
         + " (USER_ID, NO_OF_UNSUCCESSFUL_ATEMPTS, DATE_AND_TIME) " + " VALUES (?,?,?) ";
   public static final String GET_UNSUCCESS_ATTEMPT = "SELECT NO_OF_UNSUCCESSFUL_ATEMPTS "
         + " FROM USER_UNSUCCESS_LOGIN_ATTEMPTS WHERE USER_ID = ? ";
   public static final String UPDATE_UNSUCCESS_ATTEMPT = " UPDATE USER_UNSUCCESS_LOGIN_ATTEMPTS SET "
         + " NO_OF_UNSUCCESSFUL_ATEMPTS = ?,  DATE_AND_TIME =?  WHERE USER_ID = ? ";
   public static final String UPDATE_USER_STATUS = "UPDATE USER_PROFILE SET USER_STATUS = ? WHERE USER_ID=?";
   public static final String UPDATE_USER_PASSWORD = "UPDATE USER_PROFILE SET PWD=? WHERE USER_NAME=? AND USER_ID=?;";
   public static final String GET_SEARCH_USER_DETAILS = "SELECT DISTINCT UP.USER_ID,UP.USER_NAME,UP.PWD,UP.FIRST_NAME,UP.MIDDLE_NAME,"
		     + "UP.LAST_NAME,UP.GENDER,UP.PHONE,UP.EMAIL,UP.ADDRESS,UP.CONTRACTOR_COMPANY,UP.ADDRESS1,UP.ADDRESS2,UP.CITY,"
		     + "UP.STATE,UP.ZIP,UP.USER_STATUS,UP.USER_ROLE,UP.CREATED_TIME,UP.LAST_UPDATED_TIME,UP.ACCOUNT_NUMBER,UP.RESET_FLAG,UP.TAX_ID "
	         + "FROM USER_PROFILE UP, USER_TO_COMPANY U2C, COMPANY_PROFILE CP "
	         + "WHERE (UP.USER_ID = U2C.USER_ID AND CP.COMPANY_ID = U2C.COMPANY_ID ) "
	         + "AND (UP.USER_NAME LIKE ? OR UP.FIRST_NAME LIKE ? OR UP.LAST_NAME LIKE ? ) "
	         + "AND CP.COMPANY_NAME LIKE ?;";
   public static final String RESET_PASSWORD_FLAG_Y = "UPDATE USER_PROFILE SET RESET_FLAG='Y' WHERE USER_ID=?";
   public static final String RESET_PASSWORD_FLAG_N = "UPDATE USER_PROFILE SET RESET_FLAG='N' WHERE USER_ID=?";

   /**
    * Returns Login Details for the given user name.
    * 
    * @param userName - User name for which login details will be retrieved.
    * @return User - User details for the given user name.
    */
   User getUserDetails( String userName );

   /**
    * Get the unsuccessful attempt of user
    * 
    * @param userId - long unique id of user.
    * @return int count of unsuccessful attempt
    */
   public int getUnsuccesssAttempt( long userId );

   /**
    * insert unsuccessful attempt of user into data source
    * 
    * @param userId - long unique id of user.
    * @return int count of unsuccessful attempt
    */
   public int insertUnsuccessAttempt( long userId );

   /**
    * Update unsuccessful attempt of user into data source
    * 
    * @param userId - long unique id of user.
    * @param unSuccessAttemptCount - int count of unsuccessful attempt
    */
   public void updateUnsuccesssAttempt( long userId, int unSuccessAttemptCount );

   /**
    * Delete unsuccessful attempt from data source
    * 
    * @param userId - long unique id of user.
    */
   public void deleteUnsuccesssAttempt( long userId );

   public void UpdateUserStatusToInActive( long userId );

   public Map<Integer, User> getAllUsers();

   public List<User> getUserList();

   /**
    * Add New User into DB
    * 
    * @param user - contains user details
    */
   public void addUser( User user );

   /**
    * Retrieve user map from DB
    * @return status map - key will be role_name and value will be role_id
    */
   public Map<String, Long> getRoleMap();

   /**
    * Retrieve user details for given usedId
    * 
    * @param userId - get User details for this id
    * @return User - contains user details.
    * 
    */
   public User getUserById( long userId );
   /**
    * Returns Login Details for the given user name.
    * 
    * @param userName - User name for which login details will be retrieved.
    * @return User - User details for the given user name.
    */
   public long getUserById( String userName );

   /**
    * Update existing user for given User object
    * 
    * @param user - contains user details
    */
   public void updateUser( User user );
   /**
    * Delete User for given user id
    * 
    * @param userId - delete user for this id
    */
   public void deleteUser( long userId );
   /**
    * DisAssocaite companies for given used id
    */
   public void deleteUserCompany( long userId );
   /**
    * Retrieve user status from DB
    * @return status map - key will be status_name and value will be status_id
    */
   public Map<String, Long> getUserStatusMap();

   public String getPwd( long userId );

   public void addUserCompany( User user );
   /**
    * Retrieve company id's for given user id
    */
   public List<Long> getUserCompany( long userId );

   /**
    * Search users for given value
    * 
    * @param searchUser - search user for this value
    * @param searchCompany - search companies associated with user for this value
    * @return User List
    */
   public List<User> searchUser( String searchUser, String searchCompany );
   /**
    * Retrieve company Name for give user id
    */
   public List<String> getUserCompanyNames( long userId );

   public void modifyPwdResetFlag( boolean flag, long userId );
   
   public List<User> getUserListForManager(long managerId);
   
   public Map<String, Long> getRoleMapForManager();
   
   public void addUserToManager(User manager, User user);
}
