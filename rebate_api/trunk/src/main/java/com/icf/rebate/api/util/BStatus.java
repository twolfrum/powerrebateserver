package com.icf.rebate.api.util;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
public class BStatus
{
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String statusCode;
   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
   String statusMsg;

   public String getStatusCode()
   {
      return statusCode;
   }

   public void setStatusCode( String statusCode )
   {
      this.statusCode = statusCode;
   }

   public String getStatusMsg()
   {
      return statusMsg;
   }

   public void setStatusMsg( String statusMsg )
   {
      this.statusMsg = statusMsg;
   }

   public String formatJSONString() throws JsonGenerationException, JsonMappingException, IOException
   {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(this);
   }
}
