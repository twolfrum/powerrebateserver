package com.icf.rebate.console.util;
public class Constants
{
   public static final String LOGIN = "login";
   public final static String LOGOUT = "logout";
   public final static String FAILED = "failed";
   public final static String FORGOT = "forgot";
   public final static int USER_MANAGEMENT = 0;
   public final static int COMPANY_MANAGEMENT = 1;
   public static final String REBATE_API_PROPERTY = "rebateapi";
   public static final String ADMIN_USERNAME = "ADMIN_USERNAME";
}
