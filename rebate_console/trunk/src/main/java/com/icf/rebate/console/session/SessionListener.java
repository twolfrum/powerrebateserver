package com.icf.rebate.console.session;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.logger.MPLoggerManager;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.console.util.UtilManager;

public class SessionListener implements HttpSessionListener {

	private IMPLogger logger;
	public static final String THIS_CLASS = SessionListener.class.getCanonicalName();

	public SessionListener() {
		initializeLoggerManager();
	}

	public void sessionCreated(HttpSessionEvent event) {
		final String THIS_METHOD = "sessionCreated";
		logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD, "session created : " + event.getSession().getId());
	}

	public void sessionDestroyed(HttpSessionEvent event) {

		final String THIS_METHOD = "sessionDestroyed";

		// get the destroying session...
		HttpSession session = event.getSession();
		//logger.log(IMPLogger.DEBUG, THIS_CLASS, THIS_METHOD,
		//		"session destroyed :" + session.getId() + " Logging out user...");

		try {
			// logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out called.");
			if (session != null) {
				session.invalidate();
			}
			// logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out completed successfully.");
		} catch (Exception e) {
			logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD,
					"error while logging out at session destroyed : " + e.getMessage(), e);
		}
	}

	private void initializeLoggerManager() {
		if (logger == null) {
			MPLoggerManager loggerManager = (MPLoggerManager) UtilManager
					.getBeanInstance(RebateApiConstant.LOGGER_MANAGER);
			this.logger = loggerManager.getLogger(RebateApiConstant.UI_LOGGER);
		}
	}
}