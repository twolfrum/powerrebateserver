package com.icf.rebate.console.company;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;

import com.icf.rebate.api.company.exception.CompanyHasUsersException;
import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.company.impl.CompanyManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.logger.MPLoggerManager;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.console.user.BUserManagement;
import com.icf.rebate.console.util.Constants;
import com.icf.rebate.console.util.MenuBakingBean;
import com.icf.rebate.console.util.UtilManager;
/**
 * This class implements company CRUD operations.
 * 
 */
public class BCompanyManagement
{
   private static final String THIS_CLASS = "BCompanyManagement";
   private List<Company> companyList;
   MenuBakingBean bMenuBakingBean;
   private Company currentCompany;
   private List<String> stateList;
   private long selectedCompanyId;
   private boolean readable;
   private boolean enableSaveButton = false;
   private boolean enableButton = false;
   private boolean enableEditButton = true;
   private boolean enableCancelButton = false;
   private boolean enableDeleteButton = false;
   CompanyManager companyManager;
   private boolean readableCompany;
   private List<User> companyUserList;
   private boolean displayContractors;
   BUserManagement bUserManagement;
   private long selectedUserId;
   private boolean showcompanyPopUp;
   private boolean showcompanyToUsersPopUp;
   private IMPLogger logger;

   @PostConstruct
   public void prepareCompanyDetails()
   {
      try
      {
         initializeLoggerManager();
         this.companyList = companyList();
         this.setReadable(true);
         this.readableCompany = true;
         this.setDisplayContractors(true);
         if (companyList != null && companyList.size() > 0)
         {
            currentCompany = companyList.get(0);
         }
         populateStateMap();
         this.showcompanyPopUp = false;
         this.showcompanyToUsersPopUp = false;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
   }

   private void populateStateMap()
   {
      try
      {
         this.stateList = com.icf.rebate.api.util.UtilManager.getAllStates();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Updates right panel on click of company
    */
   public void updateRightPanel()
   {
      try
      {
         this.readable = true;
         this.setDisplayContractors(true);
         this.readableCompany = true;
         this.currentCompany = companyManager.getCompanyById(selectedCompanyId);
         this.enableSaveButton = false;
         this.enableButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
         this.enableEditButton = true;
         this.showcompanyPopUp = false;
         this.showcompanyToUsersPopUp = false;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
   }

   /**
    * Updates right panel for selected company id
    * 
    * @param selectedCompanyId Id of the selected Company
    */
   public void updateRightPanel( long selectedCompanyId )
   {
      try
      {
         this.readable = true;
         this.setDisplayContractors(true);
         this.readableCompany = true;
         this.currentCompany = companyManager.getCompanyById(selectedCompanyId);
         this.enableSaveButton = false;
         this.enableButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
         this.enableEditButton = true;
         this.showcompanyPopUp = false;
         this.showcompanyToUsersPopUp = false;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
   }

   /**
    * This method is called on click of ADD Company button - this makes all the fields in  company registration page empty
    */
   public void addCompany()
   {
      try
      {
         this.readable = false;
         this.setDisplayContractors(false);
         this.readableCompany = false;
         this.currentCompany = new Company();
         this.enableSaveButton = true;
         this.enableEditButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
         this.showcompanyPopUp = false;
         this.showcompanyToUsersPopUp = false;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
   }

   /**
    * This method is called on click of Edit Company button - fields in the company registration page becomes editable
    */
   public void editCompany()
   {
      try
      {
         this.readable = false;
         this.readableCompany = true;
         this.enableSaveButton = true;
         this.enableEditButton = false;
         this.enableButton = true;
         this.enableCancelButton = true;
         this.enableDeleteButton = true;
         this.setDisplayContractors(true);
         this.showcompanyPopUp = false;
         this.showcompanyToUsersPopUp = false;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
   }

   /**
    * This method used to delete selected company
    */
   public void deleteCompany()
   {
      final String THIS_METHOD = "deleteCompany";
      try
      {
         if (currentCompany.getCompanyId() > 0)
         {
            companyManager.deleteCompany(currentCompany.getCompanyId());
            FacesContext.getCurrentInstance().addMessage(null,
                  new FacesMessage(FacesMessage.SEVERITY_INFO, "Company deleted successfully.", ""));
            logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "company " + currentCompany.getCompanyName()
                  + " deleted successfully.");
            this.companyList = companyList();
            currentCompany = companyList.get(0);
            readable = true;
            readableCompany = true;
            this.enableSaveButton = false;
            this.enableButton = false;
            this.enableCancelButton = false;
            this.enableDeleteButton = false;
            this.enableEditButton = true;
            this.showcompanyPopUp = false;
            this.showcompanyToUsersPopUp = false;
         }
      }
      catch (CompanyHasUsersException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Company Has Users Exception  occurred while deleting user", e);
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while deleting company", ex);
         ex.printStackTrace();
      }
   }

   /**
    * This method is called on click of cancel company button
    */
   public void cancelCompany()
   {
      try
      {
         this.companyList = companyList();
         this.currentCompany = UtilManager.getUpdatedCompany(currentCompany, companyList);
         readable = true;
         readableCompany = true;
         this.enableSaveButton = false;
         this.enableButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
         this.enableEditButton = true;
         this.showcompanyPopUp = false;
         this.showcompanyToUsersPopUp = false;
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
   }

   /**
    * This method is used to add or update company based on company id zero or
    * greater than zero
    */
   public void saveCompany()
   {
      final String THIS_METHOD = "saveCompany";
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "enter save company");
      try
      {
         if (isValidCompany())
         {
        	 logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "comapny is valid");
            Long companyId = currentCompany.getCompanyId();
            if (companyId != null && companyId > 0)
            {
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "comapny update begins");
               companyManager.updateCompany(currentCompany);
               FacesContext.getCurrentInstance().addMessage(null,
                     new FacesMessage(FacesMessage.SEVERITY_INFO, "Company updated successfully.", ""));
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Company " + currentCompany.getCompanyName()
                     + " updated successfully.");
               readable = true;
               this.enableSaveButton = false;
               this.enableButton = false;
               this.enableCancelButton = false;
               this.enableDeleteButton = false;
               this.enableEditButton = true;
               this.setDisplayContractors(true);
               readableCompany = true;
            }
            else
            {
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "company add begins");
               companyManager.addCompany(currentCompany);
               com.icf.rebate.api.util.UtilManager.companyDirecory(currentCompany.getCompanyName());
               FacesContext.getCurrentInstance().addMessage(null,
                     new FacesMessage(FacesMessage.SEVERITY_INFO, "Company created successfully.", ""));
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Company " + currentCompany.getCompanyName()
                     + " created successfully.");
               this.companyList = companyList();
               this.currentCompany = UtilManager.getUpdatedCompanyByName(currentCompany, companyList);
               readable = true;
               this.enableSaveButton = false;
               this.enableButton = false;
               this.enableCancelButton = false;
               this.enableDeleteButton = false;
               this.enableEditButton = true;
               this.setDisplayContractors(true);
               readableCompany = true;
            }
         }
      }
      catch (InvalidDataException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Invalid data exception occurred while saving company", e);
      }
      catch (Exception excep)
      {
         UtilManager.prepareErrorMessages(excep.getMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Invalid data exception occurred while saving company",
               excep);
      }
   }

   /**
    * Checks if company is valid or not
    * 
    * @return boolean true if valid else false.
    */
   private boolean isValidCompany()
   {
      if (( currentCompany.getCompanyName().trim() == null || currentCompany.getCompanyName().trim().length() == 0 )
            && ( currentCompany.getState().trim() == null || currentCompany.getState().trim().length() == 0 )
            && ( currentCompany.getProgramNumber().trim() == null || currentCompany.getProgramNumber().trim().length() == 0 ))
      {
         UtilManager.prepareErrorMessages("Please enter mandatory fields.");
         return false;
      }
      if (currentCompany.getCompanyName().trim() == null || currentCompany.getCompanyName().trim().length() == 0)
      {
         UtilManager.prepareErrorMessages("Company name is mandatory.");
         return false;
      }
      if (currentCompany.getState().trim() == null || currentCompany.getState().trim().length() == 0)
      {
         UtilManager.prepareErrorMessages("Please select a state.");
         return false;
      }
      if (currentCompany.getProgramNumber().trim() == null || currentCompany.getProgramNumber().trim().length() == 0) {
    	  UtilManager.prepareErrorMessages("Please enter a program number.");
    	  return false;
      }
      else
      {
         return true;
      }
   }

   /**
    * Gets users associated to selected company
    */
   public void getAssociatedUsers()
   {
      final String THIS_METHOD = "getAssociatedUsers";
      try
      {
         this.companyUserList = currentCompany.getUsersList();
         this.showcompanyPopUp = false;
         this.showcompanyToUsersPopUp = true;
         DataTable table = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
               .findComponent(":tabView:rightCompanyForm:contarctorDatatableId");
         table.setValueExpression("sortBy", null);
         table.setFirst(0);
         table.reset();
         table.setSortColumn(null);
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Invalid data exception occurred whilegetting users for company.",
               ex);
      }
   }

   /**
    * Show delete dialog
    */
   public void showDeletePopUp()
   {
      this.showcompanyPopUp = true;
      this.showcompanyToUsersPopUp = false;
   }

   /**
    * show Company_TO-user popup
    */
   public void showcompanyToUserPopUp()
   {
      this.showcompanyPopUp = false;
      this.showcompanyToUsersPopUp = true;
   }

   /**
    * navigates to user Management tab
    */
   public void goToUserManagementTab()
   {
      try
      {
         bUserManagement.setActiveIndex(Constants.USER_MANAGEMENT);
         bUserManagement.updateRightPanel(this.selectedUserId);
      }
      catch (Exception ex)
      {
         ex.printStackTrace();
      }
   }

   private List<Company> companyList() throws InternalServerException
   {
      return companyManager.getCompanyList();
   }

   public void initializeCompanyManagement()
   {
   }

   public MenuBakingBean getbMenuBakingBean()
   {
      return bMenuBakingBean;
   }

   public void setbMenuBakingBean( MenuBakingBean bMenuBakingBean )
   {
      this.bMenuBakingBean = bMenuBakingBean;
   }

   public List<Company> getCompanyList()
   {
      return companyList;
   }

   public void setCompanyList( List<Company> companyList )
   {
      this.companyList = companyList;
   }

   public Company getCurrentCompany()
   {
      return currentCompany;
   }

   public void setCurrentCompany( Company currentCompany )
   {
      this.currentCompany = currentCompany;
   }

   public List<String> getStateList()
   {
      return stateList;
   }

   public void setStateList( List<String> stateList )
   {
      this.stateList = stateList;
   }

   public long getSelectedCompanyId()
   {
      return selectedCompanyId;
   }

   public void setSelectedCompanyId( long selectedCompanyId )
   {
      this.selectedCompanyId = selectedCompanyId;
   }

   public boolean isReadable()
   {
      return readable;
   }

   public void setReadable( boolean readable )
   {
      this.readable = readable;
   }

   public boolean isEnableSaveButton()
   {
      return enableSaveButton;
   }

   public void setEnableSaveButton( boolean enableSaveButton )
   {
      this.enableSaveButton = enableSaveButton;
   }

   public boolean isEnableButton()
   {
      return enableButton;
   }

   public void setEnableButton( boolean enableButton )
   {
      this.enableButton = enableButton;
   }

   public boolean isEnableEditButton()
   {
      return enableEditButton;
   }

   public void setEnableEditButton( boolean enableEditButton )
   {
      this.enableEditButton = enableEditButton;
   }

   public boolean isEnableCancelButton()
   {
      return enableCancelButton;
   }

   public void setEnableCancelButton( boolean enableCancelButton )
   {
      this.enableCancelButton = enableCancelButton;
   }

   public boolean isEnableDeleteButton()
   {
      return enableDeleteButton;
   }

   public void setEnableDeleteButton( boolean enableDeleteButton )
   {
      this.enableDeleteButton = enableDeleteButton;
   }

   public CompanyManager getCompanyManager()
   {
      return companyManager;
   }

   public void setCompanyManager( CompanyManager companyManager )
   {
      this.companyManager = companyManager;
   }

   public boolean isReadableCompany()
   {
      return readableCompany;
   }

   public void setReadableCompany( boolean readableCompany )
   {
      this.readableCompany = readableCompany;
   }

   public List<User> getCompanyUserList()
   {
      return companyUserList;
   }

   public void setCompanyUserList( List<User> companyUserList )
   {
      this.companyUserList = companyUserList;
   }

   public boolean isDisplayContractors()
   {
      return displayContractors;
   }

   public void setDisplayContractors( boolean displayContractors )
   {
      this.displayContractors = displayContractors;
   }

   public BUserManagement getbUserManagement()
   {
      return bUserManagement;
   }

   public void setbUserManagement( BUserManagement bUserManagement )
   {
      this.bUserManagement = bUserManagement;
   }

   public long getSelectedUserId()
   {
      return selectedUserId;
   }

   public void setSelectedUserId( long selectedUserId )
   {
      this.selectedUserId = selectedUserId;
   }

   public boolean isShowcompanyPopUp()
   {
      return showcompanyPopUp;
   }

   public void setShowcompanyPopUp( boolean showcompanyPopUp )
   {
      this.showcompanyPopUp = showcompanyPopUp;
   }

   public boolean isShowcompanyToUsersPopUp()
   {
      return showcompanyToUsersPopUp;
   }

   public void setShowcompanyToUsersPopUp( boolean showcompanyToUsersPopUp )
   {
      this.showcompanyToUsersPopUp = showcompanyToUsersPopUp;
   }

   /**
    * Initializing the LoggerManger to log the logs in the logger
    * */
   private void initializeLoggerManager()
   {
      if (logger == null)
      {
         MPLoggerManager loggerManager = (MPLoggerManager) UtilManager
               .getBeanInstance(RebateApiConstant.LOGGER_MANAGER);
         this.logger = loggerManager.getLogger(RebateApiConstant.UI_LOGGER);
      }
   }

   
}
