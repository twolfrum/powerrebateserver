package com.icf.rebate.console.session;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.logger.MPLoggerManager;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.console.util.Constants;
import com.icf.rebate.console.util.UtilManager;

public class BSessionManager {

		public static final String THIS_CLASS = BSessionManager.class.getSimpleName();
	
		private IMPLogger logger;
		
	
		public BSessionManager() {
			initializeLoggerManager();
		}
	
		/**
		 * logout- invalidate the session and redirect to login page
		 * 
		 * @return String - based on the value redirect to home page or login page
		 */
		   public String logout()
		   {
		      final String THIS_METHOD = "logout";
		      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out called.");
		      HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
		            .getRequest();
		      HttpSession session = request.getSession(false);
		      if (session != null)
		      {
		         session.invalidate();
		      }
		      session = request.getSession(true);
		      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out completed successfully.");
		      return Constants.LOGOUT;
		   }
	
	   /**
	    * Initializing the LoggerManger to log the logs in the logger
	    **/
	   private void initializeLoggerManager()
	   {
	      if (logger == null)
	      {
	         MPLoggerManager loggerManager = (MPLoggerManager) UtilManager
	               .getBeanInstance(RebateApiConstant.LOGGER_MANAGER);
	         this.logger = loggerManager.getLogger(RebateApiConstant.UI_LOGGER);
	      }
	   }
}
