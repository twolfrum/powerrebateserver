package com.icf.rebate.console.util;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
@ManagedBean(name = "bMenuBakingBean")
@SessionScoped
public class MenuBakingBean implements Serializable
{
   private String currentPage;

   public String getCurrentPage()
   {
      return currentPage;
   }

   public void setCurrentPage( String currentPage )
   {
      this.currentPage = currentPage;
   }
}
