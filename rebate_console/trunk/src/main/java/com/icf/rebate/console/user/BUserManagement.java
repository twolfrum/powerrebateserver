package com.icf.rebate.console.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.TabChangeEvent;

import com.icf.rebate.api.company.impl.CompanyManager;
import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.logger.MPLoggerManager;
import com.icf.rebate.api.user.exception.EmailIdAlreadyExist;
import com.icf.rebate.api.user.exception.InvalidPasswordException;
import com.icf.rebate.api.user.exception.UserAlreadyExist;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.user.impl.UserManager;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.console.company.BCompanyManagement;
import com.icf.rebate.console.login.BLoginManagement;
import com.icf.rebate.console.util.Constants;
import com.icf.rebate.console.util.MenuBakingBean;
import com.icf.rebate.console.util.UtilManager;
/**
 * This class implements User CRUD operations
 * 
 */
public class BUserManagement implements Serializable
{
   private static final String THIS_CLASS = "BUserManagement";
   private List<User> userList;
   MenuBakingBean bMenuBakingBean;
   UserManager userManager;
   CompanyManager companyManager;
   private User currentUser;
   private Map<String, Long> userRoleMap;
   private Map<String, Long> userStatusMap;
   private long selectedUserId;
   private boolean readable;
   private boolean readableUser;
   private boolean enableSaveButton = false;
   private boolean enableButton = false;
   private boolean enableEditButton = false;
   private boolean enableCancelButton = false;
   private boolean enableDeleteButton = false;
   private boolean isManager = false;
   private Map<String, Long> companyMap;
   private List<Long> selectedCompanyList;
   private List<String> associatedCompanyList;
   private int activeIndex;
   private boolean show;
   private String searchUser;
   private String searchCompany;
   private boolean showSearchPopup;
   private List<User> searchList;
   BLoginManagement bLoginManagement;
   ConfigurationManager configurationManager;
   private IMPLogger logger;
   

   /**
    * constructor
    */
   public BUserManagement ()
   {
      show = false;
      showSearchPopup = false;
      initializeLoggerManager();
   }

   @PostConstruct
   public void prepareUserDetails()
   {
      final String THIS_METHOD = "prepareUserDetails";
      try
      {
         configurationManager = (ConfigurationManager) UtilManager.getBeanInstance(RebateApiConstant.CONFIG_MANAGER);
         this.activeIndex = Constants.USER_MANAGEMENT;
         this.isManager = bLoginManagement.getCurrentUser().getUserRole() == 3;
         this.userList = userList();
         this.setReadable(true);
         this.readableUser = true;
         if (userList != null && userList.size() > 0)
         {
            currentUser = userList.get(0);
            this.selectedCompanyList = currentUser.getCompanyList();
            // If admin get all companies
            if (!isManager) {
            	this.companyMap = companyManager.getAllCompanyNames();
            } else { // If manager only get companies bound to manager
            	this.companyMap = companyManager.getAllCompanyNamesForManager(bLoginManagement.getCurrentUser().getUserId());
            }
            this.associatedCompanyList = getAssociatedCompanyNameList(selectedCompanyList);
            enableUpdatingUser();
         }
         populateRolesList();
         populateStatusList();
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while constructing user details", ex);
      }
   }

   /**
    * Prepare Roles list
    */
   private void populateRolesList()
   {
      final String THIS_METHOD = "populateRolesList";
      try
      {
         this.userRoleMap = new HashMap<String, Long>();
         if (isManager) {
        	 this.userRoleMap = userManager.getRoleMapForManager();
         } else { // admin 
        	 this.userRoleMap = userManager.getRoleMap();
         }
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while preparing role list", e);
      }
   }

   /**
    * Prepare status map
    */
   private void populateStatusList()
   {
      final String THIS_METHOD = "populateStatusList";
      try
      {
         this.setUserStatusMap(new HashMap<String, Long>());
         this.setUserStatusMap(userManager.getUserStatusMap());
      }
      catch (Exception e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while preparing status list", e);
      }
   }

   /**
    * Updates right panel on click of user
    */
   public void updateRightPanel()
   {
      final String THIS_METHOD = "updateRightPanel";
      try
      {
         this.readable = true;
         this.readableUser = true;
         this.currentUser = userManager.getUserById(selectedUserId);
         enableUpdatingUser();
         this.selectedCompanyList = currentUser.getCompanyList();
         this.associatedCompanyList = getAssociatedCompanyNameList(selectedCompanyList);
         this.enableSaveButton = false;
         this.enableButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while updating right pannel", ex);
      }
   }

   /**
    * Update right panel for selected user id
    * 
    * @param selectedUserId - Id for the selected User
    */
   public void updateRightPanel( long selectedUserId )
   {
      final String THIS_METHOD = "updateRightPanel";
      try
      {
         this.readable = true;
         this.readableUser = true;
         this.currentUser = userManager.getUserById(selectedUserId);
         enableUpdatingUser();
         this.selectedCompanyList = currentUser.getCompanyList();
         this.associatedCompanyList = getAssociatedCompanyNameList(selectedCompanyList);
         this.enableSaveButton = false;
         this.enableButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while updating right pannel", ex);
      }
   }

   /**
    * This method call on click of ADD button- User registration page becomes
    * blank
    */
   public void addUser()
   {
      final String THIS_METHOD = "addUser";
      try
      {
         this.readable = false;
         this.readableUser = false;
         this.currentUser = new User();
         this.selectedCompanyList = new ArrayList<Long>();
         this.associatedCompanyList = new ArrayList<String>();
         this.enableSaveButton = true;
         this.enableEditButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
      }
      catch (Exception excep)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred on click of add button", excep);
      }
   }

   /**
    * This method call on click of Edit button - User registration page becomes
    * editable
    */
   public void editUser()
   {
      final String THIS_METHOD = "editUser";
      try
      {
         this.readable = false;
         this.readableUser = true;
         this.enableSaveButton = true;
         this.enableEditButton = false;
         this.enableButton = true;
         this.enableCancelButton = true;
         this.enableDeleteButton = true;
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred on click of edit button", ex);
      }
   }

   /**
    * This method used to delete selected user
    */
   public void deleteUser()
   {
      final String THIS_METHOD = "deleteUser";
      try
      {
         if (currentUser.getUserId() > 0)
         {
            if (currentUser.getUserName().equals(bLoginManagement.getUsername()))
            {
               UtilManager.prepareErrorMessages("Not allowed to delete Logged in user");
            }
            else
            {
               userManager.deleteUser(currentUser.getUserId());
               FacesContext.getCurrentInstance().addMessage(null,
                     new FacesMessage(FacesMessage.SEVERITY_INFO, "User deleted successfully.", ""));
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User " + currentUser.getUserName()
                     + " deleted successfully.");
               this.userList = userList();
               currentUser = userList.get(0);
               this.selectedCompanyList = currentUser.getCompanyList();
               this.associatedCompanyList = getAssociatedCompanyNameList(selectedCompanyList);
               readableUser = true;
               readable = true;
               this.enableSaveButton = false;
               this.enableButton = false;
               this.enableCancelButton = false;
               this.enableDeleteButton = false;
               this.enableEditButton = true;
            }
         }
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while deleting user", ex);
      }
   }

   /**
    * This method calls on click of cancel button
    */
   public void cancelUser()
   {
      final String THIS_METHOD = "cancelUser";
      try
      {
         this.userList = userList();
         /* currentUser = userList.get(0); */
         this.currentUser = UtilManager.getUpdatedUser(currentUser, userList);
         this.selectedCompanyList = currentUser.getCompanyList();
         readableUser = true;
         readable = true;
         this.enableSaveButton = false;
         this.enableButton = false;
         this.enableCancelButton = false;
         this.enableDeleteButton = false;
         this.enableEditButton = true;
      }
      catch (Exception ex)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred on click of cancel button", ex);
      }
   }

   /**
    * This method is used to add or update user based on used id zero or greater
    * than zero
    */
   public void saveUser()
   {
      final String THIS_METHOD = "saveUser";
      try
      {
         if (isValidUser())
         {
            Long userId = currentUser.getUserId();
            if (userId != null && userId > 0)
            {
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User update begins");
               currentUser.setCompanyList(this.selectedCompanyList);
               userManager.updateUser(currentUser);
               FacesContext.getCurrentInstance().addMessage(null,
                     new FacesMessage(FacesMessage.SEVERITY_INFO, "User updated successfully.", ""));
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User " + currentUser.getUserName()
                     + " updated successfully.");
               this.userList = userList();
               this.selectedCompanyList = currentUser.getCompanyList();
               this.associatedCompanyList = getAssociatedCompanyNameList(selectedCompanyList);
               readable = true;
               readableUser = true;
               this.enableSaveButton = false;
               this.enableButton = false;
               this.enableCancelButton = false;
               this.enableDeleteButton = false;
               enableUpdatingUser();
            }
            else
            {
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User add begins");
               currentUser.setCompanyList(this.selectedCompanyList);
               userManager.addUser(currentUser);
               // TODO check or delete
               // If a manager is adding this user
               /*if (isManager) {
            	   userManager.assignUserToManager(bLoginManagement.getCurrentUser(), currentUser);
               }*/
               FacesContext.getCurrentInstance().addMessage(null,
                     new FacesMessage(FacesMessage.SEVERITY_INFO, "User added successfully.", ""));
               logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User " + currentUser.getUserName()
                     + " added successfully.");
               this.userList = userList();
               this.selectedCompanyList = currentUser.getCompanyList();
               this.associatedCompanyList = getAssociatedCompanyNameList(selectedCompanyList);
               this.currentUser = UtilManager.getUpdatedUserByName(currentUser, userList);
               /* currentUser = userList.get(0); */
               readableUser = true;
               readable = true;
               this.enableSaveButton = false;
               this.enableButton = false;
               this.enableCancelButton = false;
               this.enableDeleteButton = false;
               enableUpdatingUser();
            }
         }
      }
      catch (InvalidDataException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Invalid data exception occurred while saving user", e);
      }
      catch (InvalidPasswordException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Invalid password exception occurred while saving user",
               e);
      }
      catch (UserAlreadyExist e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "user already exists exception occurred while saving user", e);
      }
      catch (InternalServerException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "internal server exception occurred while saving user", e);
      }
      catch (EmailIdAlreadyExist e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "email id already exists exception occurred while saving user", e);
      }
      catch (Exception excep)
      {
         UtilManager.prepareErrorMessages(excep.getMessage());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "exception occurred while saving user", excep);
      }
   }

   /**
    * This method call on tab change - used to update right and left panel
    * 
    * @param event
    */
   public void tabChanged( TabChangeEvent event )
   {
      final String THIS_METHOD = "tabChanged";
      try
      {
    	  // TODO check or remove
    	  /*if (!bLoginManagement.isAdmin()) {
    		  UtilManager.prepareErrorMessages("Unauthorized");
    		  return;
    	  }*/
         // User Tab changes
         this.companyMap = companyManager.getAllCompanyNames();
         currentUser = userList.get(0);
         updateRightPanel(currentUser.getUserId());
         // company Tab changes
         BCompanyManagement bCompanyManagement = UtilManager.getCompanyManagemnetInstance();
         bCompanyManagement.setCurrentCompany(bCompanyManagement.getCompanyList().get(0));
         bCompanyManagement.updateRightPanel(bCompanyManagement.getCompanyList().get(0).getCompanyId());
      }
      catch (InternalServerException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Internal Server Exception occurs on tab change", e);
      }
   }

   /**
    * Retrieve user list
    * 
    * @return User list
    * @throws InternalServerException - throws if server error occurs
    */
   private List<User> userList() throws InternalServerException
   {
      if (isManager) {
    	  return userManager.getUserListForManager(bLoginManagement.getCurrentUser().getUserId());
      } else { // admin
	   return userManager.getUserList();
      }
   }

   /**
    * Get Company names for corresponding company id's
    * 
    * @param selectedList - list of company id's
    * @return CompanyId list
    */
   public List<String> getAssociatedCompanyNameList( List<Long> selectedList )
   {
      final String THIS_METHOD = "getAssociatedCompanyNameList";
      Map<Long, String> selectedMap = new HashMap<Long, String>();
      List<String> associatedCompanyList = new ArrayList<String>();
      try
      {
         selectedMap = companyManager.getAllCompany();
         if (selectedMap != null && selectedList != null)
         {
            for (int i = 0; i < selectedList.size(); i++)
            {
               final String value = selectedList.get(i) + "";
               if (selectedMap.containsKey(Long.parseLong(value)))
               {
                  associatedCompanyList.add(selectedMap.get(Long.parseLong(value)));
               }
            }
         }
      }
      catch (InternalServerException e)
      {
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Internal Server Exception occured while getting company list", e);
      }
      return associatedCompanyList;
   }

   public void showDeletePopup()
   {
      show = true;
   }

   /**
    * go to user profile page on click of user from search user data table
    */
   public void goToUserUserPrifle()
   {
      updateRightPanel(this.selectedUserId);
      showSearchPopup = false;
      searchUser = null;
      searchCompany = null;
   }

   /**
    * Show editable button based on conditions.
    */
   public void enableUpdatingUser()
   {
      if (bLoginManagement.getIsAdmin())
      {
         this.enableEditButton = true;
      }
      else
      {
         if (this.currentUser.getUserName().equals(
               configurationManager.getPropertyAsString(Constants.REBATE_API_PROPERTY, Constants.ADMIN_USERNAME)) || this.currentUser.getUserRole() == 1)
         {
            this.enableEditButton = false;
         }
         else if (currentUser.getUserId() == bLoginManagement.getCurrentUser().getUserId()) { // Don't update yourself (Managers)
        	 this.enableEditButton = false;
         }
         else
         {
            this.enableEditButton = true;
         }
      }
   }

   /**
    * This method called once search button is clicked.Based on value from text
    * box it will search users
    */
   public void searchUser()
   {
      searchList = userManager.searchUser(searchUser, searchCompany);
      showSearchPopup = true;
      DataTable table = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
            .findComponent(":tabView:searchForm:searchDataTableId");
      table.setValueExpression("sortBy", null);
      table.setFirst(0);
      table.reset();
      table.setSortColumn(null);
      logger.log(IMPLogger.INFO, "Searching for: " + searchUser + "; " + searchCompany);
   }

   public void clearSearchUser()
   {
      searchUser = "";
      searchCompany = "";
   }

   public void initializeUserManagement()
   {
   }

   public List<User> getUserList()
   {
      return userList;
   }

   public void setUserList( List<User> userList )
   {
      this.userList = userList;
   }

   public MenuBakingBean getbMenuBakingBean()
   {
      return bMenuBakingBean;
   }

   public void setbMenuBakingBean( MenuBakingBean bMenuBakingBean )
   {
      this.bMenuBakingBean = bMenuBakingBean;
   }

   public UserManager getUserManager()
   {
      return userManager;
   }

   public void setUserManager( UserManager userManager )
   {
      this.userManager = userManager;
   }

   public User getCurrentUser()
   {
      return currentUser;
   }

   public void setCurrentUser( User currentUser )
   {
      this.currentUser = currentUser;
   }

   public Map<String, Long> getUserRoleMap()
   {
      return userRoleMap;
   }

   public void setUserRoleMap( Map<String, Long> userRoleMap )
   {
      this.userRoleMap = userRoleMap;
   }

   public long getSelectedUserId()
   {
      return selectedUserId;
   }

   public void setSelectedUserId( long selectedUserId )
   {
      this.selectedUserId = selectedUserId;
   }

   public boolean isReadable()
   {
      return readable;
   }

   public void setReadable( boolean readable )
   {
      this.readable = readable;
   }

   public boolean isEnableSaveButton()
   {
      return enableSaveButton;
   }

   public void setEnableSaveButton( boolean enableSaveButton )
   {
      this.enableSaveButton = enableSaveButton;
   }

   public boolean isEnableEditButton()
   {
      return enableEditButton;
   }

   public void setEnableEditButton( boolean enableEditButton )
   {
      this.enableEditButton = enableEditButton;
   }

   public boolean isEnableButton()
   {
      return enableButton;
   }

   public void setEnableButton( boolean enableButton )
   {
      this.enableButton = enableButton;
   }

   public boolean isEnableCancelButton()
   {
      return enableCancelButton;
   }

   public void setEnableCancelButton( boolean enableCancelButton )
   {
      this.enableCancelButton = enableCancelButton;
   }

   public boolean isEnableDeleteButton()
   {
      return enableDeleteButton;
   }

   public void setEnableDeleteButton( boolean enableDeleteButton )
   {
      this.enableDeleteButton = enableDeleteButton;
   }

   public boolean isReadableUser()
   {
      return readableUser;
   }

   public void setReadableUser( boolean readableUser )
   {
      this.readableUser = readableUser;
   }

   public Map<String, Long> getUserStatusMap()
   {
      return userStatusMap;
   }

   public void setUserStatusMap( Map<String, Long> userStatusMap )
   {
      this.userStatusMap = userStatusMap;
   }

   public CompanyManager getCompanyManager()
   {
      return companyManager;
   }

   public void setCompanyManager( CompanyManager companyManager )
   {
      this.companyManager = companyManager;
   }

   public Map<String, Long> getCompanyMap()
   {
      return companyMap;
   }

   public void setCompanyMap( Map<String, Long> companyMap )
   {
      this.companyMap = companyMap;
   }

   public List<Long> getSelectedCompanyList()
   {
      return selectedCompanyList;
   }

   public void setSelectedCompanyList( List<Long> selectedCompanyList )
   {
      this.selectedCompanyList = selectedCompanyList;
   }

   public List<String> getAssociatedCompanyList()
   {
      return associatedCompanyList;
   }

   public void setAssociatedCompanyList( List<String> associatedCompanyList )
   {
      this.associatedCompanyList = associatedCompanyList;
   }

   public int getActiveIndex()
   {
      return activeIndex;
   }

   public void setActiveIndex( int activeIndex )
   {
      this.activeIndex = activeIndex;
   }

   public boolean isShow()
   {
      return show;
   }

   public void setShow( boolean show )
   {
      this.show = show;
   }

   public String getSearchUser()
   {
      return searchUser;
   }

   public void setSearchUser( String searchUser )
   {
      this.searchUser = searchUser;
   }

   public String getSearchCompany()
   {
      return searchCompany;
   }

   public void setSearchCompany( String searchCompany )
   {
      this.searchCompany = searchCompany;
   }

   public boolean isShowSearchPopup()
   {
      return showSearchPopup;
   }

   public void setShowSearchPopup( boolean showSearchPopup )
   {
      this.showSearchPopup = showSearchPopup;
   }

   public List<User> getSearchList()
   {
      return searchList;
   }

   public void setSearchList( List<User> searchList )
   {
      this.searchList = searchList;
   }

   public BLoginManagement getbLoginManagement()
   {
      return bLoginManagement;
   }

   public void setbLoginManagement( BLoginManagement bLoginManagement )
   {
      this.bLoginManagement = bLoginManagement;
   }

   public ConfigurationManager getConfigurationManager()
   {
      return configurationManager;
   }

   public void setConfigurationManager( ConfigurationManager configurationManager )
   {
      this.configurationManager = configurationManager;
   }

   /**
    * Check is user is valid or not
    * 
    * @return true if valid else false.
    */
   private boolean isValidUser()
   {
      if (( currentUser.getUserName().trim() == null || currentUser.getUserName().trim().length() == 0 )
            && ( currentUser.getFirstName() == null || currentUser.getFirstName().length() == 0 )
            && ( currentUser.getLastName().trim() == null || currentUser.getLastName().trim().length() == 0 )
            && ( currentUser.getPassword() == null || currentUser.getPassword().length() == 0 )
            && ( currentUser.getVerifyPassword() == null || currentUser.getVerifyPassword().length() == 0 )
            && ( currentUser.getEmailId() == null || currentUser.getEmailId().trim().length() == 0 )
            && currentUser.getUserRole() == 0L && currentUser.getUserStatus() == 0L)
      {
         UtilManager.prepareErrorMessages("Please enter mandatory fields.");
         return false;
      }
      if (currentUser.getUserName().trim() == null || currentUser.getUserName().trim().length() == 0)
      {
         UtilManager.prepareErrorMessages("Username is mandatory.");
         return false;
      }
      if (currentUser.getFirstName() == null || currentUser.getFirstName().length() == 0)
      {
         UtilManager.prepareErrorMessages("Full name is mandatory.");
         return false;
      }
      if (currentUser.getLastName() == null || currentUser.getLastName().length() == 0)
      {
         UtilManager.prepareErrorMessages("Last name is mandatory.");
         return false;
      }
      if (currentUser.getUserRole() == 0L)
      {
         UtilManager.prepareErrorMessages("Please select a User role.");
         return false;
      }
      if (currentUser.getEmailId().trim() == null || currentUser.getEmailId().length() == 0)
      {
         UtilManager.prepareErrorMessages("Email id is mandatory.");
         return false;
      }
      if (currentUser.getAddress1() == null || currentUser.getAddress1().trim().length() == 0) {
    	  UtilManager.prepareErrorMessages("Address 1 is mandatory.");
    	  return false;
      }
      if (currentUser.getCity() == null || currentUser.getCity().trim().length() == 0) {
    	  UtilManager.prepareErrorMessages("City is mandatory.");
    	  return false;
      }
      if (currentUser.getState() == null || currentUser.getState().trim().length() == 0) {
    	  UtilManager.prepareErrorMessages("State is mandatory.");
    	  return false;
      }
      if (currentUser.getZip() == null || currentUser.getZip().trim().length() == 0) {
    	  UtilManager.prepareErrorMessages("Zip Code is mandatory.");
    	  return false;
      }
      if (currentUser.getPassword().trim() == null || currentUser.getPassword().length() == 0)
      {
         UtilManager.prepareErrorMessages("Password is mandatory.");
         return false;
      }
      if (currentUser.getVerifyPassword().trim() == null || currentUser.getVerifyPassword().length() == 0)
      {
         UtilManager.prepareErrorMessages("Confirm password is mandatory.");
         return false;
      }
      if (currentUser.getUserStatus() == 0L)
      {
         UtilManager.prepareErrorMessages("Please select a status.");
         return false;
      }
      if (currentUser.getContractorCompany() == null || currentUser.getContractorCompany().trim().equals("")) {
    	  UtilManager.prepareErrorMessages("Contractor Company is mandatory.");
    	  return false;
      }
      if (currentUser.getUserRole() == 2L && ( selectedCompanyList != null && selectedCompanyList.size() <= 0L ))
      {
         UtilManager.prepareErrorMessages("User should be associated at least one company.");
         return false;
      }
      if (currentUser.getUserRole() == 2L && ( selectedCompanyList != null && selectedCompanyList.size() > 5L ))
      {
         UtilManager.prepareErrorMessages("Max of 5 companies can only be associated to a contractor.");
         return false;
      }
      else
      {
         return true;
      }
   }

   /**
    * Initializing the LoggerManger to log the logs in the logger
    * */
   private void initializeLoggerManager()
   {
      if (logger == null)
      {
         MPLoggerManager loggerManager = (MPLoggerManager) UtilManager
               .getBeanInstance(RebateApiConstant.LOGGER_MANAGER);
         this.logger = loggerManager.getLogger(RebateApiConstant.UI_LOGGER);
      }
   }

   
}
