package com.icf.rebate.console.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.icf.rebate.api.util.ThreadLocalImpl;
import com.icf.rebate.console.login.BLoginManagement;
public final class LoginFilter implements Filter
{
   private FilterConfig filterConfig = null;

   @Override
   public void init( FilterConfig filterConfig ) throws ServletException
   {
      this.filterConfig = filterConfig;
   }

   @Override
   public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException,
         ServletException
   {
      HttpServletRequest httpRequest = null;
      HttpServletResponse httpResponse = null;
      HttpSession session = null;
      try
      {
         httpRequest = (HttpServletRequest) request;
         httpResponse = (HttpServletResponse) response;
         session = httpRequest.getSession(false);
         if (session != null)
         {
            BLoginManagement loginBean = (BLoginManagement) session.getAttribute("bLoginManagement");
            if (loginBean == null || loginBean.getCurrentUser() == null)
            {
               if (loginBean != null)
               {
                  loginBean.setLoginErrorMessages("");
               }
            }
         }
         chain.doFilter(request, response);
         
         ThreadLocalImpl.setThreadLocalUserInfo(httpRequest,"");
      }
      catch (Exception ex)
      {
      }
   }

   @Override
   public void destroy()
   {
   }
}
