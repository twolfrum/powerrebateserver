package com.icf.rebate.console.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.icf.rebate.console.login.BLoginManagement;
public final class AuthenticationFilter implements Filter
{
   private FilterConfig filterConfig = null;

   @Override
   public void init( FilterConfig filterConfig ) throws ServletException
   {
      this.filterConfig = filterConfig;
   }

   @Override
   public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException,
         ServletException
   {
      HttpServletRequest httpRequest = null;
      HttpServletResponse httpResponse = null;
      HttpSession session = null;
      try
      {
         httpRequest = (HttpServletRequest) request;
         httpResponse = (HttpServletResponse) response;
         if (isSessionInvalid(httpRequest))
         {
            session = httpRequest.getSession(true);
            httpResponse.sendRedirect("login.jsf?errorCode=2");
            return;
         }
         String uri = httpRequest.getRequestURI();
         httpResponse.setHeader("cache-control", "private, max-age=3600");
         httpResponse.setHeader("Cache-Control", "private, max-age=3600");
         // httpResponse.setHeader("Pragma", "no-cache");
         // httpResponse.setDateHeader("Expires", 0);
         if (uri.indexOf("/css") > 0)
         {
            chain.doFilter(request, response);
         }
         else if (uri.indexOf("/images") > 0)
         {
            chain.doFilter(request, response);
         }
         else if (uri.indexOf("/javax.faces.resource") > 0)
         {
            chain.doFilter(request, response);
         }
         else if (uri.indexOf("/js") > 0)
         {
            chain.doFilter(request, response);
         }
         else if (uri.indexOf("/images") > 0)
         {
            chain.doFilter(request, response);
         }
         else if (uri.indexOf("/register") > 0)
         {
            chain.doFilter(request, response);
         }
         else
         {
            session = httpRequest.getSession(false);
            if (session != null)
            {
               BLoginManagement loginBean = (BLoginManagement) session.getAttribute("bLoginManagement");
               if (loginBean == null || loginBean.getCurrentUser() == null)
               {
                  // Refresh close the error popup
                  if (loginBean != null)
                  {
                     loginBean.setLoginErrorMessages("");
                  }
                  filterConfig.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
               }
               else
               {
                  if (uri.indexOf("/login") > 0)
                  {
                     httpResponse.sendRedirect("home.jsf");
                  }
                  else
                  {
                     chain.doFilter(request, response);
                  }
               }
            }
            else
            {
               session = httpRequest.getSession(true);
               filterConfig.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
            }
         }
      }
      catch (Exception ex)
      {
         if (! ex.getCause().getClass().getName().equalsIgnoreCase("java.net.SocketException"))
         {
            session = ( (HttpServletRequest) request ).getSession(true);
            session.invalidate();
            session = ( (HttpServletRequest) request ).getSession(true);
            httpResponse.sendRedirect("login.jsf?errorCode=3");
         }
      }
   }

   @Override
   public void destroy()
   {
   }

   private boolean isSessionInvalid( HttpServletRequest httpRequest )
   {
      boolean sessionInValid = ( httpRequest.getRequestedSessionId() != null )
            && ! httpRequest.isRequestedSessionIdValid();
      return sessionInValid;
   }
}
