package com.icf.rebate.console.elresolver;

import javax.el.CompositeELResolver;

import org.primefaces.application.exceptionhandler.PrimeExceptionHandlerELResolver;
import org.springframework.web.jsf.el.SpringBeanFacesELResolver;

// Combines Spring EL resolver and Prime Faces ExceptionHandler's EL Resolver
public class PrimeSpringCompositeELResolver extends CompositeELResolver {

	public PrimeSpringCompositeELResolver() {
		add(new PrimeExceptionHandlerELResolver());
		add(new SpringBeanFacesELResolver());
	}
	
}
