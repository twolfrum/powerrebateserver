package com.icf.rebate.console.util;

import java.util.List;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.icf.rebate.api.company.impl.Company;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.console.company.BCompanyManagement;
public class UtilManager
{
   /**
    * Prepares Error Messages to be displayed in console
    * 
    * @param errorMessage - Message will display in UI
    */
   public static void prepareErrorMessages( String errorMessage )
   {
      FacesContext.getCurrentInstance().addMessage(null,
            new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, ""));
   }

   public static User getUpdatedUser( User currentUser, List<User> userList )
   {
      User user = null;
      if (userList != null && currentUser != null)
      {
         for (User userBean : userList)
         {
            if (userBean.getUserId() == currentUser.getUserId())
            {
               user = userBean;
               break;
            }
         }
      }
      else
      {
         user = new User();
      }
      return user;
   }

   public static User getUpdatedUserByName( User currentUser, List<User> userList )
   {
      User user = null;
      if (userList != null && currentUser != null)
      {
         for (User userBean : userList)
         {
            if (userBean.getUserName().equals(currentUser.getUserName().toLowerCase()))
            {
               user = userBean;
               break;
            }
         }
      }
      else
      {
         user = new User();
      }
      return user;
   }

   public static Company getUpdatedCompany( Company currentCompany, List<Company> companyList )
   {
      Company compnay = null;
      if (companyList != null && companyList != null)
      {
         for (Company companyBean : companyList)
         {
            if (companyBean.getCompanyId() == currentCompany.getCompanyId())
            {
               compnay = companyBean;
               break;
            }
         }
      }
      else
      {
         compnay = new Company();
      }
      return compnay;
   }

   public static Company getUpdatedCompanyByName( Company currentCompany, List<Company> companyList )
   {
      Company compnay = null;
      if (companyList != null && companyList != null)
      {
         for (Company companyBean : companyList)
         {
            if (companyBean.getCompanyName().equals(currentCompany.getCompanyName()))
            {
               compnay = companyBean;
               break;
            }
         }
      }
      else
      {
         compnay = new Company();
      }
      return compnay;
   }

   public static Object getBeanInstance( String beanName )
   {
      return com.icf.rebate.api.util.UtilManager.getBeanInstance(beanName);
   }

   public static BCompanyManagement getCompanyManagemnetInstance()
   {
      BCompanyManagement bCompanyManagement = null;
      if (bCompanyManagement == null)
      {
         ELContext elContext = FacesContext.getCurrentInstance().getELContext();
         bCompanyManagement = (BCompanyManagement) FacesContext.getCurrentInstance().getApplication().getELResolver()
               .getValue(elContext, null, "bCompanyManagement");
      }
      return bCompanyManagement;
   }
}
