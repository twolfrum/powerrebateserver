package com.icf.rebate.console.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * This file was designed as a place where additional functionality can be
 * performed during the JSF resource request life-cycle. JSF handles resource
 * requests slightly different than regular JSF requests (a "resource" is
 * something like an image, css file, etc.).
 */
// @WebFilter(filterName="FacesResourceFilter", urlPatterns={"*.jsf"},
// displayName="JSF Resource filter.")
public class FacesResourceFilter implements Filter
{
   private final static String FACES_RESOURCE_URL = "javax.faces.resource...";
   private final static String LOCATION_KEY = "ln";
   private final static String DOT_DOT = "..";
   private static Log log = LogFactory.getLog(FacesResourceFilter.class);

   public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException,
         ServletException
   {
      try
      {
         HttpServletRequest path = (HttpServletRequest) request; // this is a
                                                                 // downcast
                                                                 // (which is
                                                                 // bad usually)
                                                                 // but we can
                                                                 // safely do it
                                                                 // in this file
         // Check for "dot dot" path traversal
         // ----------------------------------
         // There's a vulnerability in JSF which allows for hackers to traverse
         // the directory
         // structure. This vulnerability is typical performed by something
         // like:
         // project-name/javax.faces.resources/stylesheet.css.jsf?ln=\..\..\..\..\sensitive.txt%00.html
         // What happens is that the ".jsf" sends the request thru the JSF
         // servlet where the ".jsf"
         // gets stripped (resulting in a regular resource request:
         // stylesheet.css). But then the "ln="
         // confuses the application by not only traversing the path (with
         // /../../) but there's a
         // "%00.html" which tricks the system to load/display the file.
         
         //case1 == http://projectName/console/javax.faces.resource.../WEB-INF/web.xml.jsf
         String relativePath = path.getRequestURL().toString();
         if (relativePath.contains(FACES_RESOURCE_URL))
         {
            throw new Exception("The server encountered an unexpected condition which prevented it from fulfilling the request.");
         }
         
         //case2 == http://localhost:8080/console/javax.faces.resource/stylesheet.css.jsf?ln=\..\..\..\..\..\..\..\..\..\..\..\..\..\boot.ini%00.html
         
         if (path.getRequestURL().indexOf(FACES_RESOURCE_URL) >= 0)
         {
            Enumeration<String> e = request.getParameterNames();
            while (e.hasMoreElements())
            {
               String key = (String) e.nextElement();
               if (key.indexOf(LOCATION_KEY) >= 0)
               {
                  String value = request.getParameter(key);
                  if (value.indexOf(DOT_DOT) >= 0)
                  {
                     log.error("Detected a dot dot pattern in an \"" + LOCATION_KEY + "\" parameter. The value is: "
                           + value);
                     throw new Exception("The server encountered an unexpected condition which prevented it from fulfilling the request.");
                  }
               }
            }
         }
      }
      catch (Exception e)
      {
         throw new ServletException(e.getMessage());
      }
      // Add headers for IE compatibility
      HttpServletResponse resp = (HttpServletResponse) response;
      resp.addHeader("X-UA-Compatible", "IE=EmulateIE8");
      resp.addHeader("Cache-Control", "no-cache, must-revalidate");
      // Call the next filter (continue request processing)
      chain.doFilter(request, resp);
   }

   public void init( FilterConfig filterConfig ) throws ServletException
   {
   }

   public void destroy()
   {
   }
}