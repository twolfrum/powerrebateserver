package com.icf.rebate.console.login;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

import com.icf.rebate.api.config.ConfigurationManager;
import com.icf.rebate.api.exception.InternalServerException;
import com.icf.rebate.api.exception.InvalidDataException;
import com.icf.rebate.api.logger.IMPLogger;
import com.icf.rebate.api.logger.MPLoggerManager;
import com.icf.rebate.api.login.exception.InActiveUserException;
import com.icf.rebate.api.login.exception.UserAuthenticationException;
import com.icf.rebate.api.login.exception.UserBlockedException;
import com.icf.rebate.api.login.exception.UserNameNotFoundException;
import com.icf.rebate.api.login.exception.UserNotAllowedException;
import com.icf.rebate.api.login.impl.LoginManager;
import com.icf.rebate.api.login.impl.PasswordResponse;
import com.icf.rebate.api.login.impl.UserPasswordTrackingBean;
import com.icf.rebate.api.user.exception.UserDoesNotExist;
import com.icf.rebate.api.user.impl.User;
import com.icf.rebate.api.user.impl.UserManager;
import com.icf.rebate.api.util.RebateApiConstant;
import com.icf.rebate.console.util.Constants;
import com.icf.rebate.console.util.MenuBakingBean;
import com.icf.rebate.console.util.UtilManager;
/**
 * This class implements login authentication,logout,change password and forget password 
 *
 */
public class BLoginManagement implements Serializable
{
   private static final String THIS_CLASS = "BLoginManagement";
   private String username;
   private String password;
   private int windowheight = 595;
   MenuBakingBean bMenuBakingBean;
   LoginManager loginManager;
   private String loggedUser;
   private String fusername;
   private User currentUser;
   private String selection;
   private String loginErrorMessages;
   private String confirmPassword;
   private String newPassword;
   private String changePasswordErrorMessages;
   private boolean isAdmin = false;
   private String changePasswordSuccessMessages;
   UserPasswordTrackingBean pwdTrackingBean;
   ConfigurationManager configurationManager;
   private boolean changePasswordSuccessPopup;
   UserManager userManager;
   private IMPLogger logger;

   /**
    * constructor
    */
   public BLoginManagement ()
   {
      initializeLoggerManager();
      loginErrorMessages = "";
      changePasswordSuccessPopup = false;
   }

   /**
    * Authenticates a user with the given user name and password.
    * @return string with value home or login - decides to which page redirect
    */
   public String authenticate()
   {
      final String THIS_METHOD = "authenticate";
      try
      {
         configurationManager = (ConfigurationManager) UtilManager.getBeanInstance(RebateApiConstant.CONFIG_MANAGER);
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Calling api to authenticate the user");
         this.currentUser = loginManager.authenticateUser(username, password, true);
         this.loggedUser = currentUser.getUserName() + "!";
         if (this.currentUser.getUserName().equals(
               configurationManager.getPropertyAsString(Constants.REBATE_API_PROPERTY, Constants.ADMIN_USERNAME)))
         {
            isAdmin = true;
         }
         bMenuBakingBean.setCurrentPage("/user/UserManagement.xhtml");
         loginErrorMessages = "";
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "User authentication completed successfully.");
      }
      catch (InvalidDataException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         loginErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Invalid data exception occurred during authentication",
               e);
         return null;
      }
      catch (UserNameNotFoundException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         loginErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "User name not found exception occurred during authentication", e);
         return null;
      }
      catch (InActiveUserException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         loginErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "In active user exception occurred during authentication", e);
         return null;
      }
      catch (UserAuthenticationException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         loginErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "User authentication exception occurred during authentication", e);
         return null;
      }
      catch (UserBlockedException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         loginErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "User blocked exception occurred during authentication",
               e);
         return null;
      }
      catch (UserNotAllowedException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         loginErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "User not allowed exception occurred during authentication", e);
         return null;
      }
      catch (Exception excep)
      {
         UtilManager.prepareErrorMessages(excep.getMessage());
         loginErrorMessages = excep.getMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD, "Exception occurred during authentication", excep);
         return null;
      }
      return "home";
   }
/**
 * logout- invalidate the session and redirect to login page
 * 
 * @return String - based on the value redirect to home page or login page
 */
   public String logout()
   {
      final String THIS_METHOD = "logout";
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out called.");
      HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
            .getRequest();
      HttpSession session = request.getSession(false);
      if (session != null)
      {
         session.invalidate();
      }
      session = request.getSession(true);
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out completed successfully.");
      return Constants.LOGOUT;
   }
   
   /**
    * logout- invalidate the session and redirect to login page
    * 
    * @return String - based on the value redirect to home page or login page
    */
   public String sessionExpired()
   {
      final String THIS_METHOD = "logout";
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out called.");
      HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
            .getRequest();
      HttpSession session = request.getSession(false);
      if (session != null)
      {
         session.invalidate();
      }
      logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Log out completed successfully.");
      return Constants.LOGOUT;
   }

   /**
    * 
    * @return String - go to forgot.xhtml page based on value
    */
   public String goToForgotPage()
   {
      selection = "";
      loginErrorMessages = "";
      return Constants.FORGOT;
   }

   /**
    * 
    * @return String - go to login page based on value
    */
   public String goToLoginPage()
   {
      fusername = null;
      selection = "";
      return Constants.LOGIN;
   }
   /**
    * Forgot password called for given user name
    * @return String - to which page redirect
    */
   public String forgotPassword()
   {
      final String THIS_METHOD = "forgotPassword";
      try
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Forgot password called.");
         loginManager.forgetPasswordFromUserName(fusername);
         fusername = null;
         selection = "success";
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: Forgot password completed successfully.");
      }
      catch (UserDoesNotExist e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         fusername = null;
         selection = "";
         System.out.println(selection.length());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "User diesnot exists  Exception occurred when the user clicked on forgot password link", e);
         return Constants.FORGOT;
      }
      catch (InvalidDataException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         fusername = null;
         selection = "";
         System.out.println(selection.length());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Invalid Data Exception occurred when the user clicked on forgot password link", e);
         return Constants.FORGOT;
      }
      catch (InternalServerException e)
      {
         UtilManager.prepareErrorMessages(e.getErrorMessage());
         fusername = null;
         selection = "";
         System.out.println(selection.length());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Internal server Exception occurred when the user clicked on forgot password link", e);
         return Constants.FORGOT;
      }
      catch (Exception excep)
      {
         UtilManager.prepareErrorMessages(excep.getMessage());
         fusername = null;
         selection = "";
         System.out.println(selection.length());
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "exception occurred when the user clicked on forgot password link", excep);
         return Constants.FORGOT;
      }
      return Constants.FORGOT;
   }

   /**
    * This method will display change password pop up 
    */
   public void showchagePasswordPopUp()
   {
      changePasswordErrorMessages = null;
   }
   /**
    * Change existed password with new password
    */
   public void changePassword()
   {
      final String THIS_METHOD = "changePassword";
      try
      {
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: change password called.");
         changePasswordErrorMessages = null;
         if (( newPassword.trim() == null || newPassword.trim().length() == 0 )
               && ( confirmPassword.trim() == null || confirmPassword.length() == 0 ))
         {
            changePasswordErrorMessages = "Please enter mandatory fields.";
            return;
         }
         pwdTrackingBean = new UserPasswordTrackingBean();
         this.currentUser = userManager.getUserById(currentUser.getUserId());
         pwdTrackingBean.setUserId(currentUser.getUserId());
         pwdTrackingBean.setUserName(currentUser.getUserName());
         pwdTrackingBean.setOldPassword(currentUser.getPassword());
         pwdTrackingBean.setNewPassword(newPassword);
         pwdTrackingBean.setConfirmNewPassword(confirmPassword);
         PasswordResponse response = loginManager.updateLoggedInUserDetail(pwdTrackingBean, true);
         if (! response.isSuccess())
         {
            List<String> errorMessages = response.getErrorMesseages();
            if (errorMessages != null && errorMessages.size() > 0)
            {
               changePasswordErrorMessages = errorMessages.get(0);
            }
         }
         else
            changePasswordErrorMessages = null;
         setChangePasswordSuccessMessages("Password changed successfully.");
         logger.log(IMPLogger.INFO, THIS_CLASS, THIS_METHOD, "Action: change password completed successfully.");
      }
      catch (InvalidDataException e)
      {
         this.changePasswordErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Invalid Data exception occured for change password request", e);
         e.printStackTrace();
      }
      catch (InternalServerException e)
      {
         this.changePasswordErrorMessages = e.getErrorMessage();
         logger.log(IMPLogger.ERROR, THIS_CLASS, THIS_METHOD,
               "Internal server error occured for change password request", e);
         e.printStackTrace();
      }
      catch (Exception e)
      {
         this.changePasswordErrorMessages = e.getMessage();
         e.printStackTrace();
      }
      if (changePasswordErrorMessages == null)
      {
         RequestContext rc = RequestContext.getCurrentInstance();
         rc.execute("PF('cp').hide();");
         changePasswordSuccessPopup = true;
         rc.execute("PF('changePasswordSuccessPopUp').show();");
      }
   }

   public String getUsername()
   {
      return username;
   }

   public void setUsername( String username )
   {
      this.username = username;
   }

   public String getPassword()
   {
      return password;
   }

   public void setPassword( String password )
   {
      this.password = password;
   }

   public void setbMenuBakingBean( MenuBakingBean bMenuBakingBean )
   {
      this.bMenuBakingBean = bMenuBakingBean;
   }

   public int getWindowheight()
   {
      return windowheight;
   }

   public void setWindowheight( int windowheight )
   {
      this.windowheight = windowheight;
   }

   public LoginManager getLoginManager()
   {
      return loginManager;
   }

   public void setLoginManager( LoginManager loginManager )
   {
      this.loginManager = loginManager;
   }

   public MenuBakingBean getbMenuBakingBean()
   {
      return bMenuBakingBean;
   }

   public String getLoggedUser()
   {
      return loggedUser;
   }

   public void setLoggedUser( String loggedUser )
   {
      this.loggedUser = loggedUser;
   }

   public String getFusername()
   {
      return fusername;
   }

   public void setFusername( String fusername )
   {
      this.fusername = fusername;
   }

   public User getCurrentUser()
   {
      return currentUser;
   }

   public void setCurrentUser( User currentUser )
   {
      this.currentUser = currentUser;
   }

   public String getSelection()
   {
      return selection;
   }

   public void setSelection( String selection )
   {
      this.selection = selection;
   }

   public String getLoginErrorMessages()
   {
      return loginErrorMessages;
   }

   public void setLoginErrorMessages( String loginErrorMessages )
   {
      this.loginErrorMessages = loginErrorMessages;
   }

   public String getConfirmPassword()
   {
      return confirmPassword;
   }

   public void setConfirmPassword( String confirmPassword )
   {
      this.confirmPassword = confirmPassword;
   }

   public String getNewPassword()
   {
      return newPassword;
   }

   public void setNewPassword( String newPassword )
   {
      this.newPassword = newPassword;
   }

   public String getChangePasswordErrorMessages()
   {
      return changePasswordErrorMessages;
   }

   public void setChangePasswordErrorMessages( String changePasswordErrorMessages )
   {
      this.changePasswordErrorMessages = changePasswordErrorMessages;
   }

   public UserPasswordTrackingBean getPwdTrackingBean()
   {
      return pwdTrackingBean;
   }

   public void setPwdTrackingBean( UserPasswordTrackingBean pwdTrackingBean )
   {
      this.pwdTrackingBean = pwdTrackingBean;
   }

   public String getChangePasswordSuccessMessages()
   {
      return changePasswordSuccessMessages;
   }

   public void setChangePasswordSuccessMessages( String changePasswordSuccessMessages )
   {
      this.changePasswordSuccessMessages = changePasswordSuccessMessages;
   }

   public boolean isChangePasswordSuccessPopup()
   {
      return changePasswordSuccessPopup;
   }

   public void setChangePasswordSuccessPopup( boolean changePasswordSuccessPopup )
   {
      this.changePasswordSuccessPopup = changePasswordSuccessPopup;
   }

   public UserManager getUserManager()
   {
      return userManager;
   }

   public void setUserManager( UserManager userManager )
   {
      this.userManager = userManager;
   }

   public boolean getIsAdmin()
   {
      return isAdmin;
   }

   public void setIsAdmin( boolean isAdmin )
   {
      this.isAdmin = isAdmin;
   }

   public ConfigurationManager getConfigurationManager()
   {
      return configurationManager;
   }

   public void setConfigurationManager( ConfigurationManager configurationManager )
   {
      this.configurationManager = configurationManager;
   }

   /**
    * Initializing the LoggerManger to log the logs in the logger
    * */
   private void initializeLoggerManager()
   {
      if (logger == null)
      {
         MPLoggerManager loggerManager = (MPLoggerManager) UtilManager
               .getBeanInstance(RebateApiConstant.LOGGER_MANAGER);
         this.logger = loggerManager.getLogger(RebateApiConstant.UI_LOGGER);
      }
   }
   
   
}
